<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class kardex_cab extends Model
{
    protected $table='kardex_cab';

    protected $primaryKey='cod_kardex';


    public $timestamps=false;

    protected $fillable=['cod_kardex', 'movimiento','cod_material','cantidad', 'costo_total_sin_igv', 'costo_total_con_igv', 'igv', 'fecha', 'RUC_empresa'];

    protected $guarded=[];
}
