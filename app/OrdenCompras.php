<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenCompras extends Model
{
    protected $table='orden_compras';

    protected $fillable=['id','fecha','RUC_proveedor','RUC_empresa','tipo_compra','dias_creditos','observaciones','subtotal','total','IGV','cod_material','estado_ordcomp'];

    protected $primaryKey="id";

    public $timestamps=true;

    protected $guarded=[];
}


