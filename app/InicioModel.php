<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class InicioModel extends Model
{
    protected $table='contacto_pagina';
  protected $primaryKey="id_contacto";
  public $timestamps=false;


  protected $fillable=['empresa_contacto','nombre_contacto','telefono_contacto'];

  protected $guarded=[];
}
