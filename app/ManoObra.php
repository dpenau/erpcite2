<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ManoObra extends Model
{
    protected $table='mano_obra';
    protected $primaryKey="id";
    public $timestamps=false;


    protected $fillable=['proceso_id', 'RUC_empresa','descripcion','tipo_sueldo_id','sueldo_mensual','otros_sueldos','beneficio_social','costo_mensual'];

    protected $guarded=[];
}
