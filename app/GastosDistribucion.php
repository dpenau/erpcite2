<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class GastosDistribucion extends Model
{
  protected $table='gasto_distribucion';

  protected $primaryKey="id_gastoDistr";

  public $timestamps=false;

  protected $fillable=['area', 'descripcion', 'gasto_mensual', 'estado', 'RUC_empresa','fecha_creacion'];

  protected $guarded=[];
}
