<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table='proveedor';

  protected $primaryKey="RUC_proveedor";

  public $timestamps=false;


  protected $fillable=['nom_proveedor','direc_proveedor','direc_tienda','nomb_contacto','telefono_contacto','cel_proveedor','correo_proveedor','estado_proveedor','RUC_empresa'
  ,'proveedor_categoria','correo_contacto','nomb_contacto2','telefono_contacto2','correo_contacto2'];

  protected $guarded=[];
}
