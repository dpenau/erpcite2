<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class comentario extends Model
{
  protected $table='comentarios';

protected $primaryKey="codigo_comentario";

public $timestamps=false;


protected $fillable=['RUC_empresa','titulo','comentario'];

protected $guarded=[];
}
