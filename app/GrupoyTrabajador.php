<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class GrupoyTrabajador extends Model
{
    protected $table = 'grupo_trabajador';

    public $timestamps = false;

    protected $fillable = ['codigo_grupo_trabajo', 'codigo_grupo_trabajador'];

    protected $guarded = [];
}
