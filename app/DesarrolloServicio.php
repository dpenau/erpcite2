<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DesarrolloServicio extends Model
{
    protected $table='desarrollo_servicio';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'RUC_empresa',
                        'descripcion_material',
                        'importe',
                        'estado_registro'];

    protected $guarded=[];
    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','cod_proceso');
    }
}
