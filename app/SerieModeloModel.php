<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class SerieModeloModel extends Model
{
  protected $table='serie_modelo';

  protected $primaryKey="codigo";

  protected $keyType = "string";

  public $timestamps=false;


  protected $fillable=['codigo_modelo','codigo_serie','estado','RUC_empresa'];

  protected $guarded=[];
}
