<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class MaterialIndirecto extends Model
{
    protected $table='material_indirecto';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'empresa_id',
                        'tipo_material_id',
                        'descripcion',
                        'unidad_compra_id',
                        'costo_unitario',
                        'consumo',
                        'meses_duracion',
                        'costo_mensual',
                        'estado_registro'];

    protected $guarded=[];

    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','cod_proceso');
    }
    public function empresa()
    {
        return $this->belongsTo(Empresa::class,'empresa_id');
    }
    public function tipo_material()
    {
        return $this->belongsTo(TipoMaterialIndirecto::class,'tipo_material_id','id');
    }
}
