<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ReporteVentasModel extends Model
{
  protected $table='orden_pedido';

  protected $primaryKey="codigo_pedido";

  public $timestamps=false;


  protected $fillable=['codigo_cliente','total_pedido','fecha'];

  //protected $guarded=[];

    //
}
