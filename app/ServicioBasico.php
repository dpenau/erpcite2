<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ServicioBasico extends Model
{
    protected $table='servicio_basico';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'RUC_empresa',
                        'descripcion',
                        'costo_mensual',
                        'estado_registro'];

    protected $guarded=[];

    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','cod_proceso');
    }
}
