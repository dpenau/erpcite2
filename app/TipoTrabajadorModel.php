<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class TipoTrabajadorModel extends Model
{
    protected $table='tipo_trabajador';

    protected $primaryKey="cod_tipo_trabajador";
  
    public $timestamps=false;
  
  
    protected $fillable=['descrip_tipo_trabajador', 'estado_tipo_trabajador'];
  
    protected $guarded=[];
}
