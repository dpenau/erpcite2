<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class UnidadMedidaModel extends Model
{
    protected $table='unidad_medida';

    protected $primaryKey="cod_unidad_medida";

    public $timestamps=false;


    protected $fillable=['unidad','descrip_unidad_medida', 'magnitud_unidad_medida','estado_unidad_medida'];

    protected $guarded=[];
}
