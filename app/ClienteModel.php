<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
  protected $table='cliente';

  protected $primaryKey="codigo";

  protected $keyType = "string";

  public $timestamps=false;


  protected $fillable=['documento','nombre', 'destino','telefono','direccion','estado','RUC_empresa'];

  protected $guarded=[];
}
