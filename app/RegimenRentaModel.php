<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class RegimenRentaModel extends Model
{
    protected $table='regimen_renta';

    protected $primaryKey="cod_regimen_renta";
  
    public $timestamps=false;
  
  
    protected $fillable=['descrip_regimen_renta', 'estado_regimen_renta'];
  
    protected $guarded=[];
}
