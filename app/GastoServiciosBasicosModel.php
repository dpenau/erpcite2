<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class GastoServiciosBasicosModel extends Model
{
  protected $table='gasto_servicios_basicos';

  protected $primaryKey="cod_gasto_servicio_basico";

  public $timestamps=false;


  protected $fillable=['cod_area', 'descrip_servicio_basico', 'gasto_mensual', 'RUC_empresa', 'estado','fecha_creacion'];

  protected $guarded=[];
}
