<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DepreciacionModel extends Model
{
  protected $table='gasto_depreciacion_mantenimiento';

  protected $primaryKey="cod_activo";

  public $timestamps=false;


  protected $fillable=['descrip_activo','marca','cod_tipo_activo',
  'cod_area','anio_uso','anio_compra','valor_unitario_sin_IGV',
  'cantidad','gasto_mantenimiento','frecuencia_mantenimiento_anual',
  'cod_tipo_adquisicion','RUC_empresa','estado_depreciacion',
  'depreciacion_mensual'];

  protected $guarded=[];
}
