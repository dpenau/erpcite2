<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class fecha_historial extends Model
{
  protected $table='fecha_historial';


  public $timestamps=false;


  protected $fillable=['cod_trabajador','descripcion'];

  protected $guarded=[];
}
