<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenProduccion extends Model
{
  protected $table='orden_pedido_produccion';

  protected $primaryKey="codigo_orden_pedido_produccion";

  protected $keyType="string";

  public $timestamps=false;

  protected $fillable=['codigo_orden_pedido','RUC_empresa','fecha_creacion','codigo_serie_articulo','cantidades','ord_prod_seq','estado_orden_pedido_produccion'];

  protected $guarded=[];
}
