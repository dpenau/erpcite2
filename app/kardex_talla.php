<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class kardex_talla extends Model
{
    protected $table='kardex_talla';
    protected $primaryKey='id_kardex';
    public $timestamps=false;
    protected $fillable=['id_kardex', 'cod_material','stock_actual', 'costo_total_sin_igv', 'costo_total_con_igv', 'igv', 'RUC_empresa'];
    protected $guarded=[];
}
