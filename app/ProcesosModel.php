<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ProcesosModel extends Model
{
    protected $table='procesos';

    public $timestamps=false;


    protected $fillable=['codigo_orden_pedido_produccion','DNI_trabajador'];

    protected $guarded=[];
}
