<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ManoObraModel extends Model
{
  protected $table='mano_obra';

public $timestamps=false;
  protected $primaryKey="id";

protected $fillable=['sueldo_mensual','beneficios','otros','gasto_mensual','cod_area','RUC_empresa','puesto','estado','fecha_creacion'];

protected $guarded=[];
}
