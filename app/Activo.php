<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
    protected $table='activo';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'descripcion',
                        'tipo_activo_id',
                        'tipo_adquisicion',
                        'anos_uso',
                        'ano_compra',
                        'costo_mensual',
                        'estado_registro',
                        'RUC_empresa'];

    protected $guarded=[];
}
