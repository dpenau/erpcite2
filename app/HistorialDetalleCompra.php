<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class HistorialDetalleCompra extends Model
{
    protected $table = 'historial_detalle_orden_compra';

    
    protected $primaryKey="id";

    public $timestamps = false;

    protected $fillable = ['id', 'cod_temp_mat', 'cantidad_faltante', 'fecha', 'movimiento'];

    protected $guarded = [];
}