<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OperacionDirectaModel extends Model
{
  protected $table='operacion_directa';

  protected $primaryKey="cod_operacion_d";

  public $timestamps=false;

  protected $fillable=['RUC_empresa','cod_proceso','operacion_nombre','cod_tipo_pago','costo','beneficio','otros_costos','costo_par','estado_operacion','estado_registro'];

  protected $guarded=[];
}
