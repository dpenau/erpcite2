<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
  protected $table='serie';

  protected $primaryKey="cod_serie";

  public $timestamps=false;


  protected $fillable=['nombre_serie','tallaInicial','tallaFinal','RUC_empresa','estado_serie'];
				
  protected $guarded=[];
}
