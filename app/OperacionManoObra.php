<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OperacionManoObra extends Model
{
  protected $table='mano_obra';

public $timestamps=false;
  protected $primaryKey="cod_mano_obra";

protected $fillable=['cod_operacion_directa','cod_combinacion_modelo','RUC_empresa'];

protected $guarded=[];
}
