<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'material';

    protected $primaryKey = "cod_material";

    public $timestamps = false;

    protected $keyType = "string";
    protected $fillable = ['t_moneda', 'RUC_proveedor', 'descrip_material', 't_compra', 'costo_sin_igv_material',
        'costo_con_igv_material', 'estado_material', 'unidad_medida', 'RUC_empresa', 'cod_subcategoria',
        'stock_maximo', 'stock_minimo', 'factor_equivalencia', 'unidad_compra', 'cod_almacen', 'ubicacion',
        'cod_serie'];

    protected $guarded = [];
}
