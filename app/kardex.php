<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class kardex extends Model
{
    protected $table='kardex';
    protected $primaryKey='id_kardex';
    public $timestamps=false;
    protected $fillable=['id_kardex', 'cod_material','stock_actual', 'costo_total_sin_igv', 'costo_total_con_igv', 'igv', 'RUC_empresa'];
    protected $guarded=[];
}


/*class kardex extends Model
{
   protected $table='kardex_material';

    protected $primaryKey='cod_kardex_material';

    protected $keyType="string";

    public $timestamps=false;

    protected $fillable=['cod_almacen','cod_material','RUC_empresa','lugar_almacenaje','stock_total'];

    protected $guarded=[];
}
*/