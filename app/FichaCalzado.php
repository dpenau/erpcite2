<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class FichaCalzado extends Model
{
  protected $table='ficha_calzado';

  protected $primaryKey="cod_ficha";

  public $timestamps=false;


  protected $fillable=['cod_modelo', 'RUC_empresa', 'cod_serie','cod_coleccion','cod_linea'];

  protected $guarded=[];
}