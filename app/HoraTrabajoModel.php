<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class HoraTrabajoModel extends Model
{
  protected $table='horas_trabajo';
  protected $primaryKey="cod";
public $timestamps=false;


protected $fillable=['RUC_empresa','hora','dia'];

protected $guarded=[];
}
