<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    protected $table='proceso';
    protected $primaryKey="cod_proceso";
    public $timestamps=false;


    protected $fillable=['cod_proceso','nombre','codigo','estado_registro','RUC_empresa'];

    protected $guarded=[];
}
