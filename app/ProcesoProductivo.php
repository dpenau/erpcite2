<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ProcesoProductivo extends Model
{
    protected $table='proceso_productivo';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'RUC_empresa',
                        'numero_prototipo',
                        'descripcion_material',
                        'costo_total',
                        'importe',
                        'estado_registro'];

    protected $guarded=[];
    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','cod_proceso');
    }
}
