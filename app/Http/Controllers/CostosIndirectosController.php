<?php

namespace erpCite\Http\Controllers;

use erpCite\DesarrolloHormas;
use erpCite\DesarrolloMaterial;
use erpCite\DesarrolloServicio;
use erpCite\DesarrolloTroqueles;
use erpCite\MaterialIndirecto;
use erpCite\Proceso;
use erpCite\TipoMaterialIndirecto;
use erpCite\ProcesoProductivo;
use erpCite\ServicioBasico;
use erpCite\UnidadMedidaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CostosIndirectosController extends Controller
{
    public function index()
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        $procesos = Proceso::get();
        $unidad_compras = UnidadMedidaModel::get();
        $materiales_indirectos = MaterialIndirecto::with('proceso','tipo_material')->where('empresa_id',$ruc_empresa)->where('estado_registro','A')->get();
        $servicios_basicos = ServicioBasico::with('proceso')->where('estado_registro','A')->get();
        $tipo_materiales_indirectos = TipoMaterialIndirecto::where('estado_registro','A')->get();
        $desarrollo_hormas = DesarrolloHormas::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        $desarrollo_materiales = DesarrolloMaterial::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        $desarrollo_servicios = DesarrolloServicio::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        $desarrollo_troqueles = DesarrolloTroqueles::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        $procesos_productivos = ProcesoProductivo::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        //dd($desarrollo_materiales);
        return view('costos.indirectos.costos_operacion.index',['procesos'=>$procesos,
                                                                'unidad_compras'=>$unidad_compras,
                                                                'materiales_indirectos'=>$materiales_indirectos,
                                                                'tipo_materiales_indirectos'=>$tipo_materiales_indirectos,
                                                                'servicios_basicos'=>$servicios_basicos,
                                                                'desarrollo_hormas'=>$desarrollo_hormas,
                                                                'desarrollo_materiales'=>$desarrollo_materiales,
                                                                'desarrollo_servicios'=>$desarrollo_servicios,
                                                                'desarrollo_troqueles'=>$desarrollo_troqueles,
                                                                'procesos_productivos'=>$procesos_productivos,
                                                            ]);
    }
}
