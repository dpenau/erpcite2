<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\DepreciacionModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\DepreciacionFormRequest;
use DB;

class DepreciacionController extends Controller
{
    public function __construct(){
        $this->middleware('jefe');
    }

    public function index(Request $request){
        if ($request) {
            $depreciacion=DB::table('gasto_depreciacion_mantenimiento')
            ->join('tipo_activo','gasto_depreciacion_mantenimiento.cod_tipo_activo','=','tipo_activo.cod_tipo_activo')
            ->join('area','gasto_depreciacion_mantenimiento.cod_area','=','area.cod_area')
            ->join('tipo_adquisicion','gasto_depreciacion_mantenimiento.cod_tipo_adquisicion','=','tipo_adquisicion.cod_tipo_adquisicion')
            ->where('estado_depreciacion','=','1')
            ->where('gasto_depreciacion_mantenimiento.RUC_empresa',Auth::user()->RUC_empresa)
            //->orderBy('area.descrip_area', 'asc')
            ->paginate(10);
        return view('costos/indirectos/depreciacion/index',['depreciacion'=>$depreciacion]);
        }
    }

    public function create(Request $request){
        if($request)
        {
        $area=DB::table('area')->orderBy('descrip_area','asc')->get();
        $tipo_activo=DB::table('tipo_activo')
        ->where('tipo_activo.estado','=','1')
        ->orderBy('concepto','asc')->get();
        $tipo_adquisicion=DB::table('tipo_adquisicion')->get();
        $activo=DB::table('tipo_activo')
          ->where('tipo_activo.estado','=','1')
          ->orderBy('concepto','asc')
          ->get();
        $adquisicion=DB::table('tipo_adquisicion')
          ->where('tipo_adquisicion.estado_adquisicion','=','1')
          ->get();
        return view('costos/indirectos/depreciacion/create',['area'=>$area,'tipo_activo'=>$tipo_activo,'tipo_adquisicion'=>$tipo_adquisicion,'activo'=>$activo,'adquisicion'=>$adquisicion]);
        }
    }
    public function store()
  {
    //Se Registra el campo detalle_orden_compra
    $empresa=Auth::user()->RUC_empresa;
    $identificador=rand(100000,999999);
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$identificador;
    $depreciacion=new DepreciacionModel;
    $depreciacion->cod_activo=$res;
    $depreciacion->descrip_activo=Input::get('descrip_activo');
    $depreciacion->marca=Input::get('marca');
    $depreciacion->cod_tipo_activo=Input::get('cod_tipo_activo');
    $depreciacion->cod_area=Input::get('cod_area');
    $depreciacion->anio_uso=Input::get('anio_uso');
    $depreciacion->anio_compra=Input::get('anio_compra');
    $depreciacion->valor_unitario_sin_IGV=Input::get('valor_unitario_sin_IGV');
    $depreciacion->cantidad=Input::get('cantidad');
    $depreciacion->gasto_mantenimiento=Input::get('gasto_mantenimiento');
    $depreciacion->frecuencia_mantenimiento_anual=Input::get('frecuencia_mantenimiento_anual');
    $depreciacion->cod_tipo_adquisicion=Input::get('cod_tipo_adquisicion');
    $depreciacion->depreciacion_mensual=Input::get('depreciacion_mensual');
    $depreciacion->gasto_mensual_depreciacion=Input::get('gasto_mensual_depreciacion');
    $depreciacion->estado_depreciacion=1;
    $depreciacion->RUC_empresa=$empresa;
    $depreciacion->save();
    session()->flash('success','Gasto de Depreciacion y Mantenimiento registrado');
    return Redirect::to('costo/indirectos/depreciacion');
  }
    public function show(){
        return view('costos/indirectos/depreciacion/index');
    }
    public function edit($id)
    {
        return Redirect::to('costo/indirectos/depreciacion');
    }
    public function update()
    {
        return Redirect::to('costo/indirectos/depreciacion');
    }
    public function destroy()
    {
        $email=Input::get('email');
        $estado_depreciacion=Input::get('estado_depreciacion');
        if($estado_depreciacion==0){$mensaje="Eliminado";}
        $act=-DepreciacionModel::where('cod_activo',$email)
        ->delete();
        session()->flash('success','Gasto de Depreciacion y Mantenimiento '.$mensaje);
        return Redirect::to('costo/indirectos/depreciacion');
    }
}
