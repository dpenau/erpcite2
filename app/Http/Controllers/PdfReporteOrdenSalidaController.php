<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use Illuminate\Support\Facades\Auth;
class PdfReporteOrdenSalidaController extends Controller
{
    public function index($var){
        if ($var!="") {
          $pdf=\App::make('dompdf.wrapper');
          $pdf->loadHTML($this->convert_data($var,"0"));
        return $pdf->stream();
        }
    }
    public function tienda($var){
        if ($var!="") {
            $pdf=\App::make('dompdf.wrapper');
            $pdf->loadHTML($this->convert_data($var,"1"));
          return $pdf->stream();
        }
    }
    function get_data($query,$tipo)
    {
        if($tipo=="0")
        {
            $orden_data=DB::table('detalle_kardex_material')
            ->join('material','detalle_kardex_material.cod_kardex_material','=','material.cod_material')
            ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
            ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
            ->where('detalle_kardex_material.codigo_salida','=',$query)
            ->select('material.descrip_material','detalle_kardex_material.cantidad_salida',
            'detalle_kardex_material.trasladador_material','unidad_medida.descrip_unidad_medida'
            ,'subcategoria.cod_categoria','subcategoria.nom_subcategoria')
            ->get();
        }
        else
        {
            $orden_data=DB::table('detalle_kardex_tienda')
            ->join('kardex_tienda','detalle_kardex_tienda.cod_kardex_tienda','=','kardex_tienda.cod_kardex_tienda')
            ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
            ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
            ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
            ->where('detalle_kardex_tienda.codigo_salida','=',$query)
            ->select('material.descrip_material','detalle_kardex_tienda.cantidad_salida',
            'detalle_kardex_tienda.trasladador_material','unidad_medida.descrip_unidad_medida'
            ,'subcategoria.cod_categoria','subcategoria.nom_subcategoria')
            ->get();
        }
        return $orden_data;
    }
    function get_cabecera($query,$tipo)
    {
        if($tipo=="0")
        {
            $cabecera=DB::table('orden_salida')
            ->join('area','orden_salida.area_dirigida','=','area.cod_area')
            ->where('orden_salida.cod_orden_salida','=',$query)
            ->select('orden_salida.cod_orden_salida','orden_salida.fecha_creacion','orden_salida.area_dirigida','orden_salida.observacion'
            ,'area.descrip_area')
            ->get();
        }
        else
        {
            $cabecera=DB::table('orden_salida_tienda')
            ->join('area','orden_salida_tienda.area_dirigida','=','area.cod_area')
            ->where('orden_salida_tienda.cod_orden_salida','=',$query)
            ->select('orden_salida_tienda.cod_orden_salida','orden_salida_tienda.fecha_creacion','orden_salida_tienda.area_dirigida','orden_salida_tienda.observacion'
            ,'area.descrip_area')
            ->get();
        }
        return $cabecera;
    }
    function get_imagen()
    {
        $idempresa=Auth::user()->RUC_empresa;
        $imagen=DB::table('empresa')->where('RUC_empresa','=',$idempresa)->limit(1)->get();
        return $imagen;
    }
    function convert_data($query,$tipo)
    {
        if($tipo=="0")
        {
            $tipo_salida="KARDEX";
        }
        else
        {
            $tipo_salida="TIENDA";
        }
        $cabecera=$this->get_cabecera($query,$tipo);
        $detalle=$this->get_data($query,$tipo);
        $img=$this->get_imagen();
        $photo="";
        $output='<html><head><style>
        @page {
            margin: 0cm 0cm;
        }
        body {

            margin-top: 3cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }
        header {
            position: fixed;
            top: 0.5cm;
            left: 0.5cm;
            right: 0cm;
            height: 3cm;
        }
        footer {
            margin-right: 0cm;
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 2cm;
        }
        </style></head><body>';
        foreach ($img as $i) {
            if($i->imagen!="")
            {
                $photo=$i->imagen;
            }
        }
        foreach($cabecera as $cab)
        {
            
            if ($photo=="") {
                $output.='
                <header>
                <h1>Orden de Salida '.$query.'</h1>
                </header>
                ';
            }
            else {
                $output.='
                <header>
                <div class="row">
                    <div class="col-md-12">
                        <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
                    </div>
                </div>
                </header>
                <table style=" width:100%;border-collapse: collapse; border: 1px solid black;margin-left: auto;  margin-right: auto; font-size: 12px;">
                    <tr>
                        <th  style="font-size:40px;text-align: center;border-collapse: collapse; border: 1px solid black;">Orden de Salida '.$query.'-'.$tipo_salida.'</th>
                    </tr>
                    <tr>
                        <td style="border-collapse: collapse; border: 1px solid black;">Área Dirigida: '.$cab->descrip_area.'</td>
                    </tr>
                    <tr>
                        <td  style="border-collapse: collapse; border: 1px solid black;">Fecha de Salida: '.$cab->fecha_creacion.'</td>
                    </tr>
                    <tr>
                        <td  style="border-collapse: collapse; border: 1px solid black;">Observacion: '.$cab->observacion.'</td>
                    </tr>
                </table>
                ';
            }
        }
        $output.='<footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>';
        
        $output.='
        <h2>Materias Primas</h2>
        <table  style="width:100%;border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad de Salida</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
            </tr>

        ';
        foreach ($detalle as $dat) {
            if($dat->cod_categoria==969)
            {
                $output.='
                <tr>
                    <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
                    <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
                    <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_salida.'</td>
                    <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
                </tr>
                ';
            }
        }
        $output.='</table>';
        $output.='
        <h2>Insumos</h2>
        <table  style="width:100%;border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad de Salida</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
            </tr>
        ';
        foreach ($detalle as $dat) {
        if($dat->cod_categoria==306)
        {
            $output.='
            <tr>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_salida.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
            </tr>
            ';
        }
        }
        $output.='</table>';
        $output.='
        <h2>Suministros</h2>
        <table  style="width:100%;border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
            <tr>
                <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Detalle</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Cantidad de Salida</th>
                <th style="border-collapse: collapse; border: 1px solid black;">Unidad Medida</th>
            </tr>
        ';
        foreach ($detalle as $dat) {
        if($dat->cod_categoria==634)
        {
            $output.='
            <tr>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->nom_subcategoria.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_salida.'</td>
                <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
            </tr>
            ';
        }
        }
        $output.='</table>';
        $output.='<table style="width:100%;margin-top:15%;text-align:center;">
            <tbody>
                <tr>
                    <td>__________________</td>
                    <td>__________________</td>
                </tr>
                <tr>
                    <td>Firma de Responsable <br>de Logistica</td>
                    <td>Firma de Responsable <br>de Recepcion</td>
                </tr>
            </tbody>
        </table>';
        $output.=' </body></html>';
        return $output;
    }
}
