<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;
use erpCite\Proveedor;
use erpCite\ProveedorSubcategoria;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use erpCite\Http\Requests\MaterialRequest;
use erpCite\CategoriaModel;
use erpCite\SubcategoriaModel;
use Illuminate\Support\Facades\DB;

class ProveedorController extends Controller
{
    public function __construct()
  {
    $this->middleware('logistica');
  }
   public function index(Request $request)
  {
    if ($request) {

      $provSub=DB::table('proveedor_subcategoria')
      ->join('proveedor','proveedor_subcategoria.RUC_proveedor','=','proveedor.RUC_proveedor')
      ->join('subcategoria','proveedor_subcategoria.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->orderBy('subcategoria.nom_subcategoria','asc')
      ->get();

      $proveedorR=DB::table('proveedor')->rightJoin('categoria','proveedor.proveedor_categoria','=','categoria.cod_categoria')
      ->where('proveedor.RUC_empresa',Auth::user()->RUC_empresa);

      $proveedor=DB::table('proveedor')->leftJoin('categoria','proveedor.proveedor_categoria','=','categoria.cod_categoria')
      ->where('proveedor.RUC_empresa',Auth::user()->RUC_empresa)
      ->union($proveedorR)
      ->orderBy('nom_proveedor','asc')
      ->get();

      $subcategoria=SubcategoriaModel::join('categoria','subcategoria.cod_categoria','=','categoria.cod_categoria')
      ->where('subcategoria.estado_subcategoria','=','1')
      ->orderBy('categoria.nom_categoria','asc')
      ->get();

      $categoria=CategoriaModel::orderBy('categoria.nom_categoria','asc')
      ->get();

    //dd($proveedor);

      return view('logistica.proveedores.index',["proveedor"=>$proveedor,"subcategoria"=>$subcategoria,"categoria"=>$categoria,"provsub"=>$provSub]);
    }
  }
  public function create()
  {
    $categoria=DB::table('categoria')
    ->where('estado_categoria','=','1')->get();
    $tipo_proveedor=DB::table('subcategoria')
    ->where('estado_subcategoria','=','1')
    ->orderBy('subcategoria.nom_subcategoria','asc')->get();
    return view("logistica.proveedores.create",['categoria'=>$categoria,'tipo_proveedor'=>$tipo_proveedor]);
  }
  public function store(Request $request)
  {
    $idempresa=Auth::user()->RUC_empresa;

    $proveedor=new Proveedor;

    $sigla=DB::table('empresa')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->get();

    $var=Input::get('RUC_proveedor');
    $siglax = $sigla[0]->siglas;

    $subcategoria=DB::table('subcategoria')
    ->where('subcategoria.cod_categoria','=',Input::get('tipo_categoria'))
    ->where('subcategoria.estado_subcategoria','=','1')
    ->get();

    //dd($subcategoria);

    $res=$siglax.$var;
    //dd($res);

    $subcategorias = $_POST['subcategoria'];
    //dd($subcategorias);
    $proveedor->RUC_proveedor= $res;
    $proveedor->nom_proveedor=Input::get('nomb_proveedor');
    $proveedor->direc_proveedor=Input::get('dir_proveedor');
    $proveedor->direc_tienda=Input::get('tienda_proveedor');
    $proveedor->cel_proveedor=Input::get('tele_proveedor');
    $proveedor->nomb_contacto=Input::get('nom_contacto');
    $proveedor->telefono_contacto=Input::get('tel_contacto');
    $proveedor->correo_contacto=Input::get('correo_contacto');
    $proveedor->nomb_contacto2=Input::get('nom_contacto2');
    $proveedor->telefono_contacto2=Input::get('tel_contacto2');
    $proveedor->correo_contacto2=Input::get('correo_contacto2');
    $proveedor->correo_proveedor=Input::get('correo_proveedor');
    $proveedor->proveedor_categoria=Input::get('tipo_categoria');
    $proveedor->RUC_empresa=$idempresa;
    $proveedor->estado_proveedor=1;

    $proveedor->save();

    foreach ($subcategoria as $item){
      $guardo = "no";
      foreach ($subcategorias as $aux){
        if($item->cod_subcategoria == $aux){
          $provSub=new ProveedorSubcategoria;
          $provSub->RUC_proveedor=$res;
          $provSub->cod_subcategoria=$aux;
          $provSub->estado=1;
          $provSub->save();
          $guardo = "si";
          break;
        }
      }
      if($guardo == "no"){
        $provSub2=new ProveedorSubcategoria;
        $provSub2->RUC_proveedor=$res;
        $provSub2->cod_subcategoria=$item->cod_subcategoria;
        $provSub2->estado=0;
        $provSub2->save();
      }
    }

    session()->flash('success','Proveedor registrado');
    return Redirect::to('logistica/proveedores');
  }
  public function show()
  {
    return view('logistica.proveedores.index');
  }
  public function edit($id)
  {
    return Redirect::to('logistica/proveedores');
  }
  public function update(Request $request)
  {
    $ruc_proveedor = Input::get('ruc_editar');
    $act=Proveedor::where('RUC_proveedor', $ruc_proveedor)
    ->update(['nom_proveedor'=>Input::get('nomb_proveedor'),
    'direc_proveedor'=>Input::get('dir_proveedor'),
    'direc_tienda'=>Input::get('tienda_proveedor'),
    'nomb_contacto'=>Input::get('nom_contacto'),
    'telefono_contacto'=>Input::get('tel_contacto'),
    'correo_contacto'=>Input::get('correo_contacto'),
    'nomb_contacto2'=>Input::get('nom_contacto2'),
    'telefono_contacto2'=>Input::get('tel_contacto2'),
    'correo_contacto2'=>Input::get('correo_contacto2'),
    'cel_proveedor'=>Input::get('tele_proveedor'),
    'correo_proveedor'=>Input::get('correo_proveedor'),
    'proveedor_categoria'=>Input::get('cod_categoria')]);

    $provSubDel = DB::table('proveedor_subcategoria')
    ->where('RUC_proveedor',$ruc_proveedor)
    ->delete();

    $subcategoria=DB::table('subcategoria')
    ->where('subcategoria.cod_categoria','=',Input::get('cod_categoria'))
    ->where('subcategoria.estado_subcategoria','=','1')
    ->get();

    $subcat = Input::get('subcat');
    $subcategorias=explode(",",$subcat[0]);

    foreach ($subcategoria as $item){
      $guardo = "no";
      foreach ($subcategorias as $aux){
        if($item->cod_subcategoria == $aux){
          $provSub=new ProveedorSubcategoria;
          $provSub->RUC_proveedor=Input::get('ruc_editar');
          $provSub->cod_subcategoria=$item->cod_subcategoria;
          $provSub->estado=1;
          $provSub->save();
          $guardo = "si";
          break;
        }
      }
      if($guardo == "no"){
        $provSub2=new ProveedorSubcategoria;
        $provSub2->RUC_proveedor=Input::get('ruc_editar');
        $provSub2->cod_subcategoria=$item->cod_subcategoria;
        $provSub2->estado=0;
        $provSub2->save();
      }
    }
    session()->flash('success','Proveedor Actualizado');

    return Redirect::to('logistica/proveedores');
  }
  public function destroy($id)
  {
    $email=Input::get('email');
    $estado=Input::get('estado');
    if($estado==0){$mensaje="Desactivado";}
    else{$mensaje="Activado";}
    $act=Proveedor::where('RUC_proveedor',$email)
    ->update(['estado_proveedor'=>$estado]);
      session()->flash('success','Proveedor '.$mensaje);
    return Redirect::to('logistica/proveedores');
  }

  public function obtdatos($var)
  {
    $dato = $var;
    $empresa=Auth::user()->RUC_empresa;
    $subcategoria=DB::table('subcategoria')
    ->join('categoria','subcategoria.cod_categoria','=','categoria.cod_categoria')
    ->where('subcategoria.cod_categoria','=', $dato)
    ->where('subcategoria.estado_subcategoria','=','1')
    ->orderBy('subcategoria.nom_subcategoria','asc')
    ->get();
    return $subcategoria;
  }

  public function obtdatosubcategoria($var)
  {
    $datos=explode("+",$var);
    $categoria=$datos[0];
    $ruc=$datos[1];

    $categorias = DB::table('categoria')
    ->get();

    $subcategoria=DB::table('subcategoria')
    ->join('categoria','subcategoria.cod_categoria','=','categoria.cod_categoria')
    ->where('subcategoria.cod_categoria','=', $categoria)
    ->where('subcategoria.estado_subcategoria','=','1')
    ->orderBy('subcategoria.nom_subcategoria','asc')
    ->get();

    $provSub=DB::table('proveedor_subcategoria')
    ->join('proveedor','proveedor_subcategoria.RUC_proveedor','=','proveedor.RUC_proveedor')
    ->join('subcategoria','proveedor_subcategoria.cod_subcategoria','=','subcategoria.cod_subcategoria')
    ->where('proveedor_subcategoria.RUC_proveedor','=', $ruc)
    ->orderBy('subcategoria.nom_subcategoria','asc')
    ->get();

    if($provSub[0]->cod_categoria == $categoria){
      return $provSub;
    }
    return $subcategoria;
  }
}
