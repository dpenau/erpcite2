<?php

namespace erpCite\Http\Controllers;

use erpCite\MaterialIndirecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MaterialIndirectoController extends Controller
{
    public function index()
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        $materiales_indirectos = MaterialIndirecto::with(['empresa'])->where('RUC_emrpesa', $ruc_empresa)->where('estado_registro', 'A')->get();
        dd($materiales_indirectos);
        return view('configuracion_inicial.merma.index', ['materiales_indirectos' => $materiales_indirectos]);
    }
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $material_indirecto = MaterialIndirecto::create(
            [
                'proceso_id' => $request->proceso_id,
                'empresa_id' => $ruc_empresa,
                'tipo_material_id' => $request->tipo_material_id,
                'descripcion' => $request->descripcion,
                'unidad_compra_id' => $request->unidad_compra_id,
                'costo_unitario' => $request->costo_unitario,
                'consumo' => $request->consumo,
                'meses_duracion' => $request->meses_duracion,
                'costo_mensual' => $request->costo_mensual,
                'estado_registro' => 'A',
            ]
        );

        return redirect('costos/indirectos/operacion');
    }
    public function update($idMaterialIndirecto, Request $request)
    {
        //dd($idMaterialIndirecto);
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $material_indirecto = MaterialIndirecto::find($idMaterialIndirecto);
        $material_indirecto->fill(
            [
                'proceso_id' => $request->proceso_id,
                'empresa_id' => $ruc_empresa,
                'tipo_material_id' => $request->tipo_material_id,
                'descripcion' => $request->descripcion,
                'unidad_compra_id' => $request->unidad_compra_id,
                'costo_unitario' => $request->costo_unitario,
                'consumo' => $request->consumo,
                'meses_duracion' => $request->meses_duracion,
                'costo_mensual' => $request->costo_mensual,
                'estado_registro' => 'A',
            ]
        )->save();

        return redirect('costos/indirectos/operacion');
    }
    public function delete($idMaterialIndirecto)
    {
        $material_indirecto = MaterialIndirecto::find($idMaterialIndirecto);
        $material_indirecto->fill(
            [
                'estado_registro' => 'I',
            ]
        )->save();
        return redirect('costos/indirectos/operacion');
    }
}
