<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\Almacen;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class AlmacenController extends Controller
{
    public function __construct()
    {
        $this->middleware('logistica');
    }
    public function index(Request $request)
    {
        if ($request) {
            $almacen = DB::table('almacen')
                ->leftJoin('trabajador', 'almacen.DNI_trabajador', '=', 'trabajador.DNI_trabajador')
                ->leftJoin('categoria', 'almacen.cod_categoria', '=', 'categoria.cod_categoria')
                ->where('almacen.RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->where('almacen.estado_almacen', '=', 1)
                ->get();
            $categorias = DB::table('categoria')
                ->where('estado_categoria', '=', 1)->get();

            $trabajadores = DB::table('trabajador')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)->where('estado_trabajador', '=', 1)->where('cod_area', '=', '47812')->get();
            return view('logistica.almacen.index', ["almacen" => $almacen, "trabajadores" => $trabajadores, "categorias" => $categorias]);
        }
    }
    public function create(Request $request)
    {
        if ($request) {
            $trabajadores = DB::table('trabajador')
                ->where('RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->orderBy('apellido_paterno', 'asc')
                ->where('estado_trabajador', '=', 1)
                ->where('cod_area', '=', '47812')
                ->get();
            $categorias = DB::table('categoria')
                ->orderBy('nom_categoria', 'asc')
                ->where('estado_categoria', '=', 1)
                ->get();

            return view("logistica.almacen.create", ["trabajadores" => $trabajadores, "categorias" => $categorias]);
        }
    }
    public function store()
    {
        //Se Registra el campo detalle_orden_compra
        $empresa = Auth::user()->RUC_empresa;
        $identificador = rand(10000, 99999);
        $almacen = new Almacen;
        $almacen->cod_almacen = $identificador;
        $almacen->nom_almacen = Input::get('nombre');
        $almacen->descrip_almacen = Input::get('descripcion');
        $almacen->DNI_trabajador = Input::get('trabajador');
        $almacen->cod_categoria = Input::get('tipo_categoria');
        $almacen->estado_almacen = 1;
        $almacen->RUC_empresa = $empresa;
        $almacen->save();
        session()->flash('success', 'Almacen registrado');
        return Redirect::to('logistica/almacen');
    }
    public function show()
    {
        return view('recursos_humanos.area.index');
    }
    public function edit($id)
    {
        return Redirect::to('logistica/almacen');
    }
    public function update()
    {
        $codigo = Input::get('cod_almacen_editar');
        $nombre = Input::get('nombre');
        $encargado = Input::get('encargado');
        $categoria = Input::get('prioridad');
        $act = Almacen::where('cod_almacen', $codigo)
            ->update([
                "nom_almacen" => $nombre,
                "DNI_trabajador" => $encargado, "cod_categoria" => $categoria
            ]);
        session()->flash('success', 'Almacen Actualizado');
        return Redirect::to('logistica/almacen');
    }
    public function destroy()
    {

        $email = Input::get('email');
        $estado = Input::get('estado');

        $act = Almacen::where('cod_almacen', $email)
            ->update(['estado_almacen' => $estado]);
        if ($estado == 0) {
            session()->flash('success', 'Almacen Eliminado' );
        } else {
            session()->flash('success', 'Almacen Activado' );
        }

        return Redirect::to('logistica/almacen');
    }
}
