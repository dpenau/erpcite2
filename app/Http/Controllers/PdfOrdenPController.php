<?php

namespace erpCite\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
class PdfOrdenPController extends Controller
{ 
	public function create(Request $request){
    if ($request) {
      //$query=trim($request->get('codigo'));
      $query=$request->get('checkboxvar');
      $query6=trim($request->get('codigo'));
if($query6!=null)
{

      $pdf=\App::make('dompdf.wrapper');
      $pdf->setPaper('a4','landscape'); 
      $pdf->loadHTML($this->ladata($query6));

      return $pdf->stream();






}
    //  $query2=trim($request->get('checkboxvar[0]'));

  
      $pdf=\App::make('dompdf.wrapper');
      $pdf->setPaper('a4','landscape'); 
      $pdf->loadHTML($this->convert_data($query));

      return $pdf->stream();
    }
  }

  function convert_data($query)
  {

    $output='<table >

    <tbody>
    <tr>
    ';
    for($i=0;$i<sizeof($query);$i++)
    {
$suma=0;
    $detalle=DB::table('orden_produccion')
    ->join('empresa','orden_produccion.RUC_empresa','=','empresa.RUC_empresa')
    ->join('linea','orden_produccion.cod_linea','=','linea.cod_linea')
    ->join('serie','orden_produccion.cod_serie','=','serie.cod_serie')
    ->join('modelo','orden_produccion.cod_modelo','=','modelo.cod_modelo')

    ->where('estado_op','=','1')
    ->where('cod_op','=',$query[$i])
    ->get();

$talla_pares=DB::table('talla_pares')->where('cod_op','=',$query[$i])->get();
//dd($talla_pares[0]->talla);

if(($i%3)==0){
  $output.=
    ' </tr>
   <tr> ';
}
    foreach ($detalle as $dat) {
    
    $output.='
    acaa'.$query[$i].'

    
    <td>

    <table class="blueTable">
    <thead>
      
     </thead>
      <tr>
        <td class="tg-0pky" colspan="8">ORDEN DE PRODUCCION</td>
        <td class="tg-0pky" colspan="3">'.$dat->cod_op.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Empresa:</td>
        <td class="tg-0pky" colspan="7">'.$dat->nom_empresa.'</td>
        <td class="tg-0pky" colspan="3">Fecha de Pedido:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Cliente:</td>
        <td class="tg-0pky" colspan="7">'.$dat->cliente.'</td>
        <td class="tg-0pky" colspan="3">'.$dat->fecha_pedido.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Destino:</td>
        <td class="tg-0pky" colspan="7">'.$dat->destino.'</td>
        <td class="tg-0pky" colspan="3">Fecha de Entrega:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Marca:</td>
        <td class="tg-0pky" colspan="7">'.$dat->marca.'</td>
        <td class="tg-0pky" colspan="3">'.$dat->fecha_entrega.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Linea:</td>
        <td class="tg-0pky" colspan="4">'.$dat->nombre_linea.'</td>
        <td class="tg-0pky" colspan="3">Serie:</td>
        <td class="tg-0pky" colspan="3">'.$dat->nombre_serie.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Codigo:</td>
        <td class="tg-0pky" colspan="4">'.$dat->cod_modelo.'</td>
        <td class="tg-0pky" colspan="3">Serie de Tallas:</td>
        <td class="tg-0pky" colspan="3">'.$dat->tallaInicial.'-'.$dat->tallaFinal.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Descripción:</td>
        <td class="tg-0pky" colspan="7">'.$dat->nombre.'</td>
        <td class="tg-0pky" colspan="3">Ingreso a Prod.:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Estado:</td>
        <td class="tg-0pky" colspan="4">'.$dat->estado.'</td>';
    if($dat->estado=="Urgente"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:red;"></td>
    ';
    }
    if($dat->estado=="Normal"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:green;"></td>
    ';
    }
    if($dat->estado=="Planificable"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:yellow;"></td>
    ';
    }
  $output.=
    '    

          <td class="tg-0pky" colspan="3">'.$dat->ingreso_prod.'</td>
        </tr>
        <tr>
          <td class="tg-0pky">Serie:</td>
        
                    

';

        if(isset($talla_pares[0]))
          {$output.='<td class="tg-0pky">'.$talla_pares[0]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[1]))
          {$output.='<td class="tg-0pky">'.$talla_pares[1]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[2]))
          {$output.='<td class="tg-0pky">'.$talla_pares[2]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[3]))
          {$output.='<td class="tg-0pky">'.$talla_pares[3]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[4]))
          {$output.='<td class="tg-0pky">'.$talla_pares[4]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[5]))
          {$output.='<td class="tg-0pky">'.$talla_pares[5]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[6]))
          {$output.='<td class="tg-0pky">'.$talla_pares[6]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[7]))
          {$output.='<td class="tg-0pky">'.$talla_pares[7]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[8]))
          {$output.='<td class="tg-0pky">'.$talla_pares[8]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}} 
        if(isset($talla_pares[9]))
          {$output.='<td class="tg-0pky">'.$talla_pares[9]->talla.'</td> ';}



  $output.=
    ' 
      <td class="tg-0pky">T</td>
        </tr>
        <tr>
        <td class="tg-0pky">Pares:</td>
          ';
          if(isset($talla_pares[0]))
          {$output.='<td class="tg-0pky">'.$talla_pares[0]->pares.'</td> ';$suma=$talla_pares[0]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[1]))
          {$output.='<td class="tg-0pky">'.$talla_pares[1]->pares.'</td> ';$suma=$talla_pares[1]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[2]))
          {$output.='<td class="tg-0pky">'.$talla_pares[2]->pares.'</td> ';$suma=$talla_pares[2]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[3]))
          {$output.='<td class="tg-0pky">'.$talla_pares[3]->pares.'</td> ';$suma=$talla_pares[3]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[4]))
          {$output.='<td class="tg-0pky">'.$talla_pares[4]->pares.'</td> ';$suma=$talla_pares[4]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[5]))
          {$output.='<td class="tg-0pky">'.$talla_pares[5]->pares.'</td> ';$suma=$talla_pares[5]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[6]))
          {$output.='<td class="tg-0pky">'.$talla_pares[6]->pares.'</td> ';$suma=$talla_pares[6]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[7]))
          {$output.='<td class="tg-0pky">'.$talla_pares[7]->pares.'</td> ';$suma=$talla_pares[7]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[8]))
          {$output.='<td class="tg-0pky">'.$talla_pares[8]->pares.'</td> ';$suma=$talla_pares[8]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}} 
        if(isset($talla_pares[9]))
          {$output.='<td class="tg-0pky">'.$talla_pares[9]->pares.'</td> ';$suma=$talla_pares[9]->pares+$suma;}


          $output.=
    ' 
          <td class="tg-0pky">'.$suma.'</td>
        </tr>
      </table>

      </td>

     
'; 

$output.=
    '
            <style>

            table.blueTable {
              border: 1px solid #1C6EA4;
              background-color: #EEEEEE;
              text-align: left;
              border-collapse: collapse;
            }
            table.blueTable td, table.blueTable th {
              border: 1px solid #AAAAAA;
            }
            table.blueTable tbody td {
              font-size: 12 px;
            }
            table.blueTable tr:nth-child(even) {
              background: #D0E4F5;
            }
            table.blueTable thead {
              background: #FFFBF8;
              background: -moz-linear-gradient(top, #fffcfa 0%, #fffbf8 66%, #FFFBF8 100%);
              background: -webkit-linear-gradient(top, #fffcfa 0%, #fffbf8 66%, #FFFBF8 100%);
              background: linear-gradient(to bottom, #fffcfa 0%, #fffbf8 66%, #FFFBF8 100%);
            }
            table.blueTable thead th {
              font-size: 5px;
              font-weight: bold;
              color: #3A3A3A;
              border-left: 2px solid #D0E4F5;
            }
            table.blueTable thead th:first-child {
              border-left: none;
            }

            table.blueTable tfoot {
              font-size: 10px;
              font-weight: bold;
              color: #FFFFFF;
              background: #D0E4F5;
              background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
              background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
              background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
              border-top: 2px solid #444444;
            }
            table.blueTable tfoot td {
              font-size: 11px;
            }
            table.blueTable tfoot .links {
              text-align: right;
            }
            table.blueTable tfoot .links a{
              display: inline-block;
              background: #1C6EA4;
              color: #FFFFFF;
              padding: 1px 3px;
              border-radius: 5px;
            }


            </style>


     ';
     }
   } //fin for

   $output.=
    ' </tr>
    </tbody>
      </table>';
    return $output;
  }


function ladata($query){
 $detalle=DB::table('orden_produccion')
    ->join('empresa','orden_produccion.RUC_empresa','=','empresa.RUC_empresa')
    ->join('linea','orden_produccion.cod_linea','=','linea.cod_linea')
    ->join('serie','orden_produccion.cod_serie','=','serie.cod_serie')
    ->join('modelo','orden_produccion.cod_modelo','=','modelo.cod_modelo')

    ->where('estado_op','=','1')
    ->where('cod_op','=',$query)
    ->get();
$talla_pares=DB::table('talla_pares')->where('cod_op','=',$query)->get();
$suma=0; 
     foreach ($detalle as $dat) {
$output=
    '

<table align="center">

<tbody>
<tr>
<td>

<table class="blueTable3">
    <thead>
      
     </thead>
      <tr>
        <td class="tg-0pky" colspan="8">ORDEN DE PRODUCCION</td>
        <td class="tg-0pky" colspan="3">'.$dat->cod_op.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Empresa:</td>
        <td class="tg-0pky" colspan="7">'.$dat->nom_empresa.'</td>
        <td class="tg-0pky" colspan="3">Fecha de Pedido:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Cliente:</td>
        <td class="tg-0pky" colspan="7">'.$dat->cliente.'</td>
        <td class="tg-0pky" colspan="3">'.$dat->fecha_pedido.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Destino:</td>
        <td class="tg-0pky" colspan="7">'.$dat->destino.'</td>
        <td class="tg-0pky" colspan="3">Fecha de Entrega:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Marca:</td>
        <td class="tg-0pky" colspan="7">'.$dat->marca.'</td>
        <td class="tg-0pky" colspan="3">'.$dat->fecha_entrega.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Linea:</td>
        <td class="tg-0pky" colspan="4">'.$dat->nombre_linea.'</td>
        <td class="tg-0pky" colspan="3">Serie:</td>
        <td class="tg-0pky" colspan="3">'.$dat->nombre_serie.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Codigo:</td>
        <td class="tg-0pky" colspan="4">'.$dat->cod_modelo.'</td>
        <td class="tg-0pky" colspan="3">Serie de Tallas:</td>
        <td class="tg-0pky" colspan="3">'.$dat->tallaInicial.'-'.$dat->tallaFinal.'</td>
      </tr>
      <tr>
        <td class="tg-0pky">Descripción:</td>
        <td class="tg-0pky" colspan="7">'.$dat->nombre.'</td>
        <td class="tg-0pky" colspan="3">Ingreso a Prod.:</td>
      </tr>
      <tr>
        <td class="tg-0pky">Estado:</td>
        <td class="tg-0pky" colspan="4">'.$dat->estado.'</td>';
    if($dat->estado=="Urgente"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:red;"></td>
    ';
    }
    if($dat->estado=="Normal"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:green;"></td>
    ';
    }
    if($dat->estado=="Planificable"){
    $output.='
          <td class="tg-0pky" colspan="3" style="background-color:yellow;"></td>
    ';
    }
  $output.=
    '    
       

          <td class="tg-0pky" colspan="3">'.$dat->ingreso_prod.'</td>
        </tr>
        <tr>
                  <td class="tg-0pky">Serie:</td>

         ';
  if(isset($talla_pares[0]))
          {$output.='<td class="tg-0pky">'.$talla_pares[0]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[1]))
          {$output.='<td class="tg-0pky">'.$talla_pares[1]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[2]))
          {$output.='<td class="tg-0pky">'.$talla_pares[2]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[3]))
          {$output.='<td class="tg-0pky">'.$talla_pares[3]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[4]))
          {$output.='<td class="tg-0pky">'.$talla_pares[4]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[5]))
          {$output.='<td class="tg-0pky">'.$talla_pares[5]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[6]))
          {$output.='<td class="tg-0pky">'.$talla_pares[6]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[7]))
          {$output.='<td class="tg-0pky">'.$talla_pares[7]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[8]))
          {$output.='<td class="tg-0pky">'.$talla_pares[8]->talla.'</td> ';}
        else{ {$output.='<td class="tg-0pky"></td> ';}} 
        if(isset($talla_pares[9]))
          {$output.='<td class="tg-0pky">'.$talla_pares[9]->talla.'</td> ';}



  $output.=
    ' 
      <td class="tg-0pky">T</td>
        </tr>
        <tr>
        <td class="tg-0pky">Pares:</td>
          ';
          if(isset($talla_pares[0]))
          {$output.='<td class="tg-0pky">'.$talla_pares[0]->pares.'</td> ';$suma=$talla_pares[0]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[1]))
          {$output.='<td class="tg-0pky">'.$talla_pares[1]->pares.'</td> ';$suma=$talla_pares[1]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[2]))
          {$output.='<td class="tg-0pky">'.$talla_pares[2]->pares.'</td> ';$suma=$talla_pares[2]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[3]))
          {$output.='<td class="tg-0pky">'.$talla_pares[3]->pares.'</td> ';$suma=$talla_pares[3]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[4]))
          {$output.='<td class="tg-0pky">'.$talla_pares[4]->pares.'</td> ';$suma=$talla_pares[4]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[5]))
          {$output.='<td class="tg-0pky">'.$talla_pares[5]->pares.'</td> ';$suma=$talla_pares[5]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[6]))
          {$output.='<td class="tg-0pky">'.$talla_pares[6]->pares.'</td> ';$suma=$talla_pares[6]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[7]))
          {$output.='<td class="tg-0pky">'.$talla_pares[7]->pares.'</td> ';$suma=$talla_pares[7]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}}
        if(isset($talla_pares[8]))
          {$output.='<td class="tg-0pky">'.$talla_pares[8]->pares.'</td> ';$suma=$talla_pares[8]->pares+$suma;}
        else{ {$output.='<td class="tg-0pky"></td> ';}} 
        if(isset($talla_pares[9]))
          {$output.='<td class="tg-0pky">'.$talla_pares[9]->pares.'</td> ';$suma=$talla_pares[9]->pares+$suma;}


          $output.=
    ' 
          <td class="tg-0pky">'.$suma.'</td>
        </tr>
      </table>

      </td>

     
'; 


}

  $output.=
    '
<style>

table.blueTable3 {
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable3 td, table.blueTable3 th {
  border: 4px solid #AAAAAA;
  padding: 10px 9px;
}
table.blueTable3 tbody td {
  font-size: 18px;
}
table.blueTable3 tr:nth-child(even) {
  background: #D0E4F5;
}
table.blueTable3 thead {
  background: #1C6EA4;
  background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  border-bottom: 10px solid #444444;
}
table.blueTable3 thead th {
  font-size: 30px;
  font-weight: bold;
  color: #FFFFFF;
  border-left: 10px solid #D0E4F5;
}
table.blueTable3 thead th:first-child {
  border-left: none;
}

table.blueTable3 tfoot {
  font-size: 30px;
  font-weight: bold;
  color: #FFFFFF;
  background: #D0E4F5;
  background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  border-top: 10px solid #444444;
}
table.blueTable3 tfoot td {
  font-size: 30px;
}
table.blueTable3 tfoot .links {
  text-align: right;
}
table.blueTable3 tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}
</style>';
    return $output;

}

}
