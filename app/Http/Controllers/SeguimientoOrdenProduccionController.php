<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use erpCite\SeguimientoOrdenProduccion;

//use Illuminate\Support\Facades\Auth;


class SeguimientoOrdenProduccionController extends Controller
{
    public function index()
{
  $datosOrdenProduccion = SeguimientoOrdenProduccion::all();


  return view('Produccion.orden_produccion.seguimientoOrdenesProduccion',compact('datosOrdenProduccion'));
}

public function pdf(){
  $datosOrdenProduccion = SeguimientoOrdenProduccion::all();
  $pdf = PDF::loadView('Produccion.orden_produccion.pdf',compact('datosOrdenProduccion'));
   return $pdf->download('SeguimientoOrdenProduccion.pdf');
}


}
