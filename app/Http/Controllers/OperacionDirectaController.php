<?php

namespace erpCite\Http\Controllers;

use erpCite\Empresa;
use erpCite\OperacionDirectaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class OperacionDirectaController extends Controller
{
    public function __construct()
    {
        $this->middleware('desarrollo');
    }
    public function index(Request $request)
    {
        if ($request) {
            $operaciones = DB::table('operacion_directa')
                ->where('operacion_directa.estado_operacion', '=', 1)
                ->where('operacion_directa.estado_registro', 'A')
                ->where('operacion_directa.RUC_empresa', Auth::user()->RUC_empresa)
                ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
                ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
                ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
                ->get();
            $proceso = DB::table('proceso')
                ->where('estado_proceso', 1)
                ->where('operacion', 1)
                ->get();
            $tipo = DB::table('tipo_pago_nuevo')
                ->where('estado_pago', 1)
                ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
                ->get();
            $empresaP = DB::table('empresa')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->join('regimen_laboral', 'empresa.cod_regimen_laboral', '=', 'regimen_laboral.cod_regimen_laboral')
                ->join('politica_desarrollo', 'empresa.politica_desarrollo_id', '=', 'politica_desarrollo.id')
                ->get();

            return view('configuracion_inicial.operacion_directa.index', [
                'operaciones' => $operaciones,
                'proceso' => $proceso,
                'tipo' => $tipo,
                'empresaP' => $empresaP,

            ]);
        }
        //return view('Produccion.modelo.index');
    }
    public function create(Request $request)
    {
        $proceso = DB::table('proceso')
            ->where('operacion', 1)
            ->get();
        $tipo = DB::table('tipo_pago_nuevo')
            ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
            ->get();
        $empresaP = DB::table('empresa')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->join('regimen_laboral', 'empresa.cod_regimen_laboral', '=', 'regimen_laboral.cod_regimen_laboral')
            ->join('politica_desarrollo', 'empresa.politica_desarrollo_id', '=', 'politica_desarrollo.id')
            ->get();

        return view('configuracion_inicial.operacion_directa.create', [
            'proceso' => $proceso,
            'tipo' => $tipo,
            'empresaP' => $empresaP,

        ]);
    }
    public function store(Request $data)
    {

        $RUC_empresa = Auth::user()->RUC_empresa;

        $empresa = Empresa::where('RUC_empresa', $RUC_empresa)->first();
        if ($empresa->politica_desarrollo_id) {
            $cod_proceso = Input::get('proceso');
            $operacion_nombre = Input::get('operacion');
            $cod_tipo_pago = Input::get('tipo_operacion');
            $costo = Input::get('costo');
            $beneficio = Input::get('beneficio');
            $otros_costos = Input::get('otros_costos');
            $costo_par = Input::get('costo_par');
            $estado_operacion = 1;



            $operacionDirecta = new OperacionDirectaModel;
            $operacionDirecta->cod_proceso = $cod_proceso;

            $operacionDirecta->operacion_nombre = $operacion_nombre;
            $operacionDirecta->cod_tipo_pago = $cod_tipo_pago;
            $operacionDirecta->costo = $costo;
            $operacionDirecta->beneficio = $beneficio;
            $operacionDirecta->otros_costos = $otros_costos;
            $operacionDirecta->costo_par = $costo_par;
            $operacionDirecta->estado_operacion = $estado_operacion;
            $operacionDirecta->RUC_empresa = $RUC_empresa;

            $operacionDirecta->save();

            session()->flash('success', 'Operacion Directa Creada Satisfactoriamente');
            return Redirect::to('configuracion/operacion_directa');
        } else {
            session()->flash('error', 'Operacion Directa no creada, falta añadir Produccion promedio');
            return Redirect::to('configuracion/operacion_directa');
        }

    }
    public function show()
    {
        /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
    }
    public function edit($id)
    {
        /* return Redirect::to('logistica/clasificacion');*/
    }
    public function update(Request $data)
    {
        $codigo_operacion = Input::get('codigo_operacion');
        $cod_proceso = Input::get('proceso');
        $operacion_nombre = Input::get('operacion');
        $cod_tipo_pago = Input::get('tipo_operacion');
        $costo = Input::get('costo');
        $beneficio = Input::get('beneficio');
        $otros_costos = Input::get('otros_costos');
        $costo_par = Input::get('costo_par');

        $combinacionModel = OperacionDirectaModel::where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_operacion_d', $codigo_operacion)
            ->update([
                "cod_proceso" => $cod_proceso,
                'operacion_nombre' => $operacion_nombre,
                'costo' => $costo,
                'beneficio' => $beneficio,
                'otros_costos' => $otros_costos,
                'costo_par' => $costo_par,
                'cod_tipo_pago' => $cod_tipo_pago
            ]);

        session()->flash('success', 'Edición correctamente realizada');
        return Redirect::to('configuracion/operacion_directa');
    }

    public function destroy()
    {
        $codigo = Input::get('codigo');
        $mensaje = "";
        $act = OperacionDirectaModel::where('cod_operacion_d', $codigo)->first();
        $act->estado_registro = 'I';
        $act->save();
        session()->flash('success', 'Operación directa eliminada satisfactoriamente' . $mensaje);

        return Redirect::to('configuracion/operacion_directa');
    }
}
