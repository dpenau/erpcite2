<?php

namespace erpCite\Http\Controllers;

use erpCite\DesarrolloHormas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesarrolloHormasController extends Controller
{
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($request);
        $hormas = DesarrolloHormas::create(
            [
                'proceso_id'=>$request->proceso_id,
                'RUC_empresa'=>$ruc_empresa,
                'descripcion_material'=>$request->descripcion_material,
                'cantidad'=>$request->cantidad,
                'costo_unitario'=>$request->costo_unitario,
                'importe'=>$request->importe,
                'estado_registro'=>'A',
            ]
            );

        return redirect('costos/indirectos/operacion');
    }
}
