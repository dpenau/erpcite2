<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
use erpCite\Charts\historial;
class TrabajadorHistorialController extends Controller
{
  public function __construct()
  {
    $this->middleware('rhumanos');
  }
  public function index($var)
  {
    if ($var) {
      $detalle=DB::table('fecha_historial')->where('cod_trabajador','=',$var)->get();
      return view('recursos_humanos.trabajador.historial',["var"=>$var,"detalle"=>$detalle]);
    }
  }
}
