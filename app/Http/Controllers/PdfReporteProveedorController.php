<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;
class PdfReporteProveedorController extends Controller
{
    public function indicador()
    {
      $proveedor=DB::table('proveedor')
    ->join('categoria','proveedor.proveedor_categoria','=','categoria.cod_categoria')
    ->where('proveedor.RUC_empresa',Auth::user()->RUC_empresa)
    ->get();

      return view('logistica.proveedores.indicadores',["proveedor"=>$proveedor]);
    }
    public function obt_todo($var)
    {
      $datos=explode("_",$var);
      //$datosF=explode("-",$datos[0]);
      $data=DB::table('orden_compras')
        ->join('proveedor','orden_compras.RUC_proveedor','=','proveedor.RUC_proveedor')
        ->where('orden_compra.RUC_empresa','=',Auth::user()->RUC_empresa)
        //->whereYear('fecha','=',$datos[1])
        ->where('orden_compras.RUC_proveedor','=',$datos[0])
        ->select('orden_compras.codigo_orden','orden_compra.fecha')
        ->orderBy('orden_compra.codigo_orden')
        ->get();
        return $data;
    }
    public function obt_dato($var)
    {
      $mes=date("m");
      $año=date("Y");
      $comienzo=0;
      $data;
      switch($mes)
      {
        case 1:$comienzo=10;
        $data=DB::table('detalle_orden_compra')
        ->join('orden_compra','detalle_orden_compra.cod_orden_compra','=','orden_compra.cod_orden_compra')
        ->where('orden_compra.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereMonth('fecha_orden_compra','>=', $comienzo)
        ->orwhereMonth('fecha_orden_compra','<=',$mes)
        ->whereYear('fecha_orden_compra','>=',$año-1)
        ->where('orden_compra.RUC_proveedor','=',$var)
        ->select('orden_compra.cod_orden_compra','orden_compra.fecha_entrega','detalle_orden_compra.fecha_deposito','detalle_orden_compra.fecha_pago')
        ->orderBy('orden_compra.cod_orden_compra')
        ->get();
        break;
        case 2:$comienzo=11;
        $data=DB::table('detalle_orden_compra')
        ->join('orden_compra','detalle_orden_compra.cod_orden_compra','=','orden_compra.cod_orden_compra')
        ->where('orden_compra.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereMonth('fecha_orden_compra','>=', $comienzo)
        ->orwhereMonth('fecha_orden_compra','<=',$mes)
        ->whereYear('fecha_orden_compra','>=',$año-1)
        ->where('orden_compra.RUC_proveedor','=',$var)
        ->select('orden_compra.cod_orden_compra','orden_compra.fecha_entrega','detalle_orden_compra.fecha_deposito','detalle_orden_compra.fecha_pago')
        ->orderBy('orden_compra.cod_orden_compra')
        ->get();
        break;
        case 3:$comienzo=12;
        $data=DB::table('detalle_orden_compra')
        ->join('orden_compra','detalle_orden_compra.cod_orden_compra','=','orden_compra.cod_orden_compra')
        ->where('orden_compra.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereMonth('fecha_orden_compra','>=', $comienzo)
        ->orwhereMonth('fecha_orden_compra','<=',$mes)
        ->whereYear('fecha_orden_compra','>=',$año-1)
        ->where('orden_compra.RUC_proveedor','=',$var)
        ->select('orden_compra.cod_orden_compra','orden_compra.fecha_entrega','detalle_orden_compra.fecha_deposito','detalle_orden_compra.fecha_pago')
        ->orderBy('orden_compra.cod_orden_compra')
        ->get();
        break;
        default:$comienzo=$mes-3;
        $data=DB::table('detalle_orden_compra')
        ->join('orden_compra','detalle_orden_compra.cod_orden_compra','=','orden_compra.cod_orden_compra')
        ->where('orden_compra.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereMonth('fecha_orden_compra','>=', $comienzo)
        ->orwhereMonth('fecha_orden_compra','<=',$mes)
        ->whereYear('fecha_orden_compra','=',$año)
        ->where('orden_compra.RUC_proveedor','=',$var)
        ->select('orden_compra.cod_orden_compra','orden_compra.fecha_entrega','detalle_orden_compra.fecha_deposito','detalle_orden_compra.fecha_pago')
        ->orderBy('orden_compra.cod_orden_compra')
        ->get();
        break;
      }
      return $data;
    }
    public function index(Request $request){
        if ($request) {
          $pdf=\App::make('dompdf.wrapper');
          $pdf->setPaper('a4','landscape');
          $pdf->loadHTML($this->convert_data());
    
          return $pdf->stream();
        }
      }
      function get_data()
      {
        $orden_data=DB::table('proveedor')
        ->join('categoria','proveedor.proveedor_categoria','=','categoria.cod_categoria')
        ->where('proveedor.estado_proveedor','=','1')
        ->where('proveedor.RUC_empresa',Auth::user()->RUC_empresa)
        ->get();
       
        return $orden_data;
      }
      function get_data_subCat()
      {
        $orden_data_sub=DB::table('proveedor')
        ->join('categoria','proveedor.proveedor_categoria','=','categoria.cod_categoria')
        ->join('proveedor_subcategoria','proveedor.RUC_proveedor','=','proveedor_subcategoria.RUC_proveedor')
        ->join('subcategoria','proveedor_subcategoria.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->where('proveedor.estado_proveedor','=','1')
        ->where('proveedor_subcategoria.estado','=','1')
        ->where('proveedor.RUC_empresa',Auth::user()->RUC_empresa)
        ->get();
        
        return $orden_data_sub;
      }
      function get_imagen()
      {
        $idempresa=Auth::user()->RUC_empresa;
        $imagen=DB::table('empresa')->where('RUC_empresa','=',$idempresa)->limit(1)->get();
        return $imagen;
      }
      function convert_data()
      {
        $detalle=$this->get_data();
        $detalle_sub=$this->get_data_subCat();
        $img=$this->get_imagen();
        $photo="";
        foreach ($img as $i) {
            if($i->imagen!="")
            {
              $photo=$i->imagen;
            }
        }
        $output='<html><head><style>
        @page {
              margin: 0cm 0cm;
        }
        body {
              margin-top: 2cm;
              margin-left: 2cm;
              margin-right: 2cm;
              margin-bottom: 2cm;
        }
        header {
    
              position: fixed;
              top: 0.5cm;
              left: 0.5cm;
              right: 0cm;
              height: 1cm;
        }
        footer {
              margin-right: 0cm;
              position: fixed;
              bottom: 0cm;
              left: 0cm;
              right: 0cm;
              height: 2cm;
        }
        </style></head><body>';
        if ($photo!="") {
          $output.='
          <header>
          <div class="row">
            <div class="col-md-12">
              <img  src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
            </div>
          </div>
          </header>
          <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
          ';
        }
          $output.='<h2>Lista de proveedores</h2>
        <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
        <tr>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">RUC Proveedor</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Nombre Proveedor</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Dirección</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Nombre Contacto</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Teléfono Contacto</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Categoria</th>
          <th style="border-collapse: collapse; border: 1px solid black;text-align: center;">Subcategorias</th>
        </tr>
      ';
        $mat_anterior="";
        foreach ($detalle as $dat) {
              
        
        $output.='
          <tr>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->RUC_proveedor.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->nom_proveedor.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->direc_proveedor.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->nomb_contacto.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->cel_proveedor.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->nom_categoria.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">
            ';

             foreach ($detalle_sub as $dat2) {
                if($dat->RUC_proveedor==$dat2->RUC_proveedor){
                  $output.=''.$dat2->nom_subcategoria.'<br>';
                }
            }
            $output.='</td></tr>';
          
        }
        $output.='</table>
        </body></html>
        ';
          return $output;
    
      }
}
