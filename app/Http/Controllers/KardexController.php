<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use erpCite\kardex;
use erpCite\kardex_talla;
use erpCite\kardex_talla_tallas;
use erpCite\kardex_cab;
use erpCite\kardex_cab_talla;
use erpCite\kardex_cab_talla_tallas;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class KardexController extends Controller
{
    public function __construct()
    {
        $this->middleware('logistica');
    }

    public function index(Request $request)
    {
        if ($request) {
            $ordenes = DB::table('orden_compra')
                ->join('proveedor', 'orden_compra.RUC_proveedor', '=', 'proveedor.RUC_proveedor')
                ->where('orden_compra.RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->orderBy('orden_compra.fecha_orden_compra', 'desc')
                ->select('proveedor.nom_proveedor', 'orden_compra.cod_orden_compra', 'orden_compra.fecha_orden_compra', 'orden_compra.estado_orden_compra')
                ->get();

            return view('logistica.ingreso_salida.index', ["ordenes" => $ordenes]);
        }
    }

    public function create(Request $request)
    {
        //INDEX NORMAL
        if ($request) {
            $empresa = Auth::user()->RUC_empresa;
            $now = Carbon::now();
            $date = $now->format('Y-m-d');

            $area = DB::Table('area')
                ->where('estado_area', '=', '1')
                ->orderBy('descrip_area', 'asc')
                ->get();

            $categorias = DB::table('categoria')
                ->get();

            $materiales = DB::table('kardex')
                ->join('material', 'kardex.cod_material', '=', 'material.cod_material')
                ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
                ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
                ->where('kardex.RUC_empresa', '=', $empresa)
                ->get();

            return view("logistica.kardex.salida_normal", ["fechaActual" => $now, "areas" => $area, "categorias" => $categorias, "materiales" => $materiales]);
        }
    }

    public function show()
    {
        //INDEX TALLA
        $empresa = Auth::user()->RUC_empresa;
        $now = Carbon::now();
        $date = $now->format('Y-m-d');

        $area = DB::Table('area')
            ->where('estado_area', '=', '1')
            ->orderBy('descrip_area', 'asc')
            ->get();

        $categorias = DB::table('categoria')
            ->get();

        $materiales = DB::table('kardex_talla')
            ->join('material', 'kardex_talla.cod_material', '=', 'material.cod_material')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->where('kardex_talla.RUC_empresa', '=', $empresa)
            ->get();


        return view("logistica.kardex.salida_talla", ["fechaActual" => $now, "areas" => $area, "categorias" => $categorias, "materiales" => $materiales]);

    }

    public function store(Request $request)
    {
        // 0=normal  1=talla
        $tipoSalida = input::get('tipoSalida');
        if ($tipoSalida == 0) {
            $codigo_materiales = Input::get('codigo_material');
            $empresa = Auth::user()->RUC_empresa;

            $costo_con_igv = Input::get('costo_con_igv');
            $costo_sin_igv = Input::get('costo_sin_igv');
            $cantidad_salida = Input::get('cantidad_salida');
            $fecha = Input::get('fecha');
            $contador = 0;
            foreach ($codigo_materiales as $codigo_material) {

                $karde_cab = new kardex_cab;
                $karde_cab->movimiento = 2;
                $karde_cab->cod_material = $codigo_material;
                $karde_cab->cantidad = $cantidad_salida[$contador];
                $karde_cab->costo_total_sin_igv = $cantidad_salida[$contador] * $costo_sin_igv[$contador];
                $karde_cab->costo_total_con_igv = $cantidad_salida[$contador] * $costo_con_igv[$contador];
                $karde_cab->$fecha;
                $karde_cab->igv = ($cantidad_salida[$contador] * $costo_con_igv[$contador]) - ($cantidad_salida[$contador] * $costo_sin_igv[$contador]);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();

                $kardexs = DB::table('kardex')
                    ->where('kardex.cod_material', '=', $codigo_material)
                    ->get();

                $kardex = kardex::where('cod_material', $codigo_material)
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual - $cantidad_salida[$contador],
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv - ($cantidad_salida[$contador] * $costo_sin_igv[$contador]),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv - ($cantidad_salida[$contador] * $costo_con_igv[$contador]),
                        'igv' => $kardexs[0]->igv - (($cantidad_salida[$contador] * $costo_con_igv[$contador]) - ($cantidad_salida[$contador] * $costo_sin_igv[$contador]))
                    ]);
                $contador++;
            }
            return Redirect::to('logistica/kardex');
        } else {
            $empresa = Auth::user()->RUC_empresa;
            $fecha = date("Y-m-d H:i:s");
            $now = Carbon::now();
            $codigo_materiales = Input::get('codigo_material');
            $costo_con_igv = Input::get('costo_con_igv');
            $costo_sin_igv = Input::get('costo_sin_igv');
            $salida_total = Input::get('cantidad_salida');

            $auxContador1 = 0;
            foreach ($codigo_materiales as $codigo_material) {
                $cod_kardex = $codigo_material . "-" . $now;
                $karde_cab = new kardex_cab_talla;
                $karde_cab->cod_kardex = $cod_kardex;
                $karde_cab->movimiento = 2;
                $karde_cab->cod_material = $codigo_material;
                $karde_cab->cantidad = $salida_total[$auxContador1];
                $karde_cab->costo_total_sin_igv = $salida_total[$auxContador1] * $costo_sin_igv[$auxContador1];
                $karde_cab->costo_total_con_igv = $salida_total[$auxContador1] * $costo_con_igv[$auxContador1];
                $karde_cab->fecha = $fecha;
                $karde_cab->igv = ($salida_total[$auxContador1] * $costo_con_igv[$auxContador1]) - ($salida_total[$auxContador1] * $costo_sin_igv[$auxContador1]);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();

                $kardexs = DB::table('kardex_talla')
                    ->where('kardex_talla.cod_material', '=', $codigo_material)
                    ->get();

                $kardex_talla = kardex_talla::where('cod_material', $codigo_material)
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual - $salida_total[$auxContador1],
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv - ($salida_total[$auxContador1] * $costo_sin_igv[$auxContador1]),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv - ($salida_total[$auxContador1] * $costo_con_igv[$auxContador1]),
                        'igv' => $kardexs[0]->igv - ($salida_total[$auxContador1] * $costo_con_igv[$auxContador1]) - ($salida_total[$auxContador1] * $costo_sin_igv[$auxContador1])
                    ]);

                $cantidadTallas = Input::get('salidatallas-' . $codigo_material);
                $salida_talla = Input::get('salida_talla-' . $codigo_material);
                $auxContador2 = 0;
                //  dd( $cantidadTallas);
                foreach ($cantidadTallas as $cantidadTalla) {

                    if ($cantidadTalla != null) {
                        $talla_tallas = new kardex_cab_talla_tallas;
                        $talla_tallas->talla = $salida_talla[$auxContador2];
                        $talla_tallas->cantidad = $cantidadTalla;
                        $talla_tallas->cod_kardex = $cod_kardex;
                        $talla_tallas->save();

                        $kardexs_tallas = DB::table('kardex_talla_tallas')
                            ->join('kardex_talla', 'kardex_talla_tallas.cod_kardex', '=', 'kardex_talla.id_kardex')
                            ->where('cod_material', '=', $codigo_material)
                            ->where('talla', '=', $salida_talla[$auxContador2])
                            ->get();

                        $kardex_talla_tallas = kardex_talla_tallas::where('cod_kardex', $kardexs_tallas[0]->cod_kardex)
                            ->where('talla', '=', $salida_talla[$auxContador2])
                            ->update([
                                'cantidad' => $kardexs_tallas[0]->cantidad - $cantidadTalla
                            ]);

                    }
                    $auxContador2++;
                }
                $auxContador1++;
            }
            return Redirect::to('logistica/kardex/talla');
        }
    }

    public function edit($id)
    {
        return Redirect::to('logistica/ingreso_salida.update');
    }



    public function destroy($id)
    {
        return Redirect::to('logistica/ingreso_salida');
    }

    public function recoger()
    {
        return view('logistica.ingreso_salida.recoger');
    }

    public function obtenerSubcategoria($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $subcategoria = DB::table('subcategoria')
            ->where('cod_categoria', '=', $dato)
            ->where('estado_subcategoria', '=', '1')
            ->orderBy('nom_subcategoria','asc')
            ->get();
        return $subcategoria;
    }

    public function obtenerMaterial($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('kardex')
            ->join('material', 'kardex.cod_material', '=', 'material.cod_material')
            ->where('kardex.RUC_empresa', '=', $empresa)
            ->where('material.cod_subcategoria', '=', $dato)
            ->where('material.estado_material', '=', 1)
            //SERIE TALLA
            ->where('material.cod_serie', '=', null)
            //->orderBy('categoria.nom_categoria','asc')
            ->get();
        return $materiales;
    }

    public function obtenerMaterialTalla($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('kardex_talla')
            ->join('material', 'kardex_talla.cod_material', '=', 'material.cod_material')
            ->where('kardex_talla.RUC_empresa', '=', $empresa)
            ->where('material.cod_subcategoria', '=', $dato)
            ->where('material.estado_material', '=', 1)
            //SERIE TALLA
            ->orderBy('material.descrip_material','asc')
            ->get();
        return $materiales;
    }

    public function obtenerTallas($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $tallas = DB::table('kardex_talla_tallas')
            ->where('cod_kardex', '=', $dato)
            ->get();
        return $tallas;
    }
}
