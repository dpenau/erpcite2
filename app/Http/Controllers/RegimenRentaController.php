<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\RegimenRentaModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class RegimenRentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $rentas=DB::table('regimen_renta')->get();
            return view('Mantenimiento.Regimen_Renta.index',["rentas"=>$rentas]);
        }
    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento.Regimen_Renta.create");
        }
    }
    public function store()
    {
        $identificador=rand(10000,99999);
        $renta=new RegimenRentaModel;
        $renta->cod_regimen_renta=$identificador;
        $renta->descrip_regimen_renta=Input::get('descripcion');
        $renta->estado_regimen_renta=1;
        $renta->save();
        session()->flash('success','Régimen Renta Registrado');
        return Redirect::to('Mantenimiento/Regimen_Renta');
    }
    public function show()
    {
        return view('Mantenimiento.Regimen_Renta.index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Regimen_Renta');
    }
    public function update()
    {
      $cod=Input::get('cod_regimenr_editar');
      $descrip=Input::get('descripcion');
      $act=RegimenRentaModel::where('cod_regimen_renta',$cod)
      ->update(['descrip_regimen_renta'=>$descrip]);
      session()->flash('success','Régimen Renta Actualizado');
        return Redirect::to('Mantenimiento/Regimen_Renta');
    }
    public function destroy()
    {
      $cod=Input::get('cod_regimenr_eliminar');
      $accion=Input::get('accion');
      if($accion==0)
      {
        $mensaje="Desactivado";
      }
      else {
        $mensaje="Activado";
      }
      $act=RegimenRentaModel::where('cod_regimen_renta',$cod)
      ->update(['estado_regimen_renta'=>$accion]);
      session()->flash('success','Régimen Renta '.$mensaje);
        return Redirect::to('Mantenimiento/Regimen_Renta');
    }
}
