<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\ConfiguracionValoresModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class ConfiguracionGeneralController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
  public function index(Request $request)
  {
    if ($request) {
      $areas=DB::table('configuracion_valores')
      ->orderBy('configuracion_valores.nombre', 'asc')
      ->get();
      return view('Mantenimiento.configuracion_general.index',["areas"=>$areas]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {

      return view("Mantenimiento.configuracion_general.create");
    }

  }
  public function store()
  {
    //Se Registra el campo detalle_orden_compra
    $area=new ConfiguracionValoresModel;
    $area->nombre=Input::get('nombre');
    $area->valor=Input::get('valor');
    $area->save();
    session()->flash('success','Area registrada');
    return Redirect::to('Mantenimiento/configuracion');
  }
  public function show()
  {
    return view('Mantenimiento.area.index',["clasificacion"=>$clasi]);
  }
  public function edit($id)
  {
    return Redirect::to('Mantenimiento/configuracion');
  }
  public function update()
  {
    $cod=Input::get('codigo_valor');
    $nombre=Input::get('nombre');
    $valor=Input::get('valor');
    $act=ConfiguracionValoresModel::where('codigo_valor',$cod)
    ->update(['nombre'=>$nombre,'valor'=>$valor]);
    session()->flash('success','Valor Actualizado Satisfactoriamente');
    return Redirect::to('Mantenimiento/configuracion');
  }
  public function destroy()
  {
  }
}
