<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\ManoObraModel;
use erpCite\Empresa;
use erpCite\OtrosGastosVentas;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class OtrosgastosdeVtasController extends Controller
{
  public function __construct()
{
  $this->middleware('jefe');
}
public function index(Request $request)
{

if ($request) {
  $mes_actual=date("m");
    $sueldo=DB::Table('gasto_sueldos')
    ->where('cod_area','=','53586')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('gasto_sueldos.es_externo','=',0)
    ->whereMonth('fecha_creacion', '=', $mes_actual)
    ->get();
    $otro =DB::table('gasto_otros')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->whereMonth('fecha_creacion', '=', $mes_actual)
    ->get();
        return view('costos.indirectos.OtrosgastosdeVtas.index',["sueldo"=>$sueldo,"otro"=>$otro]);

  }

}

public function crear_sueldo()
{
  $area=DB::table('area')
  ->orderBy('descrip_area','asc')->get();
  $general=DB::Table('datos_generales')
  ->where('RUC_empresa',Auth::user()->RUC_empresa)
  ->get();

  $empresas=DB::table('empresa')
  ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
  ->select('cod_regimen_laboral')
  ->get();
  $beneficios=DB::table('escala_salarial')
  ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
  ->where('cod_regimen_laboral','=',$empresas[0]->cod_regimen_laboral)
  ->avg('tasa_beneficio');
    return view("costos.indirectos.OtrosgastosdeVtas.crear_sueldo",["area"=>$area,'beneficios'=>$beneficios]);
}
public function almacenar_sueldo()
{
  //Se Registra el campo detalle_orden_compra
  $identificador=rand(100000,999999);
  $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
  $siglax = $sigla[0]->siglas;
  $res=$siglax.'-'.$identificador;
  $idempresa=Auth::user()->RUC_empresa;
  $manoobra=new ManoObraModel;
  $manoobra->id_gastos_sueldos=$res;
  $manoobra->beneficios=Input::get('beneficios_sociales');
  $manoobra->otros=Input::get('otros_sueldos');
  $manoobra->gasto_mensual=Input::get('gasto_mensual');
  $manoobra->cod_area=Input::get('cod_area');
  $manoobra->RUC_empresa=$idempresa;
  $manoobra->sueldo_mensual=Input::get('sueldo_mensual');
  $manoobra->puesto=Input::get('puesto');
  $manoobra->fecha_creacion=Input::get('fecha_creacion');
  $manoobra->estado=1;
  $manoobra->save();
  session()->flash('success','Sueldo de marketing registrado satisfactoriamente');
  return Redirect::to('costos_indirectos/OtrosgastosdeVtas');
}
public function crear_otro()
{
  return view("costos.indirectos.OtrosgastosdeVtas.crear_otro");
}
public function almacenar_otro()
{
  $identificador=rand(100000,999999);
  $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
  $siglax = $sigla[0]->siglas;
  $res=$siglax.'-'.$identificador;
  $idempresa=Auth::user()->RUC_empresa;
  $articulo=new OtrosGastosVentas;
$articulo->id_otros=$res;
  $articulo->descripcion=Input::get('descripcion');
  $articulo->gasto=Input::get('gasto');
  $articulo->estado=1;
  $articulo->RUC_empresa=$idempresa;
  $articulo->fecha_creacion=Input::get('fecha_creacion');
  $articulo->save();
  session()->flash('success','Gasto ingresado satisfactoriamente');
  return Redirect::to('costos_indirectos/OtrosgastosdeVtas');
}
  public function eliminar_sueldo()
  {
    $email=Input::get('email');
    $act=ManoObraModel::where('id_gastos_sueldos',$email)
    ->delete();
    session()->flash('success','Sueldo Eliminado');
    return Redirect::to('costos_indirectos/OtrosgastosdeVtas');
  }
  public function eliminar_gasto()
  {
    $email=Input::get('email');
    $act=OtrosGastosVentas::where('id_otros',$email)
    ->delete();
    session()->flash('success','Gasto Eliminado');
    return Redirect::to('costos_indirectos/OtrosgastosdeVtas');
  }

}
