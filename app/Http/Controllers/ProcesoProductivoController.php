<?php

namespace erpCite\Http\Controllers;

use erpCite\ProcesoProductivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProcesoProductivoController extends Controller
{
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $hormas = ProcesoProductivo::create(
            [
                'proceso_id'=>$request->proceso_id,
                'RUC_empresa'=>$ruc_empresa,
                'numero_prototipo'=>$request->numero_prototipo,
                'descripcion_material'=>$request->descripcion_material,
                'costo_total'=>$request->costo_total,
                'importe'=>$request->importe,
                'estado_registro'=>'A',
            ]
            );

        return redirect('costos/indirectos/operacion');
    }
}
