<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
use erpCite\ClienteModel;
use erpCite\OrdenPedidoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PdfReporteClientesController extends Controller
{
    //index
    public function index(Request $request)
    {
    }
    //do pdf
    public function dopdf($var)
    {
            $checkers = explode("-",$var);

            $pdf = \App::make('dompdf.wrapper');
            //$pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($this->convert_data($checkers));
            //$pdf->render();
            return $pdf->stream();
            //return $pdf->download('Reporte_de_clientes.pdf');

    }

    function get_data()
    {

        $reporte_clientes = ClienteModel::select(
            'nombre as cliente',
            DB::raw('COUNT(*) as frecuencia_pedidos'),
            DB::raw('SUM(orden_pedido.total_pedido) as ingreso_total_pedidos'),
            DB::raw('SUM(orden_pedido.deuda) as deuda')
        )
            ->join('orden_pedido', 'cliente.codigo', '=', 'orden_pedido.codigo_cliente')
            ->where('cliente.RUC_empresa', Auth::user()->RUC_empresa)
            ->groupBy('codigo', 'nombre')
            ->get();
        return $reporte_clientes;
    }
    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
        return $imagen;
    }
    function convert_data($checkers)
    {
        $detalle = $this->get_data();
        $img = $this->get_imagen();
        $photo = "";
        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }
        $output = '<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
        if ($photo != "") {
            $output .= '
      <header>
      <div class="row">
        <div class="col-md-12">
          <img  src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      ';
        }
        $output .= '<h1>Lista de Clientes</h1>
    <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
    <tr>
      <th style="border-collapse: collapse; border: 1px solid black;">Cliente</th>';
        if ($checkers[0] == 'true') {
            $output .= '<th style="border-collapse: collapse; border: 1px solid black;">Ingreso total de pedidos</th>';
        }
        if ($checkers[1] == 'true') {
            $output .= '<th style="border-collapse: collapse; border: 1px solid black;">Frecuencia de pedidos</th>';
        }
        if ($checkers[2] == 'true') {
            $output .= '<th style="border-collapse: collapse; border: 1px solid black;">Deuda</th>';
        }
        $output .= '</tr>';

        foreach ($detalle as $dat) {
            $output .= '
      <tr>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->cliente . '</td>';
            if ($checkers[0] == 'true') {
                $output .= '<td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->ingreso_total_pedidos . '</td>';
            }
            if ($checkers[1] == 'true') {
                $output .= '<td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->frecuencia_pedidos . '</td>';
            }
            if ($checkers[2] == 'true') {
                $output .= '<td style="border-collapse: collapse; border: 1px solid black;">' . $dat->deuda . '</td>';
            }
            $output .= '</tr>';
        }
        $output .= '</table>
    </body></html>
    ';
        return $output;
    }
}
