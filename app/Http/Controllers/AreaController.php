<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\Area;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class AreaController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
  public function index(Request $request)
  {
    if ($request) {
      $areas=DB::table('area')
      ->orderBy('area.descrip_area', 'asc')
      ->get();
      return view('Mantenimiento.area.index',["areas"=>$areas]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {

      return view("Mantenimiento.area.create");
    }

  }
  public function store()
  {
    //Se Registra el campo detalle_orden_compra
    $identificador=rand(10000,99999);
    $area=new Area;
    $area->cod_area=$identificador;
    $area->descrip_area=Input::get('area');
    $area->estado_area=1;
    $area->save();
    session()->flash('success','Area registrada');
    return Redirect::to('Mantenimiento/area');
  }
  public function show()
  {
    return view('Mantenimiento.area.index',["clasificacion"=>$clasi]);
  }
  public function edit($id)
  {
    return Redirect::to('Mantenimiento/area');
  }
  public function update()
  {
    $cod=Input::get('cod_area_editar');
    $descrip_nueva=Input::get('descripcion');
    $act=Area::where('cod_area',$cod)
    ->update(['descrip_area'=>$descrip_nueva]);
    session()->flash('success','Area Actualizada Satisfactoriamente');
    return Redirect::to('Mantenimiento/area');
  }
  public function destroy()
  {
    $cod=Input::get('cod_area_eliminar');
    $accion=Input::get('accion');
    if($accion==0)
    {
      $mensaje="desactivado";
    }
    else {
      $mensaje="activado";
    }
    $act=Area::where('cod_area',$cod)
    ->update(['estado_area'=>$accion]);
    session()->flash('success','Area '.$mensaje);
    return Redirect::to('Mantenimiento/area');
  }
}
