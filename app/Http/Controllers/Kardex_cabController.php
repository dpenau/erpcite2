<?php

namespace erpCite\Http\Controllers;

use erpCite\kardex_cab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

class Kardex_cabController extends Controller
{
  //LISTADO KARDEX NORMAL
    public function index(Request $request)
    {
        if ($request) {
            $empresa=Auth::user()->RUC_empresa;
            $kardex=DB::table('kardex_material')
            ->join('material','kardex_material.cod_material','=','material.cod_material')
            ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
            ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
            ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
            ->where('kardex_material.RUC_empresa','=',$empresa)
            ->orderBy('material.descrip_material','asc')
            ->get();
            $materiales=DB::Table('kardex_material')
            ->join('material','kardex_material.cod_material','=','material.cod_material')
            ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
            ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
            ->where('kardex_material.RUC_empresa','=',$empresa)
            ->orderBy('material.descrip_material','asc')
            ->select('material.cod_material','material.descrip_material','kardex_material.cod_kardex_material')
            ->get();
            $minimo=$maximo=0;
            $mat_minimo=$mat_maximo=[];
            for ($i=0; $i < count($kardex); $i++) {
              if ($kardex[$i]->stock_total <= $kardex[$i]->stock_minimo) {
                $minimo++;
                $mat_minimo[$i]=$kardex[$i]->descrip_material;
              }
              else {
                if ($kardex[$i]->stock_total >= $kardex[$i]->stock_maximo) {
                  $maximo++;
                  $mat_maximo[$i]=$kardex[$i]->descrip_material;
                }
              }
            }
            $subcategoria=DB::Table('subcategoria')
            ->where('estado_subcategoria','=','1')
            ->orderBy('nom_subcategoria','asc')
            ->get();
            $categoria=DB::Table('categoria')
            ->where('estado_categoria','=','1')
            ->orderBy('nom_categoria','asc')
            ->get();
            $area=DB::Table('area')
            ->where('estado_area','=','1')
            ->orderBy('descrip_area','asc')
            ->get();
            $almacenes=DB::Table('almacen')
            ->where('RUC_empresa','=',$empresa)
            ->where('estado_almacen','=',1)
            ->orderBy('nom_almacen','asc')
            ->get();
            $almacen_normales=DB::table('almacen')
            ->where('estado_almacen','=',1)
            ->where('RUC_empresa','=',$empresa)
            ->get();

            //OFICIAL
            $kardex_total = DB::table('kardex')
            ->join('material','kardex.cod_material','=','material.cod_material')
            ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
            ->join('almacen','material.cod_almacen','=','almacen.cod_almacen')
            ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
            ->where('kardex.RUC_empresa','=',$empresa)
            ->get();


            //dd($kardex_total);
            //dd($kardex_total);
            $valores=DB::table('configuracion_valores')->get();
            return view('logistica.kardex.index',["categoria"=>$categoria,"kardex_total"=>$kardex_total, "mat_maximo"=>$mat_maximo,"mat_minimo"=>$mat_minimo,"minimo"=>$minimo,"maximo"=>$maximo,"almacen_normal"=>$almacen_normales,"almacenes"=>$almacenes,"kardex"=>$kardex,'area'=>$area,"materiales"=>$materiales,"subcategoria"=>$subcategoria,"valores"=>$valores]);
          }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \erpCite\kardex_cab  $kardex_cab
     * @return \Illuminate\Http\Response
     */
    // INDICE INVENTARIO POR TALLAS
    public function show(Request $request)
    {
      if ($request) {
        $empresa=Auth::user()->RUC_empresa;
        $kardex=DB::table('kardex_material')
        ->join('material','kardex_material.cod_material','=','material.cod_material')
        ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
        ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
        ->where('kardex_material.RUC_empresa','=',$empresa)
        ->orderBy('material.descrip_material','asc')
        ->get();
        $categoria=DB::Table('categoria')
        ->where('estado_categoria','=','1')
        ->orderBy('nom_categoria','asc')
        ->get();
        $materiales=DB::Table('kardex_material')
        ->join('material','kardex_material.cod_material','=','material.cod_material')
        ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
        ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->where('kardex_material.RUC_empresa','=',$empresa)
        ->orderBy('material.descrip_material','asc')
        ->select('material.cod_material','material.descrip_material','kardex_material.cod_kardex_material')
        ->get();

        $minimo=$maximo=0;
        $mat_minimo=$mat_maximo=[];
        for ($i=0; $i < count($kardex); $i++) {
          if ($kardex[$i]->stock_total <= $kardex[$i]->stock_minimo) {
            $minimo++;
            $mat_minimo[$i]=$kardex[$i]->descrip_material;
          }
          else {
            if ($kardex[$i]->stock_total >= $kardex[$i]->stock_maximo) {
              $maximo++;
              $mat_maximo[$i]=$kardex[$i]->descrip_material;
            }
          }
        }
        $subcategoria=DB::Table('subcategoria')
        ->where('estado_subcategoria','=','1')
        ->orderBy('nom_subcategoria','asc')
        ->get();
        $area=DB::Table('area')
        ->where('estado_area','=','1')
        ->orderBy('descrip_area','asc')
        ->get();
        $almacenes=DB::Table('almacen')
        ->where('RUC_empresa','=',$empresa)
        ->where('estado_almacen','=',1)
        ->orderBy('nom_almacen','asc')
        ->get();
        $almacen_normales=DB::table('almacen')
        ->where('estado_almacen','=',1)
        ->where('RUC_empresa','=',$empresa)
        ->get();
        $kardex_total = DB::table('kardex_talla')
        ->join('material','kardex_talla.cod_material','=','material.cod_material')
        ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->join('almacen','material.cod_almacen','=','almacen.cod_almacen')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        //->join('kardex_talla_tallas','kardex_talla.id_kardex','=','kardex_talla_tallas.cod_kardex')
        ->where('kardex_talla.RUC_empresa','=',$empresa)
        ->get();

        //dd($kardex_total);
        $kardex_total_talla = DB::table('kardex_talla_tallas')
        ->join('kardex_talla','kardex_talla_tallas.cod_kardex','=','kardex_talla.id_kardex')
        ->get();

        //dd($kardex_total);
        $valores=DB::table('configuracion_valores')->get();
        return view('logistica.kardex.index_talla',["categoria"=>$categoria,"kardex_total"=>$kardex_total,"kardex_total_talla"=>$kardex_total_talla,  "mat_maximo"=>$mat_maximo,"mat_minimo"=>$mat_minimo,"minimo"=>$minimo,"maximo"=>$maximo,"almacen_normal"=>$almacen_normales,"almacenes"=>$almacenes,"kardex"=>$kardex,'area'=>$area,"materiales"=>$materiales,"subcategoria"=>$subcategoria,"valores"=>$valores]);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \erpCite\kardex_cab  $kardex_cab
     * @return \Illuminate\Http\Response
     */
    public function edit(kardex_cab $kardex_cab)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \erpCite\kardex_cab  $kardex_cab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kardex_cab $kardex_cab)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \erpCite\kardex_cab  $kardex_cab
     * @return \Illuminate\Http\Response
     */
    public function destroy(kardex_cab $kardex_cab)
    {
        //
    }
}
