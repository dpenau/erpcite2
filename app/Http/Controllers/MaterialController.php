<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Material;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\CostoModelo;
use erpCite\MaterialSuministroModel;
use erpCite\Talla;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('logistica');
    }

    public function index(Request $request)
    {

        if ($request) {
            $materiales = DB::table('material As mat')
                ->join('unidad_medida as uni', 'mat.unidad_compra', '=', 'uni.cod_unidad_medida')
                ->join('subcategoria as sub', 'mat.cod_subcategoria', '=', 'sub.cod_subcategoria')
                ->join('categoria as ca', 'sub.cod_categoria', '=', 'ca.cod_categoria')
                ->where('RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->where('mat.estado_material', '=', 1)
                ->orderBy('ca.nom_categoria', 'asc')
                ->orderBy('mat.descrip_material', 'asc')
                ->get();
            $subcategorias = DB::table('subcategoria')
                ->where('subcategoria.estado_subcategoria', '=', 1)
                ->where('subcategoria.cod_categoria','!=',999)
                ->join('categoria', 'subcategoria.cod_categoria', '=', 'categoria.cod_categoria')
                ->orderBy('subcategoria.nom_subcategoria', 'asc')
                ->get();
            $unidad_medida = DB::table('unidad_medida')
                ->where('unidad_medida.estado_unidad_medida', '=', 1)
                ->orderBy('unidad_medida.descrip_unidad_medida', 'asc')->get();
            $serie = DB::table('serie')
                ->where('serie.estado_serie', '=', 1)
                ->orderBy('serie.nombre_serie', 'asc')->get();
            //dd($materiales);
            return view('logistica.articulo.index', ["materiales" => $materiales, "subcategorias" => $subcategorias, 'serie' => $serie, 'unidad_medida' => $unidad_medida]);

        }
    }

    public function create(Request $request)
    {
        if ($request) {
            $categoria = DB::table('categoria')->get();
            $subcategorias = DB::table('subcategoria')->where('subcategoria.estado_subcategoria', '=', 1)->orderBy('nom_subcategoria', 'asc')->get();
            $unidad_medida = DB::table('unidad_medida')
                ->where('unidad_medida.estado_unidad_medida', '=', 1)
                ->orderBy('unidad_medida.descrip_unidad_medida', 'asc')
                ->get();
            $unidad_compra = DB::table('unidad_compra')
                ->where('unidad_compra.estado_unidad_medida', '=', 1)
                ->orderBy('unidad_compra.descrip_unidad_compra', 'asc')
                ->get();
            $serie = DB::table('serie')
                ->where('serie.RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->where('serie.estado_serie', '=', 1)
                ->orderBy('serie.nombre_serie', 'asc')->get();
            $almacen = DB::table('almacen')
                ->where('almacen.RUC_empresa', "=", Auth::user()->RUC_empresa)
                ->where('almacen.estado_almacen', '=', 1)
                ->orderBy('almacen.nom_almacen', 'asc')->get();
            return view("logistica.articulo.create", ["categoria" => $categoria, 'serie' => $serie, "subcategorias" => $subcategorias, "almacen" => $almacen, 'unidad_medida' => $unidad_medida]);
        }

    }

    public function store()
    {
        //Se Registra el campo detalle_orden_compra
        $sigla = DB::table('empresa')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();

        $siglax = $sigla[0]->siglas;
        $res = $siglax;
        $identificador = rand(100000, 999999);
        $t_compra = Input::get('t_compra');
        // dd($t_compra);
        $talla_inicial = Input::get('talla_inicial');
        // dd($talla_inicial);
        $talla_final = Input::get('talla_final');
        if ($t_compra == "1") {
            if ($talla_final != "" && $talla_final <= $talla_inicial) {
                session()->flash('error', 'Ingrese Correctamente las tallas');
            } else {
                if ($talla_final == "") {
                    $cantidad_de_plantas = 1;
                } else {
                    $cantidad_de_plantas = $talla_final - $talla_inicial;
                    $cantidad_de_plantas = $cantidad_de_plantas + 1;
                }
                $idempresa = Auth::user()->RUC_empresa;
                $descripcion = Input::get('descripcion_material');
                $costo_sin_igv_material = Input::get('costo_sin_igv');
                $costo_con_igv_material = Input::get('costo_con_igv');
                $unidad_medida = Input::get('unidad_compra');
                $serie = Input::get('serie');
                $almacen = Input::get('almacen');
                $cod_subcategoria = Input::get('subcategoria');
                $stock_maximo = Input::get('stock_maximo');
                $stock_minimo = Input::get('stock_minimo');
                $factor_equivalencia = null;
                $unidad_compra = Input::get('unidad_compra');
                $ubicacion = Input::get('location');

                //estado materianl
                //Ruc empresa

                $identificador = rand(1000000, 9999999);
                $articulo = new Material;
                $articulo->cod_material = $res . $identificador;
                $articulo->descrip_material = $descripcion;
                $articulo->costo_sin_igv_material = $costo_sin_igv_material;
                $articulo->costo_con_igv_material = $costo_con_igv_material;
                $articulo->unidad_medida = $unidad_medida;
                $articulo->cod_subcategoria = $cod_subcategoria;
                $articulo->stock_maximo = $stock_maximo;
                $articulo->cod_serie = $serie;
                $articulo->cod_almacen = $almacen;
                $articulo->ubicacion = $ubicacion;
                $articulo->stock_minimo = $stock_minimo;
                $articulo->unidad_compra = $unidad_compra;
                $articulo->estado_material = 1;
                $articulo->RUC_empresa = $idempresa;
                $articulo->t_compra = $t_compra;
                $articulo->save();

                for ($i = 0; $i < $cantidad_de_plantas; $i++) {
                    $identificadorF = rand(1000000, 9999999);
                    $tallas = new Talla;
                    $tallas->codigo_talla = $identificadorF;
                    $tallas->talla_num = $talla_inicial;
                    $tallas->cod_material = $res . $identificador;
                    $tallas->save();
                    $talla_inicial = $talla_inicial + 1;
                }
                session()->flash('success', 'Material ingresado satisfactoriamente entrando IF');
            }
        } else {
            $idempresa = Auth::user()->RUC_empresa;
            $articulo = new Material;
            $articulo->cod_material = $res . $identificador;
            $articulo->descrip_material = Input::get('descripcion_material');
            $articulo->costo_sin_igv_material = Input::get('costo_sin_igv');
            $articulo->costo_con_igv_material = Input::get('costo_con_igv');
            $articulo->unidad_medida = Input::get('unidad_compra');
            $articulo->cod_subcategoria = Input::get('subcategoria');
            $articulo->stock_maximo = Input::get('stock_maximo');
            $articulo->stock_minimo = Input::get('stock_minimo');
            $articulo->cod_almacen = Input::get('almacen');
            $articulo->ubicacion = Input::get('location');
            $articulo->unidad_compra = Input::get('unidad_compra');
            $articulo->estado_material = 1;
            $articulo->t_compra = 0;

            $articulo->RUC_empresa = $idempresa;
            $articulo->save();
            session()->flash('success', 'Material ingresado satisfactoriamente Else');
        }
        return Redirect::to('logistica/articulo');
    }

    public function edit($id)
    {
        return Redirect::to('logistica/clasificacion');
    }

    public function update()
    {
        $idempresa = Auth::user()->RUC_empresa;
        //talla
        $es_correccion = Input::get('correccion');
        $codigo = Input::get('cod_material');
        $descrip_material = Input::get('descripcion');
        $t_inicial = Input::get('talla_inicial');
        $t_final = Input::get('talla_final');
        $costo_sin_igv_material = Input::get('costo_sin_igv');
        $costo_con_igv_material = Input::get('costo_con_igv');
        $ubicacion = Input::get('ubicacion_material');
        $unidad_medida = Input::get('unidad_medida');
        $cod_subcategoria = Input::get('subcategoria');
        $stock_maximo = Input::get('stock_maximo');
        $stock_minimo = Input::get('stock_minimo');
        $unidad_compra = Input::get('unidad_compra');
        $cod_serie = Input::get('serie');

        //Normal
        $es_correccionN = Input::get('correccion_normal');
        $codigoN = Input::get('cod_material_normal');
        $descrip_materialN = Input::get('descripcion_normal');
        $t_inicialN = Input::get('talla_inicial_normal');
        $t_finalN = Input::get('talla_final_normal');
        $costo_sin_igv_materialN = Input::get('costo_sin_igv_normal');
        $costo_con_igv_materialN = Input::get('costo_con_igv_normal');
        $ubicacionN = Input::get('ubicacion_material_normal');
        $unidad_medidaN = Input::get('unidad_medida_normal');
        $cod_subcategoriaN = Input::get('subcategoria_normal');
        $stock_maximoN = Input::get('stock_maximo_normal');
        $stock_minimoN = Input::get('stock_minimo_normal');
        $unidad_compraN = Input::get('unidad_compra_normal');
        $cod_serieN = Input::get('serie_normal');

        $mensaje = "";
        if ($t_inicial != null) {
            $matTalla = Material::where('cod_material', '=', $codigo)
                ->update(['cod_material' => $codigo,
                    'descrip_material' => $descrip_material,
                    'cod_subcategoria' => $cod_subcategoria,
                    'stock_maximo' => $stock_maximo,
                    'stock_minimo' => $stock_minimo,
                    'unidad_medida' => $unidad_compra,
                    'unidad_compra' => $unidad_compra,
                    'costo_sin_igv_material' => $costo_sin_igv_material,
                    'costo_con_igv_material' => $costo_con_igv_material,
                    'ubicacion' => $ubicacion,
                    'cod_serie' => $cod_serie]);

            $tallaTabla = DB::table('talla')
                ->where('cod_material', $codigo)
                ->delete();

            $cantidad_de_plantas = $t_final - $t_inicial;
            $cantidad_de_plantas = $cantidad_de_plantas + 1;

            for ($i = 0; $i < $cantidad_de_plantas; $i++) {
                $tallas = new Talla;
                $tallas->talla_num = $t_inicial;
                $tallas->cod_material = $codigo;
                $tallas->save();
                $t_inicial = $t_inicial + 1;
            }

            session()->flash('success', 'Material por Tallas Actualizado satisfactoriamente');
        } else {
            $matNormal = Material::where('cod_material', '=', $codigoN)
                ->update(['cod_material' => $codigoN,
                    'descrip_material' => $descrip_materialN,
                    'cod_subcategoria' => $cod_subcategoriaN,
                    'stock_maximo' => $stock_maximoN,
                    'stock_minimo' => $stock_minimoN,
                    'unidad_medida' => $unidad_compraN,
                    'unidad_compra' => $unidad_compraN,
                    'costo_sin_igv_material' => $costo_sin_igv_materialN,
                    'costo_con_igv_material' => $costo_con_igv_materialN,
                    'ubicacion' => $ubicacionN,
                    'cod_serie' => null
                ]);


            session()->flash('success', 'Material Normal Actualizado satisfactoriamente');
        }

        session()->flash('warning', $mensaje);
        return Redirect::to('logistica/articulo');
    }

    public function destroy()
    {
        $email = Input::get('email');
        $estado = Input::get('accion');
        if ($estado == 0) {
            $mensaje = "Eliminado";
        } else {
            $mensaje = "Activado";
        }
        $act = Material::where('cod_material', $email)
            ->update(['estado_material' => $estado]);
        session()->flash('success', 'Material ' . $mensaje);
        return Redirect::to('logistica/articulo');
    }

    public function indicador_vista()
    {
        $mayor_ano = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereNotNull('detalle_kardex_material.fecha_salida')
            ->whereYear('detalle_kardex_material.fecha_salida', '=', date('Y'))
            ->select('detalle_kardex_material.cod_kardex_material', 'material.descrip_material', DB::raw('SUM(detalle_kardex_material.cantidad_salida) as cantidad_salida'), DB::raw('SUM(detalle_kardex_material.cantidad_salida*detalle_kardex_material.costo_material) as costo_material'))
            ->groupBy('material.descrip_material')
            ->orderby('cantidad_salida', 'desc')
            ->get();
        $ingresos = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereYear('detalle_kardex_material.fecha_ingreso', '=', date('Y'))
            ->select('detalle_kardex_material.fecha_ingreso', 'material.cod_material', 'material.descrip_material', 'detalle_kardex_material.stock', 'detalle_kardex_material.cantidad_ingresada', 'detalle_kardex_material.costo_material')
            ->orderBy('detalle_kardex_material.fecha_ingreso', 'asc')
            ->get();
        $salida = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereYear('detalle_kardex_material.fecha_salida', '=', date('Y'))
            ->select('detalle_kardex_material.fecha_salida', 'material.cod_material', 'material.descrip_material', 'detalle_kardex_material.stock', 'detalle_kardex_material.cantidad_salida', 'detalle_kardex_material.costo_material')
            ->orderBy('detalle_kardex_material.fecha_salida', 'asc')
            ->get();
        $mayor_mes = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereNotNull('detalle_kardex_material.fecha_salida')
            ->whereYear('detalle_kardex_material.fecha_salida', '=', date('Y'))
            ->whereMonth('detalle_kardex_material.fecha_salida', '=', date('m'))
            ->select('detalle_kardex_material.cod_kardex_material', 'material.descrip_material', DB::raw('SUM(detalle_kardex_material.cantidad_salida) as cantidad_salida'))
            ->groupBy('material.descrip_material')
            ->orderby('cantidad_salida', 'desc')
            ->get();
        $menor_ano = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereNotNull('detalle_kardex_material.fecha_salida')
            ->whereYear('detalle_kardex_material.fecha_salida', '=', date('Y'))
            ->select('detalle_kardex_material.cod_kardex_material', 'material.descrip_material', DB::raw('SUM(detalle_kardex_material.cantidad_salida) as cantidad_salida'))
            ->groupBy('material.descrip_material')
            ->orderby('cantidad_salida', 'asc')
            ->get();
        $menor_mes = DB::table('detalle_kardex_material')
            ->join('material', 'detalle_kardex_material.cod_kardex_material', '=', 'material.cod_material')
            ->where('material.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->whereNotNull('detalle_kardex_material.fecha_salida')
            ->whereYear('detalle_kardex_material.fecha_salida', '=', date('Y'))
            ->whereMonth('detalle_kardex_material.fecha_salida', '=', date('m'))
            ->select('detalle_kardex_material.cod_kardex_material', 'material.descrip_material', DB::raw('SUM(detalle_kardex_material.cantidad_salida) as cantidad_salida'))
            ->groupBy('material.descrip_material')
            ->orderby('cantidad_salida', 'asc')
            ->get();
        $cadena = [$mayor_ano, $menor_ano, $mayor_mes, $menor_mes, $ingresos, $salida];
        return view('logistica.kardex.indicador', ['datos' => $cadena]);

    }

    public function eliminar()
    {
        $ruc = Auth::user()->RUC_empresa;
        $codigo = Input::get('codigo');
        $nombre = Input::get('nombre');
        $nombre_desglosado = explode('-', $nombre);
        $flag = Input::get('todo');
        if ($flag == "t") {
            $toda_talla = DB::table('material')
                ->select('cod_material', 'descrip_material')
                ->where('descrip_material', 'LIKE', $nombre_desglosado[0] . "-%")
                ->where('RUC_empresa', '=', $ruc)
                ->get();
            for ($i = 0; $i < count($toda_talla); $i++) {
                $tienda = DB::table('kardex_tienda')
                    ->select('cod_kardex_tienda', 'codigo_material')
                    ->where('codigo_material', '=', $toda_talla[$i]->cod_material)
                    ->where('RUC_empresa', '=', $ruc)
                    ->get();
                $material = DB::Table('kardex_material')
                    ->select('cod_kardex_material')
                    ->where('cod_material', '=', $toda_talla[$i]->cod_material)
                    ->where('RUC_empresa', '=', $ruc)
                    ->get();
                $detalle = DB::Table('detalle_orden_compra')
                    ->select('id_detalle_oc')
                    ->where('cod_material', '=', $toda_talla[$i]->cod_material)
                    ->get();
                if (count($tienda) > 0) {
                    for ($j = 0; $j < count($tienda); $j++) {
                        $eliminar_detalle = DB::table('detalle_kardex_tienda')
                            ->where('cod_kardex_tienda', '=', $tienda[$j]->cod_kardex_tienda)
                            ->delete();
                        $eliminar_tienda = DB::table('kardex_tienda')
                            ->where('cod_kardex_tienda', '=', $tienda[$j]->cod_kardex_tienda)
                            ->delete();
                    }
                }
                if (count($material) > 0) {
                    $eliminar_detalle = DB::table('detalle_kardex_material')
                        ->where('cod_kardex_material', '=', $material[0]->cod_kardex_material)
                        ->delete();
                    $eliminar_tienda = DB::table('kardex_material')
                        ->where('cod_kardex_material', '=', $material[0]->cod_kardex_material)
                        ->delete();
                }
                if (count($detalle) > 0) {
                    for ($j = 0; $j < count($detalle); $j++) {
                        $eliminar_detalle = DB::table('detalle_orden_compra')
                            ->where('id_detalle_oc', '=', $detalle[$j]->id_detalle_oc)
                            ->delete();
                    }
                }
                $eliminar = DB::table('material')
                    ->where('cod_material', '=', $toda_talla[$i]->cod_material)
                    ->delete();
            }
        } else {
            $tienda = DB::table('kardex_tienda')
                ->select('cod_kardex_tienda', 'codigo_material')
                ->where('codigo_material', '=', $codigo)
                ->where('RUC_empresa', '=', $ruc)
                ->get();
            $material = DB::Table('kardex_material')
                ->select('cod_kardex_material')
                ->where('cod_material', '=', $codigo)
                ->where('RUC_empresa', '=', $ruc)
                ->get();
            $detalle = DB::Table('detalle_orden_compra')
                ->select('id_detalle_oc')
                ->where('cod_material', '=', $codigo)
                ->get();
            if (count($tienda) > 0) {
                for ($j = 0; $j < count($tienda); $j++) {
                    $eliminar_detalle = DB::table('detalle_kardex_tienda')
                        ->where('cod_kardex_tienda', '=', $tienda[$j]->cod_kardex_tienda)
                        ->delete();
                    $eliminar_tienda = DB::table('kardex_tienda')
                        ->where('cod_kardex_tienda', '=', $tienda[$j]->cod_kardex_tienda)
                        ->delete();
                }
            }
            if (count($material) > 0) {
                $eliminar_detalle = DB::table('detalle_kardex_material')
                    ->where('cod_kardex_material', '=', $material[0]->cod_kardex_material)
                    ->delete();
                $eliminar_tienda = DB::table('kardex_material')
                    ->where('cod_kardex_material', '=', $material[0]->cod_kardex_material)
                    ->delete();
            }
            if (count($detalle) > 0) {
                for ($j = 0; $j < count($detalle); $j++) {
                    $eliminar_detalle = DB::table('detalle_orden_compra')
                        ->where('id_detalle_oc', '=', $detalle[$j]->id_detalle_oc)
                        ->delete();
                }
            }
            $eliminar = DB::table('material')
                ->where('cod_material', '=', $codigo)
                ->delete();
        }
        session()->flash('success', 'Material Removido');
        return Redirect::to('logistica/articulo');
    }
}
