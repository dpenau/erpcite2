<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Modelo;
use erpCite\SerieModeloModel;
use erpCite\OrdenPedidoModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ReporteProductosController extends Controller
{
  public function __construct()
{
  $this->middleware('desarrollo');
}
public function index(Request $request)
{
if ($request) {
$LineasModelos=DB::table('modelo')
->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
->join('linea','modelo.cod_linea','=','linea.cod_linea')
->join('material','modelo.cod_horma','=','material.cod_material')
->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
->where(function($query){
  $query->orwhere('modelo.estado_modelo','=','0')
  ->orWhere('modelo.estado_modelo','=','1');
})
->get();
$seriemodelo=DB::Table('serie_modelo')
->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
->where('serie_modelo.estado','=','1')
->get();

$cantidades=DB::Table('detalle_orden_pedido')
->join('serie_modelo','detalle_orden_pedido.codigo_serie_articulo','=','serie_modelo.codigo')
->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
->select('detalle_orden_pedido.codigo_serie_articulo','detalle_orden_pedido.cantidades','coleccion.codigo_coleccion','coleccion.nombre_coleccion','serie_modelo.codigo_modelo')
->get();

$todaserie=DB::table('serie')
->where('RUC_empresa',Auth::user()->RUC_empresa)
->get();
$capellada=DB::table('capellada')
->get();
$Lineas=DB::table('linea')
->where('RUC_empresa',Auth::user()->RUC_empresa)
->where('estado_linea','=',1)
->get();
$coleccion=DB::Table('coleccion')
->where('RUC_empresa',Auth::user()->RUC_empresa)
->where('estado_coleccion','=',1)
->get();
$horma=DB::table('material')
->where('RUC_empresa',Auth::user()->RUC_empresa)
->where('cod_subcategoria','=','969')
->where('t_compra','=','1')
->orderBy('descrip_material','asc')
->get();
    return view('Ventas.Reportes.ReporteProductos.index',["horma"=>$horma,"coleccion"=>$coleccion,"Lineas"=>$Lineas,"LineasModelos"=>$LineasModelos,"seriemodelo"=>$seriemodelo,"todaserie"=>$todaserie,"capellada"=>$capellada,"cantidades"=>$cantidades]);
  }
//return view('Produccion.modelo.index');
}
public function create()
{

}
public function store(Request $data)
{

}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit()
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update()
  {
  }
public function destroy()
{

}
}
