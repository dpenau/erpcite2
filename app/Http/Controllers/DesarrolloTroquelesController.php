<?php

namespace erpCite\Http\Controllers;

use erpCite\DesarrolloTroqueles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesarrolloTroquelesController extends Controller
{
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $hormas = DesarrolloTroqueles::create(
            [
                'proceso_id'=>$request->proceso_id,
                'RUC_empresa'=>$ruc_empresa,
                'descripcion_material'=>$request->descripcion_material,
                'cantidad'=>$request->cantidad,
                'costo_unitario'=>$request->costo_unitario,
                'importe'=>$request->importe,
                'estado_registro'=>'A',
            ]
            );

        return redirect('costos/indirectos/operacion');
    }
}
