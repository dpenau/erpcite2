<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RecuperarPassController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
  public function index(Request $request)
  {
    if ($request) {
      return view('Mantenimiento.usuarios.index');
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      return view('Mantenimiento.usuarios.index');
    }

  }
  public function store(Request $data)
  {
    $data->validate([
    'dni' => 'required',
    'password' => 'required|confirmed',
    ]);
    $pass=Input::get('password');
    $pass=Hash::make($pass);
    $dni=Input::get('dni');
    $usuario=User::where('id',$dni)
    ->update(['password'=>$pass]);
    session()->flash('success','Contraseña del usuario '.$dni.' Cambiada');
    return Redirect::to('Admin');
  }
  public function show()
  {
    return view('Usuario.index');
  }
  public function edit()
  {
    return Redirect::to('Usuario');
  }
  public function update()
  {

    return Redirect::to('Usuario');
  }
  public function destroy()
  {

    return Redirect::to('Usuario');
  }
}
