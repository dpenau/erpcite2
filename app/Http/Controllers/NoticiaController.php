<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\NoticiaModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class NoticiaController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
  public function index()
  {
    $noticias=DB::table('noticias')
    ->join('categoria_noticias','noticias.categoria_difusion','=','categoria_noticias.cod_categoria')
    ->select('noticias.cod','noticias.url_imagen','noticias.nombre_evento','noticias.descripcion','categoria_noticias.nombre_categoria','categoria_noticias.cod_categoria')
    ->get();
    $categoria=DB::table('categoria_noticias')
    ->get();
    return view('Mantenimiento.noticias.index',["noticias"=>$noticias,"categoria"=>$categoria]);
  }
  public function create()
  {
    $categoria_noticia=DB::table('categoria_noticias')
    ->get();
    return view('Mantenimiento.noticias.create',["categorias"=>$categoria_noticia]);
  }
  public function store(Request $data)
  {
    $codigo=Input::get('cod_evento');
    $photo="";
    $destination='photo/noticias/';
    $file= $data->photo;
    $extension=$file->getClientOriginalExtension();
    $filename=$codigo.".".$extension;
    $file->move($destination,$filename);
    $photo=$filename;
    $noticia=new NoticiaModel;
    $noticia->cod=$codigo;
    $noticia->nombre_evento=Input::get('nombre_evento');
    $noticia->descripcion=Input::get('descrip_evento');
    $noticia->url_imagen=$photo;
    $noticia->categoria_difusion=Input::get('categoria');
    $noticia->save();
    session()->flash('success','Noticia creada');
    return Redirect::to('Mantenimiento/noticias');
  }
  public function update(Request $data)
  {
    $cod_evento=Input::get('cod_evento');
    $nombre=Input::get('nombre_evento');
    $descrip=Input::get('descripcion');
    $categoria=Input::get('subcategoria');
    $photo="";
    $file= $data->photo;
    if($file!="")
    {
      $destination='photo/noticias/';
      $file= $data->photo;
      $extension=$file->getClientOriginalExtension();
      $filename=$cod_evento.".".$extension;
      $file->move($destination,$filename);
      $photo=$filename;
    }
    if($photo=="")
    {
      $noticia=NoticiaModel::where('cod',$cod_evento)
      ->update(["nombre_evento"=>$nombre,
    "descripcion"=>$descrip,
    "categoria_difusion"=>$categoria]);
    }
    else {
      $noticia=NoticiaModel::where('cod',$cod_evento)
      ->update(["nombre_evento"=>$nombre,
    "Descripcion"=>$descrip,
  "categoria_difusion"=>$categoria,
"url_imagen"=>$photo]);
    }
    session()->flash('success','Noticia Actualizada');
    return Redirect::to('Mantenimiento/noticias');
  }
  public function delete()
  {
    $cod_evento=Input::get('email');
    $noticia=NoticiaModel::where('cod',$cod_evento)
    ->delete([]);
    session()->flash('success','Evento Eliminado');
    return Redirect::to('Mantenimiento/noticias');
  }
}
