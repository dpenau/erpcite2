<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\DesarrolloProductoModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class GastosController extends Controller
{
    public function __construct(){
      $this->middleware('jefe');
    }

    public function index(){
      $politicas=DB::Table('datos_generales')
      ->where('datos_generales.RUC_empresa',Auth::user()->RUC_empresa)
      ->select('datos_generales.politica_desarrollo_producto','datos_generales.politica_desarrollo_horma','datos_generales.politica_desarrollo_troqueles')
      ->get();
      $today=date("Y-m-d");
      $prototipo=DB::table('gasto_desarrollo_producto')
      ->join('costo_modelo','gasto_desarrollo_producto.descrip_gasto','=','costo_modelo.cod_costo_modelo')
      ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
      ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
      ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join('area','gasto_desarrollo_producto.cod_area','=','area.cod_area')
      ->join('tipo_desarrollo','gasto_desarrollo_producto.cod_tipo_desarrollo','=','tipo_desarrollo.cod_tipo_desarrollo')
      ->where('gasto_desarrollo_producto.estado','=','1')
      ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=','1')
      ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
      ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_producto." month)"),">=",$today)
      ->select(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_producto." month) AS vencimiento"),'costo_modelo.cod_costo_modelo','area.descrip_area','gasto_desarrollo_producto.tipo_gasto','modelo.cod_modelo','serie.nombre_serie'
      ,'gasto_desarrollo_producto.cantidad_mensual','gasto_desarrollo_producto.gasto_total','gasto_desarrollo_producto.importe','gasto_desarrollo_producto.cod_gasto_desarrollo_producto',
      'gasto_desarrollo_producto.fecha_compra')
      ->get();
      $horma=DB::table('gasto_desarrollo_producto')
      ->join('material','gasto_desarrollo_producto.descrip_gasto','=','material.cod_material')
      ->join('area','gasto_desarrollo_producto.cod_area','=','area.cod_area')
      ->join('tipo_desarrollo','gasto_desarrollo_producto.cod_tipo_desarrollo','=','tipo_desarrollo.cod_tipo_desarrollo')
      ->where('gasto_desarrollo_producto.estado','=','1')
      ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=','2')
      ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
      ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_horma." month)"),">=",$today)
      ->select(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_horma." month) AS vencimiento"),'area.descrip_area','gasto_desarrollo_producto.tipo_gasto','material.descrip_material','gasto_desarrollo_producto.cantidad_mensual','gasto_desarrollo_producto.gasto_total','gasto_desarrollo_producto.importe','gasto_desarrollo_producto.cod_gasto_desarrollo_producto',
      'gasto_desarrollo_producto.fecha_compra')
      ->get();
      $troquel=DB::table('gasto_desarrollo_producto')
      ->join('material','gasto_desarrollo_producto.descrip_gasto','=','material.cod_material')
      ->join('area','gasto_desarrollo_producto.cod_area','=','area.cod_area')
      ->join('tipo_desarrollo','gasto_desarrollo_producto.cod_tipo_desarrollo','=','tipo_desarrollo.cod_tipo_desarrollo')
      ->where('gasto_desarrollo_producto.estado','=','1')
      ->where('gasto_desarrollo_producto.cod_tipo_desarrollo','=','3')
      ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
      ->whereDate(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_troqueles." month)"),">=",$today)
      ->select(DB::raw("DATE_ADD(gasto_desarrollo_producto.fecha_compra, INTERVAL +".$politicas[0]->politica_desarrollo_troqueles." month) AS vencimiento"),'area.descrip_area','gasto_desarrollo_producto.tipo_gasto','material.descrip_material','gasto_desarrollo_producto.cantidad_mensual','gasto_desarrollo_producto.gasto_total','gasto_desarrollo_producto.importe','gasto_desarrollo_producto.cod_gasto_desarrollo_producto',
      'gasto_desarrollo_producto.fecha_compra')
      ->get();
      return view("costos/indirectos/gastos/index",['politica'=>$politicas,'prototipo'=>$prototipo,'horma'=>$horma,'troquel'=>$troquel]);
    }

    public function create(){
      $areas=DB::Table('area')
      ->where('descrip_area','=','Desarrollo de Producto')
      ->get();
      $desarrollo=DB::Table('tipo_desarrollo')
      ->where('estado','=','1')
      ->get();
      $empresa=DB::table('empresa')
      ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
      ->select('RUC_empresa')
      ->get();
        return view("costos/indirectos/gastos/create",['areas'=>$areas,'desarrollo'=>$desarrollo,'empresa'=>$empresa]);
    }
    public function tipo_desarrollo($var)
    {
      $tmp=$var;
      $data;
      switch($var)
      {
        case 1://obtener modelos costeados aprobados
          $data=DB::Table('costo_modelo')
          ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
          ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
          ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
          ->where('costo_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
          ->whereNotNull('costo_modelo.estado')
          ->where('modelo.estado_modelo','=','1')
          ->select('costo_modelo.cod_costo_modelo','modelo.cod_modelo','modelo.imagen','costo_modelo.total_materiales','serie.nombre_serie')
          ->orderby('modelo.cod_modelo','asc')
          ->get();
        break;
        case 2://obtener materiales hormas
          $data=DB::table('material')
          ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
          ->where('material.estado_material','!=','0')
          ->where('material.cod_subcategoria','=','969')
          ->where('t_compra','=','1')
          ->select('descrip_material','costo_sin_igv_material','cod_material')
          ->orderby('descrip_material','asc')
          ->get();
        break;
        case 3://obtener materiales troqueles
          $data=DB::table('material')
          ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
          ->where('material.estado_material','!=','0')
          ->where('material.cod_subcategoria','=','496')
          ->where('t_compra','=','1')
          ->select('descrip_material','costo_sin_igv_material','cod_material')
          ->orderby('descrip_material','asc')
          ->get();
        break;
        default:
      }
      return $data;
    }
    public function verificar_no_registrado($var)
    {
      $resultado=DB::table('gasto_desarrollo_producto')
      ->where('descrip_gasto','=',$var)
      ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
      ->select('cod_gasto_desarrollo_producto')
      ->get();
      $res=count($resultado);
      return $res;
    }
    public function store(){
      $empresa=Auth::user()->RUC_empresa;
      $identificador=rand(100000,999999);
      $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      $siglax = $sigla[0]->siglas;
      $res=$siglax.'-'.$identificador;
      $depreciacion=new DesarrolloProductoModel;
      $depreciacion->cod_area=Input::get('area');
      $depreciacion->descrip_gasto=Input::get('descrip_gasto');
      $depreciacion->cantidad_mensual=Input::get('mes');
      $depreciacion->gasto_total=Input::get('total');
      $depreciacion->importe=Input::get('importe');
      $depreciacion->tipo_gasto=Input::get('tipo_gasto');
      $depreciacion->cod_tipo_desarrollo=Input::get('tipo_desarrollo');
      $depreciacion->fecha_compra=Input::get('fecha_compra');
      $depreciacion->cod_gasto_desarrollo_producto=$res;
      $depreciacion->estado=1;
      $depreciacion->RUC_empresa=$empresa;
      $depreciacion->save();
      session()->flash('success','Gasto de Desarrollo de producto registrado');
      return Redirect::to('costo/indirectos/gastos');
    }
    public function update(){
      session()->flash('success','Gasto de Desarrollo de producto Actualizado');
      return Redirect::to('costo/indirectos/gastos');
    }
    public function destroy(){
      $email=Input::get('email');
      $act=DesarrolloProductoModel::where('cod_gasto_desarrollo_producto',$email)
      ->delete();
      session()->flash('success','Gasto de Desarrollo de producto Eliminado');
      return Redirect::to('costo/indirectos/gastos');
    }
}
