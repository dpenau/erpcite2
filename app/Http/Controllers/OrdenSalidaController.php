<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
class OrdenSalidaController extends Controller
{
    public function index()
    {
        $ordenes=DB::table('orden_salida')
        ->join('area','orden_salida.area_dirigida','=','area.cod_area')
        ->where('orden_salida.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->orderBy('orden_salida.fecha_creacion','desc')
        ->select('orden_salida.cod_orden_salida','orden_salida.fecha_creacion','area.descrip_area')
        ->get();
        return view('logistica.o_salida.index',['ordenes'=>$ordenes]);
    }
    public function obtener()
    {
        $ordenes=DB::table('orden_salida')
        ->join('area','orden_salida.area_dirigida','=','area.cod_area')
        ->where('orden_salida.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->orderBy('orden_salida.fecha_creacion','desc')
        ->select('orden_salida.cod_orden_salida','orden_salida.fecha_creacion','area.descrip_area')
        ->get();
        return $ordenes;
    }
}
