<?php

namespace erpCite\Http\Controllers;

use erpCite\DesarrolloMaterial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesarrolloMaterialController extends Controller
{
    public function create(Request $request)
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        //dd($ruc_empresa);
        $hormas = DesarrolloMaterial::create(
            [
                'proceso_id'=>$request->proceso_id,
                'RUC_empresa'=>$ruc_empresa,
                'numero_prototipo'=>$request->numero_prototipo,
                'descripcion_material'=>$request->descripcion_material,
                'costo_total'=>$request->costo_total,
                'importe'=>$request->importe,
                'estado_registro'=>'A',
            ]
            );

        return redirect('costos/indirectos/operacion');
    }
    public function update($idDesarrolloMaterial, Request $request)
    {
        $desarrollo_material =  DesarrolloMaterial::find($idDesarrolloMaterial);
        $desarrollo_material->fill([
            'proceso_id'=>$request->proceso_id,
            'numero_prototipo'=>$request->numero_prototipo,
            'descripcion_material'=>$request->descripcion_material,
            'costo_total'=>$request->costo_total,
            'importe'=>$request->importe,
            'estado_registro'=>'A',
        ])->save();
        return redirect('costos/indirectos/operacion');
    }
}
