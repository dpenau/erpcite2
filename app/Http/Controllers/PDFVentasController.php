<?php

namespace erpCite\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use PDF;
use \Milon\Barcode\DNS1D;



class PDFVentasController extends Controller
{

  public function index(Request $request){
    if ($request) {
      $query=trim($request->get('codigo'));
      $pdf=\App::make('dompdf.wrapper');
      $pdf->setPaper('a4','landscape');
      $pdf->loadHTML($this->convert_data($query));

      return $pdf->stream();
    }
  }

function get_data($query)
  {
    $orden_data = DB::table('cliente','orden_pedido')
     ->select('orden_pedido.codigo_pedido','cliente.nombre','orden_pedido.total_pedido','orden_pedido.fecha')
    ->join('orden_pedido','orden_pedido.codigo_cliente','=','cliente.codigo')
    ->get();
   
    return $orden_data;
  }

  function get_data2($query)
  {
     $SumaTotales = DB::table('cliente','orden_pedido')
    ->join('orden_pedido','orden_pedido.codigo_cliente','=','cliente.codigo')
    ->sum('orden_pedido.total_pedido');
   
    return $SumaTotales;
  }
  
  
 function get_imagen()
  {
    $idempresa=Auth::user()->RUC_empresa;
    $imagen=DB::table('empresa')->where('RUC_empresa','=',$idempresa)->limit(1)->get();
    return $imagen;
  }





 function convert_data($query)
  {
    $detalle=$this->get_data($query);
    $detalle2=$this->get_data2($query);
    $img=$this->get_imagen();
    $photo="";
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    if ($photo!="") {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img  src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      ';
    }
  	$output.='<h1>Reporte de Ventas </h1>
    <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
    <tr>
      <th style="border-collapse: collapse; border: 1px solid black;">Codigo de Pedido</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Nombre</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Fecha</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Ganancia</th>
      
    </tr>
  ';
    foreach ($detalle as $dat) {
      $output.='
      <tr>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->codigo_pedido.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->nombre.'</td>
         <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->fecha.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'.$dat->total_pedido.'</td>
      </tr>
      ';
    }
    $output.='
    <tr>
    <td style="border-collapse: collapse; border: 1px solid black;text-align: center;"></td>
    <td style="border-collapse: collapse; border: 1px solid black;text-align: center;"></td>
    <td style="border-collapse: collapse; border: 1px solid black;text-align: center;"> TOTAL DE GANANCIA:</td>
    <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">'. $detalle2.'</td>
    </tr>';
    $output.='</table>
    </body></html>
    ';
      return $output;

  }

  }