<?php

namespace erpCite\Http\Controllers;

use erpCite\Empresa;
use erpCite\OperacionDirectaModel;
use erpCite\PoliticaDesarrollo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class EmpresaController extends Controller
{
    public function show()
    {

        $empresa = DB::table('empresa')
        ->join('tipo_contribuyente','tipo_contribuyente.cod_tipo_contribuyente','=','empresa.cod_tipo_contribuyente')
        ->join('regimen_laboral','regimen_laboral.cod_regimen_laboral','=','empresa.cod_regimen_laboral')
        ->join('regimen_renta','regimen_renta.cod_regimen_renta','=','empresa.cod_regimen_renta')
        ->where('RUC_empresa',Auth::user()->RUC_empresa)
        ->first();
        if(!$empresa){
            $empresa = DB::table('empresa')
            ->join('tipo_contribuyente','tipo_contribuyente.cod_tipo_contribuyente','=','empresa.cod_tipo_contribuyente')
            ->join('regimen_laboral','regimen_laboral.cod_regimen_laboral','=','empresa.cod_regimen_laboral')
            ->join('regimen_renta','regimen_renta.cod_regimen_renta','=','empresa.cod_regimen_renta')
            ->where('RUC_empresa',Auth::user()->RUC_empresa)
            ->first();
        }
        $roles=DB::table('roles')->where('id','!=','1')->get();
        $tipo_contribuyente=DB::table('tipo_contribuyente')->get();
        $regimen_laboral=DB::table('regimen_laboral')->get();
        $regimen_renta=DB::table('regimen_renta')->get();
        $politica_desarrollo=DB::table(('politica_desarrollo'))->find($empresa->politica_desarrollo_id);

        //dd($empresa);
        return view('configuracion_inicial.empresa.index',['empresa'=>$empresa,
                                                            'roles'=>$roles,
                                                            'tipo_contribuyente'=>$tipo_contribuyente,
                                                            'regimen_laboral'=>$regimen_laboral,
                                                            'regimen_renta'=>$regimen_renta,
                                                            'politica_desarrollo'=>$politica_desarrollo]);
    }
    public function configuracion()
    {

        $empresa = DB::table('empresa')
        ->join('tipo_contribuyente','tipo_contribuyente.cod_tipo_contribuyente','=','empresa.cod_tipo_contribuyente')
        ->join('regimen_laboral','regimen_laboral.cod_regimen_laboral','=','empresa.cod_regimen_laboral')
        ->join('regimen_renta','regimen_renta.cod_regimen_renta','=','empresa.cod_regimen_renta')
        ->join('politica_desarrollo','id','=','politica_desarrollo_id')
        ->where('RUC_empresa',Auth::user()->RUC_empresa)
        ->first();
        if(!$empresa){
            $empresa = DB::table('empresa')
            ->join('tipo_contribuyente','tipo_contribuyente.cod_tipo_contribuyente','=','empresa.cod_tipo_contribuyente')
            ->join('regimen_laboral','regimen_laboral.cod_regimen_laboral','=','empresa.cod_regimen_laboral')
            ->join('regimen_renta','regimen_renta.cod_regimen_renta','=','empresa.cod_regimen_renta')
            ->where('RUC_empresa',Auth::user()->RUC_empresa)
            ->first();
        }
        $roles=DB::table('roles')->where('id','!=','1')->get();
        $tipo_contribuyente=DB::table('tipo_contribuyente')->get();
        $regimen_laboral=DB::table('regimen_laboral')->get();
        $regimen_renta=DB::table('regimen_renta')->get();
        $politica_desarrollo=DB::table(('politica_desarrollo'))->find($empresa->politica_desarrollo_id);

        return view('configuracion_inicial.empresa.modalconfiguracion',['empresa'=>$empresa,
                                                            'roles'=>$roles,
                                                            'tipo_contribuyente'=>$tipo_contribuyente,
                                                            'regimen_laboral'=>$regimen_laboral,
                                                            'regimen_renta'=>$regimen_renta,
                                                            'politica_desarrollo'=>$politica_desarrollo]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $empresa = Empresa::where('RUC_empresa', Auth::user()->RUC_empresa)->first();

        $name = Auth::user()->RUC_empresa;
        if( !file_exists('photo/'.$name)){
            mkdir('photo/'.$name);
        }

        if($empresa->politica_desarrollo_id == null) {
            $politica_desarrollo = PoliticaDesarrollo::insertGetId(
                [
                    'produccion_promedio' => $request->produccion_promedio,
                    'producto'=>$request->producto,
                    'hormas' => $request->hormas,
                    'troqueles' => $request->troqueles
                ]
            );

            $operaciones_destajo = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',1)->get();
            $operaciones_jornal = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',2)->get();
            $operaciones_planilla = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',3)->get();

            foreach ($operaciones_destajo as $operacion ) {
                $operacion->fill([
                    "costo_par" => $operacion['costo']/12 + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            foreach ($operaciones_jornal as $operacion ) {
                $operacion->fill([
                    "costo_par" => $operacion['costo']*4/$request->produccion_promedio + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            foreach ($operaciones_planilla as $operacion ) {
                $operacion->fill([
                    "costo_par" => ($operacion['costo']+$operacion['beneficio'])/$request->produccion_mensual + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            $destination = 'photo';
            $file = $request->foto;
            if ($file!="") {
                $extension = $file->getClientOriginalExtension();
                $filename = $name.".".$extension;

                $file->move($destination, $filename);
                $photo = $filename;
                $empresa->fill(
                    [
                        'correo'=>$request->correo_mod,
                        'telefono'=>$request->telefono_mod,
                        'pagina_web'=>$request->pagina_web,
                        'reparticion_utilidades'=>$request->repaticion_utilidades,
                        'imagen'=>$photo,
                        'cod_regimen_laboral'=> $request->regimen_laboral,
                        'cod_regimen_renta' => $request->regimen_renta,
                        'cod_tipo_contribuyente' => $request-> tipo_contribuyente,
                        'politica_desarrollo_id' => $politica_desarrollo
                    ]
                )->save();
            }else{
                $empresa->fill(
                    [
                        'correo'=>$request->correo_mod,
                        'telefono'=>$request->telefono_mod,
                        'pagina_web'=>$request->pagina_web,
                        'reparticion_utilidades'=>$request->repaticion_utilidades,
                        'cod_regimen_laboral'=> $request->regimen_laboral,
                        'cod_regimen_renta' => $request->regimen_renta,
                        'cod_tipo_contribuyente' => $request-> tipo_contribuyente,
                        'politica_desarrollo_id' => $politica_desarrollo
                    ]
                )->save();
            }
        } else {
            $politica_desarrollo = PoliticaDesarrollo::updateOrCreate(
                [
                    'id' => $empresa->politica_desarrollo_id
                ],
                [
                    'produccion_promedio' => $request->produccion_promedio,
                    'producto'=>$request->producto,
                    'hormas' => $request->hormas,
                    'troqueles' => $request->troqueles
                ]
            );

            $operaciones_destajo = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',1)->get();
            $operaciones_jornal = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',2)->get();
            $operaciones_planilla = OperacionDirectaModel::where('RUC_empresa',$empresa->RUC_empresa)->where('cod_tipo_pago',3)->get();

            foreach ($operaciones_destajo as $operacion ) {
                $operacion->fill([
                    "costo_par" => $operacion['costo']/12 + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            foreach ($operaciones_jornal as $operacion ) {
                $operacion->fill([
                    "costo_par" => $operacion['costo']*4/$request->produccion_promedio + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            foreach ($operaciones_planilla as $operacion ) {
                $operacion->fill([
                    "costo_par" => ($operacion['costo']+$operacion['beneficio'])/$request->produccion_promedio + $operacion['otros_costos']/$request->produccion_promedio
                ])->save();
            }

            $destination = 'photo';
            $file = $request->foto;
            if ($file!="") {
                $extension = $file->getClientOriginalExtension();
                $filename = $name.".".$extension;

                $file->move($destination, $filename);
                $photo = $filename;
                $empresa->fill(
                    [
                        'correo'=>$request->correo_mod,
                        'telefono'=>$request->telefono_mod,
                        'pagina_web'=>$request->pagina_web,
                        'reparticion_utilidades'=>$request->repaticion_utilidades,
                        'imagen'=>$photo,
                        'cod_regimen_laboral'=> $request->regimen_laboral,
                        'cod_regimen_renta' => $request->regimen_renta,
                        'cod_tipo_contribuyente' => $request-> tipo_contribuyente,
                        'politica_desarrollo_id' => $empresa->politica_desarrollo_id
                    ]
                )->save();
            }else{
                $empresa->fill(
                    [
                        'correo'=>$request->correo_mod,
                        'telefono'=>$request->telefono_mod,
                        'pagina_web'=>$request->pagina_web,
                        'reparticion_utilidades'=>$request->repaticion_utilidades,
                        'cod_regimen_laboral'=> $request->regimen_laboral,
                        'cod_regimen_renta' => $request->regimen_renta,
                        'cod_tipo_contribuyente' => $request-> tipo_contribuyente,
                        'politica_desarrollo_id' => $empresa->politica_desarrollo_id
                    ]
                )->save();
            }
        }
        DB::commit();
        return Redirect::to('configuracion/empresa');
    }
}
