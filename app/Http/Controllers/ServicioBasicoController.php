<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\GastoServiciosBasicosModel;
use erpCite\Http\Requests\GastosFormRequest;
use DB;
use erpCite\ServicioBasico;

class ServicioBasicoController extends Controller
{
    public function index()
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        $servicios_basicos = ServicioBasico::with('proceso')->where('RUC_empresa',$ruc_empresa)->where('estado_registro','A')->get();
        return response()->json(["servicios_basicos" => $servicios_basicos]);
    }
    public function create(Request $request)
    {
        //dd($request);
        $ruc_empresa = Auth::user()->RUC_empresa;
        $servicio_basico = ServicioBasico::create([
            'proceso_id' => $request->proceso_id,
            'RUC_empresa' => $ruc_empresa,
            'descripcion' => $request->descripcion,
            'costo_mensual' => $request->costo_mensual,
            'estado_registro' => 'A'
        ]);
        return redirect('costos/indirectos/operacion');
    }
    public function update($idServicioBasico, Request $request)
    {
        $servicio_basico = ServicioBasico::find($idServicioBasico);
        $servicio_basico->fill([
            'proceso_id' => $request->proceso_id,
            'descripcion' => $request->descripcion,
            'costo_mensual' => $request->costo_mensual,
            'estado_registro' => 'A'
        ])->save();
        return response()->json(["resp" => "Actualizado Correctamente"]);
    }
    public function delete($idServicioBasico)
    {
        $servicio_basico = ServicioBasico::find($idServicioBasico);
        $servicio_basico->fill([
            'estado_registro' => 'I',
        ])->save();
        return response()->json(["resp" => "Eliminado Correctamente"]);
    }
}
