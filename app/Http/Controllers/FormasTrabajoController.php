<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use erpCite\Trabajador;
use erpCite\GrupoDeTrabajo; 
use erpCite\GrupoyTrabajador;
use Illuminate\Http\Request;
use DB;

class FormasTrabajoController extends Controller
{
    public function __construct()
    {
        $this->middleware('jefe');
    }

    public function index(  )
    {
        $default_processes = array( 'Cortado', 'Habilitado','Aparado', 'Alistado','Montaje','Acabado' );
        return view('Planeacion.formas_trabajo.index',[ "default_processes"=>$default_processes ]);
    }

    public function updateStuffProcess( ){
        $trabajadores = Trabajador::select('DNI_trabajador', DB::raw('CONCAT(nombres, ", ", apellido_paterno) as nombre'),'puesto', 'especialidad', 'eficiencia')
            ->where([
            ['RUC_empresa', '=', Auth::user()->RUC_empresa],
            ['estado_trabajador', '=', 1 ]])
            ->get();
            
        return $trabajadores;
    }

    public function getArmedTeams(){
        $grupostrabajo = GrupoDeTrabajo::select('grupo_trabajo.codigo_grupo_trabajo', 'grupo_trabajo.proceso','grupo_trabajo.especialidad', 'grupo_trabajo.fecha_creacion')
        ->join( 'orden_pedido_produccion', 'grupo_trabajo.codigo_orden_pedido_produccion','=', 'orden_pedido_produccion.codigo_orden_pedido_produccion')
        ->where( 'orden_pedido_produccion.RUC_empresa','=', Auth::user()->RUC_empresa)
        ->get();

        $gruposyNroTrabajadores = GrupoyTrabajador::select('codigo_grupo_trabajo', DB::raw('COUNT(*) as num_integrantes' ))
        ->groupBy('codigo_grupo_trabajo')
        ->get();

        $resultados = array();
        foreach( $grupostrabajo as $unGrupo  ){
            $oneitem = array(
                "codigo_grupo_trabajo" => $unGrupo->codigo_grupo_trabajo,
                "proceso" => $unGrupo->proceso,
                "especialidad" => $unGrupo->especialidad,
                "num_integrantes" => $this->getWorkersAmount( $gruposyNroTrabajadores, $unGrupo->codigo_grupo_trabajo ),
                "fecha_creacion" => $unGrupo->fecha_creacion
            );
            array_push( $resultados, $oneitem );
        }

        return response()->json($resultados);
    }

    public function getWorkersOfGroup( $idgrupo ){
        $trabajadores = GrupoyTrabajador::select('codigo_grupo_trabajador')
        ->where( 'codigo_grupo_trabajo', '=', $idgrupo )
        ->get();   

        return $trabajadores;
    }

    private function getWorkersAmount( $gruposyNroTrabajadores ,  $cod_grupo ){
        foreach ( $gruposyNroTrabajadores as $groupynro  ){
            if( $groupynro->codigo_grupo_trabajo === $cod_grupo ){
                return $groupynro->num_integrantes;
            }
        }
        return 0;
    }
}
