<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\Trabajador;
use erpCite\Sueldos;
use erpCite\fecha_historial;
use erpCite\EscalaSalarialModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\DetalleCostoModeloMano;
use erpCite\CostoModelo;
use Illuminate\Support\Facades\DB;

class TrabajadorController extends Controller
{
  public function __construct()
  {
    $this->middleware('rhumanos');
  }
  public function costeo()
  {
    $empresa=Auth::user()->RUC_empresa;
    $actualizar=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')->select('cod_costo_modelo')->get();
    $actualizar_ind=Sueldos::where('RUC_empresa','=',$empresa)
    ->update(['estado'=>2]);
    $anterior="";
    for($i=0;$i<count($actualizar);$i++)
    {
      if($anterior!=$actualizar[$i]->cod_costo_modelo)
      {
        $act=DetalleCostoModeloMano::where('cod_costo_modelo',$anterior)
        ->update(['estado_trabajador'=>2]);
        $anterior=$actualizar[$i]->cod_costo_modelo;
        $modelo=DB::table('costo_modelo')
        ->where('cod_costo_modelo','=',$anterior)
        ->where('RUC_empresa','=',$empresa)
        ->select('estado')
        ->get();
        if(count($modelo)!=0)
        {
          if($modelo[0]->estado==9)
          {
            $act=CostoModelo::where('cod_costo_modelo',$anterior)
            ->update(['estado'=>"8"]);
          }
          else {
            if ($modelo[0]->estado==1 || $modelo[0]->estado==3) {
              $act=CostoModelo::where('cod_costo_modelo',$anterior)
              ->update(['estado'=>"7"]);
            }
          }
        }
      }
    }
  session()->flash('warning','Precio de costos mano de obra desactualizado');
  }

  public function index(Request $request)
  {
    if ($request) {
      $trabajador=DB::table('trabajador')
      ->join('area','trabajador.cod_area','=','area.cod_area')
      ->join('tipo_trabajador','trabajador.cod_tipo_trabajador','=','tipo_trabajador.cod_tipo_trabajador')
      ->join('distrito','trabajador.cod_distrito','=','distrito.cod_distrito')
      ->join('ciudad','distrito.cod_ciudad','=','ciudad.cod_ciudad')
      ->where('trabajador.RUC_empresa',Auth::user()->RUC_empresa)
      ->orderBy('trabajador.apellido_paterno','asc')
      ->get();
      $areas=DB::table('area')->orderBy('descrip_area','asc')
      ->where('estado_area','=',1)->get();
      $tipo_trabajador=DB::table('tipo_trabajador')->where('estado_tipo_trabajador','=','1')->get();
      $ciudad=DB::table('ciudad')->get();
      $distrito=DB::table('distrito')
      ->join('ciudad','distrito.cod_ciudad','=','ciudad.cod_ciudad')
      ->orderBy('ciudad.nomb_ciudad', 'asc')->get();
      return view('recursos_humanos.trabajador.index',["trabajador"=>$trabajador,"areas"=>$areas,'tipo_trabajador'=>$tipo_trabajador,'ciudad'=>$ciudad,'distrito'=>$distrito]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      $areas=DB::table('area')->orderBy('descrip_area','asc')
      ->where('estado_area','=',1)->get();
      $tipo_trabajador=DB::table('tipo_trabajador')->where('estado_tipo_trabajador','=','1')->get();
      $ciudad=DB::table('ciudad')->get();
      $distrito=DB::table('distrito')->orderBy('nomb_distrito', 'asc')->get();
      return view("recursos_humanos.trabajador.create",["areas"=>$areas,'tipo_trabajador'=>$tipo_trabajador,'ciudad'=>$ciudad,'distrito'=>$distrito]);
    }

  }
  public function store()
  {
    //Se Registra el campo detalle_orden_compra
    $empresa=Auth::user()->RUC_empresa;
    $fecha_ingreso=Input::get("f_ingreso");
    $date = date_create($fecha_ingreso);
    $primer_ingreso=date_format($date, 'Y-m-d H:i:s');
    $user=Auth::user()->email;
    $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
    $var=Input::get('dni');
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$var;
    $trabajador=new Trabajador;
    $trabajador->DNI_trabajador=$res;
    $trabajador->nombres=strtoupper(Input::get('nombre'));
    $trabajador->apellido_paterno=strtoupper(Input::get('apellidop'));
    $trabajador->apellido_materno=strtoupper(Input::get('apellidom'));
    $trabajador->direccion=strtoupper(Input::get('direccion'));
    $trabajador->telefono=strtoupper(Input::get('telefono'));
    $trabajador->cod_distrito=strtoupper(Input::get('distrito'));
    $trabajador->experiencia_laboral=strtoupper(Input::get('experiencia'));
    $trabajador->sexo=strtoupper(Input::get('sexo'));
    $trabajador->fecha_nacimiento=strtoupper(Input::get('fecha'));
    $trabajador->puesto=strtoupper(Input::get('puesto'));
    $trabajador->estado_trabajador=1;
    $trabajador->cod_tipo_trabajador=strtoupper(Input::get('tipo'));
    $trabajador->cod_area=strtoupper(Input::get('area'));
    $trabajador->g_instruccion=strtoupper(Input::get('grado_instruccion'));
    $trabajador->correo=Input::get('correo');
    $trabajador->usuario=$user;
    $trabajador->RUC_empresa=$empresa;
    $trabajador->save();
    $fecha=new fecha_historial;
    $fecha->cod_trabajador=$res;
    $fecha->fecha=$primer_ingreso;
    $fecha->descripcion="Primer Ingreso";
    $fecha->save();
    session()->flash('success','trabajador registrado');
    return Redirect::to('recursos_humanos/trabajador');
  }
  public function show()
  {
    return view('recursos_humanos.area.index');
  }
  public function edit($id)
  {

  }
  public function update()
  {
    $DNI_trabajador=Input::get('cod_dni_editar');
    $nombres=Input::get('nombre');
    $apellido_paterno=Input::get('apellidop');
    $apellido_materno=Input::get('apellidom');
    $direccion=Input::get('direccion');
    $telefono=Input::get('telefono');
    $cod_distrito=Input::get('distrito');
    $experiencia_laboral=Input::get('experiencia');
    $sexo=Input::get('sexo');
    $fecha_nacimiento=Input::get('fecha');
    $puesto=Input::get('puesto');
    $cod_tipo_trabajador=Input::get('tipo');
    $cod_area=Input::get('area');
    $ant_tipo_trabajador=Trabajador::where('DNI_trabajador',$DNI_trabajador)->select('cod_tipo_trabajador')->get();
    $act=Trabajador::where('DNI_trabajador',$DNI_trabajador)
    ->update(['nombres'=>$nombres,'apellido_paterno'=>$apellido_paterno,'apellido_materno'=>$apellido_materno,
    'telefono'=>$telefono,'direccion'=>$direccion,'cod_distrito'=>$cod_distrito,'sexo'=>$sexo,'fecha_nacimiento'=>$fecha_nacimiento,
  'puesto'=>$puesto,'experiencia_laboral'=>$experiencia_laboral,'cod_tipo_trabajador'=>$cod_tipo_trabajador,'cod_area'=>$cod_area]);
  if($cod_tipo_trabajador != 1)
  {
    $deletedRows = EscalaSalarialModel::where('cod_trabajador', $DNI_trabajador)
    ->update(['estado'=>0]);

  }
  else {
    $deletedRows = EscalaSalarialModel::where('cod_trabajador', $DNI_trabajador)
    ->update(['estado'=>1]);
  }
  if(($ant_tipo_trabajador[0]->cod_tipo_trabajador==1 && $cod_tipo_trabajador!=1)
  ||($ant_tipo_trabajador[0]->cod_tipo_trabajador!=1 && $cod_tipo_trabajador==1))
  {
    $deletedRows = EscalaSalarialModel::where('cod_trabajador', $DNI_trabajador)->get();
    if(count($deletedRows)!=0)
    {
      $this->costeo();
    }
  }
  session()->flash('success','trabajador Actualizado');
    return Redirect::to('recursos_humanos/trabajador');


  }
    public function destroy($id)
    {
    $DNI_trabajador=Input::get('cod_dni_eliminar');
    $accion=Input::get('accion');
    if($accion==0)
    {
      $mensaje="desactivado";
      $festado="Salida";
    }
    else {
      $mensaje="activado";
      $festado="Reingreso";
    }
    $act=Trabajador::where('DNI_trabajador',$DNI_trabajador)
    ->update(['estado_trabajador'=>$accion]);
    $fecha=new fecha_historial;
    $fecha->cod_trabajador=$DNI_trabajador;
    $fecha->descripcion=$festado;
    $fecha->save();
    $empresa=Auth::user()->RUC_empresa;
    $regimen_empresa=db::table('empresa')->where('RUC_empresa','=',$empresa)->select('cod_regimen_laboral')->get();
    $tipo_trabajador=db::table('trabajador')
    ->join('escala_salarial','trabajador.DNI_trabajador','=','escala_salarial.cod_trabajador')
    ->where('trabajador.DNI_trabajador',$DNI_trabajador)->select('trabajador.cod_tipo_trabajador','escala_salarial.cod_regimen_laboral')->get();
    if(count($tipo_trabajador))
    {
      if($tipo_trabajador[0]->cod_tipo_trabajador==1 &&
      $tipo_trabajador[0]->cod_regimen_laboral==$regimen_empresa[0]->cod_regimen_laboral)
      {
        $deletedRows = EscalaSalarialModel::where('cod_trabajador', $DNI_trabajador)->get();
        if(count($deletedRows)!=0)
        {
          $this->costeo();
        }
      }
    }

    session()->flash('success','Trabajador '. $mensaje);
    return Redirect::to('recursos_humanos/trabajador');
  }
  public function verificar($var)
  {
    $empresa=Auth::user()->RUC_empresa;
    $existe=DB::Table('escala_salarial')
    ->where('RUC_empresa','=',$empresa)
    ->where('cod_trabajador','=',$var)
    ->select('cod_trabajador')
    ->get();
    return count($existe);
  }
}
