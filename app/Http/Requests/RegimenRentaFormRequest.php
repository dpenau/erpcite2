<?php

namespace erpCite\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegimenRentaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod_regimen_renta'=>'required|max:11',
            'descrip_regimen_renta'=>'required|max:50',
        ];
    }
}
