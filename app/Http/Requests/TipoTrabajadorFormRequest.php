<?php

namespace erpCite\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoTrabajadorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod_tipo_trabajador'=>'required|max:11',
            'descrip_tipo_trabajador'=>'required|max:15',

        ];
    }
}
