<?php

namespace erpCite\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnidadMedidaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod_unidad_medida'=>'required|max:11',
            'unidad_medida'=>'required|max:10',
            'descrip_unidad_medida'=>'required|max:30',
            'magnitud_unidad_medida'=>'required|max:30',
        ];
    }
}
