<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class TipoContribuyenteModel extends Model
{
    protected $table='tipo_contribuyente';

    protected $primaryKey="cod_tipo_contribuyente";
  
    public $timestamps=false;
  
  
    protected $fillable=['descrip_tipo_contribuyente', 'estado_tipo_contribuyente'];
  
    protected $guarded=[];
}
