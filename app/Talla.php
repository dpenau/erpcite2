<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Talla extends Model
{
    protected $table='talla';

    protected $primaryKey="codigo_talla";
  
    public $timestamps=false;
  
  
    protected $fillable=['codigo_talla','talla_num','cod_material'];
  
    protected $guarded=[];
}
