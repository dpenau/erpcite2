<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionValoresModel extends Model
{
  protected $table='configuracion_valores';

  protected $primaryKey="codigo_valor";

  public $timestamps=false;


  protected $fillable=['nombre','valor'];

  protected $guarded=[];
}
