<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class SeguimientoOrdenProduccion extends Model
{
    protected $table='detalle_orden_pedido';

    //protected $primaryKey="codigo_orden_pedido";

  public $timestamps=false;
  
}
