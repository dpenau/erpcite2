<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OtrosGastosVentas extends Model
{
  protected $table='gasto_otros';

protected $primaryKey="id_otros";

public $timestamps=false;


protected $fillable=['descripcion','gasto','estado','RUC_empresa','fecha_creacion'];





protected $guarded=[];
}
