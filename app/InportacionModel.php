<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class InportacionModel extends Model
{
  protected $table='importaciones';
  protected $primaryKey="codigo_importaciones";
  public $timestamps=false;


  protected $fillable=['id_detalle_oc','tipo_gasto_moneda','estado_gasto','descripcion_gasto','importe_gasto'];

  protected $guarded=[];
}
