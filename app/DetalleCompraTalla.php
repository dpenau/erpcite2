<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCompraTalla extends Model
{
    protected $table='detalle_orden_compra_talla';

    protected $primaryKey='cod_temp_mat';
  
    public $timestamps=false;
  
    protected $fillable=['subvalor_total','cantidad','costo_total','cod_material','cod_cab_ordencompra','cantidad_faltante'];
  
    protected $guarded=[];
}
