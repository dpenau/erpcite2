<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class TipoMaterialIndirecto extends Model
{
    protected $table='tipo_material_indirecto';
    protected $primaryKey="id";
    public $timestamps=false;


    protected $fillable=['codigo','descripcion','estado_registro'];

    protected $guarded=[];
}
