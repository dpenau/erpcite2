<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class MaterialSuministroModel extends Model
{
  protected $table='gasto_suministro';

    protected $primaryKey="cod_material";
  public $timestamps=false;


  protected $fillable=['consumo','meses_duracion','cod_area',
  'RUC_empresa',
  'estado_suministro',
  'gasto_mensual_suministro'];

  protected $guarded=[];
}
