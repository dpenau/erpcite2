<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//VISTA INICIAL RUTA

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('inicio/inicio');
});

Route::get('/home', function () {
    return Redirect::to('Admin');
});
Route::post('contactar/inicio', 'InicioController@store');

Route::get('/', function () {
    return view('auth/login');
});
//FIN VISTA INICIAL RUTA
//ADMIN RUTAS
//INDICADORES PARA ADMINISTRADOR
Route::get('Admin/indicador/operacion', 'AdminController@indicador_operaciones');

Route::get('Admin/indicador/obt_data/{var}', 'AdminController@obtener_data');
//FIN INDICADORES PARA ADMINISTRADOR
//MANTENIMIENTO ADMIN

Route::resource('Admin', 'AdminController');
Route::get('Mantenimiento/contacto', 'AdminController@contacto');
Route::resource('Mantenimiento/comentarios', 'AdminComentarioController');
Route::resource('Mantenimiento/area', 'AreaController');
Route::resource('Mantenimiento/Subcategoria', 'ClasificacionController');
Route::resource('Mantenimiento/Categoria', 'CategoriaController');
Route::resource('Mantenimiento/Roles', 'RolesController');
Route::resource('Mantenimiento/Contribuyente', 'TipoContribuyenteController');
Route::resource('Mantenimiento/Regimen_Renta', 'RegimenRentaController');
Route::resource('Mantenimiento/Regimen_Laboral', 'RegimenLaboralController');
Route::resource('Mantenimiento/Tipo_Trabajador', 'TipoTrabajadorController');
Route::resource('Mantenimiento/Unidad_Medida', 'UnidadMedidaController');
Route::resource('Mantenimiento/recuperar_clave', 'RecuperarPassController');
Route::resource('Mantenimiento/configuracion', 'ConfiguracionGeneralController');
Route::get('Mantenimiento/noticias/', 'NoticiaController@index');
Route::get('Mantenimiento/noticias/create', 'NoticiaController@create');
Route::post('Mantenimiento/noticias/store', 'NoticiaController@store');
Route::put('Mantenimiento/noticias/update', 'NoticiaController@update');
Route::delete('Mantenimiento/noticias/delete', 'NoticiaController@delete');
Route::resource('Mantenimiento/tipo_proveedor', 'TipoProveedorController');
Route::get('Mantenimiento/comentario/exportar', 'PdfComentarioController@index')->name("descargar_comentario");
//FIN MANTENIMIENTO ADMIN
//FIN ADMIN RUTAS
//LOGISTICA RUTAS
//ALMACEN RUTA
Route::resource('logistica/almacen', 'AlmacenController');
//FIN ALMACEN RUTA
//PROVEEDOR RUTA
Route::resource('logistica/proveedores', 'ProveedorController');
Route::get('logistica/proveedores/consulta/ruc/{var}', 'ProveedorConsultaController@index')->name('consulta');
Route::get('Logistica/proveedores/reporte/lmateriales', 'PdfReporteProveedorController@index');
Route::get('logistica/proveedores/consulta/obtener_material/{var}', 'ProveedorController@obtdatos');
Route::get('logistica/proveedores/consulta/obtener_subcategoria/{var}', 'ProveedorController@obtdatosubcategoria');
Route::get('Logistica/proveedores/reporte/indicador', 'PdfReporteProveedorController@indicador');
Route::get('Logistica/proveedores/reporte/obt_datos/{var}', 'PdfReporteProveedorController@obt_dato');
Route::get('Logistica/proveedores/reporte/obt_todo/{var}', 'PdfReporteProveedorController@obt_todo');
//FIN PROVEEDOR RUTA
//MATERIAL RUTA
Route::resource('logistica/articulo', 'MaterialController');
Route::get('indicadores/kardex', 'MaterialController@indicador_vista');
Route::get('indicadores/kardex/mas_salidos/mes/top_10', 'MaterialController@mas_mes_salido');
Route::get('indicadores/kardex/mas_salidos/ano/top_10', 'MaterialController@mas_ano_salido');
Route::get('indicadores/kardex/menos_salidos/mes/top_10', 'MaterialController@menos_mes_salido');
Route::get('indicadores/kardex/menos_salidos/ano/top_10', 'MaterialController@menos_ano_salido');
Route::get('Logistica/pdf/materiales/{var}', 'PdfMaterialController@index');
Route::get('Logistica/pdf/ordenCompras/{var}', 'PdfOrdenComprasController@index');
Route::get('Produccion/pdf/ficha_producto/{var}', 'PdfFichaProductoController@index');
Route::get('Logistica/pdf/ordenComprasTalla/{var}', 'PdfOrdenComprasTallaController@index');
Route::post('logistica/articulo/eliminar', 'MaterialController@eliminar');
//FIN MATERIAL RUTA
//ORDEN DE COMPRA RUTA
Route::get('logistica/orden_compras', 'OrdenCompraController@index');
Route::post('logistica/orden_compras/guardar', 'OrdenCompraController@store');
Route::POST('logistica/orden/corregir', 'OrdenCompraController@corregir_material');
Route::get('orden_compra/obtener_material/{var}', 'OrdenCompraController@obtdatos');
Route::post('orden_compra/cancelar_orden', 'OrdenCompraController@cancelar_orden');
Route::get('salida_material/obtener_material/{var}', 'OrdenCompraController@obtmateriales_kardex');
Route::get('logistica/kardex/obtener', 'OrdenCompraController@obt_kardex');
Route::get('tienda/obtener/', 'OrdenCompraController@obt_kardex_tienda');
Route::get('recoger/obtener/', 'OrdenCompraController@obt_rordendes');
Route::get('/material_suministro/obtener/{var}', 'OrdenCompraController@obt_suministro');
Route::get('/costo/actualizar/mat_indirecto', 'OrdenCompraController@mat_indirecto');
Route::get('pdf/orden_compra', 'PdfController@index')->name('descargar');
//FIN ORDEN DE COMPRA RUTA
//ORDEN COMPRAS ---> BY UNSA
Route::resource('logistica/orden_compras', 'OrdenCompraController');
Route::get('logistica/orden_compra', 'OrdenCompraController@index');
Route::get('logistica/orden_compras/create', 'OrdenCompraController@create');
Route::get('logistica/orden_compras/create_talla', 'OrdenCompraController@show');
Route::get('logistica/orden_compras/consulta/obtener_material', 'OrdenCompraController@dateActual');
Route::post('logistica/orden_compras/store', 'OrdenCompraController@store');
Route::post('logistica/orden_compras/actualizar', 'OrdenCompraController@update');
Route::post('logistica/orden_compras/eliminarOrdenC', 'OrdenCompraController@destroy');

Route::get('logistica/orden_compras/consulta/editar/{var}', 'OrdenCompraController@obtlistMat');
Route::get('logistica/orden_compras/consulta/eliminar/{var}', 'OrdenCompraController@eliminar');
Route::get('logistica/orden_compras/consulta/recepcion_mat/{var}', 'OrdenCompraController@getRecepcion');
Route::get('logistica/orden_compras/consulta/obtener_cat/{var}', 'OrdenCompraController@obtdatosCat');
Route::get('logistica/orden_compras/consulta/obtener_subcat/{var}', 'OrdenCompraController@obtdatosSubCat');
Route::get('logistica/orden_compras/consulta/obtener_subcategoria/{var]', 'OrdenCompraController@obt_subcategoria');
Route::get('logistica/orden_compras/consulta/obtener_mat/{var}', 'OrdenCompraController@obtdatosMateriales');
Route::get('logistica/orden_compras/consulta/obtener_mat_talla/{var}', 'OrdenCompraController@obtdatosMaterialesTalla');
Route::get('logistica/orden_compras/consulta/recepcionar_todo/{var}', 'OrdenCompraController@recepcionarTodo');
Route::get('logistica/orden_compras/consulta/recepcionar_todo_talla/{var}', 'OrdenCompraController@recepcionarTodoTalla');

//FIN ORDENCOMPRAS

//KARDEX - INVENTARIO  ---> BY UNSA

Route::get('logistica/kardex', 'Kardex_cabController@index');
Route::get('logistica/kardex/talla', 'Kardex_cabController@show');
Route::post('logistica/kardex/store', 'KardexController@store');
Route::get('logistica/kardex/salida_normal', 'KardexController@create');
Route::get('logistica/kardex/salida_talla', 'KardexController@show');
Route::get('logistica/kardex/salida/obtenerSubcategoria/{var}', 'KardexController@obtenerSubcategoria');
Route::get('logistica/kardex/salida/obtenerMaterial/{var}', 'KardexController@obtenerMaterial');
Route::get('logistica/kardex/salida/obtenerMaterialTalla/{var}', 'KardexController@obtenerMaterialTalla');
Route::get('logistica/kardex/salida/obtenerTallas/{var}', 'KardexController@obtenerTallas');



//FIN KARDEX - INVENTARIO

//RECEPCION DE COMPRA RUTA
Route::resource('logistica/ingreso_salida', 'KardexController');
Route::get('logistica/recoger/{var}', 'RecogerController@index')->name('recogida');
Route::post('logistica/recoger/create', 'RecogerController@store');
Route::get('logistica/importacion/{var}', 'InportacionController@index')->name('importacion');
Route::post('logistica/nueva_importacion', 'InportacionController@store');
Route::put('logistica/editar_importacion', 'InportacionController@update');
Route::put('logistica/accion_importacion', 'InportacionController@destroy');
//FIN RECEPCION DE COMPRA

//KARDEX RUTA
Route::resource('logistica/kardex_mat', 'KardexMatController');
Route::get('pdf/kardex_actual/{var}', 'PdfKardexActualController@index');
Route::POST('tienda/salida', 'KardexMatController@salida_tienda');
Route::get('kardex/excel', 'KardexMatController@excel');
Route::post('kardex/subir', 'KardexMatController@subirexcel');
Route::get('logistica/orden_compra_vista/{var}', 'OrdenCompraViewController@index')->name('ver');
Route::get('logistica/historial/{var}', 'HistorialController@index')->name('historial');
Route::post('logistica/kardex/cambiar_ubicacion', 'HistorialController@cambiar_ubicacion');
Route::resource('Logistica/Kardex/reporte', 'PdfReporteKardexController');
Route::post('Logistica/Kardex/reporte_material_normal', 'PdfReporteKardexController@material_normal');
Route::post('logistica/kardex/orden_salida', 'KardexSalidaController@orden_salida');
Route::get('logistica/kardex/orden_salida_pdf/{var}', 'PdfReporteOrdenSalidaController@index');
Route::get('logistica/ordenes_salida', 'OrdenSalidaController@index');
Route::get('logistica/obt_ordenes_salida_obt', 'OrdenSalidaController@obtener');
Route::get('pdf/reporte_almacen/{var}', 'PdfAlmacenController@index');
//FIN KARDEX RUTA
//TIENDA RUTA
Route::Get('logistica/tiendas', 'KardexMatController@tienda');
Route::resource('logistica/salida_material', 'KardexSalidaController');
Route::get('logistica/historial/tienda/{var}', 'HistorialController@tiendas_historia');
Route::post('logistica/tienda/cambiar_ubicacion', 'HistorialController@cambiar_ubicacion_tienda');
Route::post('Logistica/tienda/reporte', 'PdfReporteKardexController@reporte_tienda');
Route::post('tienda/guardar_salida', 'KardexSalidaController@guardar_salida');
Route::get('tienda/salida_material', 'KardexSalidaController@salida_material');
Route::get('tienda/devolucion_material', 'KardexSalidaController@devolucion_material');
Route::get('tienda/devolucion_material/obt_material/{var}', 'KardexSalidaController@obt_material');
Route::get('pdf/tienda_actual/{var}', 'PdfKardexActualController@tienda_actual');
Route::get('salida_tienda/obtener_material/{var}', 'KardexSalidaController@obtmateriales_tienda');
Route::get('logistica/tienda/orden_salida_pdf/{var}', 'PdfReporteOrdenSalidaController@tienda');
//FIN TIENDA RUTA
//FIN LOGISTICA RUTAS
//DESARROLLO DE PRODUCTO RUTAS
//COLECCION RUTAS
Route::resource('Produccion/coleccion', 'ColeccionCalzadoController');
//FIN COLECCION RUTAS
//LINEAS RUTAS
Route::resource('Produccion/lineas_calzado', 'LineasCalzadoController');
//FIN LINEAS RUTAS
//SERIES RUTAS
Route::resource('Produccion/series_calzado', 'SeriesCalzadoController');
//FIN SERIES RUTAS
//MODELOS RUTAS
Route::resource('Produccion/modelos_calzado', 'ModelosCalzadoController');
//FIN MODELOS RUTAS

//FICHA MODELOS RUTAS
Route::resource('Produccion/ficha_calzado', 'FichaCalzadoController');
//FIN FICHA MODELOS RUTAS

//COMBINACIONES MODELOS
Route::resource('Produccion/combinacion_calzado', 'CombinacionCalzadoController');
    //Creacion propiamente
Route::POST('Produccion/combinacion_calzado/createCombinacion/{id}', 'CombinacionCalzadoController@show');
Route::get('Produccion/combinacion_calzado/listado/{id}', 'CombinacionCalzadoController@index');
    //Datos que jala para poder realizar la creacion
Route::get('Produccion/combinacion_calzado/create/{id}', 'CombinacionCalzadoController@create');
//FIN COMBINACIONES DE MODELOS

//COMBINACIONES FICHA  MODELOS
Route::resource('Produccion/combinacion_ficha', 'CombinacionFichaCalzadoController');
Route::resource('Produccion/combinacion_ficha/delete', 'EliminacionCombinacionFichaController');
Route::POST('Produccion/combinacion_ficha/createCombinacion/{id}', 'CombinacionFichaCalzadoController@show');
Route::get('Produccion/combinacion_ficha/listado/{id}', 'CombinacionFichaCalzadoController@index');
Route::get('Produccion/combinacion_ficha/getlistado/{id}', 'CombinacionFichaCalzadoController@getMaterialesDirectosFicha');
Route::get('Produccion/combinacion_ficha/getmanosdirecta/{id}', 'CombinacionFichaCalzadoController@getManoObraDirecta');
Route::get('Produccion/combinacion_ficha/ficha_producto/{id}', 'CombinacionFichaCalzadoController@ficha');
Route::get('Produccion/combinacion_ficha/ficha_producto_m/{id}', 'CombinacionFichaCalzadoController@ficha_obra');
Route::get('Produccion/combinacion_ficha/ficha_material/{id}', 'CombinacionFichaCalzadoController@material');
Route::get('Produccion/combinacion_ficha/ficha_obra/{id}', 'CombinacionFichaCalzadoController@mano_obra');
Route::POST('Produccion/combinacion_ficha/create', 'CombinacionFichaCalzadoController@create');
Route::POST('Produccion/combinacion_ficha/create_mano', 'CombinacionFichaCalzadoController@create_mano');
Route::POST('Produccion/combinacion_ficha/aprobar', 'CombinacionFichaCalzadoController@aprobar');
Route::POST('Produccion/combinacion_ficha/aprobar_m', 'CombinacionFichaCalzadoController@aprobar_m');
Route::POST('Produccion/combinacion_ficha/proceso', 'CombinacionFichaCalzadoController@proceso');
Route::get('Produccion/combinacion_ficha/actualizar/{id}', 'CombinacionFichaCalzadoController@actualizar_materiales');
//FIN COMBINACIONES FICHA  MODELOS


//FICHA DE PROTOTIPADO
Route::get('modelo/ficha_producto/costo_modelo/{var}', 'ModelosCalzadoController@detalle_costeo_modelo');
Route::get('modelo/ficha_producto/obt_material/{var}', 'ModelosCalzadoController@obt_material');
Route::POST('modelo/ficha_producto/guardar', 'ModelosCalzadoController@guardar_modelo');
Route::POST('modelo/ficha_producto/guardar_operacion', 'ModelosCalzadoController@guardar_mano');
Route::get('modelo/ficha_producto/descargar/{var}', 'ModelosCalzadoController@descarga');
Route::get('modelo/ficha_producto/crear_ficha', 'ModelosCalzadoController@crear_ficha');
Route::get('modelo/ficha_producto', 'ModelosCalzadoController@ficha_producto');
//FIN FICHA DE PROTOTIPADO
//FIN DESARROLLO DE PRODUCTO RUTAS
//COSTOS RUTAS
//CONFIGURACION RUTAS
Route::resource('Configuracion/Costos_Directos', 'ConfCostosDirectosController');
Route::post('Configuracion/Costos_Directos', 'ConfCostosDirectosController@index');
//Route::post('Configuracion/Costos_Directos','ConfCostosDirectosController@ref')->name('normindex');
Route::post('Configuracion/Costos_Directos', 'ConfCostosDirectosController@create')->name('normindex');
Route::post('Configuracion/Costos_Directos/create', 'ConfCostosDirectosController@store');
//FIN CONFIGURACION RUTAS
//COSTOS DIRECTOS RUTAS
Route::get('costo/directo/material/{var}', 'CostosDirectosMaterialController@index')->name('costos_directos');
Route::post('costo/directo/material/create', 'CostosDirectosMaterialController@store');
Route::put('costo/directo/material/update', 'CostosDirectosMaterialController@update');
Route::get('costo/directo/obt_material/{var}', 'CostosDirectosMaterialController@obt_material');
Route::get('costo/directo/mano/{var}', 'CostosDirectosObraController@index')->name('mano_directo');
Route::post('costo/directo/mano/{var}', 'CostosDirectosObraController@store');
Route::put('costo/directo/mano/{var}', 'CostosDirectosObraController@update');
Route::get('costo/directo/norma_tecnica/{var}', 'NormaTecnicaController@index')->name('norma_tecnica');
Route::post('costo/directo/norma_tecnica/create', 'NormaTecnicaController@store');
Route::put('costo/directo/norma_tecnica/update', 'NormaTecnicaController@update');
Route::get('costo/vermodelos', 'VerModelo@index');
Route::get('costo/act_costo/', 'VerModelo@actualizar_costo');
Route::get('costo/directo/articulo/{var}', 'ModeloArticuloController@index')->name('modelo_articulo');
Route::get('costo/directo/nuevo_articulo/{var}', 'ModeloArticuloController@nuevo')->name('nuevo_articulo');
Route::post('costo/directo/reg_articulo/create', 'ModeloArticuloController@store');
//FIN COSTOS DIRECTOS RUTAS
//COSTOS INDIRECTOS RUTAS
Route::get('costos_indirectos/financiero', 'FinancieroController@index');
Route::get('costos_indirectos/financiero/crear_prestamo', 'FinancieroController@crear_prestamo');
Route::post('costos_indirectos/financiero/guardar_prestamo', 'FinancieroController@almacenar_prestamo');
Route::get('costos_indirectos/financiero/crear_gasto', 'FinancieroController@crear_gasto');
Route::get('costos_indirectos/financiero/crear_letra', 'FinancieroController@crear_letra');
Route::post('costos_indirectos/financiero/guardar_gasto', 'FinancieroController@almacenar_gasto');
Route::post('costos_indirectos/financiero/eliminar_gasto', 'FinancieroController@eliminar');
Route::get('costos_indirectos/MaterialesAdm', 'MaterialesAdmController@index');
Route::get('costos_indirectos/MaterialesAdm/crear_material', 'MaterialesAdmController@crear_material');
Route::post('costos_indirectos/MaterialesAdm/guardar_material', 'MaterialesAdmController@guardar_material');
Route::get('costos_indirectos/MaterialesAdm/crear_sueldo', 'MaterialesAdmController@crear_sueldo');
Route::post('costos_indirectos/MaterialesAdm/guardar_sueldo', 'MaterialesAdmController@guardar_sueldo');
Route::get('costos_indirectos/MaterialesAdm/crear_representacion', 'MaterialesAdmController@crear_representacion');
Route::post('costos_indirectos/MaterialesAdm/guardar_representacion', 'MaterialesAdmController@guardar_representacion');
Route::post('costos_indirectos/MaterialesAdm/borrar_material', 'MaterialesAdmController@borrar_material');
Route::post('costos_indirectos/MaterialesAdm/borrar_sueldo', 'MaterialesAdmController@borrar_sueldo');
Route::post('costos_indirectos/MaterialesAdm/borrar_representacion', 'MaterialesAdmController@borrar_representacion');
Route::resource('costos_indirectos/GastosDistTran', 'GastosDistTranController');
Route::resource('costos_indirectos/GastosVtasyMkt', 'GastosVtasyMktController');
Route::get('costos_indirectos/OtrosgastosdeVtas', 'OtrosgastosdeVtasController@index');
Route::get('costos_indirectos/OtrosgastosdeVtas/crear_sueldo', 'OtrosgastosdeVtasController@crear_sueldo');
Route::post('costos_indirectos/OtrosgastosdeVtas/guardar_sueldo', 'OtrosgastosdeVtasController@almacenar_sueldo');
Route::get('costos_indirectos/OtrosgastosdeVtas/crear_otro', 'OtrosgastosdeVtasController@crear_otro');
Route::post('costos_indirectos/OtrosgastosdeVtas/guardar_otro', 'OtrosgastosdeVtasController@almacenar_otro');
Route::post('costos_indirectos/OtrosgastosdeVtas/eliminar_sueldo', 'OtrosgastosdeVtasController@eliminar_sueldo');
Route::post('costos_indirectos/OtrosgastosdeVtas/eliminar_gasto', 'OtrosgastosdeVtasController@eliminar_gasto');
Route::Get('costos_indirectos/externo', 'ExternoController@index');
Route::get('costos_indirectos/externo/crear', 'ExternoController@create');
Route::post('costos_indirectos/externo/guardar', 'ExternoController@store');
Route::post('costos_indirectos/externo/borrar', 'ExternoController@delete');
Route::resource('costo/indirectos/mano_obra', 'ManoObraController');
Route::resource('costo/indirectos/material_suministro', 'MaterialSuministroController');
Route::post('costo/indirecto/material_suministro/create', 'MaterialSuministroController@store');
Route::put('costo/indirecto/material_suministro/updateall', 'MaterialSuministroController@updateall');
Route::resource('costo/indirectos/depreciacion', 'DepreciacionController');
Route::resource('costo/indirectos/gastos', 'GastosController');
Route::get('costo/indirectos/desarrollo_obtener/{var}', 'GastosController@tipo_desarrollo');
Route::get('desarrollo_obtener/verificar/{var}', 'GastosController@verificar_no_registrado');
Route::resource('costo/indirectos/servicio_basico', 'ServicioBasicoController');
//FIN COSTOS INDIRECTOS RUTAS
//FORMULACION PRECIO DE VENTA RUTAS
Route::get('costo/precio_venta', 'FormulacionPrecioVentaController@index');
Route::get('costo/precio_venta/obt_articulo/{var}', 'FormulacionPrecioVentaController@obtarticulo');
Route::get('costo/precio_venta/obt_datos/{var}', 'FormulacionPrecioVentaController@obtdatos');
Route::post('costo/precio_venta/guardar_precio/create', 'FormulacionPrecioVentaController@guardar');
//FIN FORMULACION PRECIO DE VENTA RUTAS
//INDICADORES COSTO RUTAS
Route::get('costo/indicadores', 'IndicadoresCostosController@index');
//FIN INDICADORES COSTO TURAS
//FIN COSTOS RUTAS
//VENTAS RUTAS
Route::resource('Ventas/clientes', 'ClientesController');
Route::resource('Ventas/pedidos', 'OrdenPedidoController');
Route::resource('Ventas/Reportes/ReporteProductos', 'ReporteProductosController');
Route::resource('Ventas/Reportes/ReporteClientes', 'ReporteClientesController');
Route::get('Ventas/Reportes/ReporteClientes/exportar/{var}', 'PdfReporteClientesController@dopdf');
Route::resource('Ventas/Reportes/ReporteVentas', 'ReporteVentasController');
Route::get('Ventas/pdf/reporteven', 'PdfVentasController@index')->name("ventas_descargar");
Route::get('configuracion/Ventas', 'ClientesController@configuracion');
Route::get('ventas/obt_descuento/{var}', 'ClientesController@obt_descuentos');
Route::get('ventas/nuevo_descuento/{var}', 'ClientesController@nuevo');
Route::get('ventas/tipo_descuento/{var}', 'ClientesController@todos_descuentos');
//FIN VENTAS RUTAS
//RRHH RUTAS
Route::get('verificar/trabajadores/{var}', 'TrabajadorController@verificar');
Route::get('recursos_humanos/pdf/trabajadores', 'PdfTrabajadorController@index')->name("trabajador_descargar");
Route::get('recursos_humanos/historial/{var}', 'TrabajadorHistorialController@index')->name("trabajador_historial");
Route::resource('recursos_humanos/escala_salarial', 'EscalaSalarialController');
Route::resource('recursos_humanos/configuracion', 'HoraTrabajoController');
Route::resource('recursos_humanos/beneficios', 'BeneficiosController');
//FIN RRHH RUTAS
Route::post('devolucion/guardar_devolucion', 'KardexSalidaController@guardar_devolucion');
Route::resource('Subcategoria', 'ClasificacionController');
Route::resource('Categoria', 'CategoriaController');
Route::resource('configuracion_inicial', 'ConfiguracionController');

Route::resource('bienvenida', 'BienvenidaController');
Route::resource('Usuario', 'CambiarContrasenaController');
Route::resource('recursos_humanos/trabajador', 'TrabajadorController');
Route::resource('recursos_humanos/Usuarios', 'UserChangeController');
Route::get('User/{var}', 'UsuarioController@index')->name('usuarios');
Route::post('User/', 'UsuarioController@store');
Route::get('recursos_humanos/trabajador/consulta/dni/{var}', 'TrabajadorConsultaController@index')->name('dni');
Route::resource('pdf/orden_produccion', 'PdfOrdenPController');
Route::post('Produccion/orden_produccion/pdf', 'OrdenProduccionController@pdf666')->name('pdfordencompra'); //
Route::get('Produccion/orden_produccion/pdf', 'OrdenProduccionController@pdf667')->name('pdfordencompra2');
Route::get('Produccion/orden_produccion/create/{cod_pedido}', 'OrdenProduccionController@create');
Route::post('Produccion/orden_produccion/create', 'OrdenProduccionController@store');
Route::get('Produccion/orden_produccion/create/{id}/{id2}', 'OrdenProduccionController@ajaxAct')->name('myform.index');
Route::get('Produccion/orden_produccion/obt_hormas/{var}', 'OrdenProduccionController@obt_hormas');
Route::resource('actualizaciones/comentarios', 'ComentarioController');

Route::get('create-chart/{type}', 'ChartController@makeChart');
Route::get('Produccion/orden_produccion/{id}', 'OrdenProduccionController@index');
Route::get('/Produccion/orden_produccion/ficha_de_orden_de_produccion/{id}', 'OrdenProduccionController@show');
Route::get('/Produccion/orden_produccion/eliminar/{id}', 'OrdenProduccionController@destroy');
Route::get('/Produccion/orden_produccion/act_lista/{var}', 'OrdenProduccionController@act_lista');
Route::get('Produccion/seguimiento_orden_produccion', 'SeguimientoOrdenProduccionController@index');
Route::get('Produccion/seguimiento_orden_produccion/pdf', "SeguimientoOrdenProduccionController@pdf")->name('SeguimientoOrdenProduccion.pdf');
//VERSION 2



Route::get('/novedades', 'NovedadController@index');
Route::get('/novedades/noticias', 'NovedadController@obtener_noticias');

Route::get('pedido/obtener_detalle/{var}', 'OrdenPedidoController@obtener_detalle');
Route::get('pedido/ficha_produccion/pdf/{var}', 'OrdenProduccionController@pdf_orden');
//desde aqui modifique
Route::get('Produccion/reportes/ReporteTiempo', 'ReporteTiempoController@index');
Route::get('Produccion/reportes/ReporteGrupos', 'ReporteGruposController@index');
Route::get('Produccion/pdf/reporteven2', 'PdfReporteTiemposController@index')->name("tiempo_descargar");
Route::get('Produccion/programa_produccion', 'ProgramaProduccionController@index');
//Controllers OrdenProduccionController
//Planeacion -- by Mario_Huaypuna
Route::get('planeacion/formas_trabajo', 'FormasTrabajoController@index');
Route::get('planeacion/formas_trabajo/act_personal_proceso', 'FormasTrabajoController@updateStuffProcess');
Route::get('planeacion/formas_trabajo/obtener_equipos_armados', 'FormasTrabajoController@getArmedTeams');
Route::get('planeacion/formas_trabajo/obtener_trabajadores/{idgrupo}', 'FormasTrabajoController@getWorkersOfGroup');
//modificado por Chuy
Route::get('armado_de_equipos_de_trabajo/{id}', 'ArmadoDeEquiposDeTrabajoController@index');
//Proceso
Route::resource('Produccion/Proceso', 'ProcesosController');

//Empresa
Route::get('configuracion/empresa','EmpresaController@show');
Route::get('configuracion/empresa/configuracion','EmpresaController@configuracion');
Route::POST('configuracion/empresa/update','EmpresaController@store');

//MERMA
Route::get('configuracion/merma','MermaController@index');
Route::post('configuracion/merma/create','MermaController@create');
Route::put('configuracion/merma/update/{idMerma}','MermaController@update');
Route::delete('configuracion/merma/delete/{idMerma}','MermaController@delete');

//OPERACIONES DIRECTAS
Route::resource('configuracion/operacion_directa', 'OperacionDirectaController');
Route::get('configuracion/operacion_directa','OperacionDirectaController@index');
Route::get('configuracion/operacion_directa/create','OperacionDirectaController@create');

//COSTOS
Route::get('costos/directos','CostosDirectosController@index');
Route::get('costos/indirectos/operacion','CostosIndirectosController@index');

Route::post('costos/indirectos/operacion/materialindirecto/create','MaterialIndirectoController@create');
Route::put('costos/indirectos/operacion/materialindirecto/update/{idMaterialIndirecto}','MaterialIndirectoController@update');
Route::delete('costos/indirectos/operacion/materialindirecto/delete/{idMaterialIndirecto}','MaterialIndirectoController@delete');

Route::get('costos/indirectos/operacion/serviciobasico/get','ServicioBasicoController@index');
Route::post('costos/indirectos/operacion/serviciobasico/create','ServicioBasicoController@create');
Route::put('costos/indirectos/operacion/serviciobasico/update/{idServicioBasico}','ServicioBasicoController@update');
Route::delete('costos/indirectos/operacion/serviciobasico/delete/{idServicioBasico}','ServicioBasicoController@delete');

Route::post('costos/indirectos/operacion/desarrollo/hormascreate','DesarrolloHormasController@create');

Route::post('costos/indirectos/operacion/desarrollo/materialcreate','DesarrolloMaterialController@create');
Route::post('costos/indirectos/operacion/desarrollo/materialupdate/{idMaterial}','DesarrolloMaterialController@update');

Route::post('costos/indirectos/operacion/desarrollo/procesocreate','ProcesoProductivoController@create');
Route::post('costos/indirectos/operacion/desarrollo/serviciocreate','DesarrolloServicioController@create');
Route::post('costos/indirectos/operacion/desarrollo/troquelescreate','DesarrolloTroquelesController@create');
//Route::get('Produccion/Proceso/', 'ProcesosController@index');
//Route::get('Produccion/Proceso/create', 'ProcesosController@create');
//Route::post('Produccion/Proceso/store', 'ProcesosController@store');
Auth::routes(); // esta linea debe ir al final para evitar inconvenientes ...
