@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Beneficios
</h3>
</div>
<br>
@if(count($micro)>0)
<h4>MICRO EMPRESA</h4>
<div class="x_content table-responsive">
  <table id="example2" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Sueldo Mensual</th>
        <th class="bg-danger">Asignacion Familiar</th>
        <th>Sueldo Mensual + Asignación Familiar</th>
        <th>Sueldo anual por los Días Laborables</th>
        <th>Sueldo Anual por Dominicales</th>
        <th>Sueldo Anual por Feriados</th>
        <th>Sueldo Anual por Vacaciones</th>
        <th>Remuneración Bruta Anual</th>
        <th>Remuneración Promedio Mensual</th>
        <th class="bg-danger">Gratificación Julio</th>
        <th class="bg-danger">Gratificación Diciembre</th>
        <th class="bg-danger">SCTR</th>
        <th>ESSALUD</th>
        <th>SIS</th>
        <th class="bg-danger">CTS</th>
        <th>TOTAL COSTOS DE BENEFICIOS</th>
        <th>Tasa Adicional Sueldo Mensual</th>
        <th>Ponderado de Beneficio</th>
      </tr>
    </thead>
    <tbody>
        @foreach($micro as $mi)
        <tr>
          <td>{{$mi->apellido_paterno}} {{$mi->apellido_materno}}, {{$mi->nombres}}</td>
          <td>S/.{{number_format($mi->sueldo_mensual,2)}}</td>
          <td>No Aplica</td>
          <td>S/.{{number_format($mi->sueldo_mensual,2)}}</td>
          <?php $sueldo_laboral=number_format(($mi->sueldo_mensual*296)/30,2,'.',""); ?>
          <td>S/.{{$sueldo_laboral}}</td>
          <?php $sueldo_dominical=number_format(($mi->sueldo_mensual*52)/30,2,'.',""); ?>
          <td>S/.{{$sueldo_dominical}}</td>
          <?php $sueldo_feriado=number_format(($mi->sueldo_mensual*12)/30,2,'.',""); ?>
          <td>S/.{{$sueldo_feriado}}</td>
          <?php $sueldo_vacaciones=number_format((floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado))/(296+52+12)*15,2,'.',"");?>
          <td>S/.{{$sueldo_vacaciones}}</td>
          <?php $sueldo_bruto=number_format(floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado),2,'.',""); ?>
          <td>S/.{{$sueldo_bruto}}</td>
          <?php $sueldo_men=number_format($sueldo_bruto/12,2,'.',"");?>
          <td>S/.{{$sueldo_men}}</td>
          <td>No Aplica</td>
          <td>No Aplica</td>
          <td>No Aplica</td>
          @if($mi->tipo_seguro==1)
          <?php $sis=number_format(180,2,'.',"");?>
          <td>No Pertenece</td>
          <td>S/.{{$sis}}</td>
          @else
          <?php $essalud=number_format($sueldo_men*0.09*12,2,'.',"") ?>
          <td>S/.{{$essalud}}</td>
          <td>No Pertenece</td>
          @endif
          <td>No Aplica</td>
          @if($mi->tipo_seguro==1)
          <?php $beneficos=number_format($sis+$sueldo_vacaciones,2,'.',"") ?>
          <td>S/.{{$beneficos}}</td>
          @else
          <?php $beneficos=number_format($essalud+$sueldo_vacaciones,2,'.',"") ?>
          <td>S/.{{$beneficos}}</td>
          @endif
          <?php $tasa=number_format((($beneficos/$mi->sueldo_mensual)/12)*100,2,'.',"") ?>
          <td>%{{$tasa}}</td>
          <td>asd</td>
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
@endif
<br>
@if(count($pequeña)>0)
<h4>PEQUEÑA EMPRESA</h4>
<div class="x_content table-responsive">
  <table id="example" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Sueldo Mensual</th>
        <th class="bg-danger">Asignacion Familiar</th>
        <th>Sueldo Mensual + Asignación Familiar</th>
        <th>Sueldo anual por los Días Laborables</th>
        <th>Sueldo Anual por Dominicales</th>
        <th>Sueldo Anual por Feriados</th>
        <th>Sueldo Anual por Vacaciones</th>
        <th>Remuneración Bruta Anual</th>
        <th>Remuneración Promedio Mensual</th>
        <th>Gratificación Julio</th>
        <th>Gratificación Diciembre</th>
        <th>SCTR</th>
        <th>ESSALUD</th>
        <th class="bg-danger">SIS</th>
        <th>CTS</th>
        <th>TOTAL COSTOS DE BENEFICIOS</th>
        <th>Tasa Adicional Sueldo Mensual</th>
        <th>Ponderado de Beneficio</th>
      </tr>
    </thead>
    <tbody>
      @foreach($pequeña as $peq)
      <tr>
        <td>{{$peq->apellido_paterno}} {{$peq->apellido_materno}}, {{$peq->nombres}}</td>
        <td>S/.{{number_format($peq->sueldo_mensual,2)}}</td>
        <td>No Aplica</td>
        <td>S/.{{number_format($peq->sueldo_mensual,2)}}</td>
        <?php $sueldo_laboral=number_format(($peq->sueldo_mensual*296)/30,2,'.',""); ?>
        <td>S/.{{$sueldo_laboral}}</td>
        <?php $sueldo_dominical=number_format(($peq->sueldo_mensual*52)/30,2,'.',""); ?>
        <td>S/.{{$sueldo_dominical}}</td>
        <?php $sueldo_feriado=number_format(($peq->sueldo_mensual*12)/30,2,'.',""); ?>
        <td>S/.{{$sueldo_feriado}}</td>
        <?php $sueldo_vacaciones=number_format((floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado))/(296+52+12)*15,2,'.',"");?>
        <td>S/.{{$sueldo_vacaciones}}</td>
        <?php $sueldo_bruto=number_format(floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado),2,'.',""); ?>
        <td>S/.{{$sueldo_bruto}}</td>
        <?php $sueldo_men=number_format($sueldo_bruto/12,2,'.',"");?>
        <td>S/.{{$sueldo_men}}</td>
        <?php $gratificacion_jul=number_format($sueldo_men*0.5*(1+0.09),2,'.',"");?>
        <td>{{$gratificacion_jul}}</td>
        <?php $gratificacion_dic=number_format($sueldo_men*0.5*(1+0.09),2,'.',"");?>
        <td>{{$gratificacion_dic}}</td>
        <?php $sctr=number_format($sueldo_men*0.0123*12,2,'.',"");?>
        <td>{{$sctr}}</td>
        <?php $essalud=number_format($sueldo_men*0.09*12,2,'.',""); ?>
        <td>S/.{{$essalud}}</td>
        <td>No Pertenece</td>
        <?php $cts=number_format($sueldo_men*0.5,2,'.',""); ?>
        <td>{{$cts}}</td>
        <?php $beneficios=number_format(($gratificacion_jul+$gratificacion_dic+$sctr+$essalud+$cts+$sueldo_vacaciones),2,'.',""); ?>
        <td>{{$beneficios}}</td>
        <?php $tasa=number_format((($beneficios/$peq->sueldo_mensual)/12)*100,2,'.',""); ?>
        <td>%{{$tasa}}</td>
        <td>asd</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endif
<br>
@if(count($general)>0)
<h4>GRAN EMPRESA (GENERAL)</h4>
<div class="x_content table-responsive">
  <table id="example3" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Sueldo Mensual</th>
        <th>Asignacion Familiar</th>
        <th>Sueldo Mensual + Asignación Familiar</th>
        <th>Sueldo anual por los Días Laborables</th>
        <th>Sueldo Anual por Dominicales</th>
        <th>Sueldo Anual por Feriados</th>
        <th>Sueldo Anual por Vacaciones</th>
        <th>Remuneración Bruta Anual</th>
        <th>Remuneración Promedio Mensual</th>
        <th>Gratificación Julio</th>
        <th>Gratificación Diciembre</th>
        <th>SCTR</th>
        <th>ESSALUD</th>
        <th class="bg-danger">SIS</th>
        <th>CTS</th>
        <th>TOTAL COSTOS DE BENEFICIOS</th>
        <th>Tasa Adicional Sueldo Mensual</th>
        <th>Ponderado de Beneficio</th>
      </tr>
    </thead>
    <tbody>
      @foreach($general as $ge)
      <tr>
        <td>{{$ge->apellido_paterno}} {{$ge->apellido_materno}}, {{$ge->nombres}}</td>
        <td>S/.{{number_format($ge->sueldo_mensual,2)}}</td>
        <?php $familiar=number_format(930*0.1,2,'.',"");?>
        <td>S/.{{$familiar}}</td>
        <?php $sueldo_familia=number_format($ge->sueldo_mensual+$familiar,2,'.',""); ?>
        <td>S/.{{$sueldo_familia}}</td>
        <?php $sueldo_laboral=number_format($sueldo_familia*296/30,2,'.',"");?>
        <td>S/.{{$sueldo_laboral}}</td>
        <?php $sueldo_dominical=number_format($sueldo_familia*52/30,2,'.',"");?>
        <td>S/.{{$sueldo_dominical}}</td>
        <?php $sueldo_feriado=number_format($sueldo_familia*12/30,2,'.',"");?>
        <td>S/.{{$sueldo_feriado}}</td>
        <?php $sueldo_vacaciones=number_format(($sueldo_laboral+$sueldo_dominical+$sueldo_feriado)/(296+52+12)*30,2,'.',"");?>
        <td>S/.{{$sueldo_vacaciones}}</td>
        <?php $sueldo_bruto=number_format($sueldo_laboral+$sueldo_dominical+$sueldo_feriado,2,'.',"");?>
        <td>S/.{{$sueldo_bruto}}</td>
        <?php $sueldo_men=number_format($sueldo_bruto/12,2,'.',"");?>
        <td>S/.{{$sueldo_men}}</td>
        <?php $gratificacion_jul=number_format($sueldo_men*1*(1+0.09),2,'.',"");?>
        <td>S/.{{$gratificacion_jul}}</td>
        <?php $gratificacion_dic=number_format($sueldo_men*1*(1+0.09),2,'.',"");?>
        <td>S/.{{$gratificacion_dic}}</td>
        <?php $sctr=number_format($sueldo_men*0.0123*12,2,'.',"");?>
        <td>S/.{{$sctr}}</td>
        <?php $essalud=number_format($sueldo_men*0.09*12,2,'.',"");?>
        <td>S/.{{$essalud}}</td>
        <td>No Pertenece</td>
        <?php $cts=number_format($sueldo_men*1,2,'.',"");?>
        <td>S/.{{$cts}}</td>
        <?php $beneficios=number_format(($gratificacion_jul+$gratificacion_dic+$sctr+$essalud+$cts+$sueldo_vacaciones+$familiar)  ,2,'.',"");?>
        <td>S/.{{$beneficios}}</td>
        <?php $tasa=number_format((($beneficios/$ge->sueldo_mensual)/12)*100,2,'.',"");?>
        <td>%{{$tasa}}</td>
        <td>asd</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {
} );
</script>
@endsection
