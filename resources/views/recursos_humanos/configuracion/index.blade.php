@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Horas de trabajo
</h3>
</div>

<div class="x_content table-responsive">
  <table id="" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Lunes</th>
        <th>Martes</th>
        <th>Miercoles</th>
        <th>Jueves</th>
        <th>Viernes</th>
        <th>Sabado</th>
      </tr>
    </thead>

    <tbody>
      @if(count($horario)>0)
      {!!Form::open(array('url'=>'recursos_humanos/configuracion/update','method'=>'PUT','autocomplete'=>'off'))!!}
            {{Form::token()}}
        <?php  $lunes=$martes=$miercoles=$jueves=$viernes=$sabado=""?>
        @for($i=0;$i< sizeof($horario);$i++)
          @if ($horario[$i]->dia=="Lu")
            <?php $lunes=$horario[$i]->hora;?>
          @endif
          @if ($horario[$i]->dia=="Ma")
            <?php $martes=$horario[$i]->hora;?>
          @endif
          @if ($horario[$i]->dia=="Mi")
            <?php $miercoles=$horario[$i]->hora;?>
          @endif
          @if ($horario[$i]->dia=="Ju")
            <?php $jueves=$horario[$i]->hora;?>
          @endif
          @if ($horario[$i]->dia=="Vi")
            <?php $viernes=$horario[$i]->hora;?>
          @endif
          @if ($horario[$i]->dia=="Sa")
            <?php $sabado=$horario[$i]->hora;?>
          @endif
        @endfor
        <td><input class="form-class" type="time" required name="lunes" required value="{{$lunes}}" /></td>
        <td><input class="form-class" type="time" required name="martes" required value="{{$martes}}" /></td>
        <td><input class="form-class" type="time" required name="miercoles" required value="{{$miercoles}}" /></td>
        <td><input class="form-class" type="time" required name="jueves" required value="{{$jueves}}" /></td>
        <td><input class="form-class" type="time" required name="viernes" required value="{{$viernes}}" /></td>
        <td><input class="form-class" type="time" required name="sabado" required value="{{$sabado}}" /></td>
      @else
      {!!Form::open(array('url'=>'recursos_humanos/configuracion','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
      <td><input class="form-class" type="time" required name="lunes" /></td>
      <td><input class="form-class" type="time" required name="martes" /></td>
      <td><input class="form-class" type="time" required name="miercoles" /></td>
      <td><input class="form-class" type="time" required name="jueves" /></td>
      <td><input class="form-class" type="time" required name="viernes" /></td>
      <td><input class="form-class" type="time" required name="sabado" /></td>
      @endif
    </tbody>
  </table>
  <div class="float-right">
    <button type="submit" class="bttn-unite bttn-md bttn-success">Guardar</button>
  </div>
</div>
{!!Form::close()!!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {
} );
</script>
@endsection
