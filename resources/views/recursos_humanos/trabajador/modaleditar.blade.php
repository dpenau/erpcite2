<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$trab->DNI_trabajador}}">
	{{Form::Open(array('action'=>array('TrabajadorController@update',$trab->DNI_trabajador
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Trabajador</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_dni_editar" value="{{$trab->DNI_trabajador}}">
        <div class="form-group  ">
          <label for="total_orden_compra">DNI del trabajador:</label>
           <input type="text" class="form-control" disabled value="{{$trab->DNI_trabajador}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Nombre:</label>
           <input type="text" pattern="[A-Za-z ñÑ]{2,}" class="form-control" name="nombre" value="{{$trab->nombres}}">
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Apellido Paterno:</label>
             <input type="text" class="form-control" name="apellidop" pattern="[A-Za-zñÑ]{2,}" value="{{$trab->apellido_paterno}}">
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Apellido Materno:</label>
             <input type="text" class="form-control" name="apellidom" pattern="[A-Za-zñÑ]{2,}" value="{{$trab->apellido_materno}}">
          </div>
        </div>
        <div class="row">

          <div class="form-group  col-md-12">
            <label for="">Ciudad--Distrito</label>
            <select name="distrito" id="s_distrito" class="custom-select" required>
              <option value="" disabled>Distrito</option>
              @foreach ($distrito as $dist)
                @if($dist->cod_distrito==$trab->cod_distrito)
                  <option value="{{$trab->cod_distrito}}"  selected>{{$trab->nomb_ciudad}}--{{$trab->nomb_distrito}}</option>
                @else
                  <option value="{{$dist->cod_distrito}}" >{{$dist->nomb_ciudad}}--{{$dist->nomb_distrito}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Direccion:</label>
             <textarea class="form-control" rows="2" id="comment" name="direccion" required="required" >{{$trab->direccion}}</textarea>
          </div>
          <div class="form-group  col-md-6">
            <label for="">Tipo de Trabajador</label>
            <select name="tipo" class="custom-select" required>
              <option value="" disabled>Tipo de Trabajador</option>
              @foreach ($tipo_trabajador as $dist)
                @if($dist->cod_tipo_trabajador==$trab->cod_tipo_trabajador)
                  <option value="{{$trab->cod_tipo_trabajador}}"  selected>{{$trab->descrip_tipo_trabajador}}</option>
                @else
                  <option value="{{$dist->cod_tipo_trabajador}}" >{{$dist->descrip_tipo_trabajador}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Telefono de Contaco:</label>
             <input type="text" class="form-control" maxlength="9" pattern="[0-9]+" name="telefono" value="{{$trab->telefono}}">
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Puesto:</label>
             <textarea class="form-control" rows="2" id="comment" name="puesto" required="required">{{$trab->puesto}}</textarea>
          </div>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="">Area</label>
            <select name="area" class="custom-select" required>
              <option value=""  disabled>Area</option>
              @foreach ($areas as $dist)
                @if($dist->cod_area==$trab->cod_area)
                  <option value="{{$trab->cod_area}}"  selected>{{$trab->descrip_area}}</option>
                @else
                  <option value="{{$dist->cod_area}}" >{{$dist->descrip_area}}</option>
                @endif
              @endforeach
            </select>
          </div>
          <div class="form-group  col-md-6">
            <label for="">Sexo</label>
            <select name="sexo" class="custom-select" required>
              <option value=""  disabled>Sexo</option>
                @if($trab->sexo=="H")
                  <option value="H"  selected>Hombre</option>
                @else
                  <option value="M" >Mujer</option>
                @endif
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Fecha de Nacimiento:</label>
              <input type="date" id="tipo" required="required" name="fecha" required="required" class="form-control" value="{{$trab->fecha_nacimiento}}">
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Experiencia Laboral:</label>
             <textarea class="form-control" rows="2" id="comment" name="experiencia" required="required">{{$trab->experiencia_laboral}}</textarea>
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
