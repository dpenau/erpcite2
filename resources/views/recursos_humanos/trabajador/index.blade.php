@extends ('layouts.admin')
@section ('contenido')

<div class="preloader"></div>
<div>
<h3 class="font-weight-bold">Listado de Trabajadores <a href="trabajador/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Trabajador</button></a>
  <a href="{!! route('trabajador_descargar') !!}" target="_blank">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Exportar en PDF</button></a>
</h3>
</div>

<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th>Nombre del Trabajador</th>
        <th>Puesto</th>
        <th>Tipo</th>
        <th>Área</th>
        <th>Estado</th>
        <th>Ver mas</th>
        <th>Historial</th>
        <th>Editar</th>
        <th>Activar/Desactivar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($trabajador as $trab)
      @if($trab->estado_trabajador==0)
      <tr class="bg-danger" align="center">
      @else
          <tr align="center">
      @endif
        <td>{{$trab->apellido_paterno." ".$trab->apellido_materno.", ".$trab->nombres}}</td>
        <td>{{$trab->puesto}}</td>
        <td>{{$trab->descrip_tipo_trabajador}}</td>
        <td>{{$trab->descrip_area}}</td>
        @if($trab->estado_trabajador==0)
        <td>Desactivado</td>
        @else
        <td>Activado</td>
        @endif
        <td><a href="" data-target="#modal-vermas-{{$trab->DNI_trabajador}}" data-toggle="modal">
                                             <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-eye"></i></button>
</a></td>
      <td><a href="{!! route('trabajador_historial',['var'=>$trab->DNI_trabajador]) !!}">
                                           <button class="bttn-unite bttn-md bttn-royal"><i class="fas fa-book-open"></i></button>
      </a></td>
      <td>
         <a href="" data-target="#modal-edit-{{$trab->DNI_trabajador}}" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
      </td>
      <td>
        @if($trab->estado_trabajador==1)
         <a href="" data-target="#modal-delete-{{$trab->DNI_trabajador}}" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
        @else
        <a href="" data-target="#modal-active-{{$trab->DNI_trabajador}}" data-toggle="modal">
         <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
         @endif
      </td>
      </tr>
      @include('recursos_humanos.trabajador.modaleditar')
      @include('recursos_humanos.trabajador.modaleliminar')
      @include('recursos_humanos.trabajador.modalactivar')
      @include('recursos_humanos.trabajador.modal')
      @endforeach
    </tbody>
  </table>
</div>
@endsection
