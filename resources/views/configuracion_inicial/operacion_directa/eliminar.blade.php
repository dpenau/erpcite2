<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-eliminar-{{$trab->cod_almacen}}">
	{{Form::Open(array('action'=>array('AlmacenController@destroy',$trab->cod_almacen
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Almacen</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Desactivar el Almacen: {{$trab->nom_almacen}}?</p>
        <input type="text" style="display:none" name="email" value="{{$trab->cod_almacen}}">
        <input type="text" style="display:none" name="estado" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
