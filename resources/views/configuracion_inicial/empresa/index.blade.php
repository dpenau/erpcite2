@extends ('layouts.admin')
@section('contenido')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">SISTEMA - EMPRESA</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="x_content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row ">
                                <div class="col-md-12">
                                    <input type="text" id="ruc" name="nombre_empresa_mod" class="form-control col-md-12"
                                        placeholder="Nombre de Empresa*" value="{{ $empresa->nom_empresa }}">
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> RUC: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <input readonly="readonly" type="text" maxlength="11" id="ruc" name="ruc_empresa_mod"
                                        class="form-control" placeholder="RUC*" value="{{ $empresa->RUC_empresa }}"
                                        disabled>

                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Razón Social: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="razon_social" name="razon_social_empresa_mod"
                                        class="form-control" placeholder="Razon Social *"
                                        value="{{ $empresa->razon_social }}" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Tipo de Contribuyente: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <select name="tipo_contribuyente_mod" class="custom-select" disabled>
                                        <option value="{{ $empresa->cod_tipo_contribuyente }}" selected>
                                            {{ $empresa->descrip_tipo_contribuyente }}</option>
                                        @foreach ($tipo_contribuyente as $tipo)
                                            @if ($tipo->cod_tipo_contribuyente != $empresa->cod_tipo_contribuyente)
                                                <option value="{{ $tipo->cod_tipo_contribuyente }}">
                                                    {{ $tipo->descrip_tipo_contribuyente }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Nombre Comercial: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="nombre_comercial" name="nombre_comercial_mod"
                                        class="form-control" placeholder="Nombre Comercial*"
                                        value="{{ $empresa->nombre_comercial }}" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Domicilio: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="domicilio" name="domicilio_mod" class="form-control "
                                        placeholder="Domicilio*" value="{{ $empresa->domicilio }}" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Correo: </p>
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    <input type="email" id="correo" name="correo_mod" class="form-control"
                                        placeholder="Correo" value="{{ $empresa->correo }}" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Celular: </p>
                                    </label>
                                </div>
                                <div class=" input-group col-md-8">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">+51</span>
                                    </div>
                                    <input type="text" maxlength="9" onkeypress="return isNumberKey(event)" id="telefono"
                                        name="telefono_mod" class="form-control" placeholder="Telefono"
                                        value="{{ $empresa->telefono }}" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Pagina Web: </p>
                                    </label>
                                </div>

                                <div class="col-md-8">
                                    <input type="text" id="pagina_web" name="pagina_web_mod" class="form-control"
                                        placeholder="Pagina Web" value="{{ $empresa->pagina_web }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{Html::image('photo/'.$empresa->imagen,'alt',array('width' => 400, 'height' => 400 )) }}
                        </div>
                    </div>

                    <hr class="my-3">

                    <div class="row">
                        <div class="col-6">
                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Tipo de Renta: </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <select name="regimen_renta_mod" class="custom-select" disabled>
                                        <option value="" disabled>Regimen de Renta</option>
                                        <option value="{{ $empresa->cod_regimen_renta }}" selected>
                                            {{ $empresa->descrip_regimen_renta }}</option>
                                        @foreach ($regimen_renta as $regi)
                                            @if ($regi->cod_regimen_renta != $empresa->cod_regimen_renta)
                                                <option value="{{ $regi->cod_regimen_renta }}">
                                                    {{ $regi->descrip_regimen_renta }}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> % de Imp. a la Renta </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="porcentaje_renta" name="porcentaje_renta_mod"
                                        class="form-control" placeholder="Porcentaje Impuesto a la Renta" value=""
                                        disabled>
                                </div>
                                <div class="col-md-4">
                                    <label>
                                        <p> Afecto a: </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="utilidades" name="utilidades_mod" class="form-control"
                                        placeholder="Utilidades" value="" disabled>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Reparticion Utilidades: </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <select name="repaticion_utilidades" class="custom-select" disabled>
                                        <option value="" disabled selected>Opciones</option>
                                        <option value="1" {{$empresa->reparticion_utilidades == 1 ? "selected" : ""}}>Si</option>
                                        <option value="0" {{$empresa->reparticion_utilidades == 0 ? "selected" : ""}}>No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Regimen Laboral: </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <select name="regimen_laboral_mod" class="custom-select" disabled>
                                        <option value="" disabled>Regimen Laboral</option>
                                        <option value="{{ $empresa->cod_regimen_laboral }}" selected>
                                            {{ $empresa->descrip_regimen_laboral }}</option>
                                        @foreach ($regimen_laboral as $reg)
                                            @if ($reg->cod_regimen_laboral != $empresa->cod_regimen_laboral)
                                                <option value="{{ $reg->cod_regimen_laboral }}">
                                                    {{ $reg->descrip_regimen_laboral }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> % Beneficio: </p>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" id="porcentaje_beneficio" name="porcentaje_beneficio_mod"
                                        class="form-control" placeholder="Porcentaje Beneficio" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Prod. Mensual Prom: </p>
                                    </label>
                                </div>
                                <div class="input-group col-md-6">
                                    <input type="text" id="produccion_promedio" name="produccion_promedio"
                                        class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->produccion_promedio) ? $politica_desarrollo->produccion_promedio : ''}}" disabled>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">pares</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Pol. Des. de Producto: </p>
                                    </label>
                                </div>
                                <div class="input-group col-md-6">
                                    <input type="text" id="producto" name="producto"
                                        class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->producto) ? $politica_desarrollo->producto : ''}}" disabled>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">pares</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Pol. Des. de Hormas: </p>
                                    </label>
                                </div>
                                <div class="input-group col-md-6">
                                    <input type="text" id="hormas" name="hormas"
                                        class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->hormas) ? $politica_desarrollo->hormas : ''}}" disabled>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">pares</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-4">
                                    <label>
                                        <p> Pol. Des. de Troqueles: </p>
                                    </label>
                                </div>
                                <div class="input-group col-md-6">
                                    <input type="text" id="troqueles" name="troqueles"
                                        class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->troqueles) ? $politica_desarrollo->troqueles : ''}}" disabled>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">pares</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <hr class="my-3">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <a href="{{ url('configuracion/empresa/configuracion') }}">
                                <input type="button" name="btn" value="Configurar" id="submitBtn" data-toggle="modal"
                                    data-target="#confirm-submits"
                                    class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" />
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
