<div id="modal-edit-{{ $merma->id }}" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog"
    tabindex="-1">
    <form method="POST" action="{{ url('configuracion/merma/update/' . $merma->id) }}">
        <!-- CSRF Token -->
        @method('put')
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Merma</h4>
                </div>
                <div class="modal-body">
                    <input type="text" style="display:none" name="cod_almacen_editar" value="">
                    <div class="form-group  ">
                        <label for="tipo_merma">Tipo de Merma:</label>
                        <input type="text" class="form-control" maxlength="100" name="tipo_merma"
                            value="{{ $merma->tipo_merma }}" required>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12">
                            <label for="">Tipo de Material</label>
                            <select name="subcategoria_id" class="custom-select" required>
                                <option value="" disabled>Subcategorias</option>
                                <option value="{{ $merma->subcategoria_id }}" selected>
                                    {{ $merma->subcategoria->nom_subcategoria }}</option>
                                @foreach ($subcategorias as $item)
                                    @if ($item->cod_subcategoria != $merma->subcategoria_id)
                                        <option value="{{ $item->cod_subcategoria }}">
                                            {{ $item->nom_subcategoria }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="total_orden_compra">% de merma:</label>
                        <input type="text" class="form-control" maxlength="100" name="porcentaje_merma"
                            value="{{ $merma->porcentaje_merma }}" required>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
