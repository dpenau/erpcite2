@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">
    </div>
    <div>
        <h3 class="font-weight-bold pb-3">Configuración - Merma de Materiales
            <a href="" data-target="#modal-create" data-toggle="modal">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Nueva Merma</button></a>
        </h3>
    </div>
    <div class="x_content table-responsive">
        <table id="example" class="display">
            <thead>
                <tr>
                    <th>Tipo de Merma</th>
                    <th>Tipo de Material</th>
                    <th>% de Merma</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($mermas as $merma)
                    <tr>
                        <td>{{ $merma->tipo_merma }}</td>
                        <td>{{ $merma->subcategoria->nom_subcategoria }}</td>
                        <td>{{ $merma->porcentaje_merma }}</td>
                        <td>
                            <a href="" data-target="#modal-edit-{{ $merma->id }}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                        </td>
                        <td>
                            <a href="" data-target="#modal-eliminar-{{ $merma->id }}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                        </td>
                    </tr>
                    @include('configuracion_inicial.merma.modaleditar')
                    @include('configuracion_inicial.merma.modaleliminar')
                @endforeach
                @include('configuracion_inicial.merma.modalcrear')
            </tbody>
        </table>
    </div>
@endsection
