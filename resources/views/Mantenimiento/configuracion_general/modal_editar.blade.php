<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$lab->codigo_valor}}">
	{{Form::Open(array('action'=>array('ConfiguracionGeneralController@update',$lab->codigo_valor
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Valor</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="codigo_valor" value="{{$lab->codigo_valor}}">
        <div class="form-group  ">
          <label for="total_orden_compra">Nombre:</label>
           <input type="text" name="nombre" class="form-control"  value="{{$lab->nombre}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Valor:</label>
           <input type="text" name="valor" class="form-control" name="descripcion" value="{{$lab->valor}}">
        </div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
