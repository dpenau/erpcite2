@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Valores Generales <a href="configuracion/create">
      <button class="bttn-unite bttn-md bttn-success ">Nuevo Valor</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Valor</th>
                      <th>Editar</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($areas as $lab)
                    <tr >
                        <td>{{$lab->nombre}}</td>
                        <td>{{$lab->valor}}</td>
                        <td>
                           <a href="" data-target="#modal-edit-{{$lab->codigo_valor}}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                        </td>
                   	</tr>
                    @include('Mantenimiento.configuracion_general.modal_editar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
