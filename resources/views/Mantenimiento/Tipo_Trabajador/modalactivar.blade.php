<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-active-{{$trab->cod_tipo_trabajador}}">
	{{Form::Open(array('action'=>array('TipoTrabajadorController@destroy',$trab->cod_tipo_trabajador
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Activar Tipo de trabajador</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_tipo_trabajador_eliminar" value="{{$trab->cod_tipo_trabajador}}">
        <input type="text" style="display:none" name="accion" value="1">
				<p>Esta seguro que desea Activar la tipo de trabajador: {{$trab->descrip_tipo_trabajador}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
