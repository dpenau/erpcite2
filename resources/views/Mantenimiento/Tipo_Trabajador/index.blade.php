@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Tipo Trabajador <a href="Tipo_Trabajador/create">
      <button class="bttn-unite bttn-md bttn-success ">Nuevo Tipo Trabajador</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($trabajadores as $trab)
                    @if($trab->estado_tipo_trabajador==0)
                    <tr class="bg-danger">
                      @endif
                    	<td>{{$trab->descrip_tipo_trabajador}}</td>
                      <td>
                         <a href="" data-target="#modal-edit-{{$trab->cod_tipo_trabajador}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                      </td>
                      <td>
                        @if($trab->estado_tipo_trabajador==1)
                         <a href="" data-target="#modal-delete-{{$trab->cod_tipo_trabajador}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                        @else
                        <a href="" data-target="#modal-active-{{$trab->cod_tipo_trabajador}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                        @endif
                      </td>
                    </tr>
                    @include('Mantenimiento.Tipo_Trabajador.modalactivar')
                    @include('Mantenimiento.Tipo_Trabajador.modaleditar')
                    @include('Mantenimiento.Tipo_Trabajador.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
