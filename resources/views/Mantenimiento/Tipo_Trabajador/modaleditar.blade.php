<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$trab->cod_tipo_trabajador}}">
	{{Form::Open(array('action'=>array('TipoTrabajadorController@update',$trab->cod_tipo_trabajador
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Subcategoria</h4>
			</div>
			<div class="modal-body">
        <input type="text" name="cod_tipo_trabajador_editar" style="display:none" value="{{$trab->cod_tipo_trabajador}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo del rol:</label>
           <input type="text" class="form-control" disabled value="{{$trab->cod_tipo_trabajador}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Nombre del rol:</label>
           <input type="text" class="form-control" name="nombre" value="{{$trab->descrip_tipo_trabajador}}">
        </div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
