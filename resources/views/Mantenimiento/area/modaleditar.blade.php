<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$ar->cod_area}}">
	{{Form::Open(array('action'=>array('AreaController@update',$ar->cod_area
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Area</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_area_editar" value="{{$ar->cod_area}}">
        <div class="form-group  ">
          <label for="total_orden_compra">Codigo de Area:</label>
           <input type="text" class="form-control" disabled value="{{$ar->cod_area}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de Area:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$ar->descrip_area}}">
        </div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
