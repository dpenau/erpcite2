<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$contr->cod_tipo_contribuyente}}">
	{{Form::Open(array('action'=>array('TipoContribuyenteController@update',$contr->cod_tipo_contribuyente
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Tipo de Contribuyente</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_contribuyente_editar" value="{{$contr->cod_tipo_contribuyente}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo de tipo de Contribuyente:</label>
           <input type="text" class="form-control" disabled value="{{$contr->cod_tipo_contribuyente}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de tipo de Contribuyente:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$contr->descrip_tipo_contribuyente}}">
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
