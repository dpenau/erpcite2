@extends('layouts.app')
@section ('content')
<div class="container">


    <div>
    <h3 class="font-weight-bold">Tipo de proveedores <a href="tipo_proveedor/create">
      <button class="bttn-slant bttn-md bttn-success ">Nueva opcion</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="table_mp" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Tipo de proveedor</th>
                      <th>Rubro</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($tipo as $clas)
                    <tr>
                    	<td>{{$clas->descrip_tipo_proveedor}}</td>
                      <td>{{$clas->rubro_proveedor}}</td>
                      <td>
                         <a href="" data-target="#modal-modificar-{{$clas->cod_tipo_proveedor}}" data-toggle="modal">
                          <button class="bttn-slant bttn-md bttn-warning">Modificar</button></a>
                      </td>
                   	</tr>
                    @include('Mantenimiento.tipo_proveedor.modal')
                    @endforeach
                  </tbody>
                </table>
              </div>
              {{$tipo->render()}}
              </div>
@endsection
