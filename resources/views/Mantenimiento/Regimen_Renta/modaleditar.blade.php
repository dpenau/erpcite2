<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$rent->cod_regimen_renta}}">
	{{Form::Open(array('action'=>array('RegimenRentaController@update',$rent->cod_regimen_renta
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Regimen renta</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_regimenr_editar" value="{{$rent->cod_regimen_renta}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo de regimen renta:</label>
           <input type="text" class="form-control" disabled value="{{$rent->cod_regimen_renta}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de regimen renta:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$rent->descrip_regimen_renta}}">
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
