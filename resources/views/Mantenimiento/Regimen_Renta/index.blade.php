@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Régimen Renta <a href="Regimen_Renta/create">
      <button class="bttn-unite bttn-md bttn-success ">Nuevo Régimen Renta</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($rentas as $rent)
                    @if($rent->estado_regimen_renta==0)
                    <tr class="bg-danger">
                    @endif
                    	<td>{{$rent->descrip_regimen_renta}}</td>
                      <td>
                         <a href="" data-target="#modal-edit-{{$rent->cod_regimen_renta}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                      </td>
                      <td>
                        @if($rent->estado_regimen_renta==1)
                         <a href="" data-target="#modal-delete-{{$rent->cod_regimen_renta}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                        @else
                        <a href="" data-target="#modal-active-{{$rent->cod_regimen_renta}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                        @endif
                      </td>
                    </tr>
                    @include('Mantenimiento.Regimen_Renta.modalactivar')
                    @include('Mantenimiento.Regimen_Renta.modaleditar')
                    @include('Mantenimiento.Regimen_Renta.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
