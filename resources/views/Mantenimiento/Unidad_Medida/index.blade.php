@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Unidad de Medida <a href="Unidad_Medida/create">
      <button class="bttn-unite bttn-md bttn-success ">Nueva Unidad de Medida</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Magnitud</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($medida as $lab)
                    @if($lab->estado_unidad_medida==0)
                    <tr class="bg-danger">
                      @endif
                    	<td>{{$lab->unidad}}</td>
                        <td>{{$lab->descrip_unidad_medida}}</td>
                        <td>{{$lab->magnitud_unidad_medida}}</td>
                        <td>
                           <a href="" data-target="#modal-modificar-{{$lab->cod_unidad_medida}}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                        </td>
                        <td>
                          @if($lab->estado_unidad_medida==1)
                           <a href="" data-target="#modal-delete-{{$lab->cod_unidad_medida}}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                          @else
                          <a href="" data-target="#modal-active-{{$lab->cod_unidad_medida}}" data-toggle="modal">
                           <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                          @endif
                        </td>
                   	</tr>
                    @include('Mantenimiento.Unidad_Medida.modalactivar')
                    @include('Mantenimiento.Unidad_Medida.modal_modificar')
                    @include('Mantenimiento.Unidad_Medida.modal_eliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
