<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$role->id}}">
	{{Form::Open(array('action'=>array('RolesController@destroy',$role->id
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar regimen renta</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_rol_eliminar" value="{{$role->id}}">
				<input type="text" style="display:none" name="accion" value="0">
				<p>Esta seguro que desea desactivar el rol: {{$role->name}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
