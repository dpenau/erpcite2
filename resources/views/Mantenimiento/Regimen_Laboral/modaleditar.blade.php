<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$lab->cod_regimen_laboral}}">
	{{Form::Open(array('action'=>array('RegimenLaboralController@update',$lab->cod_regimen_laboral
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Regimen Laboral</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_regimen_editar" value="{{$lab->cod_regimen_laboral}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo de regimen laboral:</label>
           <input type="text" class="form-control" disabled value="{{$lab->cod_regimen_laboral}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de regimen laboral:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$lab->descrip_regimen_laboral}}">
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
