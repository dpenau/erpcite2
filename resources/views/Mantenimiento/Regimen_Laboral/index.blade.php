@extends('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Régimen Laboral <a href="Regimen_Laboral/create">
      <button class="bttn-unite bttn-md bttn-success ">Nuevo Régimen Laboral</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($laboral as $lab)
                    @if($lab->estado_regimen_laboral==0)
                    <tr class="bg-danger">
                    @endif
                    	<td>{{$lab->descrip_regimen_laboral}}</td>

                      <td>
                         <a href="" data-target="#modal-edit-{{$lab->cod_regimen_laboral}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                      </td>
                      <td>
                        @if($lab->estado_regimen_laboral==1)
                         <a href="" data-target="#modal-delete-{{$lab->cod_regimen_laboral}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                        @else
                        <a href="" data-target="#modal-active-{{$lab->cod_regimen_laboral}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                        @endif
                      </td>
                    </tr>
                    @include('Mantenimiento.Regimen_Laboral.modalactivar')
                    @include('Mantenimiento.Regimen_Laboral.modaleditar')
                    @include('Mantenimiento.Regimen_Laboral.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
