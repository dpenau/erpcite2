@extends ('layouts.admin')
@section ('contenido')
<script type="text/javascript">
let modelitos = @json($modelos);
let grupitos = @json($grupos);
let procesitos = @json($procesos);
let produccion = @json($produccion);
</script>
<div class="preloader">
</div>
<div>
    <h1 class="font-weight-bold">Nuevo Programa de Producción</h1>
    <hr style="border:3px solid:blank">
</div>
<br>
<div class="row">
    <div class="form-group col-md-5">
        <div class="input-group mb-9">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Orden de Producción</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
                <option selected>Seleccionar Orden</option>
                <script type="text/javascript">
                for (var i = 0; i < produccion.length; i++) {
                    document.write("<option value=\"" + produccion[i].codigo_orden_pedido_produccion + "\">" + produccion[i].codigo_orden_pedido_produccion + "</option>");
                }
                </script>
            </select>
        </div>
    </div>
</div>
<!--style="height: 10rem;-->
<div class="row">
    <div class="col-sm-5">
        <div class="card" style="height: 45rem;">
            <div class="card-body">
                <center><h5 class="card-title">Datos Orden de Producción</h5></center>
                <form>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Cliente</label>
                        <div class="col-sm-9">
                            <label for="" id="cliente"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Modelo</label>
                        <div class="col-sm-9">
                            <label for="" id="model"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Tallas</label>
                        <div class="col-sm-4">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Total:</label>
                        <div class="col-sm-9">
                            <label for="" id="cantidades"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Estado</label>
                        <div class="col-sm-9">
                            <label for="" id="estado"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Materiales</label>
                        <div class="col-sm-9">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">N°</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Gamuza</td>
                                        <td>2</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>plantilla</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>horma</td>
                                        <td>12</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>hileras</td>
                                        <td>24</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col align-self-start">
                            </div>
                            <div class="col align-self-center">
                            </div>
                            <div class="col align-self-end">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <button id="act" class="bttn-unite bttn-md bttn-success float-right mr-sm-6">Incluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="card" style="height: 45rem;">
            <div class="card-body">
                <center><h5 class="card-title">Cronograma Conforme Materiales</h5></center>
                <div class="">
                    <table id="example" class="display">
                        <thead>
                            <tr>
                                <th>Modelo</th>
                                <th>Proceso</th>
                                <th>Especialidad</th>
                                <th>Tiempo</th>
                                <th>Grupo</th>
                            </tr>
                        </thead>
                        @foreach($orden_data as $orden_data)
                        <tbody class="tabladatos">
                            <td id="first-child">{{$orden_data->codigo_modelo}}</td>
                            <td id="second-child">{{$orden_data->proceso}}</td>
                            <td>{{$orden_data->especialidad}}</td>
                            <td>{{$orden_data->tiempo}}</td>
                            <td>{{$orden_data->codigo_grupo_trabajo}}</td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-7"></div>
    <div class="col-2.5">
        <a href="{!! route('tiempo_descargar') !!}" target="_blank"></a>
        <button class="bttn-unite bttn-md bttn-warning mr-sm-5">Crear Grupos</button><a href="#"><a href="#" target="_blank"></a>
    </div>
    <div class="col-2.5">
        <a href="{!! route('tiempo_descargar') !!}" target="_blank"></a>
        <button class="bttn-unite bttn-md bttn-danger mr-sm-5">Validar Grupos</button><a href="#"><a href="#" target="_blank"></a>
    </div>
</div>
<br>

<!--Script para el llenado de las etiqetas mediante un selector-->
<script type="text/javascript">
$("#inputGroupSelect01").change(function(){
    var valor=$("#inputGroupSelect01").val();
    @foreach($produccion as $prod)
    var tmp="{{$prod->codigo_orden_pedido_produccion}}";
    if(valor==tmp)
    {
        $("#cliente").text("{{$prod->RUC_empresa}}");
        $("#model").text("{{$prod->codigo_serie_articulo}}");
        $("#estado").text("{{$prod->estado_orden_pedido_produccion}}");
        $("#cantidades").text("{{$prod->cantidades}}");
    }
    @endforeach
});
</script>

<script type="text/javascript">
//Begin filtro ingreso total
$("#cb_ingreso_total_pedidos").on("click", function() {

    $('td:nth-child(2)').toggle();
    $(".ingreso_total").toggle();
});
//End filtro ingreso total
//Begin filtro ingreso total
$("#cb_frec_pedidos").on("click", function() {

    $('td:nth-child(3)').toggle();
    $(".frecuencia").toggle();
});
//End filtro ingreso total
//Begin filtro ingreso total
$("#cb_deuda").on("click", function() {

    $('td:nth-child(4)').toggle();
    $(".deuda").toggle();
});

//End filtro ingreso total

$(document).ready(function() {

    var t = $("#tabla_reporte_clientes").DataTable();



    $(document).on('click', '#exportar_reporte_clientes', function() {

        var cb_ing_tot_ped = $('#cb_ingreso_total_pedidos').prop('checked');
        var cb_fre_ped = $('#cb_frec_pedidos').prop('checked');
        var cb_deu = $('#cb_deuda').prop('checked');
        var data=cb_ing_tot_ped+"-"+cb_fre_ped+"-"+cb_deu;
        window.open('/Ventas/Reportes/ReporteClientes/exportar/'+data)


    });

})
</script>

<script type="text/javascript">
//modelo
$(document).ready(function($) {
    $('table').show();
    $('#selectModel1').change(function() {
        $('table').show();
        var selection = $(this).val();
        if (selection === '-Todos-') {
            $('tr').show();
        }
        else {
            var dataset = $('#example .tabladatos').find('tr');
            // show all rows first
            dataset.show();
        }
        // filter the rows that should be hidden
        dataset.filter(function(index, item) {
            return $(item).find('#first-child').text().split(',').indexOf(selection) === -1;
        }).hide();
    });
});
// proceso
$(document).ready(function($) {
    $('table').show();
    $('#selectModel2').change(function() {
        $('table').show();
        var selection = $(this).val();
        if (selection === '-Todos-') {
            $('tr').show();
        }
        else {
            var dataset = $('#example .tabladatos').find('tr');
            // show all rows first
            dataset.show();
        }
        // filter the rows that should be hidden
        dataset.filter(function(index, item) {
            return $(item).find('#second-child').text().split(',').indexOf(selection) === -1;
        }).hide();
    });
});
// grupo
$(document).ready(function () {

    //
    var $ddl = $('#selectModel').on('change', search);
    var $tblRows = $("#example > tbody > tr");

    //
    function search()
    {
        //
        var ddlIndex = $ddl.prop('selectedIndex');
        var ddlText = $ddl.val();
        //Reset
        if (ddlIndex == 0 )
        {
            //show all
            $tblRows.show();
        }
        else
        {
            //
            var lastCell;
            var firstCell;

            //browse over each row
            $($tblRows).each(function (i, row)
            {
                //get content of the first cell and last cell
                firstCell = $(this).find(':first-child').text().trim();
                lastCell = $(this).find(':last-child').text().trim();

                //
                if (ddlText == lastCell)
                {

                    $(this).show();
                }
                else
                $(this).hide();
            });

        }
    }
});
</script>

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
