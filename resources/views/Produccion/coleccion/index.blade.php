@extends ('layouts.admin')
@section ('contenido')
<div>
<h3 class="font-weight-bold">Coleccion de calzado <a href="coleccion/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Coleccion de calzado</button></a></h3>
</div>
<div class="container">
<br>
<div class="x_content table-responsive">
      <table id="example" class="display" style="width:100%">
    <thead>
      <tr>
        <th></th>
        <th>Nombre Coleccion</th>
        <th>Editar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
      @foreach($coleccion as $col)
      <tr>
          <td>
        @if($col->estado_coleccion==0)
            <div class="bg-danger" style="height:20px; width:20px"></div>
        @endif
          </td>
          <td>{{$col->nombre_coleccion}}</td>
          <td>
            <a class="b_act" id="{{$col->codigo_coleccion}}"><button class="bttn-unite bttn-md bttn-warning " ><i class="far fa-edit"></i></button></a>
          </td>
          @if($col->estado_coleccion==0)
          <td>
            <a class="b_acc" id="{{$col->codigo_coleccion}}"><button class="bttn-unite bttn-md bttn-success " ><i class="fas fa-check-circle"></i></button></a>
          </td>
          @else
          <td>
            <a class="b_acc" id="{{$col->codigo_coleccion}}"><button class="bttn-unite bttn-md bttn-danger " ><i class="fas fa-trash-alt"></i></button></a>
          </td>
          @endif
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
  </div>
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Editar Coleccion</h4>
              </div>
              <div class="modal-body">
                {{Form::Open(array('action'=>array('ColeccionCalzadoController@update','1'
                ),'method'=>'patch'))}}
                <input type="text" id="cod_coleccion" style="display:none" name="cod_coleccion">
                <div class="form-group">
                  <label for="total_orden_compra">Nombre de la coleccion:</label>
                   <input type="text" id="nombre" class="form-control" maxlength="100" name="nombre">
                </div>
              </div>
              <div class="modal-footer">
        				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
        				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
        			</div>
              {{Form::Close()}}
          </div>
      </div>
  </div>
  <div class="modal fade" id="modal_accion" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="titulo"></h4>
              </div>
              <div class="modal-body">
                {{Form::Open(array('action'=>array('ColeccionCalzadoController@destroy','2'
                ),'method'=>'delete'))}}
                <p id="mensaje"></p>
                <input type="text" id="cods" style="display:none" name="codigo">
                <input type="text" id="cod_acc" style="display:none" name="accion">
                </div>
              <div class="modal-footer">
        				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
        				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
        			</div>
              {{Form::Close()}}
          </div>
      </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">
  $(document).ready( function () {
    var data =<?php echo $coleccion;?>;
    $(".b_act").on('click',function(){
      let indice=buscar($(this).attr('id'));
      modal_editar(indice)
    })
    $(".b_acc").on('click',function(){
      let indice=buscar($(this).attr('id'));
      let estado=0;
      if(data[indice].estado_coleccion==0)
      {
        estado=1;
      }
      modal_accion(indice,estado);
    })
    function modal_editar(index)
    {
      $("#cod_coleccion").val(data[index].codigo_coleccion);
      $("#nombre").val(data[index].nombre_coleccion);
      $("#myModal").modal({show:true});
    }
    function buscar(id)  {
      var index = -1;
      var filteredObj = data.find(function(item, i){
        if(item.codigo_coleccion == id){
          index = i;
          return index;
        }
      });
      return index;
    }
    function modal_accion(index,acc)  {
      switch (acc) {
        case 0: $("#titulo").text("Desactivar Coleccion");
        $("#mensaje").text("Esta seguro que desa desactivar la coleccion ?");
          break;
        case 1:$("#titulo").text("Activar Coleccion");
        $("#mensaje").text("Esta seguro que desa activar la coleccion ?");
        break;
        default:
      }
      $("#cods").val(data[index].codigo_coleccion);
      $("#cod_acc").val(acc);
      $("#modal_accion").modal({show:true});
    }
  });
  </script>
@endsection
