<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
            id="modal-delete-{{$op->cod_operacion_directa}}">
            {{Form::Open(array('action'=>array('OperacionDirectaController@destroy','2'),'method'=>'delete'))}}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titulo_elim">Eliminar Operación Directa de la Empresa</h4>
                    </div>
                    <div class="modal-body" style="text-align: left">
                        <p>¿Esta seguro que desea eliminar esta operación directa?</p>
                        <input type="text" style="display:none" name="codigo" id="codigo"
                            value="{{$op->cod_operacion_directa}}">
                        <input type="text" style="display:none" name="estado" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger"
                            data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
            {{Form::Close()}}
        </div>
