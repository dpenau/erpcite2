<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-edit-{{$modelos->cod_combinacion}}">
    {{Form::Open(array('action'=>array('CombinacionCalzadoController@update',$modelos->cod_combinacion),'files'=>true,'method'=>'patch'))}}
    <div class="modal-dialog" style="max-width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_mod">Modificar Combinación: </h4>
                <input type="hidden" id="{{$modelos->cod_combinacion}}" value="{{$modelos->cod_combinacion}}"
                    name="codigo_combinacion">
            </div>
            <div class="modal-body">
                <div class="row" style="margin: 2% 3% 0 3%;">
                    <div class="col-4" style="text-align: left;">
                        <label>Colección:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Líneas:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Serie:</label>
                    </div>
                </div>
                <div class="row" style="margin: 0 3% 0 3%;">
                @foreach ($LineasModelos as $modeloPrueba)
                    <input type="hidden" id="{{$modeloPrueba->cod_modelo}}" value="{{$modelos->cod_modelo}}"
                        name="codigo">
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " value=" {{$modeloPrueba->nombre_coleccion}}"  readonly />
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control "  value=" {{$modeloPrueba->nombre_linea}}"  readonly />

                    </div>
                    <div class="col-4" style="text-align: left;">
                   
                        <label type="text" class="form-control " readonly>
                            {{$modeloPrueba->nombre_serie}}
                        </label>
                   
                    </div>
                @endforeach
                </div>
                <div class="row" style="margin:2% 3% 0 3%;">
                    <div class="col-4" style="text-align: left;">
                        <label>Código Modelo Base:</label>
                    </div>
                    <div class="col-4" style="text-align: left;">
                        <label>Descripción de Modelos:</label>
                    </div>

                </div>
                <div class="row" style="margin: 0 3% 3% 3%;">
                    <div class="col-4" style="text-align: left;">
                        <input type="text" class="form-control " name="codCom"   value=" {{$modelos->codigo_comb}}"/>
                    </div>
                    <div class="col-8" style="text-align: left;">
                        <input type="text" class="form-control " name="descripCom" value=" {{$modelos->descripcion}}" />
                    </div>
                </div>
                <div class="row" style="margin: 0 3% 3% 5%;">
                    <div class="btn-small amber darken-s">
                        <label for="exampleFormControlFile1">Imagen de modelo:</label>
                        <input type="file" class="form-control-file" id="photo" name="photo"
                            onchange="vista_preliminar(event)">
                    </div>
                    <div>
                        <img src="" alt="" id="img-foto" width="110" height="110" style="margin-left:6%;"> </img>
                    </div>
                </div>

            </div>
            <div class="modal-footer" style="text-align:left;">
                <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-1">Guardar</button>
                <button type="button" id="cancelarModal"
                    class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-1">Cancelar</button>

            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>