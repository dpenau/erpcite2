<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-conf-{{$modelos->cod_combinacion}}">
    {{Form::Open(array('action'=>array('CombinacionFichaCalzadoController@proceso'),'method'=>'POST'))}}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_elim">¿Esta seguro que desea hacer modificaciones?</h4>
                <input type="hidden" name='codigo' value="{{$modelos->cod_combinacion }}" >
                <input type="hidden" name='codigo_modelo' value="{{$modelos->cod_modelo }}" >
            </div>
            <div class="modal-body">
                <p>Si acepta esta combinación de modelo retornará a un estado EN PROCESO</p>

            </div>
            <div class="modal-footer">
              <button type="submit" class="bttn-unite bttn-md bttn-primary">Aceptar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>
