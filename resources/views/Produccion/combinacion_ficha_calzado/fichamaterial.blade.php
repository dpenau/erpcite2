@extends ('layouts.admin')
@section ('contenido')
<div style="text-align: center;">

    <h3 class="font-weight-bold">Agregar Materiales</h3>
</div>
{{Form::Open(array('action'=>array('CombinacionFichaCalzadoController@create'),'method'=>'POST'))}}
<div style="margin: 0 4% 5% 4%;">
    <input type="hidden" name='codigo' value="{{$id_combinacion}}" >
    <div class="row" style="margin-bottom:2%;">
        <div class="col-3" style="text-align: left;">
            <label>Proceso:</label>
            <select name="proceso"  class="custom-select" id="proceso">
                <option value="" selected disabled>Seleccionar Proceso</option>
                @foreach ($proceso as $col)
                <option value="{{$col->cod_proceso}}">{{$col->codigo}}-{{$col->nombre}}</option>
                @endforeach
            </select>

        </div>
        <div class="col-2">

        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <label>Categoria:</label>
            <select required='required' class="custom-select" id="categoria">
                <option value="0" selected disabled>Seleccionar Categoria</option>
                @foreach ($categoria as $col)
                <option value="{{$col->cod_categoria}}">{{$col->nom_categoria}}</option>
                @endforeach
            </select>

        </div>
        <div class="col-3">
            <label>Subcategoria:</label>
            <select required='required' class="custom-select" id="subcategoria">
                <option value="" selected disabled>Seleccionar Subcategoria</option>

            </select>

        </div>
        <div class="col-4">
            <div class="multiselect">
                <label>Material:</label>
                <div class="selectBox" onclick="showCheckboxes()">
                    <select id="material" class="custom-select">
                        <option value="" selected disabled>Seleccionar Materiales</option>
                    </select>
                    <div class="overSelect"></div>
                </div>
                <div id="checkboxes" style="overflow-y:scroll;height:150px">
                </div>
            </div>

        </div>
        <div class="col-2" style="margin:2% 0 2% 0;">

            <a href="#">
                <button id="agregarMaterial" type="button"
                    class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar</button></a>
        </div>
    </div>

    <br>

</div>
<div style="margin: 0 4% 0 4%;">
    <input name="id_combinacion_c" type="hidden" value="{{$id_combinacion}}">

    <div class="x_content table-responsive">
        <table id="tablaMaterial">
            <thead align="center">
                <tr>

                    <th>N°</th>
                    <th>Proceso</th>
                    <th>Material</th>
                    <th>Consumo por par</th>
                    <th>Unidad de Compra</th>
                    <th>Costo por Unidad de Medida</th>
                    <th>Costo por Par</th>
                    <th></th>
                </tr>
            </thead>
            <tbody align="center">

            </tbody>
        </table>
        <input type="hidden" id="total_mat" name="total_mat">
    </div>
</div>
<div class="row" style="text-align: center;">
    <div class="col-12">
        <a href="/Produccion/combinacion_ficha/ficha_producto/{{ $id_combinacion }}">
            <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" style="margin: 2%;">Guardar</button>
            <button type="button" class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar</button>
        </a>
    </div>

</div>

{{Form::Close()}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
</script>
<style>
.button-md {
    border-color: #212121;
    background-color: #B0BEC5;

}

.button-mo {
    border-color: #212121;
    background-color: #FFFFFF;
}

.selectBox {
    position: relative;
}

.selectBox select {
    width: 100%;

}

.overSelect {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}

#checkboxes {
    display: none;
    border: 1px #dadada solid;

}

#inBox {
    margin-left: 2%;
}

#checkboxes label {
    display: block;
}

#checkboxes label:hover {
    background-color: #1e90ff;
}
</style>
<script type="text/javascript">
var t = $('#tablaMaterial').DataTable({
    "lengthMenu": [
        [100, -1],
        [100, "All"]
    ],
    'columnDefs': [{
        'targets': 0,
        'checkboxes': {
            'selectRow': true
        }
    }],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    }
});
var cat = <?php echo $categoria;?>;
var total_materiales =0;
var subcat = <?php echo $subcategoria;?>;
var materiales = <?php echo $materiales;?>;
//var proceso = <?php echo $proceso;?>;
var proceso =@json($proceso);
var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}

var subcategoria_listado = "";
var material_listado = "";

$("#categoria").change(function() {

    subcategoria_listado = subcat.filter(datas => datas.cod_categoria == this.value);

    $('#subcategoria').empty();
    $('#material').empty();
    $('#checkboxes').empty();
    $('#material').append(` <option value="" selected disabled>Seleccionar Materiales</option>`);
    $('#subcategoria').append(` <option value="" selected disabled>Seleccionar Subcategoria</option>`);
    for (var i = 0; i < subcategoria_listado.length; i++) {

        $('#subcategoria').append(`<option value="${subcategoria_listado[i].cod_subcategoria}">
                                ${subcategoria_listado[i].nom_subcategoria}
                            </option>`);

    }

});

$("#subcategoria").change(function() {

    material_listado = materiales.filter(datas => datas.cod_subcategoria == this.value);

    $('#material').empty();
    $('#checkboxes').empty();
    $('#material').append(` <option value="" selected disabled>Seleccionar Materiales</option>`);
    for (var i = 0; i < material_listado.length; i++) {

        $('#checkboxes').append(`<input name='materiales' type="checkbox" value="${material_listado[i].cod_material}">
                                ${material_listado[i].descrip_material}</input><br>
                          `);
    }


});
let contador = 1;
$("#agregarMaterial").on("click", function() {
    //t.rows().remove().draw();

    let process = proceso.filter(datas => datas.cod_proceso == $("#proceso").val());
    var sel = document.getElementById("proceso");
    var checkboxes2 = document.getElementById("checkboxes");
    checkboxes2.style.display = "none";
    expanded = false;
    if(sel.selectedIndex==0){
        alert("ELIJA UN PROCESO PARA PODER AGREGAR MATERIALES");
    }else{
       // sel.remove(sel.selectedIndex);
        $("#proceso").prop("selectedIndex", 0);
        let checkboxes = document.querySelectorAll('input[name="materiales"]:checked');
        let values = [];
        checkboxes.forEach((checkbox) => {
            values.push(checkbox.value);
        });
        console.log(values);

        $.each(materiales, function(key, value) {
            $.each(values, function(key, valor) {
                if (value.cod_material == valor) {

                    const tr = $(
                        "<tr>" +
                        "<td>" + contador + "</td>" +
                        "<td> <input name='process[]' type='hidden' value='" + process[0].cod_proceso + "' >" + process[0].nombre + "</td>"+
                        "<td> <input name='material[]' type='hidden' value='" + value
                        .cod_material + "' >" + value.descrip_material + "</td>" +
                        "<td><input name='cantidad_material[]'  type='number'  min='0' step='0.0001'  id='consumo" +
                        value.cod_material + "-" + process[0].nombre +
                        "'style='width : 90%;'required ></td>" +
                        "<td>" + value.descrip_unidad_compra + "</td>" +
                        "<td>S/." + value.costo_con_igv_material + "</td>" +
                        "<td>S/.<input name='cantidad_total[]'  type='number' value='0' min='0' step='0.0001'  id='costo_total" +
                        value.cod_material + "-" + process[0].nombre +
                        "'style='width : 80%;'required readonly ></td>" +
                        "<td><button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button></td></tr>"

                    );
                    contador++;
                    t.row.add(tr[0]).draw();
                    $('#consumo' + value.cod_material + '-' + process[0].nombre).change(function() {

                        $('#costo_total' + value.cod_material + '-' + process[0].nombre)
                            .val($(this).val() * value.costo_con_igv_material);
                        //   console.log(  $('#costo_total' + value.cod_material).val());
                        total_materiales=total_materiales+ $('#costo_total' + value.cod_material + '-' +process[0].nombre).val()*1 ;
                        console.log(total_materiales);
                        $('#total_mat').val( total_materiales.toFixed(4));
                        console.log( $('#total_mat').val());
                    });


                }

            });
        });
    }

});
$('#tablaMaterial').on('click', 'button', function() {
    t
        .row($(this).parents('tr'))
        .remove()
        .draw();

});

let checkboxes = document.querySelectorAll('input[name="cantidad_material"]');
let values = [];
checkboxes.forEach((checkbox) => {
    values.push(checkbox.value);
});
console.log(values);
</script>
@endsection
