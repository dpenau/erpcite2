@extends ('layouts.admin')
@section ('contenido')
<div style="text-align: center;">

    <h3 class="font-weight-bold">Modelos Ficha </h3>
</div>
<div class="row" style="margin: 2% 3% 0 3%;">
    <div class="col-4" style="text-align: left;">
        <label>Colección:</label>
    </div>
    <div class="col-4" style="text-align: left;">
        <label>Líneas:</label>
    </div>
    <div class="col-4" style="text-align: left;">
        <label>Serie:</label>
    </div>
</div>
<div class="row" style="margin: 0 3% 0 3%;">
    @foreach ($LineasModelos as $modelos)
    <div class="col-4" style="text-align: left;">
        <input type="text" class="form-control " value="{{$modelos->nombre_coleccion}}" readonly />
    </div>
    <div class="col-4" style="text-align: left;">
        <input type="text" class="form-control " value="{{$modelos->nombre_linea}}" readonly />
    </div>
    <div class="col-4" style="text-align: left;">
        <input type="text" class="form-control " value="{{$modelos->nombre_serie}}" readonly />
    </div>
    @endforeach
</div>
<div class="row" style="margin:2% 3% 0 3%;">
    <div class="col-4" style="text-align: left;">
        <label>Código Modelo Base:</label>
    </div>
    <div class="col-4" style="text-align: left;">
        <label>Descripción de Modelos:</label>
    </div>

</div>
<div class="row" style="margin: 0 3% 3% 3%;">
    <div class="col-4" style="text-align: left;">
        @foreach ($LineasModelos as $modelos)
        <input type="text" class="form-control " value="{{$modelos->codigo}}" readonly />
        @endforeach
    </div>
    <div class="col-8" style="text-align: left;">
        @foreach ($LineasModelos as $modelos)
        <input type="text" class="form-control " value="{{$modelos->descripcion}}" readonly />
        @endforeach
    </div>
</div>

<div class="row" style="margin: 0 0 0 3%;">
    <div class="form-group  col-md-5  col-xs-12">

        <select name="modelo_activo" id="modelo_activo" class="custom-select">
            <option value="" selected disabled>Seleccione Combinaciones en Proceso o Aprobadas</option>
            <option value="all">Todos</option>
            <option value="proceso">En Proceso</option>
            <option value="aprobada">Aprobadas</option>
        </select>

    </div>
    <div class="col-md-7  col-xs-12" id="create-combinacion" style="margin: 0 0 4% 0;">
        @foreach ($LineasModelos as $modelos)
        <!-- VISTA A OTRA PESTAÑA

  <a class="modal-crear-combinacion" id="{{ $modelos->cod_modelo}}" href="/Produccion/combinacion_calzado/create/{{$modelos->cod_modelo}}">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo modelo de calzado</button></a>
  <br>

VISTA PARA QUE APAREZCA MODAL-->

        <br>
        @endforeach
    </div>
</div>

<div style="margin: 0 4% 0 4%;">
    <div class="x_content table-responsive">
        <table id="tablaModelos">
            <thead align="center">
                <tr>
                    <th>Imagen</th>
                    <th>Código de Modelo</th>
                    <th>Descripción de Modelo</th>
                    <th>Ficha de Producto</th>
                    <th>PDF</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody align="center">
                @foreach ($listaCombinacion as $modelos)
                <tr>
                    <td width="10" height="10">
                        {{Html::image('photo/modelos/'.$modelos->RUC_empresa.'/'.$modelos->image,'alt',array('width' => 70, 'height' => 70 )) }}
                    </td>
                    <td> {{ $modelos->codigo_comb }} </td>
                    <td> {{ $modelos->descripcion }} </td>

                    <td>
                    @if ($modelos->aprobado == 1)
                        <a class="a" id=""
                        href="#" data-target="#modal-conf-{{$modelos->cod_combinacion}}"
                        data-toggle="modal">
                            <button class="bttn-unite bttn-md btn bttn-royal"><i class="fa fa-clipboard-list"></i></button></a>
                    @else
                        <a class="a" id="{{ $modelos->cod_combinacion}}"
                        href="/Produccion/combinacion_ficha/ficha_producto/{{$modelos->cod_combinacion}}">
                            <button class="bttn-unite bttn-md btn bttn-royal"><i class="fa fa-clipboard-list"></i></button></a>
                     @endif
                        </td>
                    <td>

                         <a class="generarPDFTalla" id="{{ $modelos->cod_combinacion}}" href="" data-target="#modal-edit-" data-toggle="modal">
                        <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-file-pdf"></i></button></a>
                    </td>
                    <td align="center">

                        @if ($modelos->aprobado == 1)
                        <a href="#">
                            <button class="bttn-unite bttn-md bttn-success"><i class="fa fa-check-circle"></i></button></a>
                    </td>

                    @else

                    <a href="#" >
                        <button class="bttn-unite bttn-md bttn-primary"><i class="fa fa-ellipsis-h"></i></button></a>
                    </td>
                    @endif

                </tr>
                @include('Produccion.combinacion_ficha_calzado.modalconf')
                @include('Produccion.combinacion_calzado.modalactivar')
                @include('Produccion.combinacion_calzado.modaldesactivar')
                @include('Produccion.combinacion_calzado.modaleditar')
                @endforeach
            </tbody>
        </table>

    </div>
</div>

<style>
    .bttn-purple{
        background-color:#AB47BC;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script type="text/javascript">
var data = <?php echo $LineasModelos;?>;
var combinacionLista = <?php echo $listaCombinacion;?>;
var nuevo = "prueba";
var t = $("#tabla").DataTable();
var t1 = $("#tabla").DataTable();
var tablaModelos = $("#tablaModelos").DataTable();
var listaModelos = <?php echo $LineasModelos; ?>;
var serieModelo = <?php echo  $seriemodelo; ?>;
//CIERRE MODAL CREACION COMBINACION
$("#cancelarModal").on('click', function() {
    location.reload();
});
//PREVISUALIZACION DE IMAGENES
let vista_preliminar = (event) => {
    let leer_img = new FileReader();
    let id_img = document.getElementById('img-foto');

    leer_img.onload = () => {
        if (leer_img.readyState == 2) {
            id_img.src = leer_img.result
        }
    }
    leer_img.readAsDataURL(event.target.files[0])
}
$("#tablaModelos").on('click', 'a.generarPDFTalla', function () {

    var valor = $(this).attr("id");
    console.log(valor)
    window.open("../../../Produccion/pdf/ficha_producto/" + valor)
});
$("#create-combinacion").on('click', 'a.combinacion-modelo', function() {
    console.log(serieModelo);
    var nombre = "PRUEBA";
    $("#modal-crear-combinacion").modal({
        show: true
    });
    var nombre = $(this).attr("id");

    $("#modal-crear-coleccion").val(listaModelos[0].nombre_coleccion);
    $("#modal-crear-linea").val(listaModelos[0].nombre_linea);

});
//USO DE AJAX PARA ABRIR UNA NUEVA VENTANA CON EL ID
$("#tablaModelos").on('click', 'a.combinacion', function() {

    var id = $(this).attr("id");
    console.log(id);
    $("#idOrdenCompra").val(id);

    $.ajax({
        url: "orden_compras/consulta/eliminar/" + id,
        success: function(html) {

            $.each(html, function(key, value) {
                auxiliar = 1;

            });
        }
    });

});
$("#modelo_activo").change(function() {
    //LIMPIAR
    tablaModelos.rows().remove().draw();

    //CONSTRUCCION FILTRADA
    if (this.value == "all") {
        window.location.reload();

    } else if (this.value == "aprobada") {
        $.each(combinacionLista, function(key, value) {
            if (value.aprobado == 1) {
                const tr = $(
                    "<tr>" +
                    "<td><img src='../../../photo/modelos/" + value.RUC_empresa + "/" + value.image +
                    "' width='70' height = '70'></img> </td>" +
                    "<td>" + value.codigo_comb + "</td>" +
                    "<td>" + value.descripcion + "</td>" +
                    "<td>  <a class='a' id='' href='#'' data-target='#modal-conf-"+value.cod_combinacion+"' data-toggle='modal'>"+
                            "<button class='bttn-unite bttn-md btn bttn-royal'><i class='fa fa-clipboard-list'></i></button></a> </td>" +

                    "<td>  <a href='' data-target='#modal-edit-' data-toggle='modal'>"+
                        "<button class='bttn-unite bttn-md bttn-warning'><i class='fas fa-file-pdf'></i></button></a></td> "+
                    "<td> <a href='#'> <button class='bttn-unite bttn-md bttn-success'><i class='fa fa-check-circle'></i></button></a></td></tr>"

                );
                tablaModelos.row.add(tr[0]).draw();
            }
        });



    } else if (this.value == "proceso") {


        $.each(combinacionLista, function(key, value) {
            if (value.aprobado == 0) {
                const tr = $(
                    "<tr>" +
                    "<td><img src='../../../photo/modelos/" + value.RUC_empresa + "/" + value.image +
                    "' width='70' height = '70'></img> </td>" +
                    "<td>" + value.codigo_comb + "</td>" +
                    "<td>" + value.descripcion + "</td>" +
                    "<td>  <a class='a' id='"+value.cod_combinacion+"' href='/Produccion/combinacion_ficha/ficha_producto/"+value.cod_combinacion+"'>" +
                         "<button class='bttn-unite bttn-md btn bttn-royal'><i class='fa fa-clipboard-list'></i></button></a> </td>" +

                         "<td>  <a href='' data-target='#modal-edit-' data-toggle='modal'>"+
                        "<button class='bttn-unite bttn-md bttn-warning'><i class='fas fa-file-pdf'></i></button></a></td> "+
                    " <td>   <a href='#'' > <button class='bttn-unite bttn-md bttn-primary'><i class='fa fa-ellipsis-h'></i></button></a></td></tr>"
                );
                tablaModelos.row.add(tr[0]).draw();
            }
        });

    }

});
</script>
@endsection
