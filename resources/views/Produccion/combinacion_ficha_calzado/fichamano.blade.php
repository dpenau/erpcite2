@extends ('layouts.admin')
@section ('contenido')
<div style="text-align: center;">

    <h3 class="font-weight-bold">Agregar Operaciones </h3>
</div>
{{Form::Open(array('action'=>array('CombinacionFichaCalzadoController@create_mano'),'method'=>'POST'))}}
<div style="margin: 0 4% 5% 4%;">
    <input type="hidden" name='codigo' value="{{$id_combinacion}}" >
    <div class="row" style="margin-bottom:2%;">
        <div class="col-3" style="text-align: left;">
            <label>Proceso:</label>
            <select name="proceso" class="custom-select" id="proceso">
                <option value="" selected disabled>Seleccionar Proceso</option>
                @foreach ($proceso as $col)
                <option value="{{$col->cod_proceso}}">{{$col->codigo}}-{{$col->nombre}}</option>
                @endforeach
            </select>

        </div>

    </div>
    <div class="row">
        <input name="id_combinacion_c" type="hidden" value="{{$id_combinacion}}">
        <div class="col-6">
            <div class="multiselect">
                <label>Operaciones:</label>
                <div class="selectBox" onclick="showCheckboxes()">
                    <select id="material" class="custom-select">
                        <option value="" selected disabled>Seleccionar Operaciones</option>
                    </select>
                    <div class="overSelect"></div>
                </div>
                <div id="checkboxes" style="overflow-y:scroll;height:150px">
                </div>
            </div>

        </div>
        <div class="col-2" style="margin-top: 2%">
            <a href="#" >
                <button id="guardar" type="button"
                    class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar</button></a>
        </div>


    </div>


    <br>

</div>
<div style="margin: 0 4% 0 4%;">
    <div class="x_content table-responsive">
        <table id="tablaMano">
            <thead align="center">
                <tr>

                    <th>N°</th>
                    <th>Proceso</th>
                    <th>Operacion</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody align="center">

            </tbody>
        </table>
        <input type="hidden" id="total_mat" name="total_mat">
    </div>
</div>

<div class="row" style="text-align: center;" >
    <div class="col-12 " style="margin: 0 0 0 4%;">

            <button type="submit"  class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" style="margin: 2%;">Guardar</button>
            <a   href="/Produccion/combinacion_ficha/ficha_producto_m/{{ $id_combinacion }}">
            <button type="button" class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar</button>
        </a>
    </div>

</div>
<div id="operaciones2">
</div>

{{Form::Close()}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
</script>
<style>
.button-md {
    border-color: #212121;
    background-color: #B0BEC5;

}

.button-mo {
    border-color: #212121;
    background-color: #FFFFFF;
}

.selectBox {
    position: relative;
}

.selectBox select {
    width: 100%;

}

.overSelect {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}

#checkboxes {
    display: none;
    border: 1px #dadada solid;

}

#inBox {
    margin-left: 2%;
}

#checkboxes label {
    display: block;
}

#checkboxes label:hover {
    background-color: #1e90ff;
}
</style>
<script type="text/javascript">

var total_materiales =0;
var operacion_directa = <?php echo $operacion_directa;?>;
var t = $('#tablaMano').DataTable({
    "lengthMenu": [
        [100, -1],
        [100, "All"]
    ],
    'columnDefs': [{
        'targets': 0,
        'checkboxes': {
            'selectRow': true
        }
    }],
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    }
});
//var proceso = <?php echo $proceso;?>;
var proceso =@json($proceso);
var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}

var operacion_listado = "";
var hashmap = new Map();


$("#proceso").change(function() {


    operacion_listado = operacion_directa.filter(datas => datas.cod_proceso == this.value);
    let cod_proceso = this.value;

    $('#material').empty();
    $('#checkboxes').empty();
    $('#material').append(` <option value="" selected disabled>Seleccionar Operaciones</option>`);
    for (var i = 0; i < operacion_listado.length; i++) {

        $('#checkboxes').append(`<input name='operaciones' type="checkbox" value="${operacion_listado[i].cod_operacion_d }">
                                ${operacion_listado[i].operacion_nombre} - ${operacion_listado[i].nombre_pago} - S/.${operacion_listado[i].costo_par}<br>
                          `);
    }

});

let contador = 1;
$("#guardar").on("click", function() {
    //t.rows().remove().draw();
    let process = proceso.filter(datas => datas.cod_proceso == $("#proceso").val());
    var checkboxes2 = document.getElementById("checkboxes");
    checkboxes2.style.display = "none";
    expanded = false;
    $("#proceso").prop("selectedIndex", 0);
    let checkboxes = document.querySelectorAll('input[name="operaciones"]:checked');
    let values = [];
    checkboxes.forEach((checkbox) => {
        values.push(checkbox.value);
    });
    console.log(values);

    $.each(operacion_directa, function(key, value) {
        $.each(values, function(key, valor) {
            if (value.cod_operacion_d == valor) {

                const tr = $(
                    "<tr>" +
                    "<td>" + contador + "</td>" +
                    "<td> <input name='process[]' type='hidden' value='" + process[0].cod_proceso + "' >" + process[0].nombre + "</td>"+
                    "<td> <input name='operacion[]' type='hidden' value='" + value.cod_operacion_d + "' >" + value.operacion_nombre + "</td>"+

                    "<td><button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button></td></tr>"

                );
                contador++;
                t.row.add(tr[0]).draw();

              }

        });
    });


});
$('#tablaMano').on('click', 'button', function() {
    t
        .row($(this).parents('tr'))
        .remove()
        .draw();

});

</script>
@endsection
