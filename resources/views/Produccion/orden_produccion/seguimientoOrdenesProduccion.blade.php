@extends ('layouts.admin')
@section ('contenido')
<!--
   Desarrollado por
   Jesús Albino Calderón
   México
   ITSZO && UNSA
-->

<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<div>
    <h1 class="font-weight-bold">Seguimiento de Ordenes de Producción en Proceso
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="{{route('SeguimientoOrdenProduccion.pdf')}}" target="_blank"><button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Exportar
                PDF</button></a>
    </h1>
    <hr>


</div>
<div id="tablas">
    <hr>

    <table id="example" class="display">
        <thead>
            <tr>
                <th>Orden de Producción</th>
                <th>Proceso</th>
                <th>Barra de Estado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($datosOrdenProduccion as $datos)
            <tr>
                <td>{{$datos->codigo_orden_pedido}}</td>

                <!--esta parte es para mostrar que proceso acaba de terminar el proceso-->
                @if($datos->estado_proceso==0)
                <td>Sin Planificar</td>
                @elseif($datos->estado_proceso==1)
                <td>Planificado</td>
                @elseif($datos->estado_proceso==2)
                <td>Cancelado</td>
                @elseif($datos->estado_proceso==3)
                <td>Corte</td>
                @elseif($datos->estado_proceso==4)
                <td>Habilitado</td>
                @elseif($datos->estado_proceso==5)
                <td>Aparado</td>
                @elseif($datos->estado_proceso==6)
                <td>Alistado</td>
                @elseif($datos->estado_proceso==7)
                <td>Montaje</td>
                @elseif($datos->estado_proceso==8)
                <td>Acabado</td>
                @elseif($datos->estado_proceso==9)
                <td>Terminado</td>
                @endif

                <!--muestra una barra de estado para mostrar el progreso en porcentaje que lleva un lote-->
                @if($datos->estado_proceso==0)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">Sin Planificar</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==1)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 100% " aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">Planificado</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==2)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">Cancelado</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==3)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 14%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">14%</div>
                    </div>
                </td>

                @elseif($datos->estado_proceso==4)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 30%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">28%</div>
                    </div>
                </td>

                @elseif($datos->estado_proceso==5)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 45%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">42%</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==6)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 60%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">56%</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==7)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 75%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">70%</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==8)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 90%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">84%</div>
                    </div>
                </td>
                @elseif($datos->estado_proceso==9)
                <td>
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">100%</div>
                    </div>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>

    @endsection
