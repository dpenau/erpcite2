@extends ('layouts.admin')
@section ('contenido')
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
         <div class="x_content table-responsive">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold" align="center">Nueva Orden de Produccion</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif

<table class="table table-hover" >
  <tr>
    <th class="table-primary" colspan="11"> <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
                <img src="../../img/citeccal.png" width="110" height="45">
            </div>
&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

 <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
                <img src="../../img/red_cite.png" width="100" height="45">
              </div>
              &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
            <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
                  <img src="../../img/ucsm.png" width="140" height="45">
            </div>

            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
            <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
              <img src="../../img/ppis.png" width="100" height="45">
            </div>
 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
            <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
          @foreach($empresa as $emp)
              @if($emp->imagen!="")
                {{Html::image('photo/'.$emp->imagen, 'alt 1', array('width' => '100','height' => '45'))}}



              @endif
            @endforeach
          </div>
            </div>
            </div>

        </th>
  </tr>
  {!!Form::open(array('url'=>'Produccion/orden_produccion','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
  <tr>
    <td class="table-active" colspan="8"><h3 class="font-weight-bold" align="center">ORDEN DE PRODUCCION</h3>

    </td>

    <td class="table-active" colspan="3" >


      <h4 class="font-weight-bold">
            {!! DNS1D::getBarcodeHTML($identificador,"c128")!!}<input class="font-weight-bold" name="norden" readonly value="       Nº{{$identificador}}" type="text" style="width:100%; border: 0; background-color:transparent;
  "></input></h4></td>
  </tr>
  <tr >
    <td class="table-primary" >Empresa:</td>
    <td class="tg-0pky" colspan="6">
<input name="empresa" readonly value="

      @foreach($empresa as $mat)

    {{$mat->nom_empresa}}
    @endforeach" type="text" style="width:100%; border: 0;background-color:transparent;"  >
</input>
  </td>
    <td class="table-primary"  colspan="4">Fecha de Pedido:</td>
  </tr>
  <tr>
    <td class="table-primary" >Cliente:</td>
    <td class="tg-0pky" colspan="6"> <input required name="cliente" maxlength="100" placeholder="Nombre de Cliente*" type="text" style="width:100%; border: 0;"
      />
</td>
    <td class="tg-0pky" colspan="4"><input type="date" id="fechaPedido" required="required" name="fechaPedido" required="required" class="form-control "></td>
  </tr>
  <tr>
    <td class="table-primary">Destino:</td>
    <td class="tg-0pky" colspan="6"><input required maxlength="100" name="destino" placeholder="Lugar de destino*"type="text" style="width:100%; border: 0;
" /></td>
    <td class="table-primary" colspan="4">Fecha de Entrega:</td>
  </tr>
  <tr>
    <td class="table-primary">Marca:</td>
    <td class="tg-0pky" colspan="6"><input required maxlength="50" name="marca" placeholder="Nombre de la marca*"type="text" style="width:100%; border: 0;" /></td>
    <td class="tg-0pky" colspan="4"><input type="date" id="fechaEntrega" required="required" name="fechaEntrega" required="required" class="form-control "></td>
  </tr>
  <tr>
    <td class="table-primary">Linea:</td>
    <td class="tg-0pky" colspan="4">

  <select name="state" required='required' class="custom-select">
  <option value="">--- Seleccionar Linea ---</option>
  @foreach ($states as $linea)
   <option value="{{$linea->cod_linea}}" >{{$linea->nombre_linea}}</option>
   @endforeach
   </select>







    </td>
    <td class="table-primary" colspan="2">Serie:</td>
    <td class="tg-0pky" colspan="4">
    	<select required='required' name="serie" class="custom-select">
          <option value="">--- Seleccionar Serie ---</option>

  @foreach ($serie as $serie)
   <option value="{{$serie->cod_serie}}">{{$serie->nombre_serie}}</option>
   @endforeach
   </select>
</td>
  </tr>
  <tr>
    <td class="table-primary">Codigo Modelo:</td>
    <td class="tg-0pky" colspan="4">
<select required name="city" class="form-control" style="width:350px">

</select>




    </td>
    <td class="table-primary" colspan="2">Serie de Tallas:</td>
    <td class="tg-0pky" colspan="4"><input name="iserie" readonly placeholder="Serie" type="text" style="width:100%; border: 0;background-color:transparent;
" />
</td>
  </tr>
  <tr>
    <td class="table-primary">Descripción:</td>
    <td class="tg-0pky" colspan="6">

 <input readonly  name="descripcion" placeholder="descripcion"type="text" style="width:100%; border: 0; background-color:transparent;
" />
    <td class="table-primary" colspan="4">Ingreso a Prod.:</td>
  </tr>
  <tr>
    <td class="table-primary">Estado:</td>
    <td class="tg-0pky" colspan="4">

<select required="required"  name="estado" class="custom-select">
  <option  value="" selected disabled>Estado</option>
 <option value="Urgente" >Urgente</option>
  <option value="Normal" >Normal</option>
   <option value="Planificable" >Planificable</option>
 </select>

    </td>
    <td class="tg-0pky" colspan="2" name="colorx"></td>
    <td class="tg-0pky" colspan="4"><input type="date" id="fechaProd" required="required" name="fechaProd" required="required" class="form-control "></td>
  </tr>
  <tr>
    <td class="table-primary">Serie:</td>
    <td class="table-active "><input id="o1" name="o1" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o2" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o3" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o4" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o5" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o6" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o7" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o8" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active"><input name="o9" readonly class="form-control" style="background-color:transparent;"></input></td>
    <td class="table-active">T</td>
  </tr>
  <tr>
    <td class="table-primary">Pares:</td>
    <td >

    <input name="i1" onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control " id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i2" onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>

    <td class="tg-0pky">
    	 <input name="i3" onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i4"  onkeypress="return isNumberKey(event)" maxlength="11"  readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i5"  onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i6"  onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i7"  onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
     <input name="i8"  onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i9"  onkeypress="return isNumberKey(event)" maxlength="11" readonly class="form-control" id="ex1" type="text" value="0">

    </td>
    <td class="tg-0pky">
    	 <input name="i10"readonly class="form-control" id="ex1" type="text" >

    </td>
  </tr>
</table>
      </div>

        <input type="text" style="display:none" name="lineaInput" value="s">
        <input type="text" style="display:none" name="serieInput" value="">
        <input type="text" style="display:none" name="modeloInput" value="">



<div class=" col-sm-7 d-inline d-flex justify-content-end mr-sm-5">
<button type="submit"align="center" class="bttn-unite bttn-md bttn-primary">Crear Orden de produccion</button>
        </div>
           </div>
      </div>


{!!Form::close()!!}

@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
