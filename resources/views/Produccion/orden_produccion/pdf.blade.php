<!DOCTYPE html>
<html>
    <title>Estado de las Ordenes de Producción </title>
  </head>
  
  <body>
  <h2>Seguimiento de Ordenes de Producción en Proceso   </h2>
        
        <table border='1px'>
        <thead >
            <tr>
                <th>Orden de Producción</th>
                <th>Proceso</th>
                <th>Barra de Estado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($datosOrdenProduccion as $datos)
            <tr>
                <td>{{$datos->codigo_orden_pedido}}</td>

                <!--esta parte es para mostrar que proceso acaba de terminar el proceso-->
                @if($datos->estado_proceso==0)
                <td>Sin Planificar</td>
                @elseif($datos->estado_proceso==1)
                <td>Planificado</td>
                @elseif($datos->estado_proceso==2)
                <td>Cancelado</td>
                @elseif($datos->estado_proceso==3)
                <td>Corte</td>
                @elseif($datos->estado_proceso==4)
                <td>Habilitado</td>
                @elseif($datos->estado_proceso==5)
                <td>Aparado</td>
                @elseif($datos->estado_proceso==6)
                <td>Alistado</td>
                @elseif($datos->estado_proceso==7)
                <td>Montaje</td>
                @elseif($datos->estado_proceso==8)
                <td>Acabado</td>
                @elseif($datos->estado_proceso==9)
                <td>Terminado</td>
                @endif

                <!--muestra una barra de estado para mostrar el progreso en porcentaje que lleva un lote-->
                @if($datos->estado_proceso==0)
                <td>Sin Planificar</td>
                @elseif($datos->estado_proceso==1)
                <td>Planificado</td>
                @elseif($datos->estado_proceso==2)
                <td>Cancelado</td>
                @elseif($datos->estado_proceso==3)
                <td>14%</td>
                @elseif($datos->estado_proceso==4)
                <td>30%</td>
                @elseif($datos->estado_proceso==5)
                <td>45%</td>
                @elseif($datos->estado_proceso==6)
                <td>60</td>
                @elseif($datos->estado_proceso==7)
                <td>75%</td>
                @elseif($datos->estado_proceso==8)
                <td>90%</td>
                @elseif($datos->estado_proceso==9)
                <td>100%</td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
  </body>
</html>