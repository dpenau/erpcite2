
        <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
            id="modal-active-{{$modelos->cod_combinacion}}">
            {{Form::Open(array('action'=>array('CombinacionCalzadoController@destroy',"3"   ),'method'=>'delete'))}}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titulo_act">Activar Combinacion de Modelo</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Está seguro que desea activar esta combinación de modelo?</p>
                        <input type="text" style="display:none" name="email" id="cod_act"
                            value="{{$modelos->cod_combinacion}}">
                        <input type="text" style="display:none" name="codigo_modelo" id="cod_act"
                            value="{{$modelos->cod_modelo}}">
                        <input type="text" style="display:none" name="estado" value="1">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger"
                            data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
            {{Form::Close()}}

        </div>
