<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
            id="modal-delete-{{$modelos->cod_modelo}}">
            {{Form::Open(array('action'=>array('FichaCalzadoController@destroy','2'),'method'=>'delete'))}}
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titulo_elim">Eliminar Modelo Base de Ficha de Producto</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar este modelo base?</p>
                        <input type="text" style="display:none" name="email" id="cod_elim"
                            value="{{$modelos->cod_modelo}}">
                        <input type="text" style="display:none" name="estado" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger"
                            data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
            {{Form::Close()}}
        </div>