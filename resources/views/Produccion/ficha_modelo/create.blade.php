@extends ('layouts.admin')
@section ('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 class="font-weight-bold">Agregar Modelo</h2>
            <div class="clearfix"></div>
        </div>
        <br>
        @if (count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!!Form::open(array('url'=>'Produccion/ficha_calzado','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}
        <div class="row">
            <div class="form-group  col-md-4  col-xs-12">
                <div class="">
                    <label>Coleccion:</label>
                    <select name="coleccion" required='required' class="custom-select" id="coleccion">
                        <option value="" selected disabled>--- Seleccionar Coleccion ---</option>
                        @foreach ($coleccion as $col)
                        <option value="{{$col->codigo_coleccion}}">{{$col->nombre_coleccion}}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="">
                    <label>Línea:</label>
                    <select name="linea" required='required' class="custom-select" disabled id="linea">
                          <option value="" selected disabled>--- Seleccionar Línea ---</option>
                    </select>
                </div>



            </div>
            <div class="form-group  col-md-4 offset-md-2 col-xs-12">

                <div class="">
                    <label>Serie:</label>
                    <select name="serie" required='required' class="custom-select" disabled id="serie">
                        <option value="" selected disabled>--- Seleccionar Serie ---</option>

                    </select>
                </div>

                <br>
                <div class="multiselect">
                    <label>Modelo:</label>
                    <div class="selectBox" onclick="showCheckboxes()">
                        <select id="modelo" name="serie" class="custom-select" disabled>
                            <option value="" selected disabled>--- Seleccionar Modelo ---</option>
                        </select>
                        <div class="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                           <!-- @foreach($serie as $ser)
                                <input  type="checkbox"  value="{{$ser->cod_serie}}"> {{$ser->nombre_serie}}<br>
                              @endforeach-->
                              
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <a href="{{ url('Produccion/ficha_calzado') }}">
                    <button type="button"
                        class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar</button>
                    <button type="submit"
                        class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>

                </a>
            </div>
        </div>
        <br>

        {!!Form::close()!!}

    </div>
</div>

<style>
.selectBox {
    position: relative;
}

.selectBox select {
    width: 100%;

}

.overSelect {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}

#checkboxes {
    display: none;
    border: 1px #dadada solid;

}

#inBox {
    margin-left: 2%;
}

#checkboxes label {
    display: block;
}

#checkboxes label:hover {
    background-color: #1e90ff;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
var data = <?php echo $LineasModelos;?>;
var expanded = false;
var val="heo"
function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}

var modelos_colecion = "";
var modelos_linea = "";
var modelos_serie = "";

$("#coleccion").change(function() {
   
    modelos_colecion = data.filter(datas => datas.cod_coleccion == this.value);
  
    $('#linea').empty();
    $('#linea').append(` <option value="" selected disabled>--- Seleccionar Línea ---</option>`);
    $('#linea').prop('disabled', false);
    var clean = modelos_colecion.filter((modelos_colecion, index, self) =>
    index === self.findIndex((t) => (t.cod_linea === modelos_colecion.cod_linea)));


    for (var i = 0; i < clean.length; i++) {

        $('#linea').append(`<option value="${clean[i].cod_linea}">
                                        ${clean[i].nombre_linea}
                                    </option>`);

    }
    //alert(this.value);
});

$("#linea").change(function() {
   
    modelos_linea = modelos_colecion.filter(datas => datas.cod_linea == this.value);
  
   
    $('#serie').empty();
    $('#serie').append(` <option value="" selected disabled>--- Seleccionar Serie ---</option>`);
    $('#serie').prop('disabled', false);

    var cleanLine = modelos_linea.filter((modelos_linea, index, self) =>
    index === self.findIndex((t) => (t.cod_serie === modelos_linea.cod_serie)));

    for (var i = 0; i < cleanLine.length; i++) {

        $('#serie').append(`<option value="${cleanLine[i].cod_serie}">
                                        ${cleanLine[i].nombre_serie}
                                    </option>`);

    }

    //alert(this.value);
});
$("#serie").change(function() {
    
    modelo_serie = modelos_linea.filter(datas => datas.cod_serie == this.value);
    $('#modelo').empty();
    $('#modelo').append(` <option value="" selected disabled>--- Seleccionar Modelo ---</option>`);
    $('#modelo').prop('disabled', false);
   // $('#checkboxes').prepend(`<input>NHI</input>`);
    for (var i = 0; i < modelo_serie.length; i++) {

       $('#checkboxes').append(`<input name='modelos[]' type="checkbox" value="${modelo_serie[i].cod_modelo}">
                                        ${modelo_serie[i].codigo} / ${modelo_serie[i].descripcion}</input><br>
                                 `);
    }


    //alert(this.value);
});
</script>



@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>

@endpush
@endsection