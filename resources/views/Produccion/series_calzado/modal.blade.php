<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$op->cod_serie}}">
	{{Form::Open(array('action'=>array('SeriesCalzadoController@destroy',$op->cod_serie
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Serie</h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea desactivar esta serie?</p>
				<input type="text" style="display:none" name="email" value="{{$op->cod_serie}}">
        <input type="text" style="display:none" name="estado" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
