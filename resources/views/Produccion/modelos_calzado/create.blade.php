@extends ('layouts.admin')
@section ('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 class="font-weight-bold">Nuevo Modelo</h2>
            <div class="clearfix"></div>
        </div>
        <br>
        @if (count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!!Form::open(array('url'=>'Produccion/modelos_calzado','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}
        <div class="row">
            <div class="form-group  col-md-4  col-xs-12">
                <div class="">
                    <label>Coleccion:</label>
                    <select name="coleccion" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Coleccion ---</option>
                        @foreach ($coleccion as $col)
                        <option value="{{$col->codigo_coleccion}}">{{$col->nombre_coleccion}}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="">
                    <label>Serie:</label>
                    <select name="serie" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Serie ---</option>
                        @foreach ($serie as $series)
                        <option value="{{$series->cod_serie}}">{{$series->nombre_serie}}</option>
                        @endforeach
                    </select>
                </div>
                <!--
                        <div class="multiselect">
                          <div class="selectBox" onclick="showCheckboxes()">
                            <select id="serie" name="serie" class="custom-select">
                              <option value="" selected disabled>Serie</option>
                            </select>
                            <div class="overSelect"></div>
                          </div>
                          <div id="checkboxes">
                            @foreach($serie as $ser)
                                <input id="inBox" type="checkbox" name="serie[]" value="{{$ser->cod_serie}}"> {{$ser->nombre_serie}}<br>
                              @endforeach
                          </div>
                        </div>
                        -->
                <br>
                <div class="">
                    <label for="codigo_calzado">Codigo de Modelo</label>
                    <input type="text" id="codigo_modelo" name="codigo_modelo" required="required" maxlength="40"
                        class="form-control ">
                </div>

                <div class="">
                    <label for="descripcion_modelo">Descripcion de Modelo</label>
                    <input type="text" id="descripcion_modelo" name="descripcion_modelo" required="required"
                        maxlength="50" class="form-control ">
                </div>
            </div>
            <div class="form-group  col-md-4 offset-md-2 col-xs-12">
                <div class="" style="margin-left:-3%;">
                    <label>Linea:</label>
                    <select name="linea" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Linea ---</option>
                        @foreach ($Lineas as $linea)
                        <option value="{{$linea->cod_linea}}">{{$linea->nombre_linea}}</option>
                        @endforeach
                    </select>
                </div>


                <br>
                <div class="row">
                    <div class="btn-small amber darken-s" >
                        <label for="exampleFormControlFile1">Imagen de modelo:</label>
                        <input type="file" class="form-control-file" id="photo" name="photo" required="required"
                            onchange="vista_preliminar(event)">
                    </div>
                    <div>
                        <img src="" alt="" id="img-foto" width="100" height="100" style="margin-left:1%;"> </img>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-4  col-xs-12">
                <div class="">
                    <label>Capellada:</label>
                    <select name="capellada" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Capellada ---</option>
                        @foreach ($capellada as $capell)
                        <option value="{{$capell->cod_capellada}}">{{$capell->	descrip_capellada}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="">
                    <label>Plantilla:</label>
                    <select name="plantilla" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Plantilla ---</option>
                        @foreach ($plantilla as $plantilla)
                        <option value="{{$plantilla->cod_plantilla}}">{{$plantilla->descrip_plantilla}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group  col-md-4 offset-md-2 col-xs-12">
                <div class="">
                    <label>Forro:</label>
                    <select name="forro" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Forro ---</option>
                        @foreach ($forroF as $forroF)
                        <option value="{{$forroF->cod_forro }}">{{$forroF->descrip_forro}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="">
                    <label>Piso:</label>
                    <select name="piso" required='required' class="custom-select">
                        <option value="" selected disabled>--- Seleccionar Piso ---</option>
                        @foreach ($piso as $piso)
                        <option value="{{$piso->cod_piso}}">{{$piso->descrip_piso}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <br>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <a href="{{ url('Produccion/modelos_calzado') }}">
                    <button type="button"
                        class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar</button>
                    <button type="submit"
                        class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>

                </a>
            </div>
        </div>
        <br>

        {!!Form::close()!!}

    </div>
</div>
<script>
let vista_preliminar = (event) => {
    let leer_img = new FileReader();
    let id_img = document.getElementById('img-foto');

    leer_img.onload = () => {
        if (leer_img.readyState == 2) {
            id_img.src = leer_img.result
        }
    }
    leer_img.readAsDataURL(event.target.files[0])
}
</script>
<style>
.selectBox {
    position: relative;
}

.selectBox select {
    width: 100%;

}

.overSelect {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}

#checkboxes {
    display: none;
    border: 1px #dadada solid;

}

#inBox {
    margin-left: 2%;
}

#checkboxes label {
    display: block;
}

#checkboxes label:hover {
    background-color: #1e90ff;
}
</style>
<script>
var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}
</script>



@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>

@endpush
@endsection