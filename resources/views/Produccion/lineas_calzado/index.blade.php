@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div style="text-align: center;">
  <h3 class="font-weight-bold">Colecciones y Líneas </h3>
</div>
<br>
<div class="row">
  <div class="col-5" style="margin-left:3%">
    <a href="coleccion/create">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-7">Nueva colección</button></a>
  </div>
  <div class="col-5"  style="margin-left:10%">
    <a href="lineas_calzado/create">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva línea</button></a>
  </div>
</div>
<div class="row">
  <div class="col-5 x_content table-responsive"  style="margin:3%" >
        <table id="example" class="display"  >
      <thead align="center">
        <tr>
          <th>Nombre de Colección</th>
          <th>Editar</th>
          <th>Activar/Desactivar</th>

        </tr>
      </thead>
      <tbody align="center">
        @foreach($coleccion as $co)
        @if($co->estado_coleccion==0)
        <tr class="bg-danger">
        @endif
          
          <td>{{$co->nombre_coleccion}}</td>
          <td>
            <a  class="b_act" id="{{$co->codigo_coleccion}}">
        <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
          </td>
          <td>
            @if($co->estado_coleccion==0)
            <a class="b_acc" id="{{$co->codigo_coleccion}}">
            <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
            @else
            <a class="b_acc" id="{{$co->codigo_coleccion}}">
            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
            @endif
          </td>
        </tr>
       
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="col-5 x_content table-responsive" style="margin:3%">
        <table id="example2" class="display">
      <thead align="center">
        <tr>
          
          <th>Nombre de Linea</th>
          <th>Editar</th>
          <th>Activar/Desactivar</th>

        </tr>
      </thead>
     
      <tbody align="center">
        @foreach($Lineas as $op)
        @if($op->estado_linea==0)
        <tr class="bg-danger">
        @endif
          
          <td>{{$op->nombre_linea}}</td>
          <td>
            <a href="" data-target="#modal-update-{{$op->cod_linea}}" data-toggle="modal">
        <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
          </td>
          <td>
            @if($op->estado_linea==0)
            <a href="" data-target="#modal-active-{{$op->cod_linea}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
            @else
            <a href="" data-target="#modal-delete-{{$op->cod_linea}}" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
            @endif
          </td>
        </tr>
        @include('Produccion.lineas_calzado.modal')
        @include('Produccion.lineas_calzado.modal_modificar')
        @include('Produccion.lineas_calzado.modal_activar')
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Editar Coleccion </h4>
              </div>
              <div class="modal-body">
                {{Form::Open(array('action'=>array('ColeccionCalzadoController@update','1'
                ),'method'=>'patch'))}}
         
                <input type="text" id="cod_coleccion" style="display:none" name="cod_coleccion">
                <div class="form-group">
               
                  <label for="total_orden_compra">Nombre de la coleccion:</label>
                   <input type="text" id="nombre" class="form-control" maxlength="100" name="nombre">
                </div>
              </div>
              <div class="modal-footer">
        				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
        				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
        			</div>
              {{Form::Close()}}
          </div>
      </div>
  </div>
  <div class="modal fade" id="modal_accion" role="dialog">
      <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="titulo"></h4>
              </div>
              <div class="modal-body">
                {{Form::Open(array('action'=>array('ColeccionCalzadoController@destroy','2'
                ),'method'=>'delete'))}}
                <p id="mensaje"></p>
                <input type="text" id="cods" style="display:none" name="codigo">
                <input type="text" id="cod_acc" style="display:none" name="accion">
                </div>
              <div class="modal-footer">
        				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
        				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
        			</div>
              {{Form::Close()}}
          </div>
      </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">
  $(document).ready( function () {
    var data =<?php echo $coleccion;?>;
    $(".b_act").on('click',function(){
      let indice=buscar($(this).attr('id'));
      modal_editar(indice)
    })
    $(".b_acc").on('click',function(){
      let indice=buscar($(this).attr('id'));
      let estado=0;
      if(data[indice].estado_coleccion==0)
      {
        estado=1;
      }
      modal_accion(indice,estado);
    })
    function modal_editar(index)
    {
      $("#cod_coleccion").val(data[index].codigo_coleccion);
      $("#nombre").val(data[index].nombre_coleccion);
      $("#myModal").modal({show:true});
    }
    function buscar(id)  {
      var index = -1;
      var filteredObj = data.find(function(item, i){
        if(item.codigo_coleccion == id){
          index = i;
          return index;
        }
      });
      return index;
    }
    function modal_accion(index,acc)  {
      switch (acc) {
        case 0: $("#titulo").text("Desactivar Colección");
        $("#mensaje").text("¿Está seguro que desea desactivar la colección?");
          break;
        case 1:$("#titulo").text("Activar Colección");
        $("#mensaje").text("¿Está seguro que desea activar la colección?");
        break;
        default:
      }
      $("#cods").val(data[index].codigo_coleccion);
      $("#cod_acc").val(acc);
      $("#modal_accion").modal({show:true});
    }
  });
  </script>
@endsection
