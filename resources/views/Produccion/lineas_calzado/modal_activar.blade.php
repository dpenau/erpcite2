<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-active-{{$op->cod_linea}}">
	{{Form::Open(array('action'=>array('LineasCalzadoController@destroy',$op->cod_linea),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Activar Linea</h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea activar la línea?</p>
				<input type="hidden" style="display:none" name="email" value="{{$op->cod_linea}}">
        <input type="text" style="display:none" name="estado" value="1">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
