@extends ('layouts.admin')
@section ('contenido')
<script type="text/javascript">
  let modelitos = @json($modelos);
  let grupitos = @json($grupos);
  let procesitos = @json($procesos);
  let v_models_seasons_times = @json($models_seasons_times);
  let v_colecciones = @json($colecciones);
</script>
<div class="preloader">
</div>
<div>
  <h1 class="font-weight-bold">Reporte de Tiempos </h1>
  <hr style="border:3px solid:blank">
</div>
<h2 class="font-weight-bold">Tiempos Grupos</h2>
<br>
<!--Begin de filtros de búsqueda-->
<div class="row">
  <div class="col-sm-4">
    <div class="card" style="height: 10rem;">
      <div class="card-body">
        <h5 class="card-title">Buscar Modelo</h5>
        <div class="form-group col-md-12">
          <select id="selectModelo" class="form-control" name="selectModel">
            <option value="-Todos-">Escoger Modelo</option>
            <script type="text/javascript">
              for (var i = 0; i < modelitos.length; i++) {
                document.write("<option value=\"" + modelitos[i].cod_modelo + "\">" + modelitos[i].cod_modelo + "</option>");
              }
            </script>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="height: 10rem;">
      <div class="card-body">
        <h5 class="card-title">Buscar Grupo</h5>
        <div class="form-group col-md-12">
          <select id="selectGrupo" class="form-control" name="selectGrupo">
            <option selected="selected">Todos los Grupos</option>
            <script type="text/javascript">
              for (var i = 0; i < grupitos.length; i++) {
                document.write("<option value=\"" + grupitos[i].codigo_grupo_trabajo + "\">" + grupitos[i].codigo_grupo_trabajo + "</option>");
              }
            </script>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card" style="height: 10rem;">
      <div class="card-body">
        <h5 class="card-title">Buscar Proceso</h5>
        <div class="form-group col-md-12">
          <select id="selectProceso" class="form-control" name="selectProceso">
            <option value="-Todos-">Escoger Proceso</option>
            <script type="text/javascript">
              for (var i = 0; i < procesitos.length; i++) {
                document.write("<option value=\"" + procesitos[i].proceso + "\">" + procesitos[i].proceso + "</option>");
              }
            </script>
          </select>
        </div>
      </div>
    </div>
  </div>
</div>

<!--End filtros de búsqueda-->
<div id="filtros" class="row" style="width:100%">

</div>
<hr>
<!--div class="x_content table-responsive"-->
<table id="example" class="display">
  <thead>
    <tr>
      <th>Modelo</th>
      <th>Proceso</th>
      <th>Especialidad</th>
      <th>Grupo</th>
      <th>Tiempo</th>
    </tr>

  </thead>
  @foreach($grupo_data as $grupo_data)
  <tbody class="tabladatos">
    <td id="first-child">{{$grupo_data->codigo_modelo}}</td>
    <td id="second-child">{{$grupo_data->proceso}}</td>
    <td>{{$grupo_data->especialidad}}</td>
    <td id="four-child">{{$grupo_data->codigo_grupo_trabajo}}</td>
    <td>{{$grupo_data->tiempo}}</td>
  </tbody>
  @endforeach
</table>
<div>
  <a href="{!! route('tiempo_descargar') !!}" target="_blank">
    <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Exportar PDF</button></a>
</div>
<!-- Codigo de Mario_Huaypuna .............................................................. -->
<br><br>
<div>
  <hr style="border:3px solid:blank">
</div>
<h2 class="font-weight-bold">Tiempos Modelos</h2>
<div class="x_content table-responsive">
  <!--Begin Buscar-->
  <!--End buscar-->
  <!--Begin Checks sobre los filtros-->
  <div class="row">
    <div class="col-sm-5">
      <div class="card" style="height: 5rem;">
        <div class="card-body">

          <div class="row">
            <div class="col-md-3">
              <label for="inputCity">Temporada</label>
            </div>
            <div class="col-md-9">
              <select id="temporada" class="custom-select">
                <option value="default" selected disabled>Selecciona temporada</option>
                @foreach($colecciones as $temporada)
                <option value="{{$temporada->codigo_coleccion}}">{{$temporada->nombre_coleccion}}</option>
                @endforeach
              </select>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card" style="height: 5rem;">
        <div class="card-body">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="todosLosModelos" checked disabled>
            <label class="form-check-label" for="defaultCheck1" disabled>
              Todos los modelos
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--End Checks sobre los filtros-->
  <br>

  <!--Begin table-->
  <div id="reporte_tiempos_modelos" class="x_content table-responsive">
    <table id="tabla_reporte_tiempos_modelos" class="display" style=" width:100%">
      <thead>
        <tr>
          <th class="temporada">Temporada</th>
          <th>Modelo</th>
          <th>Tiempo Promedio</th>
        </tr>
      </thead>

      <tbody>
        @foreach($models_seasons_times as $md_temp)
        <tr>
          <td>{{$md_temp->nombre_coleccion}}</td>
          <td>{{$md_temp->cod_modelo}}</td>
          <td>{{ $md_temp->total_tiempos / $md_temp->num_ordenes_produccion }} segundos</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!--End table-->
  <!-- other table -->
  <br>

  <button id="exportar_reporte_tiempos_modelos" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Exportar PDF</button>
  <br><br>
  </div>


  <script type="text/javascript">

   $(document).ready(function() {

   //funcion para manipular el select Modelo falgcarr

   $('table').show();
      $('#selectModelo').change(function() {
        $('table').show();
        var selection = $(this).val();
        if (selection === '-Todos-') {
          $('tr').show();
        } else {
          var dataset = $('#example .tabladatos').find('tr');
          // muestra las celdas primero
          dataset.show();
        }
        // filtro de las celdas que deben ser ocultadas
        dataset.filter(function(index, item) {
          return $(item).find('#first-child').text().split(',').indexOf(selection) === -1;
        }).hide();
      });

   //funcion para manipular el select Proceso falgcarr
      $('table').show();
      $('#selectProceso').change(function() {
        $('table').show();
        var selection = $(this).val();
        if (selection === '-Todos-') {
          $('tr').show();
        } else {
          var dataset = $('#example .tabladatos').find('tr');
          // muestra las celdas primero
          dataset.show();
        }
        // filtro de las celdas que deben ser ocultadas
        dataset.filter(function(index, item) {
          return $(item).find('#second-child').text().split(',').indexOf(selection) === -1;
        }).hide();
      });


      // funcion para manipular el select Grupo falgcarr

      $('table').show();
      $('#selectGrupo').change(function() {
        $('table').show();
        var selection = $(this).val();
        if (selection === '-Todos-') {
          $('tr').show();
        } else {
          var dataset = $('#example .tabladatos').find('tr');
          // show all rows first
          dataset.show();
        }
        // filter the rows that should be hidden
        dataset.filter(function(index, item) {
          return $(item).find('#four-child').text().split(',').indexOf(selection) === -1;
        }).hide();
      });




/*      //
      var $ddl = $('#selectModel').on('change', search);
      var $tblRows = $("#example > tbody > tr");

      //
      function search() {
        //
        var ddlIndex = $ddl.prop('selectedIndex');
        var ddlText = $ddl.val();
        //Reset
        if (ddlIndex == 0) {
          //show all
          $tblRows.show();
        } else {
          //
          var lastCell;
          var firstCell;

          //browse over each row
          $($tblRows).each(function(i, row) {
            //get content of the first cell and last cell
            firstCell = $(this).find(':first-child').text().trim();
            lastCell = $(this).find(':last-child').text().trim();

            //
            if (ddlText == lastCell) {

              $(this).show();
            } else
              $(this).hide();
          });

        }
      }  */

      //Funciones de Mario_Huaypuna -------------------------------------------------------------------------------
      var t = $("#tabla_reporte_tiempos_modelos").DataTable({

        "language": {
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
          }
        },
      });


      $(document).on('click', '#exportar_reporte_tiempos_modelos', function() {
        //window.open('/Ventas/Reportes/ReporteClientes/exportar/'+data)
      });

      $("#todosLosModelos").change(function() {

        if ($('#todosLosModelos').prop('checked') === true) {
          let selected_temporada = $("#temporada").val();
          t.destroy();
          t = $('#tabla_reporte_tiempos_modelos').DataTable({

            "language": {
              "emptyTable": "No hay información",
              "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
              "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
              "infoFiltered": "(Filtrado de _MAX_ total entradas)",
              "loadingRecords": "Cargando...",
              "processing": "Procesando...",
              "search": "Buscar:",
              "zeroRecords": "Sin resultados encontrados",
              "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
            }
          });
          t.rows().remove().draw();

          for (var i = 0; i < v_models_seasons_times.length; i++) {
            t.row.add([
              v_models_seasons_times[i].nombre_coleccion,
              v_models_seasons_times[i].cod_modelo,
              ( v_models_seasons_times[i].total_tiempos / v_models_seasons_times[i].num_ordenes_produccion ) + " segundos"
            ]).draw();
          }
          $("#todosLosModelos").prop('disabled', true);
          $("#temporada").val('default');
        }
      });

      $("#temporada").change(function() {

        let selected_temporada = $("#temporada").val();
        t.destroy();
        t = $('#tabla_reporte_tiempos_modelos').DataTable({
          "language": {
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
            }
          },
          "columnDefs": [{
            "targets": [0],
            "visible": false,
            "searchable": false
          }, ]
        });

        t.rows().remove().draw();

        for (var i = 0; i < v_models_seasons_times.length; i++) {
          if (selected_temporada === v_models_seasons_times[i].cod_coleccion) {
            t.row.add([
              " ",
              v_models_seasons_times[i].cod_modelo,
              ( v_models_seasons_times[i].total_tiempos / v_models_seasons_times[i].num_ordenes_produccion ) + " segundos"
            ]).draw();
          }
        }
        $("#todosLosModelos").prop('disabled', false);
        $("#todosLosModelos").prop("checked", false);

      });

    });
  </script>

  @endsection
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
