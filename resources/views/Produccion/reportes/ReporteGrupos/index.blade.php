@extends ('layouts.admin')
@section ('contenido')

<script type="text/javascript">
    
</script>

<div class="preloader">
</div>
<div>
  <h1 class="font-weight-bold">Reporte de Grupos de Trabajo </h1>
  <hr style="border:3px solid:blank">
</div>
<br>
  <!--Begin de filtros de búsqueda-->
  
<div id="tablas">
<div>
<a class="font-weight-bold">Buscar codigo de grupo </a>      
</div>
<form class="navbar-search pull-left">
  <input type="text" class="search-query" placeholder="Search">
</form>

        <div id="filtros" class="row" style="width:100%">
             <div class="col-md-4 col-xs-12 col-sm-12" style="border:0px solid black"><br>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' value="1"  onclick="desactivar()" id='anioreporte'>
                    <label class='custom-control-label' for='anioreporte'>Activo</label>
                </div><br>
                    <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' value="1" onclick="desactivar()" id='temporadareporte'>
                    <label class='custom-control-label' for='temporadareporte'>Inactivo</label>
                </div><br>

                
            </div>
            
            </div>
            <hr>
            <!--div class="x_content table-responsive"-->
                <table id="example" class="display">
                    <thead>
                <tr>
                    <th>Grupo</th>
                    <th>Habilitado</th>
                    <th>Integrantes</th>
                    <th>Actividad</th>
                    <th>Editar</th>
                    <th>Eliminar</th>

                </tr>
            </thead>

            <tbody>
                 @foreach($Grupo_trabajo as $Grupo_trabajo)
                <tr>
                    <td>{{$Grupo_trabajo->codigo_grupo_trabajo}}</td>
                    <td> </td>
                    <td>{{$Grupo_trabajo->nombres}} {{$Grupo_trabajo->apellido_paterno}}</td>
                    @if ( $Grupo_trabajo->estado_trabajador == 1)
                    <td>Activo</td>
                    @else
                    <td>Inactivo</td>
                    @endif
  
                    <td></td>

                </tr>
                @endforeach
            </tbody>    
                </table>

                <div>
                       
            @endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    
</script>