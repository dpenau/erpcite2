<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$var}}">
{!!Form::open(array('url'=>'logistica/kardex/cambiar_ubicacion','method'=>'POST','autocomplete'=>'off'))!!}
    {{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Cambiar Ubicacion</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_material" value="{{$var}}">
        <div class="row">
          <div class="form-group  col-md-12">
            <label for="">Almacen</label>
            <select name="almacen"  class="custom-select" required>
              <option value="" disabled>Escoja uno</option>
              @foreach($almacenes as $alm)
                @if($ubicacion[0]->cod_almacen==$alm->cod_almacen)
                  <option value="{{$alm->cod_almacen}}" selected>{{$alm->nom_almacen}}</option>
                @else
                  <option value="{{$alm->cod_almacen}}" >{{$alm->nom_almacen}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group  col-md-12">
            <label for="total_orden_compra">Ubicacion:</label>
              <input  type="text" required name="ubicacion" class="form-control" value="{{$ubicacion[0]->lugar_almacenaje}}">
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
