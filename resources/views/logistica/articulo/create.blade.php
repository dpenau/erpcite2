@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nuevo Articulo</h2>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'logistica/articulo', 'method' => 'POST', 'autocomplete' => 'off']) !!}
                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12" id="serie-container"
                         style="display:none">
                        <select id="serie" name="serie" class="custom-select">
                            <option value="" selected disabled>Serie</option>
                            @foreach ($serie as $ser)
                                <option value="{{ $ser->cod_serie }}">{{ $ser->nombre_serie }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <input type="checkbox" id="s_talla" name="t_compra" value="1" style="margin-top:20px"> Compra
                        por talla
                    </div>
                    <div class="col-md-4 col-xs-12" id="tallas-pisos" style="display:none">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group  ">
                                    <label for="total_orden_compra">Talla Minima:</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" maxlength="2"
                                           name="talla_inicial" id="t_i" maxlength="2" class="form-control "
                                           disabled="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group  ">
                                    <label for="total_orden_compra">Talla Maxima:</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" maxlength="2"
                                           name="talla_final" id="t_f" maxlength="2" class="form-control "
                                           disabled="true">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <select required='required' id="categoria" class="custom-select">
                            <option value="" selected disabled>Categoria</option>
                            @foreach ($categoria as $cat)
                                <option value="{{ $cat->cod_categoria }}">{{ $cat->nom_categoria }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <select required='required' id="sub-categoria" name="subcategoria" class="custom-select">
                            <option value="" selected>Subcategoria</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12">
                        <label for="total_orden_compra">Descripcion de Material:</label>
                        <input type="text" id="subcategoria" name="descripcion_material" maxlength="70"
                               required="required"
                               class="form-control ">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="">Unidad de Compra</label>
                        <select required='required' name="unidad_compra" id="unidad_compra" class="custom-select">
                            <option value="" selected disabled>Unidad de Compra</option>
                            @foreach ($unidad_medida as $uni)
                                <option value="{{ $uni->cod_unidad_medida }}">{{ $uni->descrip_unidad_medida }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="">Almacen</label>
                        <select required='required' name="almacen" class="custom-select">
                            <option value="" selected disabled>Almacen</option>
                            @foreach ($almacen as $uni)
                                <option value="{{ $uni->cod_almacen }}">{{ $uni->nom_almacen }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="location">Ubicación</label>
                        <input type="text" id="location" name="location" required='required' class="form-control">
                    </div>
                </div>

                <div class="row" id="costos">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Costo Total:</label>
                        <input onkeypress="return isNumberKey(event)" type="text" name="costo_con_igv" maxlength="9"
                               id="total_costo"
                               required="required" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Costo sin IGV:</label>
                        <input type="number" step='any' maxlength="9" id="costo_sin_igv" name="costo_sin_igv"
                               required="required"
                               class="form-control " disabled="true">
                    </div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">IGV:</label>
                        <input onkeypress="return isNumberKey(event)" type="text" maxlength="9" value="0.18" id="igv"
                               required="required" class="form-control " disabled="true">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <input type="checkbox" id="s_stock" name="s_stock" value="1" style="margin-top:20px">Niveles de
                        Stock:
                    </div>
                    <div class="col-md-4 col-xs-12" id="stock_pisos" style="display:none">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group  ">
                                    <label for="total_orden_compra">Stock Mínimo</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" maxlength="4"
                                           name="stock_minimo" id="tipo1" disabled maxlength="4" required="required"
                                           class="form-control ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group  ">
                                    <label for="total_orden_compra">Stock Máximo:</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" maxlength="4"
                                           name="stock_maximo" id="tipo2" disabled maxlength="4" class="form-control ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <a href="{{ url('logistica/articulo') }}">
                            <button type="button"
                                    class="bttn-unite bttn-md bttn-danger  col-md-2 col-md-offset-5">Cancelar
                            </button>
                            <button type="submit" id="guardar"
                                    class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar
                            </button>
                        </a>
                    </div>
                </div>
                {!! Form::close() !!}
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                    $(document).ready(function () {
                        var idSerie = -1;
                        var serieObject = null;
                        var series = <?php echo $serie; ?>;
                        //Comprobacion de stock minimo menor al stock maximo
                        //Tipo 1 --> stock minimo


                        $('#guardar').on('click', function () {
                            var minimo = $("#tipo1").val() * 1;
                            var maximo = $("#tipo2").val() * 1;
                            if (maximo < minimo) {
                                alert("El stock mínimo debe ser menor al stock máximo");

                                $("#tipo1").val(null);
                                $("#tipo2").val(null);
                            }
                            $("#t_i").prop('disabled', false);
                            $("#t_f").prop('disabled', false);
                            $("#costo_sin_igv").prop('disabled', false);
                            $("#unidad_compra").prop('disabled', false);


                        });
                        $('#total_costo').keyup(function () {
                            var aux = $(this).val() / 1.18;

                            $('#costo_sin_igv').val(aux.toFixed(2));
                            var igvaux = $('#total_costo').val() - $('#costo_sin_igv').val();
                            $('#igv').val(igvaux.toFixed(2));

                        });
                        if ($('#igv').val() == 0.18) {
                            $('#igv').val(0);
                        }
                        $("#categoria").change(function () {
                            let valor = $("#categoria").val();
                            switch (valor) {
                                case "306":
                                    $("#sub-categoria").empty();
                                    $("#sub-categoria").append(
                                        "<option value='' selected disabled>Subcategoria</option>");
                                    @foreach ($subcategorias as $sub)
                                    @if ($sub->cod_categoria == 306)
                                    $("#sub-categoria").append("<option value='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</option>");
                                    @endif
                                        @endforeach
                                        break;
                                case "634":
                                    $("#sub-categoria").empty();
                                    $("#sub-categoria").append(
                                        "<option value='' selected disabled>Subcategoria</option>");
                                    @foreach ($subcategorias as $sub)
                                    @if ($sub->cod_categoria == 634)
                                    $("#sub-categoria").append("<option value='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</option>");
                                    @endif
                                        @endforeach
                                        break;
                                case "969":
                                    $("#sub-categoria").empty();
                                    $("#sub-categoria").append(
                                        "<option value='' selected disabled>Subcategoria</option>");
                                    @foreach ($subcategorias as $sub)
                                    @if ($sub->cod_categoria == 969)
                                    $("#sub-categoria").append("<option value='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</option>");
                                    @endif
                                        @endforeach
                                        break;

                            }
                        });
                        var pre = $("#costo_sin_igv").val();


                        function buscarSerie(id) {

                            var index = -1;
                            var filteredObj = series.find(function (item, i) {

                                if (item.cod_serie == id) {
                                    index = i;
                                    return index;
                                }
                            });
                            return index;
                        }

                        $('#serie').on('change', function () {

                            $("#modal-data").empty();

                            var id = $(this).val();
                            var index = buscarSerie(id);
                            serieObject = series[index];

                            $("#t_i").val(serieObject.tallaInicial);
                            $("#t_f").val(serieObject.tallaFinal);
                            //modal_editar(index);
                        });
                        var c = 0;

                        $("#s_talla").change(function () {
                            //var inputNombre = document.getElementsById("#t_i");
                            // inputNombre.value = "DYP";

                            if ($(this).prop("checked")) {

                                // $("#t_i").val('30');
                                var u_c = "92970";
                                $("#unidad_compra option[value='" + u_c + "']").prop("selected", true);
                                $("#unidad_compra").prop('disabled', true);
                                $("#serie-container").toggle('fast');
                                $("#tallas-pisos").toggle('fast');

                                $("#t_i").prop('disabled', true);
                                $("#t_f").prop('disabled', true);
                                $("#serie").prop('disabled', false);


                            } else {

                                //  $("#t_i").val("");
                                $("#unidad_compra").prop('disabled', false);
                                $("#t_i").prop('disabled', true);
                                $("#t_f").prop('disabled', true);
                                $("#tallas-pisos").toggle('slow');
                                $("#serie-container").toggle('slow');
                            }
                        });
                        $("#s_stock").change(function () {
                            if ($(this).prop("checked")) {
                                $("#tipo1").val("");
                                $("#stock_pisos").toggle('fast');
                                $("#tipo1").prop('disabled', false);
                                $("#tipo2").prop('disabled', false);
                            } else {
                                $("#tipo1").val("");
                                $("#tipo1").prop('disabled', true);
                                $("#tipo2").prop('disabled', true);
                                $("#stock_pisos").toggle('slow');
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $('#liAlmacen').addClass("treeview active");
            $('#liCategorias').addClass("active");
        </script>
    @endpush
@endsection
