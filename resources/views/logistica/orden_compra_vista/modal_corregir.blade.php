<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-reporte">
{!!Form::open(array('url'=>'logistica/orden/corregir','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desea Corregir la orden?</h4>
			</div>
            
			<div class="modal-body">
                <input type="text" style="display:none" name='orden' value='{{$var}}'>
			</div>

			<div class="modal-footer">
				<a  target="_blank">
					<button  class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				</a>

				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
