@extends ('layouts.admin')
@section ('contenido')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $("#inst").click(function(event){
        $(".block").prop('disabled',false);
        $(".borr").prop('disabled',true);

    });
  });

</script>
<div>
  <h1 class="asd">Orden de Compra N° {{$var}}</h1>
  <a href="" data-target="#modal-reporte" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Correccion de orden</button>
        </a>
</div>
@include('logistica.orden_compra_vista.modal_corregir')
<div class="row">
  @foreach($cabecera as $cab)
  <div class="col-md-4">
    <p>RUC: {{$cab->RUC_proveedor}}</p>
    <p>Proveedor: {{$cab->nom_proveedor}}</p>
    <p>Direccion: {{$cab->direc_proveedor}}</p>
  </div>
  <div class="col-md-4">
    @if($cab->tipo_moneda_orden==0)
    <p>Tipo de Moneda: Soles</p>
    @else
    <p>Tipo de Moneda: Dolares</p>
    <p>Tipo de Cambio: {{$cab->t_cambio}}</p>
    @endif
  </div>
  <div class="col-md-4">
    <p>Fecha de entrega Aproximada: {{$cab->fecha_entrega}}</p>
  </div>
  @endforeach
</div>
  <hr class=" my-4">
  <input type="text" name="cod_orden" value="{{$var}}" style="display:none">
<h2 class="">Materias Primas</h2>
<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Unidad de Compra</th>
        <th>Cantidad Pedida</th>
        <th>Cantidad Recibida</th>
        <th>Cantidad Sin Recibir</th>
        <th>Fecha de Pago</th>
        <th>Ver Otros Pagos</th>
      </tr>
    </thead>
    <tbody>
      @foreach($detalleorden as $det)
      <tr>
        <td>{{$det->cod_material}}</td>
        <td>{{$det->nom_subcategoria}}</td>
        <td>{{$det->descrip_material}}</td>
        <td>{{$det->descrip_unidad_compra}}</td>
        <td>{{$det->cantidad}}</td>
        <td>{{$det->cantidad-$det->cantidad_restante}}</td>
        <td>{{$det->cantidad_restante}}</td>
        <td>{{$det->fecha_deposito}}</td>
        <td>
          <a href="{!! route('importacion',['var'=>$det->id_detalle_oc.'+'.$det->descrip_material]) !!}">
            <button type="button" class="bttn-unite bttn-md bttn-success ">
              <i class="fas fa-plus"></i>
            </button>
          </a>
        </td>
      </tr>
        @endforeach
    </tbody>
  </table>
</div>
<h2>Insumos</h2>
<div class="x_content table-responsive">
  <table id="example2" class="display">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Unidad de Compra</th>
        <th>Cantidad Pedida</th>
        <th>Cantidad Recibida</th>
        <th>Cantidad Sin Recibir</th>
        <th>Fecha de Pago</th>
        <th>Ver Otros Pagos</th>
      </tr>
    </thead>
    <tbody>
      @foreach($detalleordeninsu as $det)
        <tr>
        <td>{{$det->cod_material}}</td>
        <td>{{$det->nom_subcategoria}}</td>
        <td>{{$det->descrip_material}}</td>
        <td>{{$det->descrip_unidad_compra}}</td>
        <td>{{$det->cantidad}}</td>
        <td>{{$det->cantidad-$det->cantidad_restante}}</td>
        <td>{{$det->cantidad_restante}}</td>
        <td>{{$det->fecha_deposito}}</td>
        <td>
          <a href="{!! route('importacion',['var'=>$det->id_detalle_oc.'+'.$det->descrip_material]) !!}">
            <button type="button" class="bttn-unite bttn-md bttn-success ">
              <i class="fas fa-plus"></i>
            </button>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<h2>Suministros</h2>
<div class="x_content table-responsive">
  <table id="example3" class="display">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Unidad de Compra</th>
        <th>Cantidad Pedida</th>
        <th>Cantidad Recibida</th>
        <th>Cantidad Sin Recibir</th>
        <th>Fecha de Pago</th>
        <th>Ver Otros Pagos</th>
      </tr>
    </thead>
    <tbody>
      @foreach($detalleordensumi as $det)
        <tr>
        <td>{{$det->cod_material}}</td>
        <td>{{$det->nom_subcategoria}}</td>
        <td>{{$det->descrip_material}}</td>
        <td>{{$det->descrip_unidad_compra}}</td>
        <td>{{$det->cantidad}}</td>
        <td>{{$det->cantidad-$det->cantidad_restante}}</td>
        <td>{{$det->cantidad_restante}}</td>
        <td>{{$det->fecha_deposito}}</td>
        <td>
          <a href="{!! route('importacion',['var'=>$det->id_detalle_oc.'+'.$det->descrip_material]) !!}">
            <button type="button" class="bttn-unite bttn-md bttn-success ">
              <i class="fas fa-plus"></i>
            </button>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<div class="row">

  @foreach($cabecera as $cab)
  <div class="col-md-12">
      <p> Comentario</p>
<textarea disabled name="name" rows="8" style="width:100%" >{{$cab->comentario_oc}}</textarea>
  </div>

  @endforeach
</div>
<a href="{!! url('logistica/ingreso_salida') !!}" >
  <button class="bttn-unite bttn-md bttn-danger ">Atras</button></a>
@endsection
