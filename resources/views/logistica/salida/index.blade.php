@extends ('layouts.admin')
@section ('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
          <div class="x_title ">
              <h1 class="d-inline font-weight-bold ">Salida de Material </h1>
              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

            <div class="clearfix"></div>
          </div>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

          <div class="x_content">
            {!!Form::open(array('url'=>'logistica/salida_material','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="">
              <div class="row">
                <div class="col-md-4">
                  <h4>Area</h4>
                  <select required id="area_sal" name="area" class="custom-select">
                    <option value="" selected disabled>Area</option>
                    @foreach($area as $ar)
                      <option value="{{$ar->cod_area}}" >{{$ar->descrip_area}}</option>
  									@endforeach
                  </select>
                </div>
                <div class="col-md-4" style="display:none" id="almacen_sal">
                  <label for="">Tienda</label>
                  <select class="custom-select" required name="almacen_sal" id="select_almacen_sal">
                    <option value="" disabled selected>Seleccione una Tienda</option>
                    @foreach($tiendas as $ti)
                      <option value="{{$ti->cod_almacen}}">{{$ti->nom_almacen}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <h4>Categoria</h4>
                  <select  name="role" id="categoria" class="custom-select">
                    <option value="" selected disabled>Categoria</option>
                    @foreach($categorias as $cat)
                      <option value="{{$cat->cod_categoria}}" >{{$cat->nom_categoria}}</option>
  									@endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Subcategoria</h4>
                  <select  name="role" id="sub-categoria" class="custom-select">
                    <option value="" selected disabled>Subcategoria</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Materiales</h4>
                  <input type="text" class="form-control" id="filtro" placeholder="buscar..." value="">
                  <div id="materiales" style="overflow-y:scroll;height:150px">

                  </div>
                </div>
              </div>
              <div id="alerta" style="display:none; margin:20px;">
                <div class="alert alert-danger" role="alert">
                  Ya agrego ese Material
                </div>
              </div>
              <div id="buscando" style="display:none; margin:20px;">
                <div class="alert alert-success" role="alert">
                  Obteniendo materiales...
                </div>
              </div>
            </div>

              <h2>Materias Primas</h2>
              <div class="x_content table-responsive">
                <table id="table_mp" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo de Material</th>
                      <th>Descripcion</th>
                      <th>Subcategoria</th>
                      <th>Stock Actual</th>
                      <th>Cantidad a Entregar</th>
                      <th>Unidad de Compra</th>
                      <th>Persona de Traslado</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <h2>Insumos</h2>
              <div class="table-responsive">
                <table id="table_ins" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo de Material</th>
                      <th>Descripcion</th>
                      <th>Subcategoria</th>
                      <th>Stock Actual</th>
                      <th>Cantidad a Entregar</th>
                      <th>Unidad de Compra</th>
                      <th>Persona de Traslado</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <h2>Suministros</h2>
              <div class="table-responsive">
                <table id="table_sum" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo de Material</th>
                      <th>Descripcion</th>
                      <th>Subcategoria</th>
                      <th>Stock Actual</th>
                      <th>Cantidad a Entregar</th>
                      <th>Unidad de Compra</th>
                      <th>Persona de Traslado</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <div class="">
                <div class="row">
                  <div class="col-md-3">
                    <h4>Categoria</h4>
                    <select  name="role" id="cat" class="custom-select">
                      <option value="" selected disabled>Categoria</option>
                      @foreach($categorias as $cat)
                        <option value="{{$cat->cod_categoria}}" >{{$cat->nom_categoria}}</option>
    									@endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <h4>Subcategoria</h4>
                    <select  name="role" id="subcat" class="custom-select">
                      <option value="" selected disabled>Subcategoria</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <h4>Familia</h4>
                    <select  name="role" id="fam" class="custom-select">
                      <option value="" selected disabled>Subcategoria</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <h4>Materiales</h4>
                    <div id="mats">

                    </div>
                  </div>
                </div>
              </div>
              <h2>Salida de Materiales con Talla</h2>
              <div class="table-responsive">
                <table id="tabla_talla" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Descripccion</th>
                      <th>18</th>
                      <th>19</th>
                      <th>20</th>
                      <th>21</th>
                      <th>22</th>
                      <th>23</th>
                      <th>24</th>
                      <th>25</th>
                      <th>26</th>
                      <th>27</th>
                      <th>28</th>
                      <th>29</th>
                      <th>30</th>
                      <th>31</th>
                      <th>32</th>
                      <th>33</th>
                      <th>34</th>
                      <th>35</th>
                      <th>36</th>
                      <th>37</th>
                      <th>38</th>
                      <th>39</th>
                      <th>40</th>
                      <th>41</th>
                      <th>42</th>
                      <th>43</th>
                      <th>44</th>
                      <th>45</th>
                      <th>46</th>
                      <th>Unidad de Compra</th>
                      <th>Persona de Traslado</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <div class=" col-md-12">
                <button type="submit" class="bttn-unite bttn-md bttn-primary" target="_blank" id="sub">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger">Cancelar</button>
              </div>
            {!!Form::close()!!}
            <script>
              $(document).ready(function(){

                $("#area_sal").on('change',function(){
                  let val=$(this).val();
                  console.log(val)
                  if(val=="47812")
                  {
                    $("#almacen_sal").show("fast")
                    $("#select_almacen_sal").prop('required',true);
                  }
                  else
                  {
                    $("#almacen_sal").hide("fast")
                    $("#select_almacen_sal").prop('required',false);
                  }
                })
                var mats_obt;
                var lista_materiales=[];
                var cod_materiales=[];
                $("#filtro").keyup(function(){
                  var valor=$("#filtro").val();
                  if(lista_materiales.length==0)
                  {
                    alert("Primero seleccione una categoria y una subcategoria")
                    $("#filtro").val("");
                  }
                  else {
                    var tmp=lista_materiales;
                    if(valor!="")
                    {
                      for (var i = 0; i < tmp.length; i++) {
                        if(tmp[i].indexOf(valor)==-1)
                        {
                          $("#d"+cod_materiales[i]).hide();
                        }
                        else {
                          $("#d"+cod_materiales[i]).show();
                        }
                      }
                    }
                    else
                    {
                      for (var i = 0; i < tmp.length; i++) {
                          $("#d"+cod_materiales[i]).show();
                      }
                    }
                  }
                })
                $("#sub").click(function(){
                  $(".block").attr("disabled",false);
                  setTimeout(function(){ $(".block").prop('disabled',true); }, 100);
                })
                //Materiales con tallas
                $("#cat").change(function(){
                  $("#mats").empty();
                  $("#fam").empty();
                  let valor=$("#cat").val();
                  switch(valor)
                  {
                    case "306":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==306)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                    case "634":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==634)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                    case "969":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==969)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                  }
                });
                $("#subcat").change(function(){
                  $("#fam").empty()
                  $("#mats").empty()
                  var valor=$("#subcat").val();
                  recuperar_materiales(valor,1);
                })
                $("#fam").change(function(){
                    var mat=[]
                    $("#mats").empty()
                    var valor=$("#fam option:selected").text();
                    $.each(mats_obt, function(key,value){
                      var codigo=value.cod_material;
                      var nombre=value.descrip_material;
                      var completo_sintalla=nombre.substr(0,nombre.lastIndexOf(" "));
                      if(valor===completo_sintalla)
                      {
                        var completo=nombre.substr(0,nombre.indexOf("-"));
                        if (mat.indexOf(completo)==-1) {
                          mat.push(completo);
                          $("#mats").append("<div class='custom-control custom-checkbox'><input  type='checkbox' class='custom-control-input' id='talla"+value.cod_material+"'><label class='custom-control-label' for=talla"+value.cod_material+">"+completo+"</label></div>")
                          $("#talla"+value.cod_material).off("click");
                          $("#talla"+value.cod_material).on("click",function(){
                            if(!$(this).checked)
                            {
                              if ( !$("#"+value.cod_material+"talla").length > 0 ) {
                                    var nombre_completo=value.descrip_material;
                                    var descripcion="";
                                    descripcion=nombre_completo.substr(0,nombre_completo.indexOf("-"));
                                    descripcion=descripcion.toString();
                                    var tallas=[];
                                    var stocks=[]
                                    var codigo=[]
                                    $.each(mats_obt, function(key,value_dos){
                                      var nomb=value_dos.descrip_material;
                                      var stock=value_dos.stock_total;
                                      var cod=value_dos.cod_material;
                                      var n=nomb.substr(0,nomb.indexOf("-"));
                                      var talla_n=nomb.substr(nomb.indexOf("-")+1);
                                      if(descripcion===n)
                                      {
                                        codigo.push(cod)
                                        tallas.push(talla_n)
                                        stocks.push(stock)
                                      }
                                    });
                                    var contador=0;
                                    var tallasaceptadas="";
                                    for (var i = 18; i < 47 ; i++) {
                                      if (tallas.indexOf(i.toString())>-1) {
                                        tallasaceptadas+="<td style='display:none'><input name='"+value.cod_material+"stocks[]' value='"+stocks[contador]+"' ></td><td style='display:none'><input name='"+value.cod_material+"cods[]' value='"+codigo[contador]+"' ></td><td><input name='"+value.cod_material+"ca[]' style='width:50px' type='text' placeholder='"+stocks[contador]+"'></td>"
                                        contador++;
                                      }
                                      else {
                                        tallasaceptadas+="<td><input disabled style='width:50px' class='' type='text'></td>"
                                      }
                                    }
                                    $('#tabla_talla> tbody:last-child').append("<tr id='"+value.cod_material+"talla'>"
                                    +"<td style='display:none'><input  class='' type='text' name='idtalla[]'  value='"+value.cod_material+"'></td>"
                                    +"<td><input class='block' type='text' name='tdescrip[]'  disabled value='"+descripcion+"'></td>"
                                    +tallasaceptadas
                                    +"<td><input class='form-control block' name='unidadtalla[]' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                                    +"<td><input required class='' type='text' id='"+value.cod_material+"perso' name='"+value.cod_material+"persona[]' ></td>"
                                    +"<td><a class='btn btn-danger' id='"+value.cod_material+"tallab'>-</a></td>"
                                    +"</tr>");
                                    $("#"+value.cod_material+"tallac").off("change");
                                    $("#"+value.cod_material+"tallac").on("change",function(){
                                      if($(this).val()==1)
                                      {
                                        $("#"+value.cod_material+"talladi").attr("disabled",false);
                                      }
                                      else {
                                        $("#"+value.cod_material+"talladi").attr("disabled",true);
                                      }
                                    });
                                    $("#"+value.cod_material+"tallab").off("click");
                                    $("#"+value.cod_material+"tallab").on("click",function(){
                                      $("#"+value.cod_material+"talla").remove();
                                      $("#talla"+value.cod_material+"").attr("disabled", false);
                                      $("#talla"+value.cod_material+"").prop("checked", false);
                                    });
                              }
                              else {
                                  $("#alerta").toggle("slow");
                                setTimeout(function() {
                                      $("#alerta").hide("slow");
                                  }, 2000);
                              }
                              $("#talla"+value.cod_material).attr("disabled", true);
                            }
                          });
                        }
                      }
                    });


                });
                //Materiales sin tallas
                $( "#categoria" ).change(function() {
                  $("#materiales").empty();
                    let valor=$("#categoria").val();
                    switch (valor)
                    {
                      case "306":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==306)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                      case "634":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==634)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                      case "969":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==969)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                    }
                });
                $("#sub-categoria").change(function(){
                  var valor=$("#sub-categoria").val();
                    $("#materiales").empty();
                    lista_materiales=[];
                    cod_materiales=[];
                    recuperar_materiales(valor,0)
                });
                function recuperar_materiales(subcategoria,t_c)
                {
                  let cod=subcategoria+"+"+t_c;
                  $("#buscando").toggle("slow");
                  $.ajax({
                    url: "../salida_material/obtener_material/"+cod,
                    success: function(html){
                      $("#buscando").toggle("slow");
                      if(t_c==0)
                      {
                        generar_opciones(html)
                      }
                      else {
                        generar_opciones_talla(html);
                      }
                    }
                  });
                }
                function generar_opciones(datos) {
                  console.log(datos)
                  mats_obt=datos;
                  $.each(mats_obt, function(key,value){
                    var nombre=value.descrip_material;
                    var codigo=value.cod_material;
                    lista_materiales.push(nombre);
                    cod_materiales.push(codigo);
                    $("#materiales").append("<div id='d"+codigo+"' class='custom-control custom-checkbox'><input  type='checkbox' class='custom-control-input' id='"+value.cod_kardex_material+"'><label class='custom-control-label' for='"+value.cod_kardex_material+"'>"+nombre+"</label></div>");
                    $("#"+value.cod_kardex_material).off("click");
                    $("#"+value.cod_kardex_material).on("click",function(){
                      if(!$(this).checked)
                        {
                          if ( !$("#"+value.cod_kardex_material+"v").length > 0 ) {
                            let categoria=value.cod_categoria;
                            switch (categoria) {
                              case 306:
                                $('#table_ins> tbody:last-child').append("<tr id='"+value.cod_kardex_material+"v'>"
                                +"<td><input class='form-control block' type='text' name='codigo[]' disabled value='"+value.cod_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.nom_subcategoria+"'></td>"
                                +"<td><input class='form-control block' type='text' name='stock[]' disabled value='"+value.stock_total+"'></td>"
                                +"<td><input class='form-control ' type='text' name='salida[]' required></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_unidad_medida+"'></td>"
                                +"<td><input class='form-control ' type='text' name='persona[]' required></td>"
                                +"<td><a class='btn btn-danger' id='"+value.cod_kardex_material+"b'>-</a></td>"
                                +"</tr>");
                                $("#"+value.cod_kardex_material+"b").off("click");
                                $("#"+value.cod_kardex_material+"b").on("click",function(){
                                  $("#"+value.cod_kardex_material+"v").remove();
                                  $("#"+value.cod_kardex_material).attr("disabled", false);
                                  $("#"+value.cod_kardex_material).prop("checked", false);
                                });
                              break;
                              case 634:
                                $('#table_sum> tbody:last-child').append("<tr id='"+value.cod_kardex_material+"v'>"
                                +"<td><input class='form-control block' type='text' name='codigo[]' disabled value='"+value.cod_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.nom_subcategoria+"'></td>"
                                +"<td><input class='form-control block' type='text' name='stock[]' disabled value='"+value.stock_total+"'></td>"
                                +"<td><input class='form-control ' type='text' name='salida[]' required></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_unidad_medida+"'></td>"
                                +"<td><input class='form-control ' type='text' name='persona[]' required></td>"
                                +"<td><a class='btn btn-danger' id='"+value.cod_kardex_material+"b'>-</a></td>"
                                +"</tr>");
                                $("#"+value.cod_kardex_material+"b").off("click");
                                $("#"+value.cod_kardex_material+"b").on("click",function(){
                                  $("#"+value.cod_kardex_material+"v").remove();
                                  $("#"+value.cod_kardex_material).attr("disabled", false);
                                  $("#"+value.cod_kardex_material).prop("checked", false);
                                });
                              break;
                              case 969:
                                $('#table_mp> tbody:last-child').append("<tr id='"+value.cod_kardex_material+"v'>"
                                +"<td><input class='form-control block' type='text' name='codigo[]' disabled value='"+value.cod_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_material+"'></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.nom_subcategoria+"'></td>"
                                +"<td><input class='form-control block' type='text' name='stock[]' disabled value='"+value.stock_total+"'></td>"
                                +"<td><input class='form-control ' type='text' name='salida[]' required></td>"
                                +"<td><input class='form-control' type='text'  disabled value='"+value.descrip_unidad_medida+"'></td>"
                                +"<td><input class='form-control ' type='text' name='persona[]' required></td>"
                                +"<td><a class='btn btn-danger' id='"+value.cod_kardex_material+"b'>-</a></td>"
                                +"</tr>");
                                $("#"+value.cod_kardex_material+"b").off("click");
                                $("#"+value.cod_kardex_material+"b").on("click",function(){
                                  $("#"+value.cod_kardex_material+"v").remove();
                                  $("#"+value.cod_kardex_material).attr("disabled", false);
                                  $("#"+value.cod_kardex_material).prop("checked", false);
                                });
                              break;
                              default:
                            }
                          }
                          else {
                              $("#alerta").toggle("slow");
                            setTimeout(function() {
                                  $("#alerta").hide("slow");
                              }, 2000);
                          }
                          $("#"+value.cod_kardex_material).attr("disabled", true);
                        }
                    });

                  })

                }
                function generar_opciones_talla(datos){
                  mats_obt=datos;
                  $("#fam").append("<option value='' selected disabled>Familia</option>");
                  var lista=[];
                  $.each(mats_obt, function(key,value){
                      var nombre=value.descrip_material;
                      var completo_sintalla=nombre.substr(0,(nombre.lastIndexOf(" ")));
                      var familia=completo_sintalla;
                      if(lista.indexOf(familia)==-1)
                      {
                        lista.push(familia)
                      }
                  });
                  if(lista.length>0)
                  {
                    lista.forEach(function(element) {
                      $("#fam").append("<option value='element'>"+element+"</option>");
                    });
                  }
                }
              });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
