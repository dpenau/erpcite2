<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$m->codigo_importaciones}}">
{!!Form::open(array('url'=>'logistica/editar_importacion','method'=>'PUT','autocomplete'=>'off'))!!}
      {{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Gasto {{$m->descripcion_gasto}}</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="codigo" value="{{$m->codigo_importaciones}}">
				<input type="text" style="display:none" name="detalle" value="{{$codigo_detalle}}">
        <input type="text" style="display:none" name="descrip" value="{{$nombre}}">
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de gasto:</label>
           <textarea type="text" class="form-control" required name="descripcion" style="width:100%" maxlength="300" >{{$m->descripcion_gasto}}</textarea>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="">Tipo de moneda</label>
            <select name="moneda" class="custom-select" required>
              <option value="" disabled>Escoja uno</option>
              @if($m->tipo_gasto_moneda==0)
                <option value="0" selected>Soles</option>
                <option value="1">Dolares</option>
              @else
              <option value="0" >Soles</option>
                <option value="1" selected>Dolares</option>
              @endif
            </select>
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Importe:</label>
             <input type="text" class="form-control" required name="importe" onkeypress="return isNumberKey(event)" value="{{$m->importe_gasto}}" >
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
