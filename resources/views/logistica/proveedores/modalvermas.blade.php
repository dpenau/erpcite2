<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-vermas-{{ $pro->RUC_proveedor }}">
    <div class="modal-dialog modal-lg modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ $pro->nom_proveedor }}</h4>

            </div>
            <div class="modal-body">
                <input type="text" style="display:none" name="email" value="{{ $pro->RUC_proveedor }}">
                <input type="text" style="display:none" name="estado" value="0">
				<div class="row">
					<div class="form-group  col-md-6   col-xs-12">
						<label for="total_orden_compra">Categoria: </label>
						<input type="text" class="form-control " value="{{ $pro->nom_categoria }}" disabled>
					</div>
					<div class="form-group  col-md-6 col-xs-6" id="probando">
                        <label >Materiales Principales:</label>
                        <div style="overflow-y:scroll;height:150px">
						@foreach ($provsub as $item)
							@if ($item->estado == 1 && $item->RUC_proveedor == $pro->RUC_proveedor)
								<p>{{ $item->nom_subcategoria}}</p>
							@endif
						@endforeach
						</div>
                    </div>
				</div>
                <div class="row">
                    <div class="form-group  col-md-12   col-xs-12">
                        <label for="total_orden_compra">RUC: </label>
                        <input type="text" class="form-control " value="{{ $pro->RUC_proveedor }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 1: </label>
                        <input type="text" class="form-control " value="{{ $pro->direc_proveedor }}" disabled>
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 2 (Tienda): </label>
                        <input type="text" class="form-control " value="{{ $pro->direc_tienda }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Celular del proveedor:</label>
                        <input type="text" class="form-control " value="{{ $pro->cel_proveedor }}" disabled>
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Correo:</label>
                        <input type="text" class="form-control " value="{{ $pro->correo_proveedor }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 1:</label>
                        <input type="text" class="form-control " value="{{ $pro->nomb_contacto }}" disabled>
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 1:</label>
                        <input type="text" class="form-control " value="{{ $pro->telefono_contacto }}" disabled>
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 1:</label>
                        <input type="text" class="form-control " value="{{ $pro->correo_contacto }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 2:</label>
                        <input type="text" class="form-control " value="{{ $pro->nomb_contacto2 }}" disabled>
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 2:</label>
                        <input type="text" class="form-control " value="{{ $pro->telefono_contacto2 }}" disabled>
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 2:</label>
                        <input type="text" class="form-control " value="{{ $pro->correo_contacto2 }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
