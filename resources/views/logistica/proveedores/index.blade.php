@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">

    </div>
    <div>
        <h3 class="font-weight-bold">Listado de Proveedores <a href="proveedores/create">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Proveedor</button></a>
            <a href="" data-target="#modal-reporte" data-toggle="modal">
                <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Reportes de proveedor</button>
            </a>
        </h3>
    </div>
    @include('logistica.proveedores.modal_reporte')
    <div class="x_content table-responsive">
        <table id="example" class="display">
            <thead>
                <tr>
                    <th>Tipo de Proveedor</th>
                    <th>Razon Social</th>
                    <th>Direccion 1</th>
                    <th>Celular de proveedor</th>
                    <th>Nombre de contacto</th>
                    <th>Telefono de contacto</th>
                    <th>Ver mas</th>
                    <th>Editar</th>
                    <th align="center">Activar/Desactivar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($proveedor as $pro)
                    @if ($pro->estado_proveedor == 0)
                        <tr class="bg-danger" align="center">
                    @else
                        <tr align="center">
                    @endif

                    @if ($pro->telefono_contacto != null)
                        <td>{{ $pro->nom_categoria }}</td>
                        <td>{{ $pro->nom_proveedor }}</td>
                        <td>{{ $pro->direc_proveedor }}</td>
                        <td>{{ $pro->cel_proveedor }}</td>
                        <td>{{ $pro->nomb_contacto }}</td>
                        <td>{{ $pro->telefono_contacto }}</td>
                        <td>
                            <a href="#" id="{{ $pro->RUC_proveedor}}" data-target="#modal-vermas-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button></a>
                        </td>
                        <td>
                            <a class="edit" href="#" id="{{ $pro->RUC_proveedor}}" data-target="#modal-edit" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                        </td>
                        @if ($pro->estado_proveedor == 1)
                            <td>
                                <a href="#" data-target="#modal-delete-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                            </td>
                        @else
                            <td>
                                <a href="#" data-target="#modal-active-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-success"><i
                                            class="fas fa-check-circle"></i></button></a>
                            </td>
                        @endif
                    @else
                        <td>{{ $pro->nom_categoria }}</td>
                        <td>{{ $pro->nom_proveedor }}</td>
                        <td>{{ $pro->direc_proveedor }}</td>
                        <td>{{ $pro->cel_proveedor }}</td>
                        <td>{{ $pro->nomb_contacto }}</td>
                        <td style="text-align: center;">-</td>
                        <td>
                            <a href="#" id="{{ $pro->RUC_proveedor}}" data-target="#modal-vermas-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button></a>
                        </td>
                        <td>
                            <a class="edit" href="#" id="{{ $pro->RUC_proveedor}}" data-target="#modal-edit" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                        </td>
                        @if ($pro->estado_proveedor == 1)
                            <td>
                                <a href="#" data-target="#modal-delete-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                            </td>
                        @else
                            <td>
                                <a href="#" data-target="#modal-active-{{ $pro->RUC_proveedor }}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-success"><i
                                            class="fas fa-check-circle"></i></button></a>
                            </td>
                        @endif
                    @endif


                    </tr>
                    @include('logistica.proveedores.modalvermas')
                    @include('logistica.proveedores.modaleliminar')
                    @include('logistica.proveedores.modaleditar')
                    @include('logistica.proveedores.modalactivar')

                @endforeach
            </tbody>
        </table>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            let data = @json($proveedor);
            let ruc;
            const subcategoria = @json($subcategoria)

        $("#example").on('click','a.edit',function(){
            var id = $(this).attr("id");
            var index = buscar(id);
            ruc = id;
            $("#materiales").empty();
            modal_editar(index);
            let valor=$("#categoria").val()+"+"+ruc;
            generar_opciones(valor);
        });

        function buscar(id)  {
            var index = -1;
            var filteredObj = data.find(function(item, i){
                if(item.RUC_proveedor == id){
                    index = i;
                    return index;
                }
            });
            return index;
        }

        function modal_editar(index)  {
            var cat=data[index].cod_categoria
            console.log(data);
            $("#categoria option[value='"+cat+"']").prop("selected",true)
            $("#ruc_editar").val(data[index].RUC_proveedor)
            $("#nomb_proveedor").val(data[index].nom_proveedor)
            $("#dir_proveedor").val(data[index].direc_proveedor)
            $("#dir_tienda").val(data[index].direc_tienda)
            $("#tele_proveedor").val(data[index].cel_proveedor)
            $("#correo_proveedor").val(data[index].correo_proveedor)
            $("#nom_contacto").val(data[index].nomb_contacto)
            $("#tel_contacto").val(data[index].telefono_contacto)
            $("#correo_contacto").val(data[index].correo_contacto)
            $("#nom_contacto2").val(data[index].nomb_contacto2)
            $("#tel_contacto2").val(data[index].telefono_contacto2)
            $("#correo_contacto2").val(data[index].correo_contacto2)
            $("#myModal").modal({show:true});
        }

        $('#guardar').on('click', function(){
            let valoresCheck = [];
            $('input:checkbox:checked').each(function(){
                valoresCheck.push(this.value);
            });
            $("#subcat").val(valoresCheck);
            console.log(valoresCheck);
        });

        $("#categoria").change(function() {

            let valor=$("#categoria").val()+"+"+ruc;
            $("#materiales").empty();
            generar_opciones(valor);
        });

        function generar_opciones(valor) {
            $.ajax({
                url: "proveedores/consulta/obtener_subcategoria/" + valor,
                type: "get",
                success: function(html){
                    $("#materiales").append("<h6>Seleccione Material</h6>");
                    $.each(html,function(key,value){
                        var codigo=value.cod_subcategoria;
                        var nombre=value.nom_subcategoria;
                        var estado=value.estado;

                        if( estado == "1"){
                        $("#materiales").append("<input id='" + codigo +
                                        "' name='subcategorias[]' type='checkbox'  class='subcategoria' value='"+codigo+"' checked>"+
                                        "<label for='"+codigo+"'>"+nombre+"</label><br>");
                        } else {
                        $("#materiales").append("<input id='"+ codigo +
                                        "' name='subcategorias[] 'type='checkbox'  class='subcategoria' value='"+codigo+"' >"+
                                        "<label for='"+codigo+"'>"+nombre+"</label><br>");
                        }
                    });
                    $("#materiales").append( "</div>");

                }
            });
        };
        $("#l_mats").click(function(){

            window.open("../Logistica/proveedores/reporte/lmateriales");
        });
        $("#indicador").click(function(){

          window.open("../Logistica/proveedores/reporte/indicador");
      });


        $("#consultar").click(function(){
            var ruc=$("#ruc").val();
            $("#consultar").text("Consultando...");
            $.ajax({
                url:"consulta/ruc/"+ruc,
                Type:'get',
                dataType:"json",
                success:function(datos)
                {
                $("#consultar").text("Consultar");
                if(datos[0]!="nada")
                {
                    $("#razon_social").val(datos[0]);
                    $("#direccion1").val(datos[1])
                }
                else {
                    alert("RUC no existente");
                }
                }
            })
        });
    });
    </script>
@endsection
