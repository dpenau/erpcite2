@extends ('layouts.admin')
@section ('contenido')
<div>
  <h3 class="font-weight-bold">Listado de Orden de Compra <a href="orden_compra">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Orden de Compra</button></a></h3>
</div>
<div class="">
  <div id="filtros" class="row">
    <div class="col-md-4 col-xs-12 col-sm-12">
      <h6>Fecha Inicial:</h6>
      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
           <input type="date" id="fecha_inicial" class="form-control ">
      </div>
      <h6>Fecha Final</h6>
      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
           <input type="date" id="fecha_final" class="form-control ">
      </div>
      <button id="filtrar" class="bttn-unite bttn-md bttn-success" style="margin:10px">Buscar</button>
    </div>
  </div>
    <button id="ocultar_btn" class="bttn-unite bttn-md bttn-warning float-left mr-sm-5" style="margin:10px">Ocultar</button>
  </br>
</div>
<div class="x_content table-responsive">
  <table id="tabla_ordenes" class="display">
    <thead>
      <tr>
        <th>Código</th>
        <th>Fecha de emision</th>
        <th>Proveedor</th>
        <th>Recoger Material</th>
        <th>Ver Orden</th>
        <th>Imprimir Orden</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {
  var data =<?php echo $ordenes;?>;
  setInterval(function(){cargar_datos()},15000)
  function cargar_datos(){
    $("#actualizar_datos").toggle("fast");
    $.ajax({
      url: "/recoger/obtener/",
      success: function(html){
        data=html;
        actualizar_datos();
      }
    });
  }
  function actualizar_datos()
  {
    var fecha_inicio=$("#fecha_inicial").val();
    var fecha_final=$("#fecha_final").val()
    if(fecha_inicio!="" && fecha_final!="")
    {
      $("#filtrar").trigger('click');
    }
    $("#actualizar_datos").toggle("slow");
  }
  var t=$("#tabla_ordenes").DataTable();
  $("#ocultar_btn").on('click',function(){
    if($(this).text()=="Ocultar")
    {
      $(this).text("Mostrar")
    }
    else {
      $(this).text("Ocultar")
    }
    $("#filtros").toggle("fast");

  })
  $("#filtrar").on('click',function(){
    var fecha_inicio=$("#fecha_inicial").val();
    var fecha_final=$("#fecha_final").val()
    if(fecha_inicio!="" && fecha_final!="")
    {
      t
      .rows()
      .remove()
      .draw()
      $.each(data,function(key,value){
        var fecha=value.fecha_orden_compra.split(" ")
        if(fecha_inicio<=fecha[0] && fecha[0]<=fecha_final)
        {
          if(value.estado_orden_compra==1)
          {
            var re="<a target='_blank' href='/logistica/recoger/"+value.cod_orden_compra+"'>"+
              "<button class='bttn-unite bttn-md bttn-warning'><i class='fas fa-sign-in-alt'></i></button></a>"
          }
          else {
            var re="<p>Materiales Completos</p>"
          }
          t.row.add( [
            value.cod_orden_compra,
            value.fecha_orden_compra,
            value.nom_proveedor,
            re,
            "<a href='/logistica/orden_compra_vista/"+value.cod_orden_compra+"' target='_blank'>"+
            "<button class='bttn-unite bttn-md bttn-primary'><i class='fas fa-eye'></i></button></a>",
            "<a href='/pdf/orden_compra?codigo="+value.cod_orden_compra+"' target='_blank'>"+
            "<button class='bttn-unite bttn-md bttn-success'><i class='far fa-file-pdf'></i></button></a>"
        ] ).draw( false );
        }
      });
    }
    else {
      alert("Ingrese en los dos campos")
    }
  })
});
</script>
@endsection
