@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
          <div class="x_title ">
              <h1 class="d-inline font-weight-bold ">Orden de Compra </h1>
              @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
            <div class="clearfix"></div>
          </div>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          {!!Form::open(array('url'=>'logistica/orden_compra/guardar','method'=>'POST','autocomplete'=>'off'))!!}
          {{Form::token()}}
          <div class="x_content">
            <div class="">

              <div class="row">
                <div class="col-md-4">
                  <h4>Proveedor</h4>
                  <select required name="proveedor" id="prov"  class="custom-select">
                    <option value="" selected disabled>Proveedor</option>
                    @foreach($proveedores as $prov)
                      <option value="{{$prov->RUC_proveedor}}" >{{$prov->nom_proveedor}}</option>
  									@endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Tipo de Compra</h4>
                  <select required name="t_compra" id="tipo_compra"  class="custom-select">
                    <option value="" selected disabled>Tipo de compra</option>
                    <option value='1'>Credito</option>
                    <option value='2'>Contado</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Dias de Credito</h4>
                  <input type="text" maxlength="3" required class="form-control" id="d_compra" disabled name="dias" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <h4>Tipo de Moneda</h4>
                  <select required id="moneda" name="t_moneda" class="custom-select">
                    <option value=""disabled>Tipo de moneda</option>
                    <option value="0" selected>Soles</option>
                    <option value="1">Dolares</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <div class="form-group" id="t_cambio" >
                    <label for="">Tipo de Cambio</label>
                    <input disabled required type="text" class="form-control" id="camb" name="cambio" value="1">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group" id="t_cambio" >
                    <label for="">Fecha Aproximada de Entrega</label>
                    <input required type="date" class="form-control" name="f_entrega" value="">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <h4>Categoria</h4>
                  <select  name="role" id="categoria" class="custom-select">
                    <option value="" selected disabled>Categoria</option>
                    @foreach($categorias as $cat)
                      <option value="{{$cat->cod_categoria}}" >{{$cat->nom_categoria}}</option>
  									@endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Subcategoria</h4>
                  <select  name="role" id="sub-categoria" class="custom-select">
                    <option value="" selected disabled>Subcategoria</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <h4>Materiales</h4>
                  <input type="text" class="form-control" id="filtro" placeholder="buscar..." value="">
                  <div id="materiales" style="overflow-y:scroll;height:150px">
                  </div>
                </div>
              </div>
              <div id="alerta" style="display:none; margin:20px;">
                <div class="alert alert-danger" role="alert">
                  Ya agrego ese Material
                </div>
              </div>
              <div id="buscando" style="display:none; margin:20px;">
                <div class="alert alert-success" role="alert">
                  Obteniendo materiales...
                </div>
              </div>
            </div>
          </div>
              <h2>Materias Primas</h2>
              <div class="x_content table-responsive">
                <table id="table_mp" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo</th>
                      <th>Detalle</th>
                      <th>Cantidad</th>
                      <th>Unidad de Compra</th>
                      <th class="v_t_moneda">Valor Unitario sin IGV</th>
                      <th class="t_t_moneda">Valor Total</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <h2>Insumos</h2>
              <div class="table-responsive">
                <table id="table_ins" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo</th>
                      <th>Detalle</th>
                      <th>Cantidad</th>
                      <th>Unidad de Compra</th>
                      <th class="v_t_moneda">Valor Unitario sin IGV</th>
                      <th class="t_t_moneda">Valor Total</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <h2>Suministros</h2>
              <div class="table-responsive">
                <table id="table_sum" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Codigo</th>
                      <th>Detalle</th>
                      <th>Cantidad</th>
                      <th>Unidad de Compra</th>
                      <th class="v_t_moneda">Valor Unitario sin IGV </th>
                      <th class="t_t_moneda">Valor Total </th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <h1>Compra de Materiales por Tallas</h1>
              <div class="row">
                <div class="col-md-3">
                  <h4>Categoria</h4>
                  <select  name="role" id="cat"  class="custom-select">
                    <option value="" selected disabled>Categoria</option>
                    @foreach($categorias as $cat)
                      <option value="{{$cat->cod_categoria}}" >{{$cat->nom_categoria}}</option>
  									@endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <h4>Subcategoria</h4>
                  <select  name="role" id="subcat"  class="custom-select">
                    <option value="" selected disabled>Subcategoria</option>

                  </select>
                </div>
                <div class="col-md-3">
                  <h4>Familia</h4>
                  <select  name="role" id="fam"  class="custom-select">
                    <option value="" selected disabled>Familia</option>

                  </select>
                </div>
                <div class="col-md-3">
                  <h4>Materiales</h4>
                  <div class="" id="mat">

                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table id="tallas" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Detalle</th>
                      <th>18</th>
                      <th>19</th>
                      <th>20</th>
                      <th>21</th>
                      <th>22</th>
                      <th>23</th>
                      <th>24</th>
                      <th>25</th>
                      <th>26</th>
                      <th>27</th>
                      <th>28</th>
                      <th>29</th>
                      <th>30</th>
                      <th>31</th>
                      <th>32</th>
                      <th>33</th>
                      <th>34</th>
                      <th>35</th>
                      <th>36</th>
                      <th>37</th>
                      <th>38</th>
                      <th>39</th>
                      <th>40</th>
                      <th>41</th>
                      <th>42</th>
                      <th>43</th>
                      <th>44</th>
                      <th>45</th>
                      <th>46</th>
                      <th>Unidad de Compra</th>
                      <th class="v_t_moneda">Valor Unitario sin IGV</th>
                      <th class="t_t_moneda">Valor Total</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="total_orden_compra">Total Orden de Compra sin IGV:</label>
                  <div class="form-group">
                    <input disabled style="width:50%" id="total_orden_compra" type='text' class='form-control block' name='total_orden' value="0.0" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="total_orden_compra">Total Orden de Compra Contado sin IGV:</label>
                    <input disabled style="width:50%" id="total_orden_compra_contado" type='text' class='form-control block' name='total_orden_contado' value="0.0" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="total_orden_compra">Total Orden de Compra Credito sin IGV:</label>
                    <input disabled style="width:50%" id="total_orden_compra_credito" type='text' class='form-control block' name='total_orden_credito' value="0.0" >
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="total_orden_compra">Total Orden de Compra con IGV :</label>
                  <div class="form-group">
                    <input disabled style="width:50%" id="total_orden_compra_igv" type='text' class='form-control block' name='total_orden' value="0.0" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="total_orden_compra">Total Orden de Compra Contado con IGV:</label>
                    <input disabled style="width:50%" id="total_orden_compra_contado_igv" type='text' class='form-control block' name='total_orden_contado' value="0.0" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="total_orden_compra">Total Orden de Compra Credito con IGV:</label>
                    <input disabled style="width:50%" id="total_orden_compra_credito_igv" type='text' class='form-control block' name='total_orden_credito' value="0.0" >
                  </div>
                </div>
              </div>
              <button type="button" id="calcular_total" class="bttn-unite bttn-md bttn-primary">Calcular Total</button>
              <br>
              <span>*No olvide dar click luego de algun cambio</span>
              <div class="form-group">
                <label for="comment">Comentario:</label>
                  <textarea class="form-control" rows="5" maxlength="2000" id="comment" name="comentarios"></textarea>
              </div>
              <div class=" col-md-12">
                <button type="submit" class="bttn-unite bttn-md bttn-primary" target="_blank" id="sub">Guardar e imprimir</button>
                <a href="{!! url('bienvenida') !!}" >
                  <button class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
              </div>
            {!!Form::close()!!}
            <script>
              $(document).ready(function(){
                var mats_obt;
                var lista_materiales=[];
                var cod_materiales=[];
                $("#filtro").keyup(function(){
                  var valor=$("#filtro").val();
                  if(lista_materiales.length==0)
                  {
                    alert("Primero seleccione una categoria y una subcategoria")
                    $("#filtro").val("");
                  }
                  else {
                    var tmp=lista_materiales;
                    console.log("temporal")
                    console.log(tmp)
                    if(valor!="")
                    {
                      for (var i = 0; i < tmp.length; i++) {
                        if(tmp[i].indexOf(valor)==-1)
                        {
                          $("#d"+cod_materiales[i]).hide();
                        }
                        else {
                          $("#d"+cod_materiales[i]).show();
                        }
                      }
                    }
                    else
                    {
                      for (var i = 0; i < tmp.length; i++) {
                          $("#d"+cod_materiales[i]).show();
                      }
                    }
                  }
                })
                $("#moneda").change(function(){
                  $("#fam").empty();
                  $("#mat").empty();
                  $("#subcat").empty();
                    $("#sub-categoria").empty();
                  $("#materiales").empty();
                  $("#tallas> tbody").empty();
                  $("#table_mp> tbody").empty();
                  $("#table_ins> tbody").empty();
                  $("#table_sum> tbody").empty();

                  if($("#moneda").val()=="0")
                  {
                    $("#camb").val("1");
                    $("#camb").prop("disabled",true)
                    $(".t_t_moneda").text("Valor Total (S/.)");
                    $(".v_t_moneda").text("Valor Unitario sin IGV (S/.)");
                  }
                  else {
                    $("#camb").val("");
                    $(".t_t_moneda").text("Valor Total ($)");
                    $(".v_t_moneda").text("Valor Unitario sin IGV ($)");
                    $("#camb").prop("disabled",false);
                  }
                });
                $("#tipo_compra").change(function(){
                  if($("#tipo_compra").val()==1)
                  {
                    $("#d_compra").prop("disabled",false)
                  }
                  else {
                    $("#d_compra").prop("disabled",true)
                  }
                });
                //orden de compra con tallas
                $("#cat").change(function(){
                  $("#fam").empty();
                  $("#mat").empty();
                  let valor=$("#cat").val();
                  switch (valor)
                  {
                    case "306":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==306)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                    case "634":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==634)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                    case "969":
                    $("#subcat").empty();
                    $("#subcat").append("<option value='' selected disabled>Subcategoria</option>");
                      @foreach($subcategorias as $sub)
                        @if($sub->cod_categoria==969)
                            $("#subcat").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                        @endif
                      @endforeach
                    break;
                  }
                });
                $("#subcat").change(function(){
                  $("#fam").empty();
                  $("#mat").empty();
                  var valor=$("#subcat").val();
                  recuperar_materiales(valor,1);
                });
                $("#fam").change(function(){
                    var mat=[]
                    $("#mat").empty()
                    var valor=$("#fam option:selected").text();
                    $.each(mats_obt, function(key,value){
                      var codigo=value.cod_material;
                      var nombre=value.descrip_material;
                      var completo_sintalla=nombre.substr(0,nombre.lastIndexOf(" "));
                      if(valor===completo_sintalla)
                      {
                        var completo=nombre.substr(0,nombre.indexOf("-"));
                        if (mat.indexOf(completo)==-1) {
                          mat.push(completo);
                          $("#mat").append("<div class='custom-control custom-checkbox'><input  type='checkbox' class='custom-control-input' id='talla"+codigo+"'><label class='custom-control-label' for='talla"+codigo+"'>"+completo+"</label></div>")
                          $("#talla"+codigo).off("click");
                          $("#talla"+codigo).on("click",function(){
                            if(!$(this).checked)
                            {
                              if ( !$("#"+codigo+"talla").length > 0 ) {
                                var nombre_completo=value.descrip_material;
                                var descripcion="";
                                descripcion=nombre_completo.substr(0,nombre_completo.indexOf("-"));
                                descripcion=descripcion.toString();
                                var tallas=[];
                                $.each(mats_obt, function(key,value_dos){
                                  var nomb=value_dos.descrip_material;
                                  var n=nomb.substr(0,nomb.indexOf("-"));
                                  var talla_n=nomb.substr(nomb.indexOf("-")+1);
                                  if(descripcion===n)
                                  {
                                    tallas.push(talla_n)
                                  }
                                });
                                var contador=0;
                                var tallasaceptadas="";
                                for (var i = 18; i < 47 ; i++) {
                                  if (tallas.indexOf(i.toString())>-1) {
                                    tallasaceptadas+="<td><input required class='"+codigo+"tallacant' name='"+codigo+"ca[]' style='width:50px' type='text' value='0'></td>"
                                  }
                                  else {
                                    tallasaceptadas+="<td><input disabled style='width:50px' class='' type='text'></td>"
                                  }
                                }
                                $('#tallas> tbody:last-child').append("<tr id='"+codigo+"talla'>"
                                +"<td style='display:none'><input  class='' type='text' name='idtalla[]'  value='"+codigo+"'></td>"
                                +"<td><input class='block' type='text' name='tdescrip[]'  disabled value='"+descripcion+"'></td>"
                                +tallasaceptadas
                                +"<td><input class='form-control block' name='unidadtalla[]' type='text'disabled value='"+value.descrip_material+"'></td>"
                                +"<td><input class=' block' type='text' id='"+codigo+"valorigv' name='costotalla[]' style='width:50px'  disabled value='"+value.costo_sin_igv_material+"'></td>"
                                +"<td><input class=' block' disabled type='text' id='"+codigo+"tallat' name='totaltalla[]' ></td>"
                                +"<td><a class='btn btn-danger' id='"+codigo+"tallab'>-</a></td>"
                                +"</tr>");
                                $("#"+codigo+"tallab").off("click");
                                $("#"+codigo+"tallab").on("click",function(){
                                  $("#"+codigo+"talla").remove();
                                  $("#talla"+codigo).attr("disabled", false);
                                  $("#talla"+codigo).prop("checked", false);
                                });
                              }
                              else {
                                  $("#alerta").toggle("slow");
                                setTimeout(function() {
                                      $("#alerta").hide("slow");
                                  }, 2000);
                              }
                              $("#talla"+codigo).attr("disabled", true);
                            }
                          });
                        }
                      }
                    });
                });
                //ORDEN DE COMPRA NORMAL
                $( "#categoria" ).change(function() {
                    $("#materiales").empty();
                    let valor=$("#categoria").val();
                    switch (valor)
                    {
                      case "306":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==306)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                      case "634":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==634)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                      case "969":
                      $("#sub-categoria").empty();
                      $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
                        @foreach($subcategorias as $sub)
                          @if($sub->cod_categoria==969)
                              $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
                          @endif
                        @endforeach
                      break;
                    }
                });
                $("#sub-categoria").change(function(){
                  var valor=$("#sub-categoria").val();
                    $("#materiales").empty();
                    lista_materiales=[];
                    cod_materiales=[];
                    recuperar_materiales(valor,0);
                });
                function recuperar_materiales(subcategoria,t_c) {
                  let moneda=$("#moneda").val();
                  let cod=subcategoria+"+"+moneda+"+"+t_c;
                  $("#buscando").toggle("slow");
                  $.ajax({
                    url: "../orden_compra/obtener_material/"+cod,
                    success: function(html){
                      $("#buscando").toggle("slow");
                      if(t_c==0)
                      {
                        generar_opciones(html)
                      }
                      else {
                        generar_opciones_talla(html);
                      }
                    }
                  });
                }
                function generar_opciones(datos) {
                  mats_obt=datos;
                  $.each(mats_obt, function(key,value){
                    var nombre=value.descrip_material;
                    var codigo=value.cod_material;
                    lista_materiales.push(nombre);
                    cod_materiales.push(codigo);
                    $("#materiales").append("<div id='d"+codigo+"' class='custom-control custom-checkbox'><input  type='checkbox' class='custom-control-input' id='"+codigo+"'><label class='custom-control-label' for='"+codigo+"'>"+nombre+"</label></div>");
                    $("#"+codigo).off("click");
                    $("#"+codigo).on("click",function(){
                      if(!$(this).checked)
                      {
                        if ( !$("#"+codigo+"v").length > 0 ) {
                          let categoria=value.cod_categoria;
                          switch (categoria) {
                            case 306:
                            $('#table_ins> tbody:last-child').append("<tr id='"+codigo+"v'>"
                            +"<td><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                            +"<td><textarea class='form-control' rows='4' type='text'  disabled >"+nombre+"</textarea></td>"
                            +"<td><input required class='form-control' type='text' id='"+codigo+"c' name='cantidad[]'></td>"
                            +"<td><input class='form-control block' name='unidad[]' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                            +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+value.costo_sin_igv_material+"'></td>"
                            +"<td><input class='form-control block' disabled type='text' id='"+codigo+"t' name='total[]' ></td>"
                            +"<td><a class='btn btn-danger' id='"+codigo+"b'>-</a></td>"
                            +"</tr>");
                            $("#"+codigo+"c").off("focusout");
                            $("#"+codigo+"c").on("focusout",function(){
                              var cantidad=$("#"+codigo+"c").val();
                              var preciou=value.costo_sin_igv_material;
                              var tot=0.0;
                              tot=parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6);
                              $("#"+codigo+"t").val(tot);
                            });
                            $("#"+codigo+"b").off("click");
                            $("#"+codigo+"b").on("click",function(){
                              $("#"+codigo+"v").remove();
                              $("#"+codigo+"").attr("disabled", false);
                              $("#"+codigo+"").prop("checked", false);
                            });
                            break;
                            case 634:
                            $('#table_sum> tbody:last-child').append("<tr id='"+codigo+"v'>"
                            +"<td><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                            +"<td><textarea class='form-control' rows='4' type='text'  disabled >"+nombre+"</textarea></td>"
                            +"<td><input required class='form-control' type='text' id='"+codigo+"c' name='cantidad[]'></td>"
                            +"<td><input class='form-control block' name='unidad[]' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                            +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+value.costo_sin_igv_material+"'></td>"
                            +"<td><input class='form-control block' disabled type='text' id='"+codigo+"t' name='total[]' ></td>"
                            +"<td><a class='btn btn-danger' id='"+codigo+"b'>-</a></td>"
                            +"</tr>");
                            $("#"+codigo+"c").off("focusout");
                            $("#"+codigo+"c").on("focusout",function(){
                              var cantidad=$("#"+codigo+"c").val();
                              var preciou=value.costo_sin_igv_material;
                              var tot=0.0;
                              tot=parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6);
                              $("#"+codigo+"t").val(tot);
                            });
                            $("#"+codigo+"b").off("click");
                            $("#"+codigo+"b").on("click",function(){
                              $("#"+codigo+"v").remove();
                              $("#"+codigo+"").attr("disabled", false);
                              $("#"+codigo+"").prop("checked", false);
                            });
                            break;
                            case 969:
                            $('#table_mp> tbody:last-child').append("<tr id='"+codigo+"v'>"
                            +"<td><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                            +"<td><textarea class='form-control' rows='4' type='text'  disabled >"+nombre+"</textarea></td>"
                            +"<td><input required class='form-control' type='text' id='"+codigo+"c' name='cantidad[]'></td>"
                            +"<td><input class='form-control block' name='unidad[]' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                            +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+value.costo_sin_igv_material+"'></td>"
                            +"<td><input class='form-control block' disabled type='text' id='"+codigo+"t' name='total[]' ></td>"
                            +"<td><a class='btn btn-danger' id='"+codigo+"b'>-</a></td>"
                            +"</tr>");
                            $("#"+codigo+"c").off("focusout");
                            $("#"+codigo+"c").on("focusout",function(){
                              var cantidad=$("#"+codigo+"c").val();
                              var preciou=value.costo_sin_igv_material;
                              var tot=0.0;
                              tot=parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6);
                              $("#"+codigo+"t").val(tot);
                            });
                            $("#"+codigo+"b").off("click");
                            $("#"+codigo+"b").on("click",function(){
                              $("#"+codigo+"v").remove();
                              $("#"+codigo+"").attr("disabled", false);
                              $("#"+codigo+"").prop("checked", false);
                            });
                            break;
                            default:
                          }
                        }
                        else {
                            $("#alerta").toggle("slow");
                          setTimeout(function() {
                                $("#alerta").hide("slow");
                            }, 2000);
                        }
                        $("#"+codigo).attr("disabled", true);
                      }
                    });

                  })

                }
                function generar_opciones_talla(datos){
                  mats_obt=datos;
                  $("#fam").append("<option value='' selected disabled>Familia</option>");
                  var lista=[];
                  $.each(mats_obt, function(key,value){
                    var nombre=value.descrip_material;
                    var completo_sintalla=nombre.substr(0,(nombre.lastIndexOf(" ")));
                    var familia=completo_sintalla;
                    if(lista.indexOf(familia)==-1)
                    {
                      lista.push(familia)
                    }
                  });
                  if(lista.length>0)
                  {
                    lista.forEach(function(element) {
                      $("#fam").append("<option value='element'>"+element+"</option>");
                    });
                  }
                }
              });
            </script>
            <script>
              $("#calcular_total").click(function(){
                var tallatotal=$("input[name~='totaltalla[]']");
                var total_tallas=0
                for(var i=0;i<tallatotal.length;i++)
                {
                  var id=tallatotal.get(i).id
                  id=id.substr(0,id.indexOf('t'));
                  var cantidades=$("input[class~='"+id+"tallacant']");
                  var total=0;
                  for (var j = 0; j < cantidades.length; j++) {
                    total=parseFloat(cantidades[j].value)+parseFloat(total);
                  }
                  var igv=$('#'+id+'valorigv').val()
                  total=parseFloat(total)*parseFloat(igv)
                  total_tallas=parseFloat(total)+parseFloat(total_tallas);
                  $('#'+id+'tallat').val(total)
                }
                var totales=$("input[name~='total[]']");
                var tipos=$("#tipo_compra").val();
                var tallatotales=$("input[name~='totaltalla[]']");
                var total_orde=0.00;
                var total_credito=0.00;
                var total_contado=0.00;
                total_orde=total_tallas;
                for(var i=0;i<totales.length;i++)
                 {
                   var valor=totales[i].value;
                   total_orde=parseFloat(valor)+parseFloat(total_orde);
                   if(tipos=="1")
                   {
                     total_credito=parseFloat(valor)+parseFloat(total_credito);
                   }
                   else {
                     total_contado=parseFloat(valor)+parseFloat(total_contado);
                   }
                }
                for (var i = 0; i < tallatotales.length; i++) {
                  var valor=tallatotales[i].value;
                  if(tipos=="1")
                  {
                    total_credito=parseFloat(valor)+parseFloat(total_credito);
                  }
                  else {
                    total_contado=parseFloat(valor)+parseFloat(total_contado);
                  }
                }
                $("#total_orden_compra").val(total_orde.toFixed(6));
                $("#total_orden_compra_contado").val(total_contado.toFixed(6))
                $("#total_orden_compra_credito").val(total_credito.toFixed(6))
                igv=total_orde*1.18;
                $("#total_orden_compra_igv").val(igv.toFixed(6));
                igv=total_contado*1.18;
                $("#total_orden_compra_contado_igv").val(igv.toFixed(6))
                igv=total_credito*1.18;
                $("#total_orden_compra_credito_igv").val(igv.toFixed(6))
              });
              $("#sub").click(function(event){
                $("#calcular_total").click();
                $(".block").prop('disabled',false);
                setTimeout(function(){   $(".block").prop('disabled',true); }, 100);
              });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
