<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-reporte">
{!!Form::open(array('url'=>'Logistica/Kardex/reporte','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Reporte de Kardex</h4>
			</div>
			<div class="modal-body">
        <div class="form-group row" >
            <label for="name" class="col-md-4 col-form-label text-md-right">Accion:</label>
            <div class="col-md-6">
							<select required  name="accion" class="custom-select">
								<option value="" disabled>Accion</option>
								<option value="0" selected>Todo</option>
                <option value="1">Ingreso</option>
                <option value="2">Salida</option>
                <option value="3">Devolucion</option>
							</select>
            </div>
        </div>
				<div class="form-group row" >
            <label for="name" class="col-md-4 col-form-label text-md-right">Almacenes:</label>
            <div class="col-md-6">
							<select   name="almacen_reporte" class="custom-select">
								<option value="" disabled>Almacenes</option>
								@foreach($almacen_normal as $ar)
									<option value="{{$ar->cod_almacen}}" >{{$ar->nom_almacen}}</option>
								@endforeach
							</select>
            </div>
        </div>
        <div class="form-group row" >
            <label for="name" class="col-md-4 col-form-label text-md-right">Area a entregar:</label>
            <div class="col-md-6">
							<select   name="area_reporte" class="custom-select">
								<option value="" disabled>Area</option>
								<option value="" selected>Todas las Areas</option>
								@foreach($area as $ar)
									<option value="{{$ar->cod_area}}" >{{$ar->descrip_area}}</option>
								@endforeach
							</select>
            </div>
        </div>
				<div class="form-group row" >
            <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
            <div class="col-md-6">
							<select  name="subcategoria" class="custom-select">
								<option value="" disabled>Subcategoria</option>
								<option value="" selected>Todas los Subcategorias</option>
								@foreach($subcategoria as $ar)
									<option value="{{$ar->cod_subcategoria}}" >{{$ar->nom_subcategoria}}</option>
								@endforeach
							</select>
            </div>
        </div>
        <div class="form-group row" >
            <label for="name" class="col-md-4 col-form-label text-md-right">Material:</label>
            <div class="col-md-6">
							<select  name="material_reporte" class="custom-select">
								<option value="" disabled>Materiales</option>
								<option value="" selected>Todas los Materiales</option>
								@foreach($materiales as $ar)
									<option value="{{$ar->cod_kardex_material}}" >{{$ar->descrip_material}}</option>
								@endforeach
							</select>
            </div>
        </div>
				<div class="form-group row"  >
            <label for="name" class="col-md-4 col-form-label text-md-right">Fecha Inicial:</label>
            <div class="col-md-6">
              <input  type="date" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="fecha_inicio" value="" required>
            </div>
        </div>
        <div class="form-group row"  >
            <label for="name" class="col-md-4 col-form-label text-md-right">Fecha Final:</label>
            <div class="col-md-6">
              <input  type="date" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="fecha_final" value="" required>
            </div>
        </div>
				<p>* Para abrir en otra pestaña presionar ctrl + click en Confirmar</p>
			</div>

			<div class="modal-footer">
				<a  target="_blank">
					<button  class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				</a>

				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
