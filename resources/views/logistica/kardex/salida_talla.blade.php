@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Salida de varios materiales por Tallas</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'logistica/kardex/store', 'method' => 'POST', 'autocomplete' => 'off']) !!}

                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label class="col-md-6 col-form-label text-md-right" for="total_orden_compra">Fecha:</label>
                        <div class="col-md-6">
                            <input type="date" required="required" id="fecha" name="fecha"
                                value="{{ $fechaActual->format('Y-m-d') }}">
                        </div>
                    </div>

                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="name" class="col-md-6 col-form-label text-md-right">Area a entregar:</label>
                        <div class="col-md-6">
                            <select id="area_sal" required name="area" class="custom-select">
                                <option value="" selected disabled>Area</option>
                                @foreach ($areas as $ar)
                                    <option value="{{ $ar->cod_area }}">{{ $ar->descrip_area }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12" id="modal-data">
                        <button class="bttn-unite bttn-md bttn-warning" type="button" data-target="#modal-create"
                            data-toggle="modal" data-pro="$aux" id="modalCreate">Agregar Materiales</button>
                    </div>
                </div>

                <div class="x_content table-responsive">

                    <table id="table_salida_talla" class="display">
                        <thead>
                            <tr>
                                <th>Cod. Material</th>
                                <th>Descripcion</th>
                                <th>T1</th>
                                <th>T2</th>
                                <th>T3</th>
                                <th>T4</th>
                                <th>T5</th>
                                <th>T6</th>
                                <th>T7</th>
                                <th>Cantidad de Salida</th>
                                <th>Unidad de Compra</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody id="materialSelec">
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="hidden" id="tipoSalida" name="tipoSalida" value="1" class="form-control">
                        <input type="text" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <a href="{{ url('logistica/kardex/talla') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                        <button type="submit" id="guardar" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5"
                            value="GuardarNormal">Guardar</button>
                    </div>
                </div>
                {!! Form::close() !!}

                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                    $(document).ready(function() {
                        var tabla_talla = $('#table_salida_talla').DataTable( {
                        'columnDefs': [
                        {
                            'targets': 0,
                            'checkboxes': {
                            'selectRow': true
                            }
                        }
                        ],
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        }
                        });


                        var categoria = <?php echo $categorias; ?>;
                        var material = <?php echo $materiales; ?>;
                        var datatemp = [];
                        var costoTotalMat = 0;
                        var subTotal = 0;
                        var cantidadTot = 0;

                        $("#modalCreate").click(function() {
                            $("#categoria").empty();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");

                            generar_opciones_categoria();
                            //$("#materiales input[type=checkbox]").prop("checked", false);
                        });




                        $("#categoria").change(function() {
                            let valor = $("#categoria").val();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            generar_opciones_subcategoria(valor);
                        });

                        $('#guardar').on('click', function() {
                            $('#ordenCompraN').prop('disabled', false);
                            $('#subTotalval').prop('disabled', false);
                            $('#igvSubtotal').prop('disabled', false);
                            $('#idTotal').prop('disabled', false);
                        });

                        $("#subcategoria").change(function() {
                            let valor = $("#subcategoria").val();
                            generar_opciones_materiales(valor);
                            $("#materiales").empty();
                        });

                        $("#agregar").click(function() {
                            $("#materiales input[type=checkbox]").removeAttr("disabled");
                            var valor = 1;
                            $.each(material, function(key, value) {
                                $('input[name^="material_lista"]').each(function(){
                                    codigo_temporal = $(this).val();
                                    var seleccionado = $(this).is(':checked');
                                    if(seleccionado && value.cod_material == codigo_temporal && !(datatemp.find(element => element == value.cod_material))){
                                        $.ajax({
                                            url: "salida/obtenerTallas/" + value.id_kardex,
                                            success: function(html) {
                                                var partA= "<tr>"
                                                        +"<td><input name='codigo_material[]' type='hidden'value='" + value.cod_material + "' >" + value.cod_material +"</td>"
                                                        +"<td><input name='costo_con_igv[]' type='hidden' value='" + value.costo_con_igv_material + "' ><input name='costo_sin_igv[]' type='hidden' value='" + value.costo_sin_igv_material + "' >" + value.descrip_material + "</td>";
                                                var partB= "";
                                                var contador = 0;
                                                $.each(html, function(key, valueTalla){
                                                    partB = partB + "<td><input name='salida_talla-"+value.cod_material+"[]' type='hidden' value='"+valueTalla.talla+"'>T" + valueTalla.talla + "<br><input type='number' step='0.01' name='salidatallas-"+value.cod_material+"[]' id='talla"+value.cod_material+contador+"' placeholder='"+valueTalla.cantidad+"' style='width : 120%;' min='0' max='"+valueTalla.cantidad+"'></td>";
                                                    contador++;
                                                });
                                                for (let index = 0; index < 7-contador; index++) {
                                                    partB = partB + "<td> - </td>";
                                                }
                                                var partC="<td><input name='cantidad_salida[]' type='number' step='0.01' value='0'  id='cantidad_salida" + value.cod_material +"' style='width : 80%;' readonly></td>"
                                                        +"<td><input name='unidad_medida[]' type='hidden'value='" + value._compra + "'>"+ value.unidad + "</td>"
                                                        +"<td><button class='bttn-unite bttn-md bttn-danger' name='"+value.cod_material+"'><i class='fas fa-trash-alt'></i></button></a></td>"
                                                    +"</tr>";

                                                const tr = $(partA+partB+partC);
                                                tabla_talla.row.add(tr[0]).draw();

                                                for (let i = 0; i < contador; i++) {
                                                    var suma_salida_total = 0;
                                                    $('#talla'+value.cod_material+i).change(function() {
                                                        $('input[name^="salidatallas-'+value.cod_material+'"]').each(function(){
                                                            suma_salida_total = suma_salida_total + 1*$(this).val();
                                                            $('#cantidad_salida'+value.cod_material).val(suma_salida_total);
                                                        });
                                                        suma_salida_total = 0;
                                                    });
                                                }
                                                datatemp.push(value.cod_material);
                                            }
                                        });
                                    }
                                });
                            });
                        });

                        //ELIMINANDO FILA DE MATERIAL EN ORDEN DE COMPRA
                        $('#table_salida_talla').on('click', 'button', function() {
                        console.log($(this).attr('name'));
                        var indice = datatemp.indexOf($(this).attr('name'));
                        datatemp.splice(indice,1);
                        tabla_talla
                            .row($(this).parents('tr'))
                            .remove()
                            .draw();
                        });

                        function buscarCategoria(id) {
                            var index = -1;
                            var filteredObj = categoria.find(function(item, i) {
                                if (item.cod_categoria == id) {
                                    index = i;
                                    return index;
                                }
                            });
                            return index;
                        }

                        function generar_opciones_materiales(valor) {
                            $.ajax({
                                url: "salida/obtenerMaterialTalla/" + valor,
                                success: function(html) {
                                    $.each(html, function(key, value) {
                                        var codigo = value.cod_material;
                                        var nombre = value.descrip_material;
                                        if (datatemp.find(element => element == value.cod_material)) {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' checked disabled>" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        } else {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' >" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        }

                                    });
                                }
                            });
                        };

                        function generar_opciones_categoria(valor) {
                            $("#categoria").append("<option value='' selected disabled>Categoria</option>");
                            $.each(categoria, function(key, value) {
                                $("#categoria").append(" <option value='" + value.cod_categoria + "'>" + value
                                    .nom_categoria +
                                    "</option>");
                            });
                        };

                        function generar_opciones_subcategoria(valor) {

                            $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");
                            $.ajax({
                                url: "salida/obtenerSubcategoria/" + valor,
                                success: function(html) {
                                    $.each(html, function(key, value) {
                                        var codigo = value.cod_subcategoria;
                                        var nombre = value.nom_subcategoria;
                                        $("#subcategoria").append(" <option value='" + codigo + "'>" +
                                            nombre + "</option>");

                                    });
                                }
                            });

                        };
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header float-right">
                    <h5>Agrear Materiales por Tallas</h5>
                    <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                            <label for="total_orden_compra">Categoría del Producto:</label>
                            <select required="required" id="categoria" name="tipo_categoria" class="custom-select">
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                            <label for="total_orden_compra">Subcategoría del Producto:</label>
                            <select required="required" id="subcategoria" name="tipo_subcategoria" class="custom-select">
                                <option value="" disabled>Subategoria</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                            <label for="materiales">Buscar:</label>
                            <div id="materiales" style="overflow-y:scroll;height:150px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-right: 70%;">
                    <button type="submit" id="agregar" class="bttn-unite bttn-md bttn-success"
                        data-dismiss="modal">Agregar</button>
                    <button type="button" data-dismiss="modal"
                            class="bttn-unite bttn-md bttn-danger ">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $('#liAlmacen').addClass("treeview active");
            $('#liCategorias').addClass("active");
        </script>
    @endpush
@endsection
