<div class="modal fade" id="modal-reporte" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    {!!Form::open(array('url'=>'Logistica/Kardex/reporte_material_normal','method'=>'POST','autocomplete'=>'off'))!!}
        <div class="modal-content">
            <!--Si es reporte normal se manda con el codigo 1-->
            <input type="hidden" id="tipoReporte" name="tipoReporte" value="1" class="form-control">
            <div class="modal-header float-right">
                <h5>Historial de Movimientos</h5>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>
            
            <div class="modal-body">
                <label><strong>Seleccione intervalo de fechas</strong></label>
                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra">Desde:</label>
                        <input type="date" name="desde_fecha" class="form-control " id="desde_fecha" required>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra">Hasta:</label>
                        <input type="date" name="hasta_fecha" class="form-control " id="hasta_fecha" required>
                    </div>
                   
                </div>

                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra">Categoria:</label>
                        <select  name="subcategoria" class="custom-select"  id="categoria" required>
								<option value="" selected disabled>Categoria</option>
								@foreach($categoria as $ar)
									<option value="{{$ar->cod_categoria}}" >{{$ar->nom_categoria}}</option>
								@endforeach
							</select>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra">Subcategoria:</label>
                        <div class="multiselect">
                      
                            <div class="selectBox" onclick="showCheckboxes()">
                                <select id="material" class="custom-select">
                                    <option value="" selected disabled>Subcategoria</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes" style="overflow-y:scroll;height:150px">
                            </div>
                        </div>

                    </div>
                   
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                       
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                      
                    </div>
                   
                </div>
           
                <div id="auxiliar"></div>
            </div>
            <div class="modal-footer">
				 <button type="submit"   class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" id="guardarReporte">Guardar</button> 
				 <button  type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5 "  style="  margin-right:35%;" data-dismiss="modal">Cancelar</button> 
             
			</div>
            
        </div>
        {!! Form::Close() !!}
    </div>
    
</div>

<script type="text/javascript">
    var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}

var subcat_listado = "";
            

   
</script>