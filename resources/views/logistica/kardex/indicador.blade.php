@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div class="x_content">
    <div align="center">
        <h1>Indicadores de Kardex</h1> 
        <div class="row">
            <div class="col-md-6">
                <h2>Materiales Con <strong>MAYOR</strong> salida en el <strong>mes</strong> {{date("m")}}</h2>
                <div id="mayor_mes" >
                    <canvas id="grafico_mayor_mes"></canvas>
                </div>
                <h2>Materiales Con <strong>MAYOR</strong> salida en el <strong>año</strong> {{date("Y")}}</h2>
                <div id="mayor_ano" >
                    <canvas id="grafico_mayor_ano"></canvas>
                </div>
                <h2>Materiales Con <strong>MENOR</strong> salida en el <strong>mes</strong> {{date("m")}}</h2>
                <div id="menor_mes" >
                    <canvas id="grafico_menor_mes"></canvas>
                </div>
                <h2>Materiales Con <strong>MENOR</strong> salida en el <strong>año</strong> {{date("Y")}}</h2>
                <div id="menor_ano">
                    <canvas id="grafico_menor_ano"></canvas>
                </div>
            </div>
            <div class="col-md-6">
            <h2>Materiales Con <strong>MAYOR INDICE DE ROTACION</strong> en el <strong>mes</strong> {{date("m")}}</h2>
                <div id="mayor_indice_mes" >
                    <canvas id="grafico_mayor_mes"></canvas>
                </div>
                <h2>Materiales Con <strong>MAYOR INDICE DE ROTACION</strong> en el <strong>año</strong> {{date("Y")}}</h2>
                <div id="mayor_indice_ano" >
                    <canvas id="grafico_mayor_indice_ano"></canvas>
                </div>
                <h2>Materiales Con <strong>MENOR INDICE DE ROTACION</strong> en el <strong>mes</strong> {{date("m")}}</h2>
                <div id="menor_indice_mes" >
                    <canvas id="grafico_menor_mes"></canvas>
                </div>
                <h2>Materiales Con <strong>MENOR INDICE DE ROTACION</strong> en el <strong>año</strong> {{date("Y")}}</h2>
                <div id="menor_indice_ano">
                    <canvas id="grafico_menor_ano"></canvas>
                </div>
            </div>
        </div>
        
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    var mayor =<?php echo $datos[0];?>;
    var menor =<?php echo $datos[1];?>;
    var mayor_mes =<?php echo $datos[2];?>;
    var menor_mes =<?php echo $datos[3];?>;
    var ingreso =<?php echo $datos[4];?>; 
    var salida =<?php echo $datos[5];?>; 
    grafico_mayor_mes()
    grafico_menor_mes()
    grafico_mayor_ano()
    grafico_menor_ano()
    grafico_indicador_ano()
    function random_rgba_1() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',';
    }
    function grafico_indicador_ano()
    {
        var lista_rotacion=[]
        var lista_materiales=[];
        var lista_descripcion=[]
        var lista_stocks_inicial=[];
        var lista_stocks_final=[]
        var lista_costo_final=[]
        var lista_fecha_final=[]
        var lista_ingreso=[]
        var lista_salida=[]
        let colores_borde=[];
        let colores_fondo=[];
        var cont=0;
        $.each(salida,(i,value)=>{
            let material=value.cod_material;
            let costo=value.costo_material;
            let cantidadsal=value.cantidad_salida
            $.each(ingreso,(j,val)=>{
                if(i==0)
                {
                    let tmp_material=val.cod_material
                    let tmp=lista_ingreso[tmp_material]
                    if (isNaN(tmp))
                    {
                        lista_ingreso[tmp_material]=0
                        lista_ingreso[tmp_material]=lista_ingreso[tmp_material]+(val.cantidad_ingresada*val.costo_material)
                        lista_stocks_final[tmp_material]=val.stock
                        lista_costo_final[tmp_material]=val.costo_material
                        lista_fecha_final[tmp_material]=val.fecha_ingreso
                    }
                    else{
                        lista_ingreso[tmp_material]=tmp+(val.cantidad_ingresada*val.costo_material)
                        if(lista_fecha_final[tmp_material]<val.fecha_ingreso)
                        {
                            lista_stocks_final[tmp_material]=val.stock
                            lista_costo_final[tmp_material]=val.costo_material
                            lista_fecha_final[tmp_material]=val.fecha_ingreso
                        }
                    }
                }
                if($.inArray(material,lista_materiales)!=-1)
                {
                    return 0  
                }
                else
                {
                    if(value.cod_material=val.cod_material)
                    {   
                        lista_descripcion[cont]=value.descrip_material
                        lista_materiales[cont]=material
                        if(value.fecha_salida<val.fecha_ingreso)
                        {
                            let stock=value.stock;
                            let costo=value.costo_material;
                            let cantidad_salida=value.cantidad_salida;
                            lista_stocks_inicial[material]=(stock+cantidad_salida)*costo;
                        }
                        else
                        {
                            let stock=val.stock;
                            let costo=value.costo_material;
                            let cantidad_ingresada=val.cantidad_ingresada;
                            lista_stocks_inicial[material]=(stock-cantidad_ingresada)*costo;
                        }
                        cont++
                    }
                }
            })
            if (isNaN(lista_salida[material]))
            {
                lista_salida[material]=0
                lista_salida[material]=lista_salida[material]+(cantidadsal*costo)
            }
            else{
                lista_salida[material]=lista_salida[material]+(cantidadsal*costo)
            }
            if($.inArray(material,lista_materiales)==-1)
            {
                let stock=value.stock;
                let costo=value.costo_material;
                let cantidad_salida=value.cantidad_salida;
                lista_stocks_inicial[material]=(stock+cantidad_salida)*costo;
                lista_materiales[cont]=material
                lista_descripcion[cont]=value.descrip_material
                cont++
                lista_stocks_final[material]=value.stock
                lista_costo_final[material]=value.costo_material
                lista_fecha_final[material]=value.fecha_salida
            }
            else
            {
                if(lista_fecha_final[material]<value.fecha_salida)
                {
                    lista_stocks_final[material]=value.stock
                    lista_costo_final[material]=value.costo_material
                    lista_fecha_final[material]=value.fecha_salida
                }
            }
        })
        $.each(lista_materiales,(i,value)=>{
            let stockfinal=lista_stocks_final[value]*lista_costo_final[value]
            let promedio_stock=(lista_stocks_inicial[value]+stockfinal)/2
            let cadena={}
            cadena["codigo"]=lista_descripcion[i];
            if(promedio_stock==0)
            {
                cadena["rotacion"]=0
                lista_rotacion.push(cadena);
            }
            else
            {
                cadena["rotacion"]=lista_salida[value]/promedio_stock
                lista_rotacion.push(cadena);
            }
        })
        let tamano=10
        if(lista_rotacion.length<10)
        {
            tamano=lista_rotacion.length
        }
        let litas_ordenada_rotacion=[]
        let litas_ordenada_descripcion=[]
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
            let mayor=-999
            let ubicacion=-1
            for(let j=0;j<lista_rotacion.length;j++)
            {
                if(mayor<lista_rotacion[j].rotacion)
                {
                   ubicacion=j
                   mayor=lista_rotacion[j].rotacion
                }
            }
            let tmp={}
            litas_ordenada_descripcion.push(lista_rotacion[ubicacion].codigo)
            litas_ordenada_rotacion.push(lista_rotacion[ubicacion].rotacion)
            lista_rotacion[ubicacion].rotacion=-9999
        }
    let html="<canvas id='grafico_mayor_indice_ano' ></canvas>";
      $("#mayor_indice_ano").empty();
      $("#mayor_indice_ano").append(html);
      var ctx = document.getElementById('grafico_mayor_indice_ano');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: litas_ordenada_descripcion,
              datasets: [{
                  label: 'Materiales con mayor indice de rotacion en el mes',
                  data: litas_ordenada_rotacion,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_mayor_mes()
    {
        let nombre_material=[];
        let cantidad=[];
        let colores_borde=[];
        let colores_fondo=[];
        let tamano=10;
        if(mayor_mes.length<10)
        {
            tamano=mayor_mes.length
        }
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            nombre_material[i]=mayor_mes[i].descrip_material;
            cantidad[i]=mayor_mes[i].cantidad_salida;
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
      let html="<canvas id='grafico_mayor_mes' ></canvas>";
      $("#mayor_mes").empty();
      $("#mayor_mes").append(html);
      var ctx = document.getElementById('grafico_mayor_mes');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nombre_material,
              datasets: [{
                  label: 'Materiales con mayor salida en el mes',
                  data: cantidad,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_menor_mes()
    {
        let nombre_material=[];
        let cantidad=[];
        let colores_borde=[];
        let colores_fondo=[];
        let tamano=10;
        if(mayor_mes.length<10)
        {
            tamano=mayor_mes.length
        }
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            nombre_material[i]=menor_mes[i].descrip_material;
            cantidad[i]=menor_mes[i].cantidad_salida;
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        let html="<canvas id='grafico_menor_mes' ></canvas>";
      $("#menor_mes").empty();
      $("#menor_mes").append(html);
      var ctx = document.getElementById('grafico_menor_mes');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nombre_material,
              datasets: [{
                  label: 'Materiales con menor salida en el mes',
                  data: cantidad,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_mayor_ano()
    {
        let nombre_material=[];
        let cantidad=[];
        let colores_borde=[];
        let colores_fondo=[];
        let tamano=10;
        if(mayor_mes.length<10)
        {
            tamano=mayor_mes.length
        }
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            nombre_material[i]=mayor[i].descrip_material;
            cantidad[i]=mayor[i].cantidad_salida;
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        let html="<canvas id='grafico_mayor_ano' ></canvas>";
      $("#mayor_ano").empty();
      $("#mayor_ano").append(html);
      var ctx = document.getElementById('grafico_mayor_ano');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nombre_material,
              datasets: [{
                  label: 'Materiales con mayor salida en el año',
                  data: cantidad,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_menor_ano()
    {
        let nombre_material=[];
        let cantidad=[];
        let colores_borde=[];
        let colores_fondo=[];
        let tamano=10;
        if(mayor_mes.length<10)
        {
            tamano=mayor_mes.length
        }
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            nombre_material[i]=menor[i].descrip_material;
            cantidad[i]=menor[i].cantidad_salida;
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        let html="<canvas id='grafico_menor_ano' ></canvas>";
      $("#menor_ano").empty();
      $("#menor_ano").append(html);
      var ctx = document.getElementById('grafico_menor_ano');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nombre_material,
              datasets: [{
                  label: 'Materiales con menor salida en el año',
                  data: cantidad,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
  });
</script>
@endsection
