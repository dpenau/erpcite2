@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">

    </div>
    <div style="margin-bottom: 3%; margin-top: 1%;">
        <h3 class="font-weight-bold">Abastecimiento
            <a href="orden_compras/create">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-4">Nueva Orden de Compras Material
                    Normal
                </button>
            </a>
            <a href="orden_compras/create_talla">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-4">Nueva Orden de Compras Material
                    por Talla
                </button>
            </a>
        </h3>
    </div>
    @include('logistica.proveedores.modal_reporte')
    <div class="x_content table-responsive">
        <table id="example_orden_compra" class="display">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Cod. Orden Compra</th>
                <th>Proveedor</th>
                <th>Costo Total</th>
                <th>Cantidad Total</th>
                <th>% de Recepcion</th>
                <th>Recepción de Material</th>
                <th>Generar PDF</th>
                <th>Eliminar OC</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($orden_compra as $pro)
                @if ($pro->estado_ordcomp == 0)
                    <tr class="bg-danger">
                @else
                    <tr>
                        @endif
                        <td>{{ $pro->fecha }}</td>
                        <td>{{ $pro->codigo_orden }}</td>

                        <td>{{ $pro->RUC_proveedor }}</td>
                        <td>S/.{{ $pro->total }}</td>

                        <td>{{ $pro->cantidad_total }}</td>

                        @if ($pro->cantidad_total <= $pro->cantidad_faltante_total)
                            <td>0 %</td>
                        @else
                            <td>{{ round((($pro->cantidad_total - $pro->cantidad_faltante_total) * 100) / $pro->cantidad_total, 2) }}
                                %
                            </td>
                        @endif


                        @if ($pro->tipo_orden == 0)
                            <td>
                                <a class="recepcion-talla" id="{{ $pro->codigo_orden }}" href="#"
                                   data-target="#modal-recepcion-talla" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button>
                                </a>
                            </td>
                            <td>
                                <a class="generarPDFTalla" id="{{ $pro->id }}" href="#">
                                    <button id="exportar_click_talla" class="bttn-unite bttn-md bttn-warning"><i
                                            class="fas fa-file-pdf"></i></button>
                                </a>
                            </td>
                        @else
                            <td>
                                <a class="recepcion" id="{{ $pro->codigo_orden }}" href="#"
                                   data-target="#modal-recepcion" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button>
                                </a>
                            </td>
                            <td>
                                <a class="generarPDF" id="{{ $pro->id }}" href="#">
                                    <button id="exportar_click" class="bttn-unite bttn-md bttn-warning"><i
                                            class="fas fa-file-pdf"></i></button>
                                </a>
                            </td>
                        @endif



                        @if ($pro->cantidad_total <= $pro->cantidad_faltante_total)
                            <td>
                                <a class="eliminar" id="{{ $pro->id }}" data-target="#modal-delete"
                                   data-toggle="modal" data-pro="$aux" id="modalCreate">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i>
                                    </button>
                                </a>
                            </td>
                        @else
                            <td>
                                -
                            </td>
                        @endif

                    </tr>
                    @endforeach
            </tbody>

        </table>
        @include('logistica.orden_compras.modalrecepcion')
        @include('logistica.orden_compras.modalrecepcion_talla')
    </div>
    <!-- FORM ELIMINAR -->
    <div class="modal fade" id="modal-delete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header float-right">
                    <h5>¿Seguro que desea eliminar esta orden de compra?</h5>
                    <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                    </div>
                </div>
                {{ Form::Open(['action' => ['OrdenCompraController@destroy'], 'method' => 'delete']) }}
                <div class="modal-footer" style="margin-right: 70%;">
                    <input type="hidden" name="idOrdenCompra" class="form-control " id="idOrdenCompra">
                    <button type="submit" id="aceptarEliminacion" class="bttn-unite bttn-md bttn-success">Aceptar
                    </button>
                    <a href="{{ url('logistica/orden_compra') }}">
                        <button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar
                        </button>
                    </a>

                </div>
                {!! Form::Close() !!}
            </div>
        </div>

    </div>

    <style>
        table th {
            text-align: center;
        }

        table tr {
            text-align: center;
        }

    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>



    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            var lista_detalle_orden = [];
            var lista_detalle_orden_talla = [];
            var data = <?php echo $orden_compra; ?>;
            var detalle = <?php echo $detalle_temp; ?>;
            var detalle_talla = <?php echo $detalle_talla; ?>;
            var detalle_talla_material = <?php echo $detalle_talla_material; ?>;
            var t2 = $("#customersTABLAT").DataTable({
                "lengthMenu": [[100, -1], [100, "All"]],
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            var t = $("#tabla_recepcion").DataTable({
                "lengthMenu": [[100, -1], [100, "All"]],
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });

            var recepcionModal = 0;
            var porcentajeTotal = 0;
            var cantidadTotal = 0;
            var cantidadFaltante = 0;
            var costoTotalMat = 0;
            var subTotal = 0;
            var cantidadTot = 0;
            var auxiliar = 0;

            $("#porcentajeV").val("hols");


            $("#l_mats").click(() => {
                window.open("../Logistica/proveedores/reporte/lmateriales")
            })
            $("#indicador").click(() => {
                window.open("../Logistica/proveedores/reporte/indicador")
            })

            if (auxiliar == 1) {
                auxiliar = 0;
                location.reload(true);
            }

            $("#example_orden_compra").DataTable({
                order: [[0, 'desc']],
            });
            $("#example_orden_compra").on('click', 'a.recepcion', function () {
                lista_detalle_orden = [];

                "EXAMPLE ---> OCN-2021-02-201"
                var nombre = $(this).attr("id");
                "EXAMPLE ---> id = 9"
                var index = buscar(nombre);

                t.rows().remove().draw()
                modal_recepcion(nombre, index);
                arreglo_detalle(index);


            });
            $("#example_orden_compra").on('click', 'a.generarPDF', function () {

                var valor = $(this).attr("id");
                console.log(valor)
                window.open("../Logistica/pdf/ordenCompras/" + valor)
            });
            $("#example_orden_compra").on('click', 'a.generarPDFTalla', function () {

                var valor = $(this).attr("id");
                console.log(valor)
                window.open("../Logistica/pdf/ordenComprasTalla/" + valor)
            });
            $("#example_orden_compra").on('click', 'a.recepcion-talla', function () {
                lista_detalle_orden = [];
                console.log("click");
                "EXAMPLE ---> OCT-2021-02-201"
                var nombre = $(this).attr("id");
                "EXAMPLE ---> id = 9"
                var index = buscar(nombre);

                t2.rows().remove().draw()
                modal_recepcion_talla(nombre, index);
                arreglo_detalle_talla(index);


            });
            $("#example_orden_compra").on('click', 'a.eliminar', function () {

                var id = $(this).attr("id");
                $("#idOrdenCompra").val(id);
                $("#aceptarEliminacion").on('click', function () {
                    $.ajax({
                        url: "orden_compras/consulta/eliminar/" + id,
                        success: function (html) {

                            $.each(html, function (key, value) {
                                auxiliar = 1;

                            });
                        }
                    });
                    //jQuery(selector).triggerHandler('RefreshView');
                    window.location.reload();
                    //location.reload();
                    //header("Refresh:0");

                    //self.parent.location.reload();
                });
            });

            $("#guardarRecepcion").on('click', function () {

                $("#orden_compra").prop('disabled', false);

            });
            $("#guardarRecepcionTalla").on('click', function () {
                $("#orden_compra_talla").prop('disabled', false);

            });
            //RECEPCIONAR TODO
            var cod_temp_mat = [];
            var cod_tem = "";
            var aux = 0;
            $("#recepcionar_todo").on('click', function () {
                aux = 0;
                $('input[name^="cod_temp_mat"]').each(function () {
                    cod_tem = cod_tem + $(this).val() + "-";
                    cod_temp_mat.push(aux);
                    cod_temp_mat[aux] = $(this).val();
                    aux++;
                });
                recepcionar_todo(cod_tem);

            });
            //RECEPCIONAR TODO
            var cod_temp_mat2 = [];
            var cod_tem2 = "";
            var aux2 = 0;
            $("#recepcionar_todo_talla").on('click', function () {
                aux2 = 0;
                $('input[name^="nombre"]').each(function () {
                    cod_tem2 = cod_tem2 + $(this).val() + "_";
                    cod_temp_mat2.push(aux2);
                    cod_temp_mat2[aux2] = $(this).val();
                    aux2++;
                });

                recepcionar_todo_talla(cod_tem2);

            });
            $("#cancelarTalla").on('click', function () {
                location.reload();
            });

            function recepcionar_todo(aux) {

                $.ajax({
                    url: "orden_compras/consulta/recepcionar_todo/" + aux,
                    type: "get",
                    success: function (html) {
                    }
                });
            };

            function recepcionar_todo_talla(aux2) {
                console.log(aux2);
                $.ajax({
                    url: "orden_compras/consulta/recepcionar_todo_talla/" + aux2,
                    type: "get",
                    success: function (html) {
                    }
                });
            };

            function buscar(nombre) {
                var index = -1;
                var filteredObj = data.find(function (item, i) {
                    if (item.codigo_orden == nombre) {
                        var a = item.fecha
                        $("#fecha_emision").val(item.fecha);
                        $("#fecha_emision_talla").val(item.fecha);
                        index = item.id;
                        return index;
                    }
                });
                return index;
            }

            function modal_recepcion(nombre, index) {
                $("#orden_compra").val(nombre);
                $("#orden_compra_detalle").val(lista_detalle_orden);
                $("#modal-recepcion").modal({
                    show: true
                });

            };

            function modal_recepcion_talla(nombre, index) {
                $("#orden_compra_talla").val(nombre);
                $("#orden_compra_detalle").val(lista_detalle_orden);
                $("#modal-recepcion-talla").modal({
                    show: true
                });

            };

            function arreglo_detalle(index) {
                var num = 0;
                var filteredObj = detalle.find(function (item, i) {
                    if (item.cod_cab_ordencompra == index) {
                        //  index = i;
                        if (item.cantidad_faltante != 0) {
                            t.row.add([
                                "<input name='cod_temp_mat[]' value='" + item.cod_temp_mat +
                                "' type='hidden'>" + item.cod_material,
                                "<input type='hidden' 'id=rep_cod_material' name='rep_cod_material[]' value='" +
                                item.cod_material + "' >" + item.descrip_material,
                                item.cantidad,
                                "<input type='number' step='any'style='width : 90%;'  id='recepcionInput" +
                                item.cod_material + "'  type='hidden' ></input>",
                                "<input name='cantidad_faltante[]' value='" + item
                                    .cantidad_faltante + "-" + item.cod_temp_mat + "-" + 0 +
                                "'  id='cantidad_faltante" + item.cod_material +
                                "' type='hidden'><input type='number' value='" + item
                                    .cantidad_faltante +
                                "' step='any' style='width : 90%;'   id='recepcionFalt" + item
                                    .cod_material + "' disabled='true' type='hidden' >",
                                "<input type='hidden' 'id=costo_unitario' name='costo_unitario[]' value='" +
                                item.costo_sin_igv_material + "-" + item.costo_con_igv_material +
                                "' >" + item.descrip_unidad_compra
                            ]).draw();
                        } else {
                            t.row.add([

                                "<input name='cod_temp_mat[]' value='" + item.cod_temp_mat +
                                "' type='hidden'>" + item.cod_material,
                                "<input type='hidden' 'id=rep_cod_material' name='rep_cod_material[]' value='" +
                                item.cod_material + "' >" + item.descrip_material,
                                item.cantidad,
                                "<input type='number' step='any'style='width : 90%;'  id='recepcionInput" +
                                item.cod_material + "'  type='hidden' disabled ></input>",
                                "<input name='cantidad_faltante[]' value='" + item
                                    .cantidad_faltante + "-" + item.cod_temp_mat + "-" + 0 +
                                "'  id='cantidad_faltante" + item.cod_material +
                                "' type='hidden'><input type='number' value='" + item
                                    .cantidad_faltante +
                                "' step='any' style='width : 90%;'   id='recepcionFalt" + item
                                    .cod_material + "' disabled='true' type='hidden' >",
                                "<input type='hidden' 'id=costo_unitario' name='costo_unitario[]' value='" +
                                item.costo_sin_igv_material + "-" + item.costo_con_igv_material +
                                "' >" + item.descrip_unidad_compra
                            ]).draw();
                        }
                        var valueAux = 0;
                        $("#recepcionInput" + item.cod_material).keyup(function () {
                            valueAux = $(this).val();
                            if ($(this).val() > item.cantidad_faltante) {
                                alert("Necesita colocar un valor menor a la cantidad faltante");
                                $("#recepcionFalt" + item.cod_material).val(0);
                            } else {
                                $("#recepcionFalt" + item.cod_material).val((item
                                    .cantidad_faltante - $(this).val()).toFixed(2));
                                $("#cantidad_faltante" + item.cod_material).val((item
                                        .cantidad_faltante - $(this).val()).toFixed(2) + "-" +
                                    item.cod_temp_mat + "-" + $(this).val());
                            }
                        });

                        cantidadTotal = cantidadTotal + item.cantidad;
                        lista_detalle_orden.push(num);
                        lista_detalle_orden[num] = item;
                        num = num + 1;
                    }
                });
            }

            function arreglo_detalle_talla(indexTalla) {
                console.log("entro");
                var numTalla = 0;
                var aux1 = 0;
                var filteredObj = detalle_talla.find(function (item, i) {
                    console.log(aux1);
                    var index = 0;

                    var part1;
                    var part2 = "";
                    var part3;

                    var partb1;
                    var partb2;
                    var partb3;
                    var auxiliar = 1 * item.tallaInicial;
                    var aux2 = 0;
                    var cont2 = 0;
                    if (item.cod_cab_ordencompra == indexTalla) {
                        part1 = "<tr><td><input name='nombre[]' type='hidden'value='" + item
                                .cod_material + "-" + item.cod_temp_mat + "' >" + item.cod_material + "</td>" +
                            "<td><input name='descripcion[]' type='hidden' value='" + item
                                .descrip_material + "' >" + item.descrip_material + "</td>";

                        var cont = 0;
                        var contSuma = 0;
                        var filteredObj = detalle_talla_material.find(function (itemTalla, j) {

                            if (itemTalla.cod_cab_detalle_talla == item.cod_temp_mat) {

                                part2 = part2 + "<td>T" + (itemTalla.talla) + "<br><input name='tallasFinal" + item
                                        .cod_material + "[]'  " +
                                    "'    type='hidden' value='" + itemTalla.talla + "'> <input name='tallasFinal2" + item
                                        .cod_material + "[]' type='number' min='0' step='1' max='" + itemTalla.cantidad_faltante + "' id='talla" + item.cod_material + cont + "' placeholder='" + itemTalla.cantidad_faltante +
                                    "'  style='width : 110%;'></td>";
                                cont++;
                                cont2++;
                                aux2++;
                                contSuma++;
                            }

                        });

                        for (var j = 0; j < (7 - cont); j++) {
                            part2 = part2 + "<td></td>";
                        }
                        part3 =
                            "<td style='width: 8%;'><input type='number' style='width: 80%;' value='0' name='cantidad_recepcion[]'  id='cantidad_recepcion" +
                            item.cod_material +
                            "'readonly></td>" +
                            "<td style='width: 8%'><input type='hidden' value='" + item.cantidad + "'>" +
                            item.cantidad + "</td>" +

                            "<td style='width: 8%'><input  type='hidden'value='" + item.cantidad_faltante +
                            "'>" + item.cantidad_faltante + "</td>" +
                            "<td style='width: 8%'><input type='hidden' 'id=costo_unitario' name='costo_unitario[]' value='" +
                            item.costo_sin_igv_material + "-" + item.costo_con_igv_material +
                            "' ><input name='unidad_compra[]' type='hidden'value='" + item.unidad_compra +
                            "'>" + item.descrip_unidad_compra + "</td>" +
                            "</tr>";

                        const tr2 = $(part1 + part2 + part3);


                        t2.row.add(tr2[0]).draw();
                        //   t.row.add(tr2[0]).draw();
                        // AGREGAR SEGUNDA TABLA
                        // $("#recepcionTallaMaterial").append(part1 + part2 + part3);

                        // $("#cantidadMat"+value.cod_material).val(valInputCant);


                        for (let i = 0; i < cont; i++) {
                            console.log("asd");
                            var suma_salida_total = 0;
                            $('#talla' + item.cod_material + i).change(function () {
                                console.log("cambiando");
                                $('input[name^="tallasFinal2' + item.cod_material + '"]').each(function () {
                                    console.log($(this).val());
                                    suma_salida_total = suma_salida_total + 1 * $(this).val();
                                    $('#cantidad_recepcion' + item.cod_material).val(suma_salida_total);
                                });
                                suma_salida_total = 0;
                            });
                        }

                        aux1++;
                        index++;


                        cantidadTotal = cantidadTotal + item.cantidad;
                        lista_detalle_orden_talla.push(numTalla);
                        lista_detalle_orden_talla[numTalla] = item;
                        numTalla = numTalla + 1;
                    }
                });
            }
        })
    </script>



@endsection
