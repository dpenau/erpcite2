<div class="modal fade" id="modal-recepcion-talla" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" style="max-width: 80%;" role="document">
        <div class="modal-content">
        
            <div class="modal-header float-right">
                <h5>Recepcion de Materiales Talla</h5>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>
            {{ Form::Open(['action' => ['OrdenCompraController@update','1'], 'method' => 'patch']) }}
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Orden de compra:</label>
                        <input type="text" id="orden_compra_talla" name="orden_compra_talla" maxlength="100" class="form-control" disabled>
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Fecha de emisión:</label>
                        <input type="text" name="fecha_emision_talla" class="form-control " id="fecha_emision_talla">
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Fecha de Recepcion:</label>
                        <?php $fcha = date("Y-m-d");?>
                        <input type="date" name="fecha_recepcion" class="form-control " value="<?php echo $fcha;?>">
                    </div>
                </div>
                <div>
                    <table id="customersTABLAT">
                    <thead>
                          <tr>
                              <th>Cod. Material</th>
                              <th>Descripción</th>
                              <th>T1</th>
                              <th>T2</th>
                              <th>T3</th>
                              <th>T4</th>
                              <th>T5</th>
                              <th>T6</th>
                              <th>T7</th>
                             
                              <th>Cantidad Recibida</th>
                              <th>Cantidad Pedida</th>
                              <th>Cantidad Faltante</th>
                              <th>Unidad Compra</th>
                          </tr>
                      </thead>
                        <tbody id="recepcionTallaMaterial">
                        </tbody>
                    </table>
                </div>
				<div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="0" class="form-control">
                        <input type="textarea" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
				 <button type="submit" class="btn btn-primary" id="guardarRecepcionTalla">Guardar Cambios</button> 
				 <button  type="button" class="btn btn-secondary" id="cancelarTalla">Cancelar</button> 
                 <a class="btn" id="recepcionar_todo_talla" href="{{url('logistica/orden_compras')}}">Recepcionar Todo</a>
				 <!--<button href="{{url('logistica/orden_compras')}}" type="button" class="btn btn-primary" id="recepcionar_todo">Recepcionar Todo</button> -->
			</div>
            {!! Form::Close() !!}
        </div>
    </div>
</div>



<style>
        ::placeholder{
            color:#1976D2
        }
        #customersTABLAT {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        #customersTABLAT td {
          width: 4%    
        }
        #customersTABLAT td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
          
        }
        
        #customersTABLAT tr:nth-child(even){background-color: #f2f2f2;}
        
        #customersTABLAT tr:hover {background-color: #ddd;}
        
        #customersTABLAT th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          
          color: black;
        }
          </style>


