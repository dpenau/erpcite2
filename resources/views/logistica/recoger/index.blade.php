@extends ('layouts.admin')
@section ('contenido')
<div>
  <h1 class="asd">Orden de Compra N° {{$var}}</h1>
  <a href="" data-target="#modal-reporte" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Correccion de orden</button>
        </a>
  <a href="" data-target="#modal-reporte-cancelar" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-danger float-right mr-sm-5">Cerrar orden</button>
  </a>
</div>
@include('logistica.recoger.modal_corregir')
@include('logistica.recoger.modal_confirmar')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $("#inst").click(function(event){
        $(".block").prop('disabled',false);
        $(".borr").prop('disabled',true);
    });
    $("#cancelar_but").click(()=>{
      let cod_orden=$("#cancelar_orden").val();
      console.log(cod_orden)
      $.ajax({
        type:'post',
        url:'/orden_compra/cancelar_orden',
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{'orden':cod_orden},
        success:(data)=>{
          window.location.replace("../orden_compra_vista/"+cod_orden);
        }
      })
    })
  });

</script>
<div class="row">
  @foreach($cabecera as $cab)
  <div class="col-md-4">
    <p>RUC: {{$cab->RUC_proveedor}}</p>
    <p>Proveedor: {{$cab->nom_proveedor}}</p>
    <p>Direccion: {{$cab->direc_proveedor}}</p>
  </div>
  <div class="col-md-4">
    @if($cab->tipo_moneda_orden==0)
    <p>Tipo de Moneda: Soles</p>
    @else
    <p>Tipo de Moneda: Dolares</p>
    <p>Tipo de Cambio: {{$cab->t_cambio}}</p>
    @endif
  </div>
  <div class="col-md-4">
    <p>Fecha de entrega Aproximada: {{$cab->fecha_entrega}}</p>
  </div>
  @endforeach
</div>
  <hr class=" my-4">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  {!!Form::open(array('url'=>'logistica/recoger/create','method'=>'POST','autocomplete'=>'off'))!!}
  {{Form::token()}}
  <input type="text" name="cod_orden" value="{{$var}}" style="display:none">
<h2 class="font-weight-bold">Materias Primas</h2>
<div class="x_content table-responsive">
  <table id="table_mp" class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Cantidad Sin Recibir</th>
        <th>Cantidad Recibida</th>
        <th>Unidad de compra</th>
        <th>Almacen de recepcion</th>
        <th>Lugar de Almacenaje</th>
        <th>Personal de Traslado</th>
      </tr>
    </thead>
    <tbody>
      <?php

       if(count($kardex))
         {
           for($i=0;$i<count($detalleorden);$i++)
             {
                $pos="";
               for( $j=0;$j<count($kardex);$j++)
               {
                if($detalleorden[$i]->cod_material==$kardex[$j]->cod_material)
                 {
                   $pos.=$j;
                   break;
                 }
               }
               if($pos=="")
               {
                 echo "<tr>";
                 echo "<td class='block'> <input type='text' class=' form-control' id='mpc".$i."' name='cod_material[]' disabled value='". $detalleorden[$i]->cod_material."''></td>";
                 echo "<td>". $detalleorden[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleorden[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control ' id='mpca".$i."' name='cantidad[]' disabled value='". $detalleorden[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' id='mpr".$i."' class='form-control borr' name='recogido[]'></td>";
                 echo "<td><input type='text' class='form-control' value='".$detalleorden[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select id='mp".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
                 for($h=0;$h<count($almacen);$h++)
                 {
                  $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
                }
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input type='text' id='mpclu".$i."' class='form-control borr' name='lugar[]'></td>";
                 echo "<td><input type='text' id='mpcl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }
               else {
                 echo "<tr>";
                  echo "<td> <input type='text' class='form-control block ' id='mpc".$i."' name='cod_material[]' disabled value='". $detalleorden[$i]->cod_material."''></td>";
                 echo "<td>". $detalleorden[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleorden[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control block' id='mpca".$i."' name='cantidad[]' disabled value='". $detalleorden[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' class='form-control' name='recogido[]'></td>";
                  echo "<td><input type='text' class='form-control' value='".$detalleorden[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select disabled id='mp".  $i ."'name='almacen[]' class='custom-select block'><option value=''  disabled>Almacen</option>";
                  $temalamacen.="<option selected value='".$kardex[$pos]->cod_almacen."'>".$kardex[$pos]->nom_almacen."</option>";
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input type='text' class='form-control block' name='lugar[]' value=".$kardex[$pos]->lugar_almacenaje." disabled></td>";
                 echo "<td><input type='text' class='form-control' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }?>
               <script>
                 $("#mp<?php echo $i;?>").off('change')
                 $("#mp<?php echo $i;?>").on('change',function(){
                    $("#mpc<?php echo $i;?>").addClass('block');
                    $("#mpca<?php echo $i;?>").addClass('block');
                    $("#mpr<?php echo $i;?>").removeClass("borr")
                    $("#mpclu<?php echo $i;?>").removeClass("borr")
                    $("#mpcl<?php echo $i;?>").removeClass("borr")
                    $("#mp<?php echo $i;?>").removeClass("borr")
                 });
               </script>
               <?php
             }

         }
         else {
           for($i=0;$i<count($detalleorden);$i++)
             {
               echo "<tr>";
                 echo "<td> <input type='text' class='form-control ' id='mpc".$i."' name='cod_material[]' disabled value='". $detalleorden[$i]->cod_material."''></td>";
               echo "<td>". $detalleorden[$i]->nom_subcategoria."</td>";
               echo "<td>". $detalleorden[$i]->descrip_material."</td>";
               echo "<td><input type='text' class='form-control ' id='mpca".$i."' name='cantidad[]' disabled value='". $detalleorden[$i]->cantidad_restante."'></td>";
               echo "<td><input type='text'  id='mpr".$i."' class='form-control borr' name='recogido[]'></td>";
                echo "<td><input type='text' class='form-control' value='".$detalleorden[$i]->descrip_unidad_medida."' disabled></td>";
               $temalamacen="<td><select id='mp".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
               for($h=0;$h<count($almacen);$h++)
               {
                $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
              }
               $temalamacen.= "</td>";
               echo $temalamacen;
               echo "<td><input type='text' id='mpclu".$i."' class='form-control borr' name='lugar[]'></td>";
               echo "<td><input type='text' id='mpcl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
               echo "</tr>";?>
               <script>
                 $("#mp<?php echo $i;?>").off('change')
                 $("#mp<?php echo $i;?>").on('change',function(){
                    $("#mpc<?php echo $i;?>").addClass('block');
                    $("#mpca<?php echo $i;?>").addClass('block');
                    $("#mpr<?php echo $i;?>").removeClass("borr")
                    $("#mpclu<?php echo $i;?>").removeClass("borr")
                    $("#mpcl<?php echo $i;?>").removeClass("borr")
                    $("#mp<?php echo $i;?>").removeClass("borr")
                 });
               </script>
               <?php
             }

         }?>

    </tbody>
  </table>
</div>
<h2>Insumos</h2>
<div class="x_content table-responsive">
  <table id="table_mp" class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Cantidad Sin Recibir</th>
        <th>Cantidad Recibida</th>
        <th>Unidad de compra</th>
        <th>Almacen de recepcion</th>
        <th>Lugar de Almacenaje</th>
        <th>Personal de Traslado</th>
      </tr>
    </thead>
    <tbody>
      <?php if(count($kardex))
         {
           for($i=0;$i<count($detalleordeninsu);$i++)
             {
               $pos="";
               for( $j=0;$j<count($kardex);$j++)
               {
                 if($kardex[$j]->cod_material==$detalleordeninsu[$i]->cod_material)
                 {
                   $pos.=$j;
                   break;
                 }
               }
               if($pos=="")
               {
                 echo "<tr>";
                    echo "<td> <input type='text' class='form-control ' id=insc".$i." name='cod_material[]' disabled value='". $detalleordeninsu[$i]->cod_material."''></td>";
                 echo "<td>". $detalleordeninsu[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleordeninsu[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control ' id=insca".$i." name='cantidad[]' disabled value='". $detalleordeninsu[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' id='inscr".$i."' class='form-control borr' name='recogido[]'></td>";
                  echo "<td><input type='text' class='form-control' value='".$detalleordeninsu[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select id='in".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
                 for($h=0;$h<count($almacen);$h++)
                 {
                  $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
                }
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input type='text' id='insclu".$i."' class='form-control borr' name='lugar[]'></td>";
                 echo "<td><input type='text' id='inscl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }
               else {
                 echo "<tr>";
                    echo "<td> <input type='text' class='form-control block ' id=insc".$i." name='cod_material[]' disabled value='". $detalleordeninsu[$i]->cod_material."''></td>";
                 echo "<td>". $detalleordeninsu[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleordeninsu[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control block ' id=insca".$i." name='cantidad[]' disabled value='". $detalleordeninsu[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' class='form-control ' name='recogido[]'></td>";
                  echo "<td><input type='text' class='form-control' value='".$detalleordeninsu[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select disabled id='in".  $i ."'name='almacen[]' class='custom-select block'><option value=''  disabled>Almacen</option>";
                  $temalamacen.="<option selected value='".$kardex[$pos]->cod_almacen."'>".$kardex[$pos]->nom_almacen."</option>";
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input type='text' disabled class='form-control block' name='lugar[]' value=".$kardex[$pos]->lugar_almacenaje."></td>";
                 echo "<td><input type='text' class='form-control' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }
               ?>
               <script>
                 $("#in<?php echo $i;?>").off('change')
                 $("#in<?php echo $i;?>").on('change',function(){
                    $("#insc<?php echo $i;?>").addClass('block');
                    $("#insca<?php echo $i;?>").addClass('block');
                    $("#inscr<?php echo $i;?>").removeClass('borr');
                    $("#insclu<?php echo $i;?>").removeClass('borr');
                    $("#inscl<?php echo $i;?>").removeClass('borr');
                    $("#in<?php echo $i;?>").removeClass('borr');
                 });
               </script>
               <?php
             }
         }
         else {
           for($i=0;$i<count($detalleordeninsu);$i++)
             {
               echo "<tr>";
                  echo "<td> <input type='text' class='form-control ' id=insc".$i." name='cod_material[]' disabled value='". $detalleordeninsu[$i]->cod_material."''></td>";
               echo "<td>". $detalleordeninsu[$i]->nom_subcategoria."</td>";
               echo "<td>". $detalleordeninsu[$i]->descrip_material."</td>";
               echo "<td><input type='text' class='form-control ' id=insca".$i." name='cantidad[]' disabled value='". $detalleordeninsu[$i]->cantidad_restante."'></td>";
               echo "<td><input type='text' id='inscr".$i."' class='form-control borr' name='recogido[]'></td>";
                echo "<td><input type='text' class='form-control' value='".$detalleordeninsu[$i]->descrip_unidad_medida."' disabled></td>";
               $temalamacen="<td><select id='in".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
               for($h=0;$h<count($almacen);$h++)
               {
                $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
              }
               $temalamacen.= "</td>";
               echo $temalamacen;
               echo "<td><input type='text' id='insclu".$i."' class='form-control borr' name='lugar[]'></td>";
               echo "<td><input type='text' id='inscl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
               echo "</tr>";?>
               <script>
                 $("#in<?php echo $i;?>").off('change')
                 $("#in<?php echo $i;?>").on('change',function(){
                   $("#insc<?php echo $i;?>").addClass('block');
                   $("#insca<?php echo $i;?>").addClass('block');
                   $("#inscr<?php echo $i;?>").removeClass('borr');
                   $("#insclu<?php echo $i;?>").removeClass('borr');
                   $("#inscl<?php echo $i;?>").removeClass('borr');
                   $("#in<?php echo $i;?>").removeClass('borr');
                 });
               </script>
            <?php
             }
         }?>

    </tbody>
  </table>
</div>
<h2>Suministros</h2>
<div class="x_content table-responsive">
  <table id="table_mp" class="table table-hover">
    <thead>
      <tr>
        <th>Código</th>
        <th>Subcategoria</th>
        <th>Detalle</th>
        <th>Cantidad Sin Recibir</th>
        <th>Cantidad Recibida</th>
        <th>Unidad de compra</th>
        <th>Almacen de recepcion</th>
        <th>Lugar de Almacenaje</th>
        <th>Personal de Traslado</th>
      </tr>
    </thead>
    <tbody>
      <?php if(count($kardex))
         {
           for($i=0;$i<count($detalleordensumi);$i++)
             {
               $pos="";
               for( $j=0;$j<count($kardex);$j++)
               {
                 if($kardex[$j]->cod_material==$detalleordensumi[$i]->cod_material)
                 {
                   $pos.=$j;
                   break;
                 }
               }
               if($pos=="")
               {
                 echo "<tr>";
                    echo "<td> <input type='text' class='form-control' id='sumc".$i."' name='cod_material[]' disabled value='". $detalleordensumi[$i]->cod_material."''></td>";
                 echo "<td>". $detalleordensumi[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleordensumi[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control' id='sumca".$i."' name='cantidad[]' disabled value='". $detalleordensumi[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' id='sumcr".$i."' class='form-control borr' name='recogido[]'></td>";
                  echo "<td><input type='text' class='form-control' value='".$detalleordensumi[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select id='".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
                 for($h=0;$h<count($almacen);$h++)
                 {
                  $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
                }
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input type='text' id='sumclu".$i."' class='form-control borr' name='lugar[]'></td>";
                 echo "<td><input type='text' id='sumcl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }
               else {
                 echo "<tr>";
                    echo "<td> <input type='text' class='form-control block ' id='sumc".$i."' name='cod_material[]' disabled value='". $detalleordensumi[$i]->cod_material."''></td>";
                 echo "<td>". $detalleordensumi[$i]->nom_subcategoria."</td>";
                 echo "<td>". $detalleordensumi[$i]->descrip_material."</td>";
                 echo "<td><input type='text' class='form-control block' id='sumca".$i."' name='cantidad[]' disabled value='". $detalleordensumi[$i]->cantidad_restante."'></td>";
                 echo "<td><input type='text' class='form-control ' name='recogido[]'></td>";
                  echo "<td><input type='text' class='form-control' value='".$detalleordensumi[$i]->descrip_unidad_medida."' disabled></td>";
                 $temalamacen="<td><select disabled id='".  $i ."'name='almacen[]' class='custom-select block'><option value='' selected >Almacen</option>";
                  $temalamacen.="<option selected value='".$kardex[$pos]->cod_almacen."'>".$kardex[$pos]->nom_almacen."</option>";
                 $temalamacen.= "</td>";
                 echo $temalamacen;
                 echo "<td><input disabled type='text' class='form-control block' name='lugar[]' value='".$kardex[$pos]->lugar_almacenaje."'></td>";
                 echo "<td><input type='text' class='form-control' name='persona_traslado[]'></td>";
                 echo "</tr>";
               }
               ?>
               <script>
                 $("#<?php echo $i;?>").off('change')
                 $("#<?php echo $i;?>").on('change',function(){
                   $("#sumc<?php echo $i;?>").addClass('block');
                   $("#sumca<?php echo $i;?>").addClass('block');
                   $("#sumcr<?php echo $i;?>").removeClass('borr');
                   $("#sumclu<?php echo $i;?>").removeClass('borr');
                   $("#sumcl<?php echo $i;?>").removeClass('borr');
                   $("#<?php echo $i;?>").removeClass('borr');
                 });
               </script>
               <?php
             }
         }
         else {
           for($i=0;$i<count($detalleordensumi);$i++)
             {
               echo "<tr>";
                  echo "<td> <input type='text' class='form-control ' id='sumc".$i."' name='cod_material[]' disabled value='". $detalleordensumi[$i]->cod_material."''></td>";
               echo "<td>". $detalleordensumi[$i]->nom_subcategoria."</td>";
               echo "<td>". $detalleordensumi[$i]->descrip_material."</td>";
               echo "<td><input type='text' class='form-control ' id='sumca".$i."' name='cantidad[]' disabled value='". $detalleordensumi[$i]->cantidad_restante."'></td>";
               echo "<td><input type='text' id='sumcr".$i."' class='form-control borr' name='recogido[]'></td>";
                echo "<td><input type='text' class='form-control' value='".$detalleordensumi[$i]->descrip_unidad_medida."' disabled></td>";
               $temalamacen="<td><select id='".  $i ."'name='almacen[]' class='custom-select borr'><option value='' selected disabled>Almacen</option>";
               for($h=0;$h<count($almacen);$h++)
               {
                $temalamacen.="<option value='".$almacen[$h]->cod_almacen."'>".$almacen[$h]->nom_almacen."</option>";
              }
               $temalamacen.= "</td>";
               echo $temalamacen;
               echo "<td><input type='text' id='sumclu".$i."' class='form-control borr ' name='lugar[]'></td>";
               echo "<td><input type='text' id='sumcl".$i."' class='form-control borr' name='persona_traslado[]'></td>";
               echo "</tr>";
               ?>
               <script>
                 $("#<?php echo $i;?>").off('change')
                 $("#<?php echo $i;?>").on('change',function(){
                   $("#sumc<?php echo $i;?>").addClass('block');
                   $("#sumca<?php echo $i;?>").addClass('block');
                   $("#sumcr<?php echo $i;?>").removeClass('borr');
                   $("#sumclu<?php echo $i;?>").removeClass('borr');
                   $("#sumcl<?php echo $i;?>").removeClass('borr');
                   $("#<?php echo $i;?>").removeClass('borr');
                 });
               </script>
               <?php
             }
         }?>

    </tbody>
  </table>
</div>
<h2>Materiales por Talla</h2>
<div class="x_content table-responsive">
  <table id="table_tallas" class="table table-hover">
    <thead>
      <tr>
        <th>Detalle</th>
        <th>18</th>
        <th>19</th>
        <th>20</th>
        <th>21</th>
        <th>22</th>
        <th>23</th>
        <th>24</th>
        <th>25</th>
        <th>26</th>
        <th>27</th>
        <th>28</th>
        <th>29</th>
        <th>30</th>
        <th>31</th>
        <th>32</th>
        <th>33</th>
        <th>34</th>
        <th>35</th>
        <th>36</th>
        <th>37</th>
        <th>38</th>
        <th>39</th>
        <th>40</th>
        <th>41</th>
        <th>42</th>
        <th>43</th>
        <th>44</th>
        <th>45</th>
        <th>46</th>
        <th>Unidad de Compra</th>
        <th>Almacen de recepcion</th>
        <th>Lugar de almacenaje</th>
        <th>Personal de Traslado</th>
      </tr>
    </thead>
    <tbody>
      <?php
         ?>
           <script type="text/javascript">
            var familia=[]
            var unidadcompra=[]
            var codigo=[]
            var tmp=""
              @foreach($detalletallas as $det)
                var descrip="{{$det->descrip_material}}"
                var unidad="{{$det->descrip_unidad_medida}}"
                var cod="{{$det->cod_material}}"
                descrip=descrip.substr(0,descrip.indexOf("-"))
                console.log(descrip);
                if(tmp!=descrip)
                {
                  familia.push(descrip)
                  codigo.push(cod)
                  unidadcompra.push(unidad)
                  tmp=descrip;
                }
              @endforeach

              var almacen=[]
              var codalmacen=[]
              var ubicacion=[]
              var nombre=[]
            @foreach($kardex as $kard)
              @if($kard->t_compra)
                var descrip="{{$kard->descrip_material}}"
                descrip=descrip.substr(0,descrip.indexOf("-"))
                for(var j=0;j<familia.length;j++)
                {
                  if(familia[j]==descrip)
                  {
                    if(nombre.indexOf(descrip)==-1)
                    {
                      nombre.push(descrip)
                      almacen.push("{{$kard->nom_almacen}}")
                      codalmacen.push("{{$kard->cod_almacen}}")
                      ubicacion.push("{{$kard->lugar_almacenaje}}")
                    }
                  }
                }
              @endif
            @endforeach
            var tabla=""
            var tallasaceptadas=[]
            var cantidadtallas=[]
            for(var i=0;i<familia.length;i++)
            {
              tabla+="<tr>"+"<td><input disabled name='"+codigo[i]+"' class='block' value='"+familia[i]+"'></td>";
              @foreach($detalletallas as $det)
                var descrip="{{$det->descrip_material}}"
                var nom=descrip.substr(0,descrip.indexOf("-"))
                if(nom==familia[i]) {
                  var talla_n=descrip.substr(descrip.indexOf("-")+1);
                  var cant="{{$det->cantidad_restante}}"
                  tallasaceptadas.push(talla_n)
                  cantidadtallas.push(cant)
                }
                else {
                  if(tallasaceptadas!="")
                  {
                    var c=0;
                    for (var j = 18; j < 47 ; j++) {
                      if (tallasaceptadas.indexOf(j.toString())>-1) {
                        tabla+="<td style='display:none'><input name='"+codigo[i]+"tallap[]' value="+j+"></td><td style='display:none'><input name='"+codigo[i]+"res[]' value="+cantidadtallas[c]+"></td><td><input name='"+codigo[i]+"ca[]' style='width:50px' type='text' placeholder='"+cantidadtallas[c]+"'></td>"
                        c++;
                      }
                      else {
                        tabla+="<td><input disabled style='width:50px' class='' type='text'></td>"
                      }
                    }
                    tallasaceptadas=[]
                    cantidadtallas=[]
                  }
              }
              @endforeach
              if(tallasaceptadas!="")
              {
                var c=0;
                for (var j = 18; j < 47 ; j++) {
                  if (tallasaceptadas.indexOf(j.toString())>-1) {
                    tabla+="<td style='display:none'><input name='"+codigo[i]+"tallap[]' value="+j+"></td><td style='display:none'><input name='"+codigo[i]+"res[]' value="+cantidadtallas[c]+"></td><td><input name='"+codigo[i]+"ca[]' style='width:50px' type='text' placeholder='"+cantidadtallas[c]+"'></td>"
                    c++
                  }
                  else {
                    tabla+="<td><input disabled style='width:50px' class='' type='text'></td>"
                  }
                }
                tallasaceptadas=[]
                cantidadtallas=[]
              }
              tabla+="<td><input class='form-control block' name='unidad[]' type='text'disabled value='"+unidadcompra[i]+"'></td>"
              if(nombre.length>0)
              {
                var entro=0;
                for(var j=0;j<nombre.length;j++)
                {
                  console.log(familia)
                  console.log(nombre)
                  console.log("FAMILIA :"+familia[i]+" I ESTA EN:"+i)
                  console.log("Nombre :"+nombre[j]+" J esta en:"+j)
                  console.log("INDEXOF:  "+familia.indexOf(nombre[j]))
                  if(familia[i]==nombre[j])
                  {
                    entro=1;
                    break;
                  }
                }
                if(entro==0)
                {
                  console.log("ENTRO EN ELSE, POSICION DE I:"+i)
                  var lalmacen="<td><select id='talla"+i+"' name='"+codigo[i]+"alma[]' class='custom-select'><option value='' selected disabled>Almacen</option>"
                  @foreach($almacen as $alm)
                    lalmacen+="<option value='{{$alm->cod_almacen}}'>{{$alm->nom_almacen}}</option>";
                  @endforeach
                  tabla+=lalmacen
                  tabla+="<td style='display:none'><input id='cod"+i+"' class='borr' name='codtalla[]' value='"+codigo[i]+"'/></td>"
                  tabla+="<td><input class='block' name='"+codigo[i]+"lug[]' type='text'></td>"
                  tabla+="<td><input class='block' name='"+codigo[i]+"per[]' type='text'></td>"
                }
                else {
                  console.log("ENTRO EN EL IF, POSICION DE I:"+i)
                  tabla+="<td style='display:none'><input  value='"+almacen[j]+"' ></td><td><input class='block' type='text'disabled value='"+almacen[j]+"'></td>"
                  tabla+="<td style='display:none'><input name='"+codigo[i]+"alma[]' value='"+codalmacen[j]+"' ></td>"
                  tabla+="<td style='display:none'><input id='cod"+i+" class='block' name='codtalla[]' value='"+codigo[i]+"'/></td>"
                  tabla+="<td><input class='block' name='"+codigo[i]+"lug[]' type='text'disabled value='"+ubicacion[j]+"'></td>"
                  tabla+="<td><input class='' name='"+codigo[i]+"per[]' type='text' value=''></td>"
                }
              }
              else {
                var lalmacen="<td><select id='talla"+i+"' name='"+codigo[i]+"alma[]' class='custom-select'><option value='' selected disabled>Almacen</option>"
                @foreach($almacen as $alm)
                  lalmacen+="<option value='{{$alm->cod_almacen}}'>{{$alm->nom_almacen}}</option>";
                @endforeach
                tabla+=lalmacen
                tabla+="<td style='display:none'><input id='cod"+i+"' class='borr' name='codtalla[]' value='"+codigo[i]+"'/></td>"
                tabla+="<td><input class='block' name='"+codigo[i]+"lug[]' type='text'></td>"
                tabla+="<td><input class='block' name='"+codigo[i]+"per[]' type='text'></td>"
              }

              $('#table_tallas> tbody:last-child').append(tabla)
              tabla=""
              $("#talla"+i).off('change')
              $("#talla"+i).on('change',function(){

                var a=$(this).attr('id');
                a=a.substr(5)
                $("#cod"+a).removeClass('borr')
                console.log(a)
              });
            }

           </script>

        <?php  ?>

    </tbody>
  </table>
</div>
<div class="">
  @foreach($cabecera as $cab)
  <div class="col-md-12">
      <p> Comentario</p>
<textarea maxlength="2000" name="comentarios" rows="8" style="width:100%" >{{$cab->comentario_oc}}</textarea>
  </div>
  @endforeach
</div>
<div class=" col-md-12">
  @if($cabecera[0]->estado_orden_compra==1)    
  <button type="submit" class="bttn-unite bttn-md bttn-primary " id="inst">Guardar Cambios</button>
  @endif
  <a type="button" class="bttn-unite bttn-md bttn-danger " href="{{ url('logistica/ingreso_salida') }}" >Cancelar</a>
</div>
{!!Form::close()!!}

@endsection
