@extends ('layouts.admin')
@section ('contenido')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nueva Orden de Pedido</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Ventas/pedidos','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                <div>
                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td align="center" colspan="2">
                            <img src="/photo/{{$datosempresa[0]->imagen}}" width="200px" alt="">
                          </td>
                          <td colspan="2">
                            <br>
                            <h2 class="align-middle">Orden de Pedido</h2>
                            <br>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h4>Cliente</h4>
                          </td>
                          <td>
                            <select class="custom-select" id="cli" name="cliente">
                              <option value="" disabled selected>Seleccionar Cliente</option>
                              @foreach($cliente as $cli)
                              <option value="{{$cli->codigo}}">{{$cli->nombre}}</option>
                              @endforeach
                            </select>
                          </td>
                          <td>
                            <h4>Fecha:</h4>
                          </td>
                          <td>
                            <h4>{{date('Y-m-d')}}</h4>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h4>Direccion: </h4>
                          </td>
                          <td>
                            <h4 id="direccion"></h4>
                          </td>
                          <td>
                            <h4>Telefono: </h4>
                          </td>
                          <td>
                            <h4 id="telefono"></h4>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h4>RUC:</h4>
                          </td>
                          <td colspan="1">
                            <h4 id="ruc"></h4>
                          </td>
                          <td>
                            <h4>Fecha Tentativa de Entrega: </h4>
                          </td>
                          <td colspan="3">
                            <div class="form-group">
                              <input type="date" name="fecha_tentativa" class="form-control" id="" required placeholder="">
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h4>Adelanto: </h4>
                          </td>
                          <td >
                            <div class="form-group">
                              <input type="text" name="adelanto" class="form-control" value="0" required placeholder="">
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="table-responsive">
                    <table class="table" id="descripcion">
                      <thead class="thead-dark">
                        <tr>
                          <th>Cod. Articulo - Descripcion</th>
                          <th>Cantidad</th>
                          <th>Precio por Par</th>
                          <th>Costo Total</th>
                          <th>Tipo de urgencia</th>
                          <th></th>
                          <td style="width:5%">
                            <div>
                              <button id="anadir" type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button>
                            </div>
                          </td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot class="thead-dark">
                        <tr>
                          <th colspan="3">Total:</th>
                          <th id="total" colspan="3"></th>
                          <th style="display:none"><input id="total_val" name="totales"></input></th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-12 col-sm-6 col-xs-12">
                    <button id="boton_enviar" type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                  </div>
                </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
	<script>
  $(document).ready(function(){
    var n_mostrados=0;
    var lista_articulos=<?php echo $articulos;?>;
    var lista_seleccionados=[];
    var articulos
    actualiar_lista()
    var i=0;
    $("#anadir").click(function(){
      let valor=$("#cli").val();
      if(valor!=null && n_mostrados==lista_seleccionados.length)
      {
        actualiar_lista();
        $("#descripcion").append("<tr id='f"+i+"'><td><select class='custom-select' name='articulos[]' id='"+i+"'>"+articulos+"</td><td><div class='row' ><div class='col-xs-6 col-sm-6 col-md-5 ' id='tr1"+i+"'></div><div class='col-xs-6 col-sm-6 col-md-5' id='tr2"+i+"'></div></div></td><td class='p' id='p"+i+"'></td><td class='ct' id='ctt"+i+"'></td><td style='display:none'><input id='ct"+i+"' name='costo_par[]'></input></td><td><select class='custom-select' name='urgencia[]'><option selected disabled>Tipo de Urgencia</option><option value='2' class='bg-danger text-white'>Urgente</option><option value='1' class='bg-warning'>Planificable</option><option value='0'>Normal</option></select></td><td><button id='bor"+i+"' type='button' class='bttn-unite bttn-md bttn-danger' ><i class='fas fa-trash-alt'></i></button></td></tr>");
        $("#"+i).off("change");
        $("#"+i).on("change",function(){
          let escogido=$(this).val()
          let id=$(this).attr('id')
          var opciones=$("#"+id+" option");
          $.each(opciones,function(id,val){
            if(val.value!="" && val.value!=escogido)
            {
              $(this).remove();
            }
          });
          $("#ctt"+id).text(0);
          $("#ct"+id).val("0");
          calcular_total()
          $.each(lista_articulos,function(indice,valor){
            var art=valor.codigo;
            if(art==escogido)
            {
              let tallai=valor.tallaInicial
              let tallaf=valor.tallaFinal
              let talla=parseFloat(tallaf)-parseFloat(tallai);
              let precio=valor.precio
              var tmp=parseFloat(tallai);
              $("#tr1"+id).empty();
              $("#tr2"+id).empty();
              for(let j=0;j<=talla;j++)
              {
                $("#tr1"+id).append("<input disabled  value='"+tmp+"' style='width:100%'></input></br>")
                tmp=tmp+1;
              }
              for(let j=0;j<=talla;j++)
              {
                $("#tr2"+id).append("<input class='t"+id+"' maxlength='3' value='0' name='"+art+"tallas[]' id='tallas"+id+j+"' style='width:100%'></input></br>")
                tallai=parseFloat(tallai)+1;
                $("#tallas"+id+j).off("change");
                $("#tallas"+id+j).on("change",function(){
                  var cantidades=$("input[class~='t"+id+"']");
                  var precio=parseFloat($("#p"+id).text());
                  var total_tmp=0;
                  var total_pares=0;
                  $("#tallas"+id+"tot").val("")
                  for(var k=0;k<cantidades.length;k++)
                  {
                    var tmp_cantidad=cantidades[k].value;
                    if(tmp_cantidad!="")
                    {
                      total_tmp=total_tmp+(parseFloat(tmp_cantidad)*precio);
                      total_pares=total_pares+parseFloat(tmp_cantidad);
                    }
                  }
                  $("#tallas"+id+"tot").val(total_pares)
                  $("#ct"+id).val(total_tmp.toFixed(2));
                  $("#ctt"+id).text(total_tmp.toFixed(2));
                  calcular_total()
                })
              }
              $("#tr1"+id).append("<input disabled  value='Total' style='width:100%'></input></br>");
              $("#tr2"+id).append("<input disabled class='t"+id+"' name='totales_par[]'  id='tallas"+id+"tot' style='width:100%'></input></br>")
              $("#p"+id).empty();
              $("#p"+id).append(precio)
            }
          })
          lista_seleccionados.push(escogido)
        })
        $("#bor"+i).off("click");
        $("#bor"+i).on("click",function(){
          let id=$(this).attr("id");
          id=id.substring(3);
          let codigo=$("#"+id).val();
          let valor_total_par=$("#ct"+id).val();
          let valor_total_op=$("#total_val").val();
          let nuevo_total=parseFloat(valor_total_op)-parseFloat(valor_total_par);
          $("#total_val").val(nuevo_total.toFixed(2));
          $("#total").text(nuevo_total.toFixed(2));
          $("#f"+id).remove()
          n_mostrados--;
          let tmp=lista_seleccionados
          lista_seleccionados=[]
          $.each(tmp,function(indice,valor){
            if(valor!=codigo)
            {
              lista_seleccionados.push(valor)
            }
          });

        });
        i++
        n_mostrados++;
      }
      else {
        alert("Seleccione un CLIENTE o Articulo primero")
      }
    })
    function actualiar_lista(){
      articulos=""
      articulos="<option value='' disabled selected>Escoja un articulo</option>"
      $.each(lista_articulos,function(indice,val){
        var encontrado=false;
        $.each(lista_seleccionados,function(i,valor){
          if(val.codigo==valor)
          {
            encontrado=true;
            return;
          }
        })
        if (encontrado==false) {
          articulos+="<option id='"+i+"'  value='"+val.codigo+"'>"+val.cod_modelo+" - "+val.descripcion+"</option>"
        }
      });
      articulos+="</select>";
    }
    function calcular_total(){
      var costos_totales=$("td[class~='ct']");
      var total_tmp=0;
      for(var k=0;k<costos_totales.length;k++)
      {
        var tmp_cantidad=costos_totales[k].innerHTML;
        if(tmp_cantidad!="")
        {
          total_tmp=total_tmp+parseFloat(tmp_cantidad);
        }
      }
      $("#total").text(total_tmp.toFixed(2));
      $("#total_val").val(total_tmp.toFixed(2));
    }
    $("#cli").change(function(){
      var valor=$("#cli").val();
      @foreach($cliente as $cli)
        var tmp="{{$cli->codigo}}";
        if(valor==tmp)
        {
          $("#direccion").text("{{$cli->direccion}}");
          $("#ruc").text("{{$cli->documento}}");
          $("#telefono").text("{{$cli->telefono}}");
        }
      @endforeach
    });
  })
  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
  }
	</script>
@endsection
