@extends ('layouts.admin')
@section ('contenido')

<div class="preloader"></div>
<div>
<h3 class="font-weight-bold">Listado de Clientes <a href="clientes/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Cliente</button></a>
</h3>
</div>

<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th>Documento</th>
        <th>Nombre</th>
        <th>Destino</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>Editar</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($cliente as $trab)
      @if($trab->estado==0)
      <tr class="bg-danger">
      @endif
        <td>{{$trab->documento}}</td>
        <td>{{$trab->nombre}}</td>
        <td>{{$trab->destino}}</td>
        <td>{{$trab->telefono}}</td>
        <td>{{$trab->direccion}}</td>
      <td>
         <a href="" data-target="#modal-edit-{{$trab->codigo}}" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
      </td>
      <td>
        @if($trab->estado==1)
         <a href="" data-target="#modal-delete-{{$trab->codigo}}" data-toggle="modal">
          <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
        @else
        <a href="" data-target="#modal-active-{{$trab->codigo}}" data-toggle="modal">
         <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
         @endif
      </td>
      </tr>
      @include('Ventas.cliente.modaleditar')
      @include('Ventas.cliente.modaldesactivar')
      @include('Ventas.cliente.modalactivar')
      @endforeach
    </tbody>
  </table>
</div>
@endsection
