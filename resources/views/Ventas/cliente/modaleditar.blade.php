<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$trab->codigo}}">
	{{Form::Open(array('action'=>array('ClientesController@update',$trab->codigo
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Datos de Cliente</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="codigo" value="{{$trab->codigo}}">
        <div class="form-group  ">
          <label for="total_orden_compra">Documento:</label>
           <input type="text" maxlength="11" class="form-control" onkeypress="return isNumberKey(event)" name="documento" value="{{$trab->documento}}">
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Nombre:</label>
             <input type="text" class="form-control" name="nombre" value="{{$trab->nombre}}" maxlength="500">
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Destino:</label>
             <input type="text" class="form-control" name="destino" value="{{$trab->destino}}" maxlength="100">
          </div>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Telefono:</label>
              <input onkeypress="return isNumberKey(event)" type="text" required="required" name="telefono" required="required" class="form-control" value="{{$trab->telefono}}" maxlength="9">
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Direccion:</label>
             <input type="text" required="required" name="direccion" required="required" class="form-control" value="{{$trab->direccion}}" maxlength="100">
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
