@extends ('layouts.admin')
@section ('contenido')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Registrar Cliente</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Ventas/clientes','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}


<div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">

                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Documento del cliente:</label>
                         <input type="text" title="Longitud incorrecta" name="documento" required="required" onkeypress="return isNumberKey(event)" maxlength="11" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Nombre del Cliente:</label>
                         <input type="text" maxlength="500" name="nombre" required="required" title="Solo letras" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Destino del cliente:</label>
                         <input type="text" maxlength="100" name="destino" title="Solo letras" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Telefono cliente:</label>
                         <input type="text" maxlength="9" onkeypress="return isNumberKey(event)" name="telefono" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">direccion del cliente:</label>
                         <input type="text" name="direccion" maxlength="100" title="Solo Numeros"  required="required" class="form-control ">
                      </div>
                    </div>
                  </div>
                </div>
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
	<script>
  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
  }


	</script>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");

</script>
@endpush
@endsection
