@extends ('layouts.admin')
@section ('contenido')
<div class="preloader"></div>
<div>
  <h3 class="font-weight-bold">Reporte de Clientes</h3>
</div>
<div class="x_content table-responsive">
  <!--Begin Checks sobre los filtros-->
  <div class="row">
    <div class="col-sm-3">
      <div class="card" style="height: 13rem;">
        <div class="card-body">
          <h5 class="card-title">Filtros Generales</h5>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="cb_ingreso_total_pedidos" checked>
            <label class="form-check-label" for="defaultCheck1">
              Ingreso total de pedidos
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="cb_frec_pedidos" checked>
            <label class="form-check-label" for="defaultCheck1">
              Frecuencia de pedido
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="cb_deuda" checked>
            <label class="form-check-label" for="defaultCheck1">
              Deuda
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="cb_moroso" disabled>
            <label class="form-check-label" for="defaultCheck1" disabled>
              Moroso
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="card" style="height: 13rem;">
        <div class="card-body">
          <h5 class="card-title">Filtro Moroso</h5>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" disabled>
            <label class="form-check-label" for="defaultCheck1" disabled>
              Morosos Actuales
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" disabled>
            <label class="form-check-label" for="defaultCheck1" disabled>
              Morosos Históricos
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--End Checks sobre los filtros-->
  <br>
  <!--Begin table-->
  <div class="x_content table-responsive">
    <table id="tabla_reporte_clientes" class="display" style=" width:100%">
      <thead>
        <tr>
          <th>Cliente</th>
          <th class="ingreso_total">Ingreso total</th>
          <th class="frecuencia">Frecuencia de Pedido</th>
          <th class="deuda">Deuda</th>
          <th>Moroso</th>
          <th>Ver Detalle Deuda</th>
        </tr>
      </thead>

      <tbody>
        @foreach($reporte_clientes as $repcli)
        <tr>
          <td>{{$repcli->cliente}}</td>
          <td>{{$repcli->ingreso_total_pedidos}}</td>
          <td>{{$repcli->frecuencia_pedidos}}</td>
          <td>{{$repcli->deuda}}</td>
          <td>por implementar</td>
          <td>por implementar</td>
        </tr>

        @endforeach
      </tbody>
    </table>
  </div>
  <!--End table-->
  <!-- other table -->
  <br>
  <button id="exportar_reporte_clientes" class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Exportar</button>
  
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
//BEGIN FUNCION PARA CAMBIAR EL IDIOMA DEL DATATABLE
$(document).ready(function() {
    //Begin filtro ingreso total
  $("#cb_ingreso_total_pedidos").on("click", function() {
    $('td:nth-child(2)').toggle();
    $(".ingreso_total").toggle();
  });
//End filtro ingreso total
//Begin filtro ingreso total
  $("#cb_frec_pedidos").on("click", function() {
    $('td:nth-child(3)').toggle();
    $(".frecuencia").toggle();
  });
//End filtro ingreso total
//Begin filtro ingreso total
  $("#cb_deuda").on("click", function() {
    $('td:nth-child(4)').toggle();
    $(".deuda").toggle();
  });
//End filtro ingreso total
  $('#tabla_reporte_clientes').DataTable({
  });
  var t = $("#tabla_reporte_clientes").DataTable();
  $(document).on('click', '#exportar_reporte_clientes', function() {
    var cb_ing_tot_ped = $('#cb_ingreso_total_pedidos').prop('checked');
    var cb_fre_ped = $('#cb_frec_pedidos').prop('checked');
    var cb_deu = $('#cb_deuda').prop('checked');
    var data=cb_ing_tot_ped+"-"+cb_fre_ped+"-"+cb_deu;
    window.open('/Ventas/Reportes/ReporteClientes/exportar/'+data)
  });
});
//END FUNCION PARA CAMBIAR EL IDIOMA DEL DATATABLE
</script>
@endsection