@extends ('layouts.admin')
@section ('contenido')


<section id="hero">
    <div class="hero-container">

        <div lass="row">
            <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
                <img src="img/red_cite.png" width="100" height="auto">
              </div>
            <div class="logos d-inline mt-5 mb-5 col-12 col-sm-4">
                <img src="img/citeccal.png" width="100" height="auto">
            </div>
        </div>


        <div class="row pt-4">
            <div class="col-7">
                <h1 class="h1 d-flex justify-content-center font-weight-bold">BIENVENIDO</h1>
                <h2 class="h1 lead text-white font-weight-bold">{{ Auth::user()->name }}</h2>
                <hr class="my-2 col-sm-2 ">
                @if (auth()->user()->role == 2)
                    <p class="text-white mb-4">Si desea realizar algun cambio ingrese a Configuracion Inicial:</p>

                    <a href="{!! url('configuracion/empresa') !!}">
                        <button class="bttn-unite bttn-md bttn-success ">Configuracion Inicial</button></a>
                @endif
                @if (auth()->user()->estado == 3)
                    <p class="text-white">Es su primera vez iniciando.</p>
                    <a href="{{ url('Usuario') }}" role="button"><button
                            class="bttn-unite bttn-md bttn-success">Cambio de
                            Contraseña</button></a>
                @endif
            </div>
            <div class="col-5">
                <div class="hero-logo" style="width:50%;margin-left:auto;margin-right:auto">
                    @foreach ($empresa as $emp)
                        @if ($emp->imagen != '')
                            {{ Html::image('photo/' . $emp->imagen, 'alt', ['width' => 300, 'height' => 300]) }}
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
