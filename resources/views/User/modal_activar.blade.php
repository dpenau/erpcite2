<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-activar-{{$us->email}}">
	{{Form::Open(array('action'=>array('UsuarioController@store',$us->email
  ),'method'=>'post'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Activar Usuario</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Activar el usuario</p>
        <input type="text" style="display:none" name="email" value="{{$us->email}}">
        <input type="text" style="display:none" name="estado" value="1">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-slant bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-slant bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
