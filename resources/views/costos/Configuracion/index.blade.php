@extends ('layouts.admin')
@section ('contenido')

    <div style="text-align:center;">
      @if(count($datosGenerales)>0)
	<table  class="paleBlueRows" style="margin: 0 auto;">
<thead>
<tr>
<th colspan="12"><h4>DATOS GENERALES</h4></th>

</tr>
</thead>

     <tr>
    <td><h5>Descripción</h5></td>
    <td><h5 >Renta</h5></td>
  </tr>
 @foreach($datosGenerales as $dag)


  <tr>
    <td><h6>Repartición de Utilidades:</h6></td>
    <td>
        <h6>{{$dag->descrip_retencion_utilidades}}</h6>
    </td>
  </tr>
  <tr>
    <td><h6>TCEA de Capital:</h6></td>
    <td><h6>{{$dag->TCEA_capital}}</h6>
</td>
  </tr>
  <tr>
    <td><h6>Lead Time:</h6></td>
    <td><h6>{{$dag->lead_time}}</h6></td>
  </tr>
  <tr>
    <td><h6>% Reproceso:</h6></td>
    <td><h6>{{$dag->porcentaje_reproceso}} %</h6>  </td>
  </tr>
  <tr>
    <td><h6>TCEA Crédito:</h6></td>
    <td><h6>{{$dag->TCEA_credito}} %</h6> </td>
  </tr>
  <tr>
    <td><h6>% Comisión Ventas</h6></td>
    <td><h6>{{$dag->porcentaje_comision_ventas}} %</h6> </td>
  </tr>
  <tr>
    <td><h6>Producción Mensual Promedio:</h6></td><!--sacar prod mensual promedio debe estar guardada-->
    <td><h6>{{$dag->produccion_promedio }}</h6>  </td>
  </tr>
  <tr>
    <td><h6>Produccion Total Anual {{date('Y')}}</h6></td>
    <td><h6>{{$dag->total_produccion_anual}}</h6></td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Producto: (Meses)</h6></td>
    <td><h6>{{$dag->politica_desarrollo_producto}} </h6> </td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Hormas: (Meses)</h6></td>
    <td><h6>{{$dag->politica_desarrollo_horma}} </h6> </td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Troqueles: (Meses)</h6></td>
    <td>{{$dag->politica_desarrollo_troqueles}}</td>
  </tr>
      @endforeach

</table>



</form>
<br>
<a  href="{{ url('Configuracion/Costos_Directos/create') }}">
  <button class="bttn-unite bttn-md bttn-success  mr-sm-5" align="center">Editar</button></a>

    </div>


@else
<h3>Realize la configuracion de sus costos directos:</h3>
<a href="{{ url('Configuracion/Costos_Directos/create') }}">
  <br>
  <br>
  <button align="center" class="bttn-success " >Configuracion de costos</button></a>    </div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@endif

<style type="text/css">table.paleBlueRows {
  font-family: "Times New Roman", Times, serif;
  border: 1px solid #FFFFFF;
  width: 650px;
  height: 200px;
  text-align: center;
  border-collapse: collapse;

}
table.paleBlueRows td, table.paleBlueRows th {
  border: 1px solid #FFFFFF;
  padding: 3px 2px;
}
table.paleBlueRows tbody td {
  font-size: 13px;
}
table.paleBlueRows tr:nth-child(even) {
  background: #D0E4F5;
}
table.paleBlueRows thead {
  background: #0B6FA4;
  border-bottom: 5px solid #FFFFFF;
}
table.paleBlueRows thead th {
  font-size: 17px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 2px solid #FFFFFF;
}
table.paleBlueRows thead th:first-child {
  border-left: none;
}

table.paleBlueRows tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #333333;
  background: #D0E4F5;
  border-top: 3px solid #444444;
}
table.paleBlueRows tfoot td {
  font-size: 14px;
}
</style>

@endsection
