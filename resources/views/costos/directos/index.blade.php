@extends ('layouts.admin')
@section ('contenido')
<div>
    <h3 class="font-weight-bold">Listado de modelos de calzado

    </h3>
    <div style="margin-right:-3%;">
        <a href="modelos_calzado/create">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5" style="margin-right:30%">Nuevo modelo de
                calzado</button></a>
    </div>
</div>

<br>
<div class="row">
    <div class="form-group  col-md-5  col-xs-12">

        <select name="modelo_activo" id="modelo_activo" class="custom-select">
            <option value="" selected disabled>Seleccione Modelos Activos e Inactivos</option>
            <option value="all">Todos</option>
            <option value="active">Activos</option>
            <option value="inactive">Inactivos</option>
        </select>

    </div>


</div>
<div class="x_content table-responsive" style="margin-top:2%;">
    <table id="tablaModelos" class="display">
        <thead align="center">
            <tr>

                <th>Imagen</th>
                <th>Coleccion</th>
                <th>Linea</th>
                <th>Serie</th>
                <th>Código de Modelo</th>
                <th>Descripción de Modelo</th>
                <th>Costo MDF</th>
                <th>Costo MOD</th>
                <th>CDF por Par</th>
                <th>Visualizar</th>
            </tr>
        </thead>
        <tbody align="center">
            @foreach ($LineasModelos as $modelos)
            <tr>
                <td width="10" height="10">
                    {{Html::image('photo/modelos/'.$modelos->RUC_empresa.'/'.$modelos->imagen,'alt',array('width' => 70, 'height' => 70 )) }}
                </td>
                <td> {{ $modelos->nombre_coleccion }} </td>
                <td> {{ $modelos->nombre_linea }} </td>
                <td> {{ $modelos->nombre_serie }}</td>
                <td> {{ $modelos->codigo }} </td>
                <td> {{ $modelos->descripcion }} </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
                <td>
                    <a href="#" >
                        <button class="bttn-unite bttn-md bttn-warning"><i class="fa fa-file-pdf-o"></i></button></a>
                </td>

            </tr>
            @include('Produccion.modelos_calzado.modalactivar')
            @include('Produccion.modelos_calzado.modaldesactivar')
            @include('Produccion.modelos_calzado.modaleditar')
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-imagen">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_imagen"></h4>
            </div>
            <div class="modal-body">
                <div class="border border-dark" align="center">
                    <img id="imagen_imagen" style="width:400px; height:400px" alt="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
</script>

<script type="text/javascript">
var data = <?php echo $LineasModelos;?>;
var t = $("#tabla").DataTable();
var t1 = $("#tabla").DataTable();
var tablaModelos = $("#tablaModelos").DataTable();
var listaModelos = <?php echo $LineasModelos; ?>;
console.log(listaModelos);

//USO DE AJAX PARA ABRIR UNA NUEVA VENTANA CON EL ID
$("#tablaModelos").on('click', 'a.combinacion', function() {

    var id = $(this).attr("id");
    console.log(id);


    $.ajax({
        url: "combinacion_calzado/" + id,
        success: function(html) {

            $.each(html, function(key, value) {
                auxiliar = 1;

            });
        }
    });

});
$("#modelo_activo").change(function() {
    //LIMPIAR
    tablaModelos.rows().remove().draw();

    //CONSTRUCCION FILTRADA
    if (this.value == "all") {
        window.location.reload();

    } else if (this.value == "active") {
        $.each(data, function(key, value) {
            if (value.estado_modelo == 1) {
                const tr = $(
                    "<tr>" +
                    "<td><img src='../../photo/modelos/" + value.RUC_empresa + "/" + value.imagen +
                    "' width='70' height = '70'></img> </td>" +
                    "<td>" + value.nombre_coleccion + "</td>" +
                    "<td>" + value.nombre_linea + "</td>" +
                    "<td>" + value.nombre_serie + "</td>" +
                    "<td>" + value.codigo + "</td>" +
                    "<td>" + value.descripcion + "</td>" +
                    "<td align='center'><a href='/Produccion/combinacion_calzado/listado/" + value
                    .cod_modelo + "'>(" + value.num_combinacion + ")" +
                    "<button class='bttn-unite bttn-md bttn-primary' style='margin-left:5%;'><i class='fa fa-palette'></i></button></a></td>" +

                    "<td>  <a href='#' data-target='#modal-edit-" + value.cod_modelo +
                    "' data-toggle='modal'>" +
                    "<button class='bttn-unite bttn-md bttn-warning'><i class='fas fa-edit'></i></button></a></td>" +

                    "<td> <a class='recepcion-talla' id='' href='#' data-target='#modal-delete-" +
                    value.cod_modelo + "' data-toggle='modal'>" +
                    "<button class='bttn-unite bttn-md bttn-success'><i class='fa fa-toggle-on'></i></button></a></td> </tr>"

                );
                tablaModelos.row.add(tr[0]).draw();
            }
        });



    } else if (this.value == "inactive") {


        $.each(data, function(key, value) {
            if (value.estado_modelo == 0) {
                const tr = $(
                    "<tr>" +
                    "<td><img src='../../photo/modelos/" + value.RUC_empresa + "/" + value.imagen +
                    "' width='70' height = '70'></img> </td>" +
                    "<td>" + value.nombre_coleccion + "</td>" +
                    "<td>" + value.nombre_linea + "</td>" +
                    "<td>" + value.nombre_serie + "</td>" +
                    "<td>" + value.codigo + "</td>" +
                    "<td>" + value.descripcion + "</td>" +
                    "<td align='center'><a href='#'>(" + value.num_combinacion + ")" +
                    "<button class='bttn-unite bttn-md bttn-primary' style='margin-left:5%;'><i class='fa fa-palette'></i></button></a></td>" +

                    "<td>  <a href='#' data-target='#modal-edit-" + value.cod_modelo +
                    "' data-toggle='modal'>" +
                    "<button class='bttn-unite bttn-md bttn-warning'><i class='fas fa-edit'></i></button></a></td>" +

                    "<td> <a class='recepcion-talla' id='' href='#' data-target='#modal-active-" +
                    value.cod_modelo + "' data-toggle='modal'>" +
                    "<button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-toggle-off'></i></button></a></td> </tr>"

                );
                tablaModelos.row.add(tr[0]).draw();
            }
        });

    }

    console.log(this.value);
});
</script>
@endsection
