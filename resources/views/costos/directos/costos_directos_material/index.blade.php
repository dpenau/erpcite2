@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@if(count($codigo_costo)>0)
  @if(count($detalle)>0)
      {!!Form::open(array('url'=>'costo/directo/material/update','method'=>'PUT','autocomplete'=>'off'))!!}
      {{Form::token()}}
        <script type="text/javascript">
          $(document).ready(function(){
            @foreach($detalle as $mat)
              var area="{{$mat->descrip_area}}";
              <?php
              $precio=$mat->costo_sin_igv_material;
              if($mat->t_moneda==1)
              $precio=$precio*$cambio[0]->valor;
              ?>
              switch (area)
              {
                case "P-Acabado":
                  $('#P-Acabado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Acabadov'>"
                  +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                  +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                  +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                  +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                  +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Acabadoc' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                  +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                  +"<td><input class='form-control block acabado' disabled type='text' id='{{$mat->cod_material}}P-Acabadot' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                  +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Acabadofactor' value='{{$mat->factor_equivalencia}}' ></td>"
                  +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Acabadopor' name='porcentaje[]' ></td>"
                  +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
                  +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Acabadob'>-</a></td>"
                  +"</tr>");
                  $("#{{$mat->cod_material}}P-Acabadoc").off("focusout");
                  $("#{{$mat->cod_material}}P-Acabadoc").on("focusout",function(){
                    var cantidad=$("#{{$mat->cod_material}}P-Acabadoc").val();
                    var preciou="{{$mat->costo_sin_igv_material}}";
                    var factor="{{$mat->factor_equivalencia}}";
                    var tot=0.0;
                    tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                    tot=tot.toFixed(6);
                    $("#{{$mat->cod_material}}P-Acabadot").val(tot);
                  });
                  $("#{{$mat->cod_material}}P-Acabadob").off("click");
                  $("#{{$mat->cod_material}}P-Acabadob").on("click",function(){
                    $("#{{$mat->cod_material}}P-Acabadov").remove();
                  });
                break;
                case "P-Aparado":
                $('#P-Aparado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Aparadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Aparadoc' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input class='form-control block aparado' disabled type='text' id='{{$mat->cod_material}}P-Aparadot' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Aparadofactor' value='{{$mat->factor_equivalencia}}' ></td>"
                +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Aparadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Aparadob'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Aparadoc").off("focusout");
                $("#{{$mat->cod_material}}P-Aparadoc").on("focusout",function(){
                  var cantidad=$("#{{$mat->cod_material}}P-Aparadoc").val();
                  var preciou="{{$mat->costo_sin_igv_material}}";
                  var factor="{{$mat->factor_equivalencia}}";
                  var tot=0.0;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Aparadot").val(tot);
                });
                $("#{{$mat->cod_material}}P-Aparadob").off("click");
                $("#{{$mat->cod_material}}P-Aparadob").on("click",function(){
                  $("#{{$mat->cod_material}}P-Aparadov").remove();
                });
                break;
                case "P-Corte":
                $('#P-Corte> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Cortev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Cortec' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input class='form-control block corte' disabled type='text' id='{{$mat->cod_material}}P-Cortet' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Cortefactor' value='{{$mat->factor_equivalencia}}' ></td>"
                +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Cortepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Corteb'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Cortec").off("focusout");
                $("#{{$mat->cod_material}}P-Cortec").on("focusout",function(){
                  var cantidad=$("#{{$mat->cod_material}}P-Cortec").val();
                  var preciou="{{$mat->costo_sin_igv_material}}";
                  var factor="{{$mat->factor_equivalencia}}";
                  var tot=0.0;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Cortet").val(tot);
                });
                $("#{{$mat->cod_material}}P-Corteb").off("click");
                $("#{{$mat->cod_material}}P-Corteb").on("click",function(){
                  $("#{{$mat->cod_material}}P-Cortev").remove();
                });
                break;
                case "P-Habilitado":
                $('#P-Habilitado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Habilitadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Habilitadoc' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input class='form-control block habilitado' disabled type='text' id='{{$mat->cod_material}}P-Habilitadot' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Habilitadofactor' value='{{$mat->factor_equivalencia}}' ></td>"
                +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Habilitadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Habilitadob'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Habilitadoc").off("focusout");
                $("#{{$mat->cod_material}}P-Habilitadoc").on("focusout",function(){
                  var cantidad=$("#{{$mat->cod_material}}P-Habilitadoc").val();
                  var preciou="{{$mat->costo_sin_igv_material}}";
                  var factor="{{$mat->factor_equivalencia}}";
                  var tot=0.0;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Habilitadot").val(tot);
                });
                $("#{{$mat->cod_material}}P-Habilitadob").off("click");
                $("#{{$mat->cod_material}}P-Habilitadob").on("click",function(){
                  $("#{{$mat->cod_material}}P-Habilitadov").remove();
                });
                break;
                case "P-Alistado":
                $('#P-Alistado> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Alistadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Alistadoc' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input class='form-control block alistado' disabled type='text' id='{{$mat->cod_material}}P-Alistadot' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Alistadofactor' value='{{$mat->factor_equivalencia}}' ></td>"
                +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Alistadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Alistadob'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Alistadoc").off("focusout");
                $("#{{$mat->cod_material}}P-Alistadoc").on("focusout",function(){
                  var cantidad=$("#{{$mat->cod_material}}P-Alistadoc").val();
                  var preciou="{{$mat->costo_sin_igv_material}}";
                  var factor="{{$mat->factor_equivalencia}}";
                  var tot=0.0;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Alistadot").val(tot);
                });
                $("#{{$mat->cod_material}}P-Alistadob").off("click");
                $("#{{$mat->cod_material}}P-Alistadob").on("click",function(){
                  $("#{{$mat->cod_material}}P-Alistadov").remove();
                });
                break;
                case "P-Montaje":
                $('#P-Montaje> tbody:last-child').append("<tr id='{{$mat->cod_material}}P-Montajev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='{{$mat->cod_material}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>{{$mat->descrip_material}}</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='{{$precio}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_compra}}'></td>"
                +"<td><input class='form-control' type='text' id='{{$mat->cod_material}}P-Montajec' name='cantidad[]' value='{{number_format($mat->consumo_por_par,6)}}'></td>"
                +"<td><input class='form-control' type='text'disabled value='{{$mat->descrip_unidad_medida}}'></td>"
                +"<td><input class='form-control block montaje' disabled type='text' id='{{$mat->cod_material}}P-Montajet' name='total[]' value='{{number_format($mat->total,6)}}' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='{{$mat->cod_material}}P-Montajefactor' value='{{$mat->factor_equivalencia}}' ></td>"
                +"<td><input class='form-control' disabled type='text' id='{{$mat->cod_material}}P-Montajepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
                +"<td><a class='btn btn-danger' id='{{$mat->cod_material}}P-Montajeb'>-</a></td>"
                +"</tr>");
                $("#{{$mat->cod_material}}P-Montajec").off("focusout");
                $("#{{$mat->cod_material}}P-Montajec").on("focusout",function(){
                  var cantidad=$("#{{$mat->cod_material}}P-Montajec").val();
                  var preciou="{{$mat->costo_sin_igv_material}}";
                  var factor="{{$mat->factor_equivalencia}}";
                  var tot=0.0;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6);
                  tot=tot.toFixed(6);
                  $("#{{$mat->cod_material}}P-Montajet").val(tot);
                });
                $("#{{$mat->cod_material}}P-Montajeb").off("click");
                $("#{{$mat->cod_material}}P-Montajeb").on("click",function(){
                  $("#{{$mat->cod_material}}P-Montajev").remove();
                });
                break;
              }
              var espacio=area.split(" ")
              @if($mat->estado_material_costos==3 || $mat->estado_material_costos==2)
              $("#{{$mat->cod_material}}"+espacio[0]+"v").addClass("bg-info")
              @endif
              @if($mat->estado_material_costos==0 || $mat->estado_material_costos==2)
              $("#{{$mat->cod_material}}"+espacio[0]+"v").addClass("bg-warning")
              @endif
            @endforeach
          });
        </script>

  @else
      {!!Form::open(array('url'=>'costo/directo/material/create','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
  @endif
  @foreach($codigo_costo as $cod)
  <input type="text" name="costo_codigo" style="display:none" value="{{$cod->cod_costo_modelo}}">
  @endforeach
  <div class="right_col" role="main">
    <div class="border-bottom">
      <div class="row">
        <div class="col-md-6">
          <h1>Codigo de Modelo:{{$var}}</h1>
        </div>
        <div class="col-md-6">
          <h3 id="material_total">Costo Total de materiales: S/.</h3>
          <input style="display:none" id="mt"  name="total_materiales" class="form-control" value="">
        </div>
      </div>
      <span class="badge badge-warning">Consumo desactualizado</span>
      <span class="badge badge-info">Precio desactualizado</span>
      <div class="row">
        <div class="col-md-3">
          <h4>Area a agregar material</h4>
          <select   id="area"  class="custom-select">
            <option value="0" selected disabled>Area</option>
            @foreach($area as $ar)
              <option value="{{$ar->descrip_area}}">{{$ar->descrip_area}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3">
          <h4>Categoria</h4>
          <select   id="categoria" class="custom-select">
            <option value="" selected disabled>Categoria</option>
            @foreach($categorias as $cat)
              <option value="{{$cat->cod_categoria}}" >{{$cat->nom_categoria}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3">
          <h4>Subcategoria</h4>
          <select   id="sub-categoria" class="custom-select">
            <option value="" selected disabled>Subcategoria</option>
          </select>
        </div>
        <div class="col-md-3">
          <h4>Materiales</h4>
          <input type="text" class="form-control" id="filtro" placeholder="buscar..." value="">
          <div id="materiales" style="overflow-y:scroll;height:150px">
          </div>
        </div>
      </div>
      <div id="alerta" style="display:none; margin:20px;">
        <div class="alert alert-danger" role="alert">
          No se puede agregar dos materiales iguales en una misma area
        </div>
      </div>
      <div id="buscando" style="display:none; margin:20px;">
        <div class="alert alert-success" role="alert">
          Obteniendo materiales...
        </div>
      </div>
    </div>
    <div class="">
      <h2>Area: Corte</h2>
      <div class="x_content table-responsive">
        <table id="P-Corte" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="corte_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Habilitado</h2>
      <div class="x_content table-responsive">
        <table id="P-Habilitado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="habilitado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Aparado</h2>
      <div class="x_content table-responsive">
        <table id="P-Aparado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="aparado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Alistado</h2>
      <div class="x_content table-responsive">
        <table id="P-Alistado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="alistado_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Montaje</h2>
      <div class="x_content table-responsive">
        <table id="P-Montaje" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="montaje_txt">Total:</h5>
    </div>
    <div class="">
      <h2>Area: Acabado</h2>
      <div class="x_content table-responsive">
        <table id="P-Acabado" class="table stacktable">
          <thead>
            <tr>
              <th>Descripcion del Material</th>
              <th>Valor Unitario sin IGV</th>
              <th>Unidad de Compra</th>
              <th>Consumo por par</th>
              <th>Unidad de Medida</th>
              <th>Costo por Par</th>
              <th>% de Costo</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="acabado_txt">Total:</h5>
    </div>
    <p>
      <a  id="btn_total" > <button  type="button" class="bttn-unite bttn-md bttn-primary" >Calcular Total</button></a></p>
    <button type="submit" class="bttn-unite bttn-md bttn-primary" target="_blank" id="sub">Guardar</button>
      <a  href="{{ url('costo/vermodelos') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger " >Cancelar</button></a>
      {!!Form::close()!!}
  </div>
@else
  <div class="alert alert-danger">
  <a href="{{url('costo/vermodelos')}}" class="alert-link">Cree Primero la cabecera</a>
  </div>
    <a type="button" class="bttn-unite bttn-md bttn-danger " href="{{ url('costo/vermodelos') }}" >Atras</a>
@endif


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var lista_materiales=[];
  var cod_materiales=[];
  $("#filtro").keyup(function(){
    var valor=$("#filtro").val();
    if(lista_materiales.length==0)
    {
      alert("Primero seleccione una categoria y una subcategoria")
      $("#filtro").val("");
    }
    else {
      var tmp=lista_materiales;
      if(valor!="")
      {
        for (var i = 0; i < tmp.length; i++) {
          if(tmp[i].indexOf(valor)==-1)
          {
            $("#d"+cod_materiales[i]).hide();
          }
          else {
            $("#d"+cod_materiales[i]).show();
          }
        }
      }
      else
      {
        for (var i = 0; i < tmp.length; i++) {
            $("#d"+cod_materiales[i]).show();
        }
      }
    }
  })
  $( "#categoria" ).change(function() {
    if($("#area").val()==null)
    {
      alert("Escoga un Area primero")
    }
    else {
      $("#materiales").empty();
        let valor=$("#categoria").val();
        switch (valor)
        {
          case "306":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==306)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
          case "634":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==634)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
          case "969":
          $("#sub-categoria").empty();
          $("#sub-categoria").append("<option value='' selected disabled>Subcategoria</option>");
            @foreach($subcategorias as $sub)
              @if($sub->cod_categoria==969)
                  $("#sub-categoria").append("<option value='{{$sub->cod_subcategoria}}'>{{$sub->nom_subcategoria}}</option>");
              @endif
            @endforeach
          break;
        }
    }
  });
  function obtener_material(valor)
  {
    $("#buscando").toggle("slow");
    $.ajax({
      url: "../obt_material/"+valor,
      success: function(html){
        $("#buscando").toggle("slow");
          generar_opciones(html)
      }
    });
  }
  function generar_opciones(datos)
  {
    var materiales=datos;
    var flag=0;
    var nombre_anterior="";
    var costo_anterior="";
    $.each(materiales, function(key,value){
      var sub=value.cod_subcategoria;
      var tipo=value.t_compra;
        if(tipo==1)
        {
          var nombre=value.descrip_material;
          var costo=value.costo_sin_igv_material;
          nombre=nombre.split('-');
          var nomb=nombre[0];
          if(nombre_anterior==nombre[0] && costo_anterior==costo)
          {
              flag=1;
          }
          else {
            flag=0;
            nombre_anterior=nombre[0];
            costo_anterior=costo;
          }
        }
        else {
          flag=0;
          var nomb=value.descrip_material;
        }
        if(flag==0)
        {
          var codigo=value.cod_material;
          lista_materiales.push(nomb);
          cod_materiales.push(codigo);
        $("#materiales").append("<div id='d"+codigo+"' class='custom-control'><input  type='button'  id='"+codigo+"' value='"+nomb+"'></div>");
        $("#"+codigo).off("click");
        $("#"+codigo).on("click",function(){
          if(!$(this).checked)
          {
            var area=$("#area").val();
            if ( !$("#"+codigo+area+"v").length > 0 ) {
              let precio=value.costo_sin_igv_material;
              if(value.t_moneda==1)
              {
                let cambio="{{$cambio[0]->valor}}";
                precio=precio*cambio;
              }
              switch (area) {
                case "P-Acabado":
                  $('#P-Acabado> tbody:last-child').append("<tr id='"+codigo+"P-Acabadov'>"
                  +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                  +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                  +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                  +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                  +"<td><input class='form-control' type='text' id='"+codigo+"P-Acabadoc' name='cantidad[]'></td>"
                  +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                  +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Acabadot' name='total[]' ></td>"
                  +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Acabadofactor' value='"+value.factor_equivalencia+"' ></td>"
                  +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Acabadopor' name='porcentaje[]' ></td>"
                  +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
                  +"<td><a class='btn btn-danger' id='"+codigo+"P-Acabadob'>-</a></td>"
                  +"</tr>");
                  $("#"+codigo+"P-Acabadoc").off("focusout");
                  $("#"+codigo+"P-Acabadoc").on("focusout",function(){
                    var cantidad=$("#"+codigo+"P-Acabadoc").val();
                    var preciou=value.costo_sin_igv_material;
                    var factor=value.factor_equivalencia;
                    var tot=0.00000;
                    tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                    tot=tot.toFixed(6);
                    $("#"+codigo+"P-Acabadot").val(tot);
                  });
                  $("#"+codigo+"P-Acabadob").off("click");
                  $("#"+codigo+"P-Acabadob").on("click",function(){
                    $("#"+codigo+"P-Acabadov").remove();
                  });
                break;
                case "P-Aparado":
                $('#P-Aparado> tbody:last-child').append("<tr id='"+codigo+"P-Aparadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                +"<td><input class='form-control' type='text' id='"+codigo+"P-Aparadoc' name='cantidad[]'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Aparadot' name='total[]' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Aparadofactor' value='"+value.factor_equivalencia+"' ></td>"
                +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Aparadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
                +"<td><a class='btn btn-danger' id='"+codigo+"P-Aparadob'>-</a></td>"
                +"</tr>");
                $("#"+codigo+"P-Aparadoc").off("focusout");
                $("#"+codigo+"P-Aparadoc").on("focusout",function(){
                  var cantidad=$("#"+codigo+"P-Aparadoc").val();
                  var preciou=value.costo_sin_igv_material;
                  var factor=value.factor_equivalencia;
                  var tot=0.00000;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+codigo+"P-Aparadot").val(tot);
                });
                $("#"+codigo+"P-Aparadob").off("click");
                $("#"+codigo+"P-Aparadob").on("click",function(){
                  $("#"+codigo+"P-Aparadov").remove();
                });
                break;
                case "P-Corte":
                $('#P-Corte> tbody:last-child').append("<tr id='"+codigo+"P-Cortev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                +"<td><input class='form-control' type='text' id='"+codigo+"P-Cortec' name='cantidad[]'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Cortet' name='total[]' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Cortefactor' value='"+value.factor_equivalencia+"' ></td>"
                +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Cortepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
                +"<td><a class='btn btn-danger' id='"+codigo+"P-Corteb'>-</a></td>"
                +"</tr>");
                $("#"+codigo+"P-Cortec").off("focusout");
                $("#"+codigo+"P-Cortec").on("focusout",function(){
                  var cantidad=$("#"+codigo+"P-Cortec").val();
                  var preciou=value.costo_sin_igv_material;
                  var factor=value.factor_equivalencia;
                  var tot=0.00000;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+codigo+"P-Cortet").val(tot);
                });
                $("#"+codigo+"P-Corteb").off("click");
                $("#"+codigo+"P-Corteb").on("click",function(){
                  $("#"+codigo+"P-Cortev").remove();
                });
                break;
                case "P-Habilitado":
                $('#P-Habilitado> tbody:last-child').append("<tr id='"+codigo+"P-Habilitadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                +"<td><input class='form-control' type='text' id='"+codigo+"P-Habilitadoc' name='cantidad[]'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Habilitadot' name='total[]' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Habilitadofactor' value='"+value.factor_equivalencia+"' ></td>"
                +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Habilitadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
                +"<td><a class='btn btn-danger' id='"+codigo+"P-Habilitadob'>-</a></td>"
                +"</tr>");
                $("#"+codigo+"P-Habilitadoc").off("focusout");
                $("#"+codigo+"P-Habilitadoc").on("focusout",function(){
                  var cantidad=$("#"+codigo+"P-Habilitadoc").val();
                  var preciou=value.costo_sin_igv_material;
                  var factor=value.factor_equivalencia;
                  var tot=0.00000;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+codigo+"P-Habilitadot").val(tot);
                });
                $("#"+codigo+"P-Habilitadob").off("click");
                $("#"+codigo+"P-Habilitadob").on("click",function(){
                  $("#"+codigo+"P-Habilitadov").remove();
                });
                break;
                case "P-Alistado":
                $('#P-Alistado> tbody:last-child').append("<tr id='"+codigo+"P-Alistadov'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                +"<td><input class='form-control' type='text' id='"+codigo+"P-Alistadoc' name='cantidad[]'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Alistadot' name='total[]' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Alistadofactor' value='"+value.factor_equivalencia+"' ></td>"
                +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Alistadopor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
                +"<td><a class='btn btn-danger' id='"+codigo+"P-Alistadob'>-</a></td>"
                +"</tr>");
                $("#"+codigo+"P-Alistadoc").off("focusout");
                $("#"+codigo+"P-Alistadoc").on("focusout",function(){
                  var cantidad=$("#"+codigo+"P-Alistadoc").val();
                  var preciou=value.costo_sin_igv_material;
                  var factor=value.factor_equivalencia;
                  var tot=0.00000;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+codigo+"P-Alistadot").val(tot);
                });
                $("#"+codigo+"P-Alistadob").off("click");
                $("#"+codigo+"P-Alistadob").on("click",function(){
                  $("#"+codigo+"P-Alistadov").remove();
                });
                break;
                case "P-Montaje":
                $('#P-Montaje> tbody:last-child').append("<tr id='"+codigo+"P-Montajev'>"
                +"<td style='display:none'><input class='form-control block' type='text' name='id[]' disabled value='"+codigo+"'></td>"
                +"<td><textarea class='form-control' rows='3' type='text'  disabled>"+value.descrip_material+"</textarea></td>"
                +"<td><input class='form-control block' type='text' name='costo[]'  disabled value='"+precio+"'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_compra+"'></td>"
                +"<td><input class='form-control' type='text' id='"+codigo+"P-Montajec' name='cantidad[]'></td>"
                +"<td><input class='form-control' type='text'disabled value='"+value.descrip_unidad_medida+"'></td>"
                +"<td><input class='form-control block acabado' disabled type='text' id='"+codigo+"P-Montajet' name='total[]' ></td>"
                +"<td style='display:none'><input class='form-control block' disabled type='text' id='"+codigo+"P-Montajefactor' value='"+value.factor_equivalencia+"' ></td>"
                +"<td><input class='form-control' disabled type='text' id='"+codigo+"P-Montajepor' name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
                +"<td><a class='btn btn-danger' id='"+codigo+"P-Montajeb'>-</a></td>"
                +"</tr>");
                $("#"+codigo+"P-Montajec").off("focusout");
                $("#"+codigo+"P-Montajec").on("focusout",function(){
                  var cantidad=$("#"+codigo+"P-Montajec").val();
                  var preciou=value.costo_sin_igv_material;
                  var factor=value.factor_equivalencia;
                  var tot=0.00000;
                  tot=(parseFloat(preciou).toFixed(6)*parseFloat(cantidad).toFixed(6))/parseFloat(factor).toFixed(6)
                  tot=tot.toFixed(6);
                  $("#"+codigo+"P-Montajet").val(tot);
                });
                $("#"+codigo+"P-Montajeb").off("click");
                $("#"+codigo+"P-Montajeb").on("click",function(){
                  $("#"+codigo+"P-Montajev").remove();
                });
                default:
              }
            }
            else {
                $("#alerta").toggle("slow");
              setTimeout(function() {
                    $("#alerta").hide("slow");
                }, 5000);
            }
          }
        });
      }
    });
  }
  $("#sub-categoria").change(function(){
    $("#materiales").empty();
    var valor=$("#sub-categoria").val();
    lista_materiales=[];
    cod_materiales=[];
    obtener_material(valor)
  });
  $("#sub").click(function(event){
    $("#btn_total").click();
    $(".block").prop('disabled',false);
  });
  $("#btn_total").click(function(){
      var totales=$("input[class~='acabado']");
      var cortes=$("input[class~='corte']");
      var habilitado=$("input[class~='habilitado']");
      var aparado=$("input[class~='aparado']");
      var alistado=$("input[class~='alistado']");
      var montaje=$("input[class~='montaje']");
      var total=0.00,totalcorte=0.00,totalhabilitado=0.00,totalaparado=0.00,totalalistado=0.00,totalmontaje=0.00;
      for (var i = 0; i < cortes.length; i++) {
        var valor=cortes[i].value;
        totalcorte=parseFloat(valor)+parseFloat(totalcorte);
      }
      for (var i = 0; i < habilitado.length; i++) {
        var valor=habilitado[i].value;
        totalhabilitado=parseFloat(valor)+parseFloat(totalhabilitado);
      }
      for (var i = 0; i < aparado.length; i++) {
        var valor=aparado[i].value;
        totalaparado=parseFloat(valor)+parseFloat(totalaparado);
      }
      for (var i = 0; i < alistado.length; i++) {
        var valor=alistado[i].value;
        totalalistado=parseFloat(valor)+parseFloat(totalalistado);
      }
      for (var i = 0; i < montaje.length; i++) {
        var valor=montaje[i].value;
        totalmontaje=parseFloat(valor)+parseFloat(totalmontaje);
      }
      for(var i=0;i<totales.length;i++)
       {
         var valor=totales[i].value;
         total=parseFloat(valor)+parseFloat(total);
      }
      $("#corte_txt").text("Total: S/."+totalcorte.toFixed(6))
      $("#habilitado_txt").text("Total: S/."+totalhabilitado.toFixed(6))
      $("#aparado_txt").text("Total: S/."+totalaparado.toFixed(6))
      $("#alistado_txt").text("Total: S/."+totalalistado.toFixed(6))
      $("#montaje_txt").text("Total: S/."+totalmontaje.toFixed(6))
      $("#acabado_txt").text("Total: S/."+total.toFixed(6))
      var t=totalcorte+totalhabilitado+totalaparado+totalalistado+totalmontaje+total;
      $("#material_total").text("Costo Total de Materiales Directos S/. "+t.toFixed(6))
      $("#mt").val(t)
  });
  @if(count($detalle)>0)
    $("#btn_total").click();
  @endif
});
</script>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
