@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div class="x_content">
    <div align="center">
        <h1>Indicador de Costos</h1> 
        <div class="row">
            <div class="col-md-6">
                <h2>Materiales Indirectos</h2>
                <div id="materiales_indirectos" >
                    <canvas id="grafico_materiales_indirectos"></canvas>
                </div>
            </div>
            <div class="col-md-6">
                <h2>Depreciacion y Mantenimiento</h2>
                <div id="depreciacion_mantenimiento" >
                    <canvas id="grafico_depreciacion_mantenimiento"></canvas>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2>HISTORIAL DE COSTOS {{date('Y')}}</h2>
                <div id="historial_costo" >
                    <canvas id="grafico_historial_costo"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    var areas =<?php echo $areas;?>;
    var materiales =<?php echo $materiales_indirectos;?>;
    var depreciacion =<?php echo $depreciacion?>;
    var mano_obra=<?php echo $mano_obra_indirecta;?>;
    var servicio=<?php echo $servicio;?>;
    var desarrollo=<?php echo $prototipo;?>;
    var representacion=<?php echo $representacion;?>;
    var distribucion=<?php echo $distribucion;?>;
    var otros=<?php echo $otrosmarketing;?>;
    var externos=<?php echo $externos;?>;
    grafico_materiales()
    grafico_depreciacion()
    grafico_historial()
    function random_rgba_1() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',';
    }
    function grafico_materiales()
    {
        var lista_areas=[]
        var label_areas=[]
        let colores_borde=[];
        let colores_fondo=[];
        $.each(areas,(i,value)=>{
            var cont=0;
            let descripcion=value.descrip_area;
            label_areas[i]=descripcion
            $.each(materiales,(i,val)=>{
                let area_material=val.descrip_area;
                if(descripcion==area_material)
                {
                    cont=cont+val.costo;
                }
            })
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
            lista_areas[i]=cont;
        });
        let html="<canvas id='grafico_materiales_indirectos'></canvas>";
        $("#materiales_indirectos").empty();
        $("#materiales_indirectos").append(html);
        var ctx = document.getElementById('grafico_materiales_indirectos');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: label_areas,
              datasets: [{
                  label: 'Costos Mensual de materiales Indirectos',
                  data: lista_areas,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
        });
    }
    function grafico_depreciacion()
    {
        var lista_depreciacion=[]
        var lista_mantenimiento=[]
        var label_areas=[]
        let colores_borde=[];
        let colores_fondo=[];
        $.each(areas,(i,value)=>{
            var cont=0;
            var mant=0;
            let descripcion=value.descrip_area;
            label_areas[i]=descripcion
            $.each(depreciacion,(i,val)=>{
                let area_depreciacion=val.area;
                if(descripcion==area_depreciacion)
                {
                    cont=cont+val.depreciacion;
                    mant=mant+val.mantenimiento;
                }
            })
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
            lista_depreciacion[i]=cont;
            lista_mantenimiento[i]=mant;
        });
      let html="<canvas id='grafico_depreciacion_mantenimiento' ></canvas>";
      $("#depreciacion_mantenimiento").empty();
      $("#depreciacion_mantenimiento").append(html);
      var ctx = document.getElementById('grafico_depreciacion_mantenimiento');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: label_areas,
              datasets: [{
                  label: 'DEPRECIACION por area',
                  data: lista_depreciacion,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              },
              {
                label: 'MANTENIMIENTO por area',
                data: lista_mantenimiento,
                backgroundColor: colores_borde,
                borderColor: colores_fondo,
                borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_historial()
    {
        var lista_mano_obra=[0,0,0,0,0,0,0,0,0,0,0,0]
        var lista_servicio=[0,0,0,0,0,0,0,0,0,0,0,0];
        var litas_desarrollo=[0,0,0,0,0,0,0,0,0,0,0,0]
        var lista_representacion=[0,0,0,0,0,0,0,0,0,0,0,0];
        var lista_distribucion=[0,0,0,0,0,0,0,0,0,0,0,0];
        var lista_otros=[0,0,0,0,0,0,0,0,0,0,0,0];
        var lista_externos=[0,0,0,0,0,0,0,0,0,0,0,0];
        var label_meses=["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO",
        "SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"];
        let colores_borde=[];
        let colores_fondo=[];
        color=random_rgba_1()
        colores_borde[0]=color+"0.1)";
        colores_fondo[0]=color+"1)";
        $.each(mano_obra,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_mano_obra[0]=lista_mano_obra[0]+val.gasto_mensual
                break;
                case 1:lista_mano_obra[1]=lista_mano_obra[1]+val.gasto_mensual
                break;
                case 2:lista_mano_obra[2]=lista_mano_obra[2]+val.gasto_mensual
                break;
                case 3:lista_mano_obra[3]=lista_mano_obra[3]+val.gasto_mensual
                break;
                case 4:lista_mano_obra[4]=lista_mano_obra[4]+val.gasto_mensual
                break;
                case 5:lista_mano_obra[5]=lista_mano_obra[5]+val.gasto_mensual
                break;
                case 6:lista_mano_obra[6]=lista_mano_obra[6]+val.gasto_mensual
                break;
                case 7:lista_mano_obra[7]=lista_mano_obra[7]+val.gasto_mensual
                break;
                case 8:lista_mano_obra[8]=lista_mano_obra[8]+val.gasto_mensual
                break;
                case 9:lista_mano_obra[9]=lista_mano_obra[9]+val.gasto_mensual
                break;
                case 10:lista_mano_obra[10]=lista_mano_obra[10]+val.gasto_mensual
                break;
                case 11:lista_mano_obra[11]=lista_mano_obra[11]+val.gasto_mensual
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[1]=color+"0.1)";
        colores_fondo[1]=color+"1)";
        $.each(servicio,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_servicio[0]=lista_servicio[0]+val.gasto_mensual
                break;
                case 1:lista_servicio[1]=lista_servicio[1]+val.gasto_mensual
                break;
                case 2:lista_servicio[2]=lista_servicio[2]+val.gasto_mensual
                break;
                case 3:lista_servicio[3]=lista_servicio[3]+val.gasto_mensual
                break;
                case 4:lista_servicio[4]=lista_servicio[4]+val.gasto_mensual
                break;
                case 5:lista_servicio[5]=lista_servicio[5]+val.gasto_mensual
                break;
                case 6:lista_servicio[6]=lista_servicio[6]+val.gasto_mensual
                break;
                case 7:lista_servicio[7]=lista_servicio[7]+val.gasto_mensual
                break;
                case 8:lista_servicio[8]=lista_servicio[8]+val.gasto_mensual
                break;
                case 9:lista_servicio[9]=lista_servicio[9]+val.gasto_mensual
                break;
                case 10:lista_servicio[10]=lista_servicio[10]+val.gasto_mensual
                break;
                case 11:lista_servicio[11]=lista_servicio[11]+val.gasto_mensual
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[2]=color+"0.1)";
        colores_fondo[2]=color+"1)";
        $.each(desarrollo,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_compra);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:litas_desarrollo[0]=litas_desarrollo[0]+val.importe
                break;
                case 1:litas_desarrollo[1]=litas_desarrollo[1]+val.importe
                break;
                case 2:litas_desarrollo[2]=litas_desarrollo[2]+val.importe
                break;
                case 3:litas_desarrollo[3]=litas_desarrollo[3]+val.importe
                break;
                case 4:litas_desarrollo[4]=litas_desarrollo[4]+val.importe
                break;
                case 5:litas_desarrollo[5]=litas_desarrollo[5]+val.importe
                break;
                case 6:litas_desarrollo[6]=litas_desarrollo[6]+val.importe
                break;
                case 7:litas_desarrollo[7]=litas_desarrollo[7]+val.importe
                break;
                case 8:litas_desarrollo[8]=litas_desarrollo[8]+val.importe
                break;
                case 9:litas_desarrollo[9]=litas_desarrollo[9]+val.importe
                break;
                case 10:litas_desarrollo[10]=litas_desarrollo[10]+val.importe
                break;
                case 11:litas_desarrollo[11]=litas_desarrollo[11]+val.importe
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[3]=color+"0.1)";
        colores_fondo[3]=color+"1)";
        $.each(representacion,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_representacion[0]=lista_representacion[0]+val.gasto
                break;
                case 1:lista_representacion[1]=lista_representacion[1]+val.gasto
                break;
                case 2:lista_representacion[2]=lista_representacion[2]+val.gasto
                break;
                case 3:lista_representacion[3]=lista_representacion[3]+val.gasto
                break;
                case 4:lista_representacion[4]=lista_representacion[4]+val.gasto
                break;
                case 5:lista_representacion[5]=lista_representacion[5]+val.gasto
                break;
                case 6:lista_representacion[6]=lista_representacion[6]+val.gasto
                break;
                case 7:lista_representacion[7]=lista_representacion[7]+val.gasto
                break;
                case 8:lista_representacion[8]=lista_representacion[8]+val.gasto
                break;
                case 9:lista_representacion[9]=lista_representacion[9]+val.gasto
                break;
                case 10:lista_representacion[10]=lista_representacion[10]+val.gasto
                break;
                case 11:lista_representacion[11]=lista_representacion[11]+val.gasto
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[4]=color+"0.1)";
        colores_fondo[4]=color+"1)";
        $.each(distribucion,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_distribucion[0]=lista_distribucion[0]+val.gasto_mensual
                break;
                case 1:lista_distribucion[1]=lista_distribucion[1]+val.gasto_mensual
                break;
                case 2:lista_distribucion[2]=lista_distribucion[2]+val.gasto_mensual
                break;
                case 3:lista_distribucion[3]=lista_distribucion[3]+val.gasto_mensual
                break;
                case 4:lista_distribucion[4]=lista_distribucion[4]+val.gasto_mensual
                break;
                case 5:lista_distribucion[5]=lista_distribucion[5]+val.gasto_mensual
                break;
                case 6:lista_distribucion[6]=lista_distribucion[6]+val.gasto_mensual
                break;
                case 7:lista_distribucion[7]=lista_distribucion[7]+val.gasto_mensual
                break;
                case 8:lista_distribucion[8]=lista_distribucion[8]+val.gasto_mensual
                break;
                case 9:lista_distribucion[9]=lista_distribucion[9]+val.gasto_mensual
                break;
                case 10:lista_distribucion[10]=lista_distribucion[10]+val.gasto_mensual
                break;
                case 11:lista_distribucion[11]=lista_distribucion[11]+val.gasto_mensual
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[5]=color+"0.1)";
        colores_fondo[5]=color+"1)";
        $.each(otros,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_otros[0]=lista_otros[0]+val.gasto
                break;
                case 1:lista_otros[1]=lista_otros[1]+val.gasto
                break;
                case 2:lista_otros[2]=lista_otros[2]+val.gasto
                break;
                case 3:lista_otros[3]=lista_otros[3]+val.gasto
                break;
                case 4:lista_otros[4]=lista_otros[4]+val.gasto
                break;
                case 5:lista_otros[5]=lista_otros[5]+val.gasto
                break;
                case 6:lista_otros[6]=lista_otros[6]+val.gasto
                break;
                case 7:lista_otros[7]=lista_otros[7]+val.gasto
                break;
                case 8:lista_otros[8]=lista_otros[8]+val.gasto
                break;
                case 9:lista_otros[9]=lista_otros[9]+val.gasto
                break;
                case 10:lista_otros[10]=lista_otros[10]+val.gasto
                break;
                case 11:lista_otros[11]=lista_otros[11]+val.gasto
                break;
            }
        })
        //
        color=random_rgba_1()
        colores_borde[6]=color+"0.1)";
        colores_fondo[6]=color+"1)";
        $.each(externos,(i,val)=>{
            let fecha_realizaod=new Date(val.fecha_creacion);
            let fecha_orden_string=fecha_realizaod.getMonth();
            switch(fecha_orden_string)
            {
                case 0:lista_externos[0]=lista_externos[0]+val.gasto_mensual
                break;
                case 1:lista_externos[1]=lista_externos[1]+val.gasto_mensual
                break;
                case 2:lista_externos[2]=lista_externos[2]+val.gasto_mensual
                break;
                case 3:lista_externos[3]=lista_externos[3]+val.gasto_mensual
                break;
                case 4:lista_externos[4]=lista_externos[4]+val.gasto_mensual
                break;
                case 5:lista_externos[5]=lista_externos[5]+val.gasto_mensual
                break;
                case 6:lista_externos[6]=lista_externos[6]+val.gasto_mensual
                break;
                case 7:lista_externos[7]=lista_externos[7]+val.gasto_mensual
                break;
                case 8:lista_externos[8]=lista_externos[8]+val.gasto_mensual
                break;
                case 9:lista_externos[9]=lista_externos[9]+val.gasto_mensual
                break;
                case 10:lista_externos[10]=lista_externos[10]+val.gasto_mensual
                break;
                case 11:lista_externos[11]=lista_externos[11]+val.gasto_mensual
                break;
            }
        })
        let html="<canvas id='grafico_historial_costo' ></canvas>";
        $("#historial_costo").empty();
        $("#historial_costo").append(html);
        var ctx = document.getElementById('grafico_historial_costo');
        var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: label_meses,
              datasets: [{
                  label: 'Mano de obra Indirecta a lo largo del año',
                  data: lista_mano_obra,
                  backgroundColor: colores_borde[0],
                  borderColor: colores_fondo[0],
                  borderWidth: 1
              },
              {
                  label: 'Servicios Basicos a lo largo del año',
                  data: lista_servicio,
                  backgroundColor: colores_borde[1],
                  borderColor: colores_fondo[1],
                  borderWidth: 1
              },
              {
                  label: 'Desarrollo de Producto (PROTOTIPO, HORMAS Y TROQUELES) a lo largo del año',
                  data: litas_desarrollo,
                  backgroundColor: colores_borde[2],
                  borderColor: colores_fondo[2],
                  borderWidth: 1
              },
              {
                label: 'Representacion a lo largo del año',
                  data: lista_representacion,
                  backgroundColor: colores_borde[3],
                  borderColor: colores_fondo[3],
                  borderWidth: 1
              },
              {
                label: 'Distribucion a lo largo del año',
                  data: lista_distribucion,
                  backgroundColor: colores_borde[4],
                  borderColor: colores_fondo[4],
                  borderWidth: 1
              },
              {
                label: 'Otros Gastos a lo largo del año',
                  data: lista_otros,
                  backgroundColor: colores_borde[5],
                  borderColor: colores_fondo[5],
                  borderWidth: 1
              },
              {
                label: 'Gastos de Externos a lo largo del año',
                  data: lista_externos,
                  backgroundColor: colores_borde[6],
                  borderColor: colores_fondo[6],
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_menor_ano()
    {
        let nombre_material=[];
        let cantidad=[];
        let colores_borde=[];
        let colores_fondo=[];
        let tamano=10;
        if(mayor_mes.length<10)
        {
            tamano=mayor_mes.length
        }
        for(let i=0;i<tamano;i++)
        {
            color=random_rgba_1()
            nombre_material[i]=menor[i].descrip_material;
            cantidad[i]=menor[i].cantidad_salida;
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        let html="<canvas id='grafico_menor_ano' ></canvas>";
      $("#menor_ano").empty();
      $("#menor_ano").append(html);
      var ctx = document.getElementById('grafico_menor_ano');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: nombre_material,
              datasets: [{
                  label: 'Materiales con menor salida en el año',
                  data: cantidad,
                  backgroundColor: colores_borde,
                  borderColor: colores_fondo,
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
  });
</script>
@endsection
