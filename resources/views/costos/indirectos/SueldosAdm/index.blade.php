@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto Administrativo</h1>                    
                  
                    <div class="clearfix"></div>
            </div>


            <div class="row">  
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list"> 
                <h2 class="d-inline ">Sueldos Administrativos</h2>
              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  <button type="submit" class="bttn-slant bttn-md bttn-success" target="_blank" id="sub"><i class="fas fa-plus"></i></button>
              <a href="SueldosAdm/create">  
                <button type="submit" class="bttn-slant bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Sueldo Administrativo</button></a>
                <div class="x_content table-responsive">
                    <table id="table_mp" class="table stacktable">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Descripcion de Puesto</th>
                        <th>Sueldo Mensual en Planilla</th>
                        <th>Beneficios Sociales</th>
                        <th>Otros Sueldos Mensuales</th>
                        <th>Gasto Mensual</th>
                        <th>Ver</th>


                        <th>Eliminar</th>

                        </tr>
                    </thead>
                    
                        <tbody>
                                @foreach($sueldos as $op)

                            <tr>      
 
                                <td>{{$op->area}}</td>
                                <td>{{$op->puesto}}</td>
                                <td>{{$op->sueldo_mensual}}</td>
                                <td>{{$op->beneficios}}</td>
                                <td>{{$op->otros}}</td>
                                <td>{{$op->gasto_mensual}}</td>
                                <td>
                                <a href="" data-target="#modal-vermas-{{$op->id_gastos_sueldos}}" data-toggle="modal">
                                    <button class="bttn-slant bttn-md bttn-warning "><i class="fas fa-search"></i></button></a>
                                </td>
                               
                                <td>
<a href="" data-target="#modal-delete-{{$op->id_gastos_sueldos}}" data-toggle="modal">                                    <button class="bttn-slant bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>

                            </tr>            
                                                        @include('costos_indirectos.SueldosAdm.modal')
                                                            @include('costos_indirectos.SueldosAdm.vermas')

                            @endforeach

                        </tbody>    
                        <tr>
                         <td>              
                    <h4>Costo Total de Sueldos Administrativos</h4>
                     </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>

                                                        <td></td>

         
                         <td>  

                                @foreach($total as $op)
            
                        <input style="width:100%; border: 0;background-color:transparent;
" name="{{$op->sum}}" value="{{$op->sum}}">
                            @endforeach
                                                    </tr>                              

                         </td>

                            </tr>                        
                    </table>
                </div>              
                </div>


               
            </div>
            </div>




        </div>
    </div>



@endsection