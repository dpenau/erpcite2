@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title mb-3">
                        <h2 class="font-weight-bold d-inline ">Nuevo Activo</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
    <h3 class="my-3">Depreciación</h3>
    {!!Form::open(array('url'=>'costo/indirectos/depreciacion','method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}
        <div class="row">
            <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                <h6 class="my-3">Área:</h6>
                <select required='required' id="categoria" name="cod_area" class="custom-select">
                    <option value="" selected disabled>Seleccione</option>
                    @foreach ($area as $area)
                    <option value="{{$area->cod_area}}" >{{$area->descrip_area}}</option>
                    @endforeach
                </select>

                <h6 class="my-3">Descripción del Activo:</h6>
                <input type="text" id="desc" name="descrip_activo" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Marca:</h6>
                <input type="text" id="" name="marca" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Tipo de Activo:</h6>
                <select required='required' id="activo" name="cod_tipo_activo" class="custom-select">
                    <option value="" disabled selected>Seleccione</option>
                    @foreach ($tipo_activo as $tipo)
                    <option value="{{$tipo->cod_tipo_activo}}" >{{$tipo->concepto}}</option>
                    @endforeach
                </select>

                <h6 class="my-3">Años de Depreciación:</h6>
                <input type="text" id="depreciacion" name="anios_vida_util" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Tipo de Adquisición:</h6>
                <select required='required' id="tipoadq" name="cod_tipo_adquisicion" class="custom-select">
                    <option value="" disabled selected>Seleccione</option>
                    @foreach ($tipo_adquisicion as $tipoad)
                    <option value="{{$tipoad->cod_tipo_adquisicion}}" >{{$tipoad->descrip_tipo_adquisicion}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                <h6 class="my-3">Años de Uso Antes:</h6>
                    <input type="text" id="usoantes" name="anio_uso" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Año de Compra:</h6>
                    <input type="text" id="anocomp" name="anio_compra" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Valor Unitario:</h6>
                    <input type="text" id="unitario" name="valor_unitario_sin_IGV" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Numero de Activos:</h6>
                    <input type="text" id="n_activo" name="cantidad" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Años Depreciados:</h6>
                    <input type="text" id="aniosdep" name="anios_depreciados" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Depreciación Mensual:</h6>
                    <input type="text" id="dep_mensual" name="depreciacion_mensual" maxlength="70" required="required" class="form-control ">


            </div>
        </div>
        <h3 class="my-3">Mantenimiento</h3>
        <div class="row">
            <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                <h6 class="my-3">Descripción del Activo:</h6>
                    <input type="text" id="descmant" name="" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Numero de Activos:</h6>
                    <input type="text" id="numact" name="cantidad" maxlength="70" required="required" class="form-control ">

            </div>

            <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                <h6 class="my-3">Gasto por Mantenimiento:</h6>
                    <input type="text" id="gastomant" name="gasto_mantenimiento" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Frecuencia Anual:</h6>
                    <input type="text" id="frecanual" name="frecuencia_mantenimiento_anual" maxlength="70" required="required" class="form-control ">

                <h6 class="my-3">Gasto Mensual:</h6>
                    <input type="text" id="gasttot" name="gasto_mensual_depreciacion" maxlength="70" required="required" class="form-control ">

            </div>
        </div>
        <div class="form-group my-5">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                <button  class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>
            </div>
        </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
      $("#n_activo").change(function(){
        $("#numact").val($("#n_activo").val())
      })
      $("#desc").change(function(){
        $("#descmant").val($("#desc").val())
      })
      $("#activo").change(function() {
        var codigo_escogido= $("#activo").val()
        @foreach($activo as $act)
          var codigo={{$act->cod_tipo_activo}}
          if(codigo_escogido==codigo)
          {
            $("#depreciacion").val({{$act->anios_vida_util}})
          }
        @endforeach
      });
      $("#tipoadq").change(function(){
        var codigo_escogido=$("#tipoadq").val()
          if(codigo_escogido==909)
          {
            $("#usoantes").val(0)
          }
          else{
            $("#usoantes").val("")
          }
      });
      $("#usoantes").change(function(){
        var val=  $("#anocomp").val()
        var val2= $("#usoantes").val()
        var fecha = new Date();
        var ano = fecha.getFullYear();
        if(val!="" && val2!=""){
          var total=ano-(val-val2);
          $("#aniosdep").val(total)
        }
      })
      $("#anocomp").change(function(){
        var val=  $("#anocomp").val()
        var val2= $("#usoantes").val()
        var fecha = new Date();
        var ano = fecha.getFullYear();
        if(val!="" && val2!=""){
          var total=ano-(val-val2);
          $("#aniosdep").val(total)
        }
      })
      $("#n_activo").focusout(function(){
          var valor1=$("#unitario").val()
          var valor2=$("#n_activo").val()
          var valor3=$("#aniosdep").val()
          var valor4=$("#depreciacion").val()
          if(valor1!="" && valor2!="" && valor3!=""&& valor4!="")
          {

            if(parseFloat(valor3)<=parseFloat(valor4))
            {
              var total=0.00;
              var num1=parseFloat(valor1)*parseFloat(valor2);
              var num2=parseFloat(valor4)*12;
              total=total+(num1/num2)
            }
            else {
              var total=0.00;
            }
            $("#dep_mensual").val(total.toFixed(6))
          }
      });
      $("#frecanual").focusout(function(){
          var valor1=$("#numact").val()
          var valor2=$("#gastomant").val()
          var valor3=$("#frecanual").val()
          if(valor1!="" && valor2!="" && valor3!="" )
          {
            var total=0.00;
              total=total+((parseFloat($('#gastomant').val())*parseFloat($('#numact').val())*parseFloat($('#frecanual').val()))/12)
            $("#gasttot").val(total)
          }
      });
    });
</script>
@endsection
