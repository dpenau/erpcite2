<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$dep->cod_activo}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$dep->descrip_activo}}</h4>
			</div>
			<div class="modal-body">
                <h5>Área: {{$dep->descrip_area}}</h5>
                <h5>Marca: {{$dep->marca}}</h5>
                <h5>Tipo de Activo: {{$dep->concepto}}</h5>
                <h5>Años de Depreciación: {{$dep->anios_vida_util}} años</h5>
                <h5>Tipo de Adquisición: {{$dep->descrip_tipo_adquisicion}}</h5>
                <h5>Años de Uso Antes: {{$dep->anio_uso}} años</h5>
                <h5>Años de Compra: {{$dep->anio_compra}}</h5>
                <h5>Valor Unitario: S/.{{$dep->valor_unitario_sin_IGV}}</h5>
                <h5>Número de Activos: {{$dep->cantidad}}</h5>
                <h5>Depreciacion Mensual: S/.{{$dep->depreciacion_mensual}}</h5>
                <h5>Gasto por Mantenimiento: S/.{{$dep->gasto_mantenimiento}}</h5>
                <h5>Frecuencia Anual: {{$dep->frecuencia_mantenimiento_anual}} veces</h5>
                <h5>Gasto Mensual: S/.{{$dep->gasto_mensual_depreciacion}}</h5>
			</div>
		</div>
	</div>
</div>
