@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title mb-3">
                    <h2 class="font-weight-bold d-inline ">Gastos De Desarrollo de Producto</h2>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-12">
  </div>
<div class="row">
  <div class="col-12">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
        {!!Form::open(array('url'=>'costo/indirectos/gastos','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                  <h6 class="my-3">Tipo de Producto:</h6>
                  <select required='required' id="des" name="tipo_desarrollo"  class="custom-select">
                      <option value="" selected disabled>Tipo de Producto</option>
                      @foreach($desarrollo as $des)
                        <option value="{{$des->cod_tipo_desarrollo}}" >{{$des->descrip_tipo_desarrollo}}</option>
                      @endforeach
                  </select>
                    <h6 class="my-3">Area:</h6>
                    <select required='required' name="area" id="categoria"  class="custom-select">
                        <option value=""  disabled>Area</option>
                        @foreach($areas as $ar)
                          @if(strpos($ar->descrip_area, "P-") === false)
                            <option selected  value="{{$ar->cod_area}}" >{{$ar->descrip_area}}</option>
                          @endif
                        @endforeach

                    </select>

                    <h6 class="my-3">Tipo de Gasto:</h6>
                    <select required='required' id="tg"  name="tipo_gasto" class="custom-select">
                        <option value="" selected disabled>Tipo de Gastos</option>
                        <option value="Material">Material</option>
                        <option value="Proceso">Proceso</option>
                        <option value="Servicios">Servicios</option>
                    </select>

                    <h6 class="my-3">Descripcion del Gasto:</h6>
                    <select  class="custom-select" required id="des_gasto">
                      <option value="" selected disabled>Seleccione una opcion</option>
                    </select>
                    <input type="text" style="display:none" name="descrip_gasto" id="descrip_texto">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3" id="texto">Nro. de Producto al Mes:</h6>
                        <input type="text" id="pmes" name="mes" maxlength="70"  required="required" class="form-control ">

                    <h6 class="my-3">Gasto:</h6>
                        <input type="text" id="gt" name="total" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Importe por Prototipo:</h6>
                        <input type="text" id="imp" name="importe" maxlength="70" required="required" class="form-control ">
                    <h6 class="my-3">Fecha de Compra</h6>
                        <input type="date" id="fecha" name="fecha_compra" required class="form-control">
                </div>
                <div class="col-md-4">
                  <div id="modelo_previo" style="display:none">
                      <div class="row">
                        <div class="col-md-12">
                          <label for="">Vista Previa del modelo</label>
                        </div>
                        <div class="col-md-12">
                          <img src="" id="img_modelo" alt="" width="200px">
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href="/costo/indirectos/gastos"><button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button></a>
                </div>
            </div>
        {!!Form::close()!!}
      </div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
      let lista_materiales=[]
      let datos_obtenidos
      function vaciar()
      {
        $("#pmes").val("")
        $("#gt").val("")
        $("#imp").val("");
      }
      $("#tg").change(function(){
        vaciar();
        var tg=$("#tg").val();
        if(tg=="Servicios")
        {
          $("#texto").text("Cantidades:")
        }
        else {
          $("#texto").text("Nro. de Producto al Mes:")
        }
      })
      function obtener_lista(valor)
      {
        let tmp=valor;
        let tipo=""
        if(tmp=="1")
        {
          $("#modelo_previo").show('fast')
          tipo=1
        }
        else{
          $("#modelo_previo").hide('fast')
          tipo=2
        }
        $.ajax({
          url:"/costo/indirectos/desarrollo_obtener/"+tmp,
          success:function(data)
          {
            datos_obtenidos=data
            llenar_lista(data,tipo)
          }
        })
      }
      function llenar_lista(data,tipo)
      {
        let tmp=data;
        let tmp_tipo=tipo
        let html="";
        $("#des_gasto").empty();
        $("#des_gasto").append("<option value='' selected disabled>Seleccione una opcion</option>");
        if(tipo=="1")
        {
          $.each(data,(i,val)=>{
            html+="<option value='"+val.cod_costo_modelo+"+"+val.imagen+"+"+val.total_materiales+"'>"+val.cod_modelo+"--"+val.nombre_serie+"</option>"
          })
        }
        else
        {
          let nombreant=""
          let costoant=""
          let tallas=""
          let cont=0
          $.each(data,(i,val)=>{
            let nombreact=val.descrip_material.split("-")
              if(costoant!=val.costo_sin_igv_material)
              {
                nombreant=nombreact[0]
                costoant=val.costo_sin_igv_material
                lista_materiales[cont]=i
                html+="<option value='"+val.cod_material+"+"+cont+"+"+val.costo_sin_igv_material+"'>"+val.descrip_material+"</option>"
                cont++
              }
          })
        }
        $("#des_gasto").append(html);
      }
      $("#des_gasto").change(()=>{
        let tipo=$("#des").val()
        let valor=$("#des_gasto").val().split('+')
        $.ajax({
          url:"/desarrollo_obtener/verificar/"+valor[0],
          success:function(data)
          {
            console.log(data)
            if(data!=0)
            {
              alert("ya se encuentra registrado este costo")
              $("#des_gasto").val("")
            }
            else
            {
              $("#descrip_texto").val(valor[0])
              if(tipo=="1")
              {
                let ruc="{{$empresa[0]->RUC_empresa}}"
                $("#img_modelo").attr("src","/photo/modelos/"+ruc+"/"+valor[1])
                $("#pmes").val(1)
                $("#gt").val(valor[2])
              }
              else
              {
                let cantidad=0
                if(valor[1]==lista_materiales.length-1)
                {
                  cantidad=datos_obtenidos.length-lista_materiales[valor[1]]
                }
                else
                {
                  let sig=parseInt(valor[1])+1
                  cantidad=lista_materiales[sig]-lista_materiales[valor[1]]
                }
                $("#pmes").val(cantidad)
                $("#gt").val(valor[2])
                
              }
              $("#gt").trigger("focusout")
            }
          }
        })
        
      })
      $("#pmes").change(function(){
        $("#gt").trigger("focusout")
      })
      $("#des").change(function(){
        obtener_lista($("#des").val())
        vaciar();
        var opciones="<option value='' selected disabled>Tipo de Gastos</option>"+
        "<option value='Proceso' selected>Proceso</option>"
        var opcion2="<option value=''  disabled>Tipo de Gastos</option>"+
        "<option selected value='Servicios'>Servicios</option>"
        switch ($("#des").val()) {
          case "1":
            $("#tg").empty();
            $("#tg").append(opciones);
            $("#texto").text("Cantidades Realizadas:")
          break;
          case "2":
          $("#tg").empty();
          $("#tg").append(opcion2);
          $("#texto").text("Cantidades Realizadas:")
          break;
          case "3":
          $("#tg").empty();
          $("#tg").append(opcion2);
          $("#texto").text("Cantidades Realizadas:")
          break;
          default:
        }
      });
      $("#gt").focusout(function(){

        var tp=$("#des").val();
        var tg=$("#tg").val();
        var val=$("#pmes").val()
        var val2=$("#gt").val()
        var tot=0.00;
        if(val!="" && val2!="")
        {
          if(tp=="1")
          {
            if(tg=="Material" || tg=="Proceso")
            {
              tot=tot+(val2*val);
            }
            else {
              tot=tot+(val2*val);
            }
          }
          else
          {
              tot=tot+(val2*val);
          }

          $("#imp").val(tot.toFixed(6));
        }
        else {
          alert("Llene los campos")
        }
      });
    });
</script>
@endsection
