@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Desarrollo de Producto</h1>
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Prototipo</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Hormas</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Troqueles" role="tab" aria-controls="profile">Troqueles</a>
                    </div>
                    <div class="clearfix"></div>
            </div>

            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                <h2 class="d-inline ">Desarrollo de Prototipo</h2>
                <a href="gastos/create">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Desarrollo de Prototipo</button>
                </a>
                <div>
                  <h3>Politica de desarrollo de prototipo: <strong>{{$politica[0]->politica_desarrollo_producto}} MESES</strong></h3>
                </div>
                <div class="x_content table-responsive">
                    <table id="example" class="display">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Tipo de Gasto</th>
                        <th>Descripcion del Gasto</th>
                        <th>Cantidades Realizadas</th>
                        <th>Gasto Total</th>
                        <th>Importe por Prototipo</th>
                        <th>Fecha de compra</th>
                        <th>Fecha de vencimiento</th>
                        <th>Accion</th>
                        </tr>
                    </thead>
                      @foreach($prototipo as $prot)
                        <tbody>
                            <tr>
                              <td>{{$prot->descrip_area}}</td>
                              <td>{{$prot->tipo_gasto}}</td>
                              <td>{{$prot->cod_modelo}}--{{$prot->nombre_serie}}</td>
                              <td>{{$prot->cantidad_mensual}}</td>
                              <td>{{$prot->gasto_total}}</td>
                              <td  class="pgasto">{{$prot->importe}}</td>
                              <td>{{$prot->fecha_compra}}</td>
                              <td>{{$prot->vencimiento}}</td>
                              <td>
                                    <a href="" data-target="#modal-eliminar-{{$prot->cod_gasto_desarrollo_producto}}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        </tbody>
                        @include('costos.indirectos.gastos.modal_eliminar')
                      @endforeach
                    </table>
                    <h4 id="ctp">Costo Total de Desarrollo de Prototipo</h4>
                </div>
                </div>


                <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Desarrollo de Hormas</h2>

                <a href="gastos/create">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Desarrollo de Hormas</button>
                </a>
                <h3>Politica de desarrollo de prototipo: <strong>{{$politica[0]->politica_desarrollo_horma}} MESES</strong></h3>
                <div class="x_content table-responsive">
                    <table id="example2" class="display">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Tipo de Gasto</th>
                        <th>Descripcion del Gasto</th>
                        <th>Cantidades Realizadas</th>
                        <th>Gasto Total</th>
                        <th>Importe po Hormas</th>
                        <th>Fecha de compra</th>
                        <th>Fecha de vencimiento</th>
                        <th>Accion</th>
                        </tr>
                    </thead>
                      @foreach($horma as $prot)
                        <tbody>
                            <tr>
                              <td>{{$prot->descrip_area}}</td>
                              <td>{{$prot->tipo_gasto}}</td>
                              <td>{{$prot->descrip_material}}</td>
                              <td>{{$prot->cantidad_mensual}}</td>
                              <td >{{$prot->gasto_total}}</td>
                              <td class="hgasto">{{$prot->importe}}</td>
                              <td>{{$prot->fecha_compra}}</td>
                              <td>{{$prot->vencimiento}}</td>
                                <td>
                                    <a href="" data-target="#modal-eliminar-{{$prot->cod_gasto_desarrollo_producto}}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        </tbody>
                        @include('costos.indirectos.gastos.modal_eliminar')
                        @endforeach
                    </table>
                    <h4 id="cth">Costo Total de Desarrollo de Hormas</h4>
                </div>
                </div>

            <div class="tab-pane fade" id="list-Troqueles" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Desarrollo de Troqueles</h2>

                <a href="gastos/create">
                  <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Desarrollo de Troqueles</button>
                </a>
                <h3>Politica de desarrollo de prototipo: <strong>{{$politica[0]->politica_desarrollo_troqueles}} MESES</strong></h3>

                <div class="x_content table-responsive">
                    <table id="example3" class="display">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Tipo de Gasto</th>
                        <th>Descripcion del Gasto</th>
                        <th>Cantidades Realizadas</th>
                        <th>Gasto Total</th>
                        <th>Importe po Troqueles</th>
                        <th>Fecha de compra</th>
                        <th>Fecha de vencimiento</th>
                        <th>Accion</th>
                        </tr>
                    </thead>
                      @foreach($troquel as $prot)
                        <tbody>
                            <tr>
                              <td>{{$prot->descrip_area}}</td>
                              <td>{{$prot->tipo_gasto}}</td>
                              <td>{{$prot->descrip_material}}</td>
                              <td>{{$prot->cantidad_mensual}}</td>
                              <td >{{$prot->gasto_total}}</td>
                              <td class="tgasto">{{$prot->importe}}</td>
                              <td>{{$prot->fecha_compra}}</td>
                              <td>{{$prot->vencimiento}}</td>
                                <td>
                                    <a href="" data-target="#modal-eliminar-{{$prot->cod_gasto_desarrollo_producto}}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        </tbody>
                        @include('costos.indirectos.gastos.modal_eliminar')
                        @endforeach
                    </table>
                    <h4 id="ctt">Costo Total de Troqueles</h4>
                </div>
                </div>
            </div>
            </div>




        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      var ptotal=$("td[class~='pgasto']");
      var htotales=$("td[class~='hgasto']");
      var ttotales=$("td[class~='tgasto']");
      var suma=0.00;
      var sumah=0.00;
      var sumat=0.00;
      for(var i=0;i<ptotal.length;i++)
      {
        var num=parseFloat(ptotal[i].innerText)
        suma=suma+num;
      }
      for(var i=0;i<htotales.length;i++)
      {
        var num=parseFloat(htotales[i].innerText)
        sumah=sumah+num;
      }
      for(var i=0;i<ttotales.length;i++)
      {
        var num=parseFloat(ttotales[i].innerText)
        sumat=sumat+num;
      }
      $("#ctp").html("Costo Total de Prototipo S/."+ suma)
      $("#cth").html("Costo Total de Desarrollo de Hormas S/."+ sumah)
      $("#ctt").html("Costo Total de Troqueles S/."+ sumat)
    </script>

@endsection
