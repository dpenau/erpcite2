@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Mano de Obra Indirecta </h1>
                <a href='mano_obra/create'>
                <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nueva Mano de Obra</button></a>
                <div class="clearfix"></div>
            </div>

            <div class="x_content table-responsive">
                <table id="example" class="display">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Área</th>
                            <th>Descripción de Puesto</th>
                            <th>Sueldo Mensual en Planilla S/.</th>
                            <th>Beneficios Sociales S/.</th>
                            <th>Otros Sueldos Mensuales S/.</th>
                            <th>Gastos Mensuales S/.</th>
                            <th>Fecha de creacion</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($manoobra as $mano)
                        @if($mano->descrip_area=="Desarrollo de Producto" || $mano->descrip_area=="Logistica" ||
                        $mano->descrip_area=="Producción" || $mano->descrip_area=="Seguridad y Limpieza" || $mano->descrip_area=="Mantenimiento")
                          <tr>
                            <td>
                              @if($mano->estado==2)
                                <div class="bg-info" style="height:20px; width:10px"></div>
                              @endif
                            </td>
                              <td>{{$mano->descrip_area}}</td>
                              <td>{{$mano->puesto}}</td>
                              <td>{{$mano->sueldo_mensual}}</td>
                              <td>{{$mano->beneficios}}</td>
                              <td>{{$mano->otros}}</td>
                              <td class="gasto">{{$mano->gasto_mensual}}</td>
                              <td>
                                {{$mano->fecha_creacion}}
                              </td>

                              <td>
                                  <a href="" data-target="#modal-eliminar-{{$mano->id_gastos_sueldos}}" data-toggle="modal">
                                      <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                              </td>
                          </tr>
                          @include('costos.indirectos.mano_obra.eliminar')
                          @include('costos.indirectos.mano_obra.vermas')
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <h4 id="costo_total">Costo Total de Mano de Obra Indirecta</h4>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
          var totales=$("td[class~='gasto']");
          var suma=0.00;
          for(var i=0;i<totales.length;i++)
          {
            var num=parseFloat(totales[i].innerText)
            suma=suma+num;
          }
          $("#costo_total").html("Costo Total de Mano de Obra Indirecta S/."+ suma.toFixed(6))
        });
    </script>
@endsection
