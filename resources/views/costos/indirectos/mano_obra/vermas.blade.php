<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$mano->id_gastos_sueldos}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$mano->puesto}}</h4>
			</div>
			<div class="modal-body">
        <h5>Área: {{$mano->descrip_area}}</h5>
        <h5>Sueldo Mensual en Planilla: S/.{{$mano->sueldo_mensual}}</h5>
        <h5>Beneficios Sociales: S/.{{$mano->beneficios}}</h5>
        <h5>Otros Sueldos Mensuales: S/.{{$mano->otros}}</h5>
        <h5>Gasto Mensual: S/.{{$mano->gasto_mensual}}</h5>
			</div>
		</div>
	</div>
</div>
