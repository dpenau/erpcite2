@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nuevo Material Administrativo</h2>
                <div class="clearfix"></div>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm/guardar_material','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="row" id="contenido">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Material:</h6>
                    <select required='required' id="cod_material"  name="material" class="custom-select">
                        <option value="" selected disabled>Seleccione</option>
                        @foreach ($material as $ar)
                            <option value="{{$ar->cod_material}}" >{{$ar->descrip_material}}</option>
                        @endforeach
                    </select>
                    <h6 class="my-3">Precio Unitario:</h6>
                    <input disabled type="text" id="precio"  class="form-control" >
                    <h6 class="my-3">Unidad de medida:</h6>
                    <input disabled type="text" id="medida"  class="form-control" >
                </div>
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Consumo:</h6>
                    <input onkeypress="return isNumberKey(event)" type="text" id="valor1" name="consumo" required="required" class="form-control" >
                    <h6 class="my-3">Meses de duracion:</h6>
                    <input onkeypress="return isNumberKey(event)" type="text" id="valor2" name="meses" required="required" class="form-control" >
                    <h6 class="my-3">Gasto Mensual:</h6>
                    <input  disabled class="form-control" id="gasto"  type="text" maxlength="70" >
                    <input type="text" name="gasto_mensual" style="display:none" id="gasto_mensual"  value="">
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <!--<button href="{{url('costos/indirectos/mano_obra/index')}}" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>-->
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
      $("#cod_material").change(function(){
        var codigo=$("#cod_material").val();
        @foreach($material as $mat)
          var cod="{{$mat->cod_material}}";
          if(codigo==cod)
          {
            $("#precio").val("{{$mat->costo_sin_igv_material}}");
            $("#medida").val("{{$mat->descrip_unidad_medida}}");
          }
        @endforeach
      })
      $("#valor2").focusout(function(){
        var precio=$("#precio").val();
        var consumo=$("#valor1").val();
        var meses=$("#valor2").val();
        var total=(precio*consumo)/meses
        $("#gasto").val(total.toFixed(6));
        $("#gasto_mensual").val(total.toFixed(6))
      })
    });
</script>
@endsection
