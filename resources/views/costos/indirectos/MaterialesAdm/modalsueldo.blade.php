<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$mano->id_gastos_sueldos}}">
{!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm/borrar_sueldo','method'=>'POST','autocomplete'=>'off'))!!}
		{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Sueldo Administrativo</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Eliminar el Sueldo Administrativo</p>
				<input type="text" style="display:none" name="email" value="{{$mano->id_gastos_sueldos}}">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-slant bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-slant bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
