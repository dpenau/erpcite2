@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nueva mano de Obra Administrativa</h2>
                <div class="clearfix"></div>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm/guardar_sueldo','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="row" id="contenido">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Área:</h6>
                    <select required='required' id="cod_area" name="cod_area" class="custom-select">
                        <option value="" selected disabled>Seleccione</option>
                        @foreach ($area as $ar)
                          @if($ar->cod_area=="36637")
                            <option value="{{$ar->cod_area}}" >{{$ar->descrip_area}}</option>
                          @endif
                        @endforeach
                    </select>
                    <h6 class="my-3">Puesto:</h6>
                    <textarea name="puesto" maxlength="500" name="puesto" class="form-control" rows="8" cols="80"></textarea>
                    <h6 class="my-3">Fecha de Creacion</h6>
                    <input type="date" class="form-control" name="fecha_creacion">
                </div>
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Sueldo Mensual en Planilla:</h6>
                        <input onkeypress="return isNumberKey(event)" type="text" maxlength="7" id="valor1" name="sueldo_mensual" required="required" class="form-control" >
                        @if($beneficios!="")
                        <input type="text" name="" id="beneficio" style="display:none" value="{{$beneficios}}">
                        @else
                        <input type="text" name="" id="beneficio" style="display:none" value="0">
                        @endif
                    <h6 class="my-3">Beneficios Sociales:</h6>
                    <input disabled type="text" maxlength="7" id="ben_soci"   class="form-control" >
                    <input style="display:none" type="text" id="valor2" name="beneficios_sociales"  value="">
                    <h6 class="my-3">Otros Sueldos Mensuales:</h6>
                    <input onkeypress="return isNumberKey(event)" type="text" maxlength="7" id="valor3" name="otros_sueldos" class="form-control" >

                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input  value="0" readonly required class="form-control" id="gasto_mensual1" name="gasto_mensual" type="text"maxlength="70" >
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <!--<button href="{{url('costos/indirectos/mano_obra/index')}}" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>-->
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
      $("#valor1").focusout(function(){
          var valor1=$("#valor1").val()
          var beneficio=$("#beneficio").val()
            var total=0.00;
            total=total+(parseFloat(valor1).toFixed(6)*(parseFloat(beneficio).toFixed(6)/100));
            $("#valor2").val(total.toFixed(6))
            $("#ben_soci").val(total.toFixed(6))
      });
      $("#valor3").focusout(function(){
          var valor1=$("#valor1").val()
          var valor2=$("#valor2").val()
          var valor3=$("#valor3").val()
          var beneficio=$("#beneficio").val()
            var total=0.00;
            total=total+(parseFloat(valor1)+parseFloat(valor2)+parseFloat(valor3));
            $("#gasto_mensual1").val(total.toFixed(6))
      });
    });
</script>
@endsection
