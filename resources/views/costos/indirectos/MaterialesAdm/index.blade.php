@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto Administrativos</h1>
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Sueldos Administrativos</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Materiales Administrativos</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Troqueles" role="tab" aria-controls="profile">Gastos de Representacion</a>
                    </div>
                    <div class="clearfix"></div>
            </div>


            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                <h2 class="d-inline ">Sueldos Administrativos</h2>
                <a href="MaterialesAdm/crear_sueldo">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Sueldo Administrativo</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example" class="display">
                    <thead>
                        <tr>
                          <th></th>
                        <th>Descripccion del puesto</th>
                        <th>Sueldo Mensual en planilla</th>
                        <th>Beneficio Social en planilla</th>
                        <th>Otros Sueldos Mensuales</th>
                        <th>Gasto Mensual</th>
                        <th>Fecha de Creacion</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                      <tbody>
                        @foreach($manoobra as $mano)
                        <tr>
                          <td>
                            @if($mano->estado==2)
                              <div class="bg-info" style="height:20px; width:10px"></div>
                            @endif
                          </td>
                          <td>{{$mano->puesto}}</td>
                          <td>{{$mano->sueldo_mensual}}</td>
                          <td>{{$mano->beneficios}}</td>
                          <td>{{$mano->otros}}</td>
                          <td class="pgasto">{{$mano->gasto_mensual}}</td>
                          <td>{{$mano->fecha_creacion}}</td>
                          <td>
                              <a href="" data-target="#modal-delete-{{$mano->id_gastos_sueldos}}" data-toggle="modal">
                                  <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                          </td>
                        </tr>
                        @include('costos.indirectos.MaterialesAdm.modalsueldo')
                        @endforeach
                      </tbody>
                    </table>
                    <h4 id="ctp">Costo Total de Sueldo Administrativo</h4>
                </div>
                </div>


                <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Materiales Administrativos</h2>

                <a href="MaterialesAdm/crear_material">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Materiales Administrativos</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example2" class="display">
                    <thead>
                        <tr>
                          <th></th>
                        <th>Descripcion del Gasto</th>
                        <th>Unidad de Medida</th>
                        <th>Valor Unitario</th>
                        <th>Consumo</th>
                        <th>Meses de Duracion</th>
                        <th>Gasto Mensual</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($materiales as $mat)
                        <tr>
                          <td>
                            @if($mat->estado_suministro==3 || $mat->estado_suministro==2)
                              <div class="bg-info" style="height:20px; width:10px"></div>
                            @endif
                            @if($mat->estado_suministro==0 || $mat->estado_suministro==2)
                              <div class="bg-warning" style="height:20px; width:10px"></div>
                            @endif
                          </td>
                          <td>{{$mat->descrip_material}}</td>
                          <td>{{$mat->descrip_unidad_medida}}</td>
                          <td>{{$mat->costo_sin_igv_material}}</td>
                          <td>{{$mat->consumo}}</td>
                          <td>{{$mat->meses_duracion}}</td>
                          <td class="hgasto">{{$mat->gasto_mensual_suministro}}</td>
                          <td>
                              <a href="" data-target="#modal-delete-{{$mat->cod_material}}" data-toggle="modal">
                                  <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                          </td>
                        </tr>
                        @include('costos.indirectos.MaterialesAdm.modal')
                      @endforeach
                    </tbody>
                    </table>
                    <h4 id="cth">Costo Total de Materiales Administrativos</h4>
                </div>
                </div>

            <div class="tab-pane fade" id="list-Troqueles" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Gastos de Representacion</h2>

                <a href="MaterialesAdm/crear_representacion">
                  <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Representacion</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example3" class="display">
                    <thead>
                        <tr>
                        <th>Descripcion del Gasto</th>
                        <th>Gasto</th>
                        <th>Gasto Mensual</th>
                        <th>Fecha de creacion</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($representacion as $rep)
                        <tr>
                          <td>{{$rep->descripcion}}</td>
                          <td>{{$rep->gasto}}</td>
                          <td class="tgasto">{{$rep->gasto}}</td>
                          <td>{{$rep->fecha_creacion}}</td>
                          <td>
                              <a href="" data-target="#modal-delete-{{$rep->cod_gasrepre}}" data-toggle="modal">
                                  <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                          </td>
                        </tr>
                        @include('costos.indirectos.MaterialesAdm.modalrepresentacion')
                      @endforeach
                    </tbody>
                    </table>
                    <h4 id="ctt">Costo Total de Representacion</h4>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      var ptotal=$("td[class~='pgasto']");
      var htotales=$("td[class~='hgasto']");
      var ttotales=$("td[class~='tgasto']");
      var suma=0.00;
      var sumah=0.00;
      var sumat=0.00;
      for(var i=0;i<ptotal.length;i++)
      {
        var num=parseFloat(ptotal[i].innerText)
        suma=suma+num;
      }
      for(var i=0;i<htotales.length;i++)
      {
        var num=parseFloat(htotales[i].innerText)
        sumah=sumah+num;
      }
      for(var i=0;i<ttotales.length;i++)
      {
        var num=parseFloat(ttotales[i].innerText)
        sumat=sumat+num;
      }
      $("#ctp").html("Costo Total de Sueldo Administrativo S/."+ suma.toFixed(6))
      $("#cth").html("Costo Total de Material Administrativo S/."+ sumah.toFixed(6))
      $("#ctt").html("Costo Total de Representacion S/."+ sumat.toFixed(6))
    </script>

@endsection
