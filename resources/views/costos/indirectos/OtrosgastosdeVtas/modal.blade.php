<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-eliminar-{{$mano->id_gastos_sueldos}}">
{!!Form::open(array('url'=>'costos_indirectos/OtrosgastosdeVtas/eliminar_sueldo','method'=>'POST','autocomplete'=>'off'))!!}
{{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Sueldo de Ventas y Marketing</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea eliminar</p>
        <input type="text" style="display:none" name="email" value="{{$mano->id_gastos_sueldos}}">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
