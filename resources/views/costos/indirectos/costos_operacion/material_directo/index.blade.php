<div class="pb-3">
    <h3 class="font-weight-bold">Material Directo
        <a href="" data-target="#modal-create-material" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar M. Indirectos y
                Suministros</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="material_directo" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Tipo de Material</th>
                <th>Material</th>
                <th>Unidad de Compra</th>
                <th>Costo Unitario</th>
                <th>Consumo</th>
                <th>Meses de Duración</th>
                <th>Costo Mensual</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($materiales_indirectos as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->tipo_material->descripcion }}</td>
                    <td>{{ $item->descripcion}}</td>
                    <td>{{ $item->unidad_compra_id }}</td>
                    <td>{{ $item->costo_unitario }}</td>
                    <td>{{ $item->consumo }}</td>
                    <td>{{ $item->meses_duracion }}</td>
                    <td>{{ $item->costo_mensual }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-material-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-eliminar-material-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
                @include('costos.indirectos.costos_operacion.material_directo.modaleditar')
                @include('costos.indirectos.costos_operacion.material_directo.modaleliminar')
            @endforeach
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.material_directo.modalcrear')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

        })
        let costoMensualUpdate = (event,ruta) => {
            console.log("#costo_unitario_"+ruta);
            let costo_unitario = $("#costo_unitario_"+ruta).val();
            let meses_duracion = $("#meses_duracion_"+ruta).val();
            let consumo = $("#consumo_"+ruta).val();
            console.log(costo_unitario)
            console.log(meses_duracion)
            console.log(consumo)
            let resultado = calculoCostoMensualUpdate(costo_unitario, meses_duracion, consumo);
            console.log(resultado)
            $("#costo_mensual_"+ruta).val(resultado);

        }

        let calculoCostoMensualUpdate = (cu, md, con) => {
            return (cu * (con / md));
        }

    </script>
</div>
