<div id="modal-editar-material-{{ $item->id }}" class="modal fade modal-slide-in-right" aria-hidden="true"
    role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('costos/indirectos/operacion/materialindirecto/update/' . $item->id) }}">
        <!-- CSRF Token -->
        @method('put')
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Material Indirecto y Suministro </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="proceso_id">Proceso:</label>
                            <select name="proceso_id" class="custom-select" required>
                                <option value="" selected disabled>Procesos</option>
                                <option value="{{ $item->proceso_id }}" selected>
                                    {{ $item->proceso->nombre }}</option>
                                @foreach ($procesos as $proceso)
                                    @if ($proceso->cod_proceso != $item->proceso_id)
                                        <option value="{{ $proceso->cod_proceso }}">
                                            {{ $proceso->nombre }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo_material_id">Tipo de Material</label>
                            <select name="tipo_material_id" class="custom-select" required>
                                <option value="" selected disabled>Tipo Material</option>
                                <option value="{{ $item->tipo_material_id }}" selected>
                                    {{ $item->tipo_material->descripcion }}</option>
                                @foreach ($tipo_materiales_indirectos as $tipo_material_indirecto)
                                    @if ($tipo_material_indirecto->id != $item->tipo_material_id)
                                        <option value="{{ $item->id }}">
                                            {{ $tipo_material_indirecto->descripcion }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="descripcion">Descripcion de Material:</label>
                            <input type="text" class="form-control" name="descripcion"
                                value="{{ $item->descripcion }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="unidad_compra_id">Unidad de Compra:</label>
                            <select name="unidad_compra_id" class="custom-select" required>
                                <option value="" selected disabled>Seleccione</option>
                                @foreach ($unidad_compras as $unidad_compra)
                                    <option value="{{ $unidad_compra->cod_unidad_medida }}">
                                        {{ $unidad_compra->descrip_unidad_medida }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="costo_unitario">Costo Unitario</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_unitario_{{ $item->id }}"
                                    name="costo_unitario" class="form-control" step="0.0001" min="0"
                                    value="{{ $item->costo_unitario }}"
                                    onchange="costoMensualUpdate(event,{{ $item->id }})" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="consumo">Consumo:</label>
                            <input type="number" id="consumo_{{ $item->id }}" class="form-control" name="consumo"
                                step="0.0001" min="0" value="{{ $item->consumo }}"
                                onchange="costoMensualUpdate(event,{{ $item->id }})" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="meses_duracion">Meses de Duración</label>
                            <div class=" input-group">
                                <input type="number" id="meses_duracion_{{ $item->id }}" name="meses_duracion"
                                    class="form-control" step="0.0001" min="0"
                                    value="{{ $item->meses_duracion }}"
                                    onchange="costoMensualUpdate(event,{{ $item->id }})" required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">mes</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="costo_mensual">Costo Mensual</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_mensual_{{ $item->id }}"
                                    name="costo_mensual" class="form-control" value="{{ $item->costo_mensual }}"
                                    readonly required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
