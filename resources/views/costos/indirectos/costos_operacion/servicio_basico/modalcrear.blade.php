<div id="modal-create-servicio" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">

        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Servicio Básico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="proceso">Proceso:</label>
                            <select id="proceso_id" name="proceso_id" class="custom-select" required>
                                <option value="" selected disabled>Procesos</option>
                                @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="descripcion">Descripcion del Servicio:</label>
                            <input type="text" id="descripcion" class="form-control" name="descripcion" value="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="costo_mensual">Costo Mensual:</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" id="costomensual" name="costo_mensual"
                                    class="form-control" step="0.0001" min="0" value="" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="crearServicioBasico" onclick="createServicioBasico()" class="bttn-unite bttn-md bttn-primary" data-dismiss="modal">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>

</div>
