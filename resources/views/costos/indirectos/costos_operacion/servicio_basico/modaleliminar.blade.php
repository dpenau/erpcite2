<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-eliminar-servicio">
    <!-- CSRF Token -->
    @method('delete')
    @csrf
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Servicio Básico</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea Eliminar el Servicio Basico</p>
            </div>
            <div class="modal-footer">
                <button type="submit" onclick="deleteServicioBasico()" class="bttn-unite bttn-md bttn-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>

</div>
