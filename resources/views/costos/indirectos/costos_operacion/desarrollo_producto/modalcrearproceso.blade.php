<div id="modal-create-desarrollo-proceso" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('costos/indirectos/operacion/desarrollo/procesocreate') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Proceso Productivo</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="proceso">Proceso:</label>
                            <select name="proceso_id" class="custom-select">
                                <option value="" selected disabled>Procesos</option>
                                @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="numero_prototipo">N° de Prototipos:</label>
                            <input type="text" class="form-control" name="numero_prototipo" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="descripcion_material">Descripcion del Proceso Productivo:</label>
                            <input type="text" class="form-control" name="descripcion_material" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="costo_total">Costo Total:</label>
                            <input type="text" class="form-control" name="costo_total" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="importe">Importe por:</label>
                            <input type="text" class="form-control" name="importe" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
