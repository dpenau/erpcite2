@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nuevo Material Indirecto o Suministro</h2>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
        <h6 class="my-3">Area:</h6>
        <select required='required' id="categoria"  class="custom-select">
            <option value="" selected disabled>Logistica</option>
            <option value="" ></option>
        </select>

        <h6 class="my-3">Descripcion del Gasto:</h6>
            <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">
        </select>

    </div>

    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
        <h6 class="my-3">Gasto Mensual:</h6>
            <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

    </div>
</div>
<div class="form-group my-5">
    <div class="col-md-12 col-sm-6 col-xs-12">
        <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
        <button type="submit" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>
    </div>
</div>

@endsection