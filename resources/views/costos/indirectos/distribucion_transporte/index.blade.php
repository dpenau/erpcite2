@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Distribucion y Transporte </h1>
                <button type="submit" class="bttn-slant bttn-md bttn-success" target="_blank" id="sub"><i class="fas fa-plus"></i></button>
                <button type="submit" class="bttn-slant bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Distribucion y Trnasporte</button>
                <div class="clearfix"></div>
            </div>

            <div class="x_content table-responsive">
                <table id="table_mp" class="table stacktable">
                  <thead>
                    <tr>
                      <th>Area</th>
                      <th>Descripcion de Gasto</th>
                      <th>Gasto Mensual</th>
                    </tr>
                  </thead>
                
                    <tbody>
                        <tr>
                            <td>
                                <a href="" >
                                <button class="bttn-slant bttn-md bttn-warning "><i class="fas fa-search"></i></button></a>
                            </td>
                            <td>
                                <a href="" >
                                <button class="bttn-slant bttn-md bttn-primary"><i class="fas fa-search"></i></button></a>
                            </td>
                            <td>
                                <a href="" >
                                <button class="bttn-slant bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                            </td>
                        </tr>
                    </tbody>                           
                </table>
                <h4>Costo Total de Mano de Obra Indirecta</h4>  
            </div>
        </div>
    </div>



@endsection