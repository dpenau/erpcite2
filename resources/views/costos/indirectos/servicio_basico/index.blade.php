@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Servicios Basicos </h1>
                <a href="servicio_basico/create">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Servicio Basico</button>
                </a>
                <div class="clearfix"></div>
            </div>

            <div class="x_content table-responsive">
                <table id="example" class="display">
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Descripcion del Servicio</th>
                            <th>Gasto Mensual</th>
                            <th>Fecha de Creacion</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($serviciobasico as $serviba)
                        <tr>
                            <td>{{$serviba->descrip_area}}</td>
                            <td>{{$serviba->descrip_servicio_basico}}</td>
                            <td class="mensual">{{$serviba->gasto_mensual}}</td>
                            <td>{{$serviba->fecha_creacion}} </td>
                            <td>
                                <a href="" data-target="#modal-eliminar-{{$serviba->cod_gasto_servicio_basico}}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                            </td>
                        </tr>
                        @include('costos.indirectos.servicio_basico.eliminar')
                        @endforeach
                    </tbody>
                </table>
                <h4 id="cdp">Costo Total de Servicos Basicos</h4>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
          var gasto=$("td[class~='mensual']");
          var sumadepreciacion=0.00;
          for(var i=0;i<gasto.length;i++)
          {
            var num=parseFloat(gasto[i].innerText)
            sumadepreciacion=sumadepreciacion+num;
          }
          $("#cdp").html("Costo Total de Servicos Basicos S/."+ sumadepreciacion.toFixed(6));
        });
    </script>


@endsection
