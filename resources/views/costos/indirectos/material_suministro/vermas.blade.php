<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$matsu->codigo_suministro}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$matsu->descrip_material}}</h4>
			</div>
			<div class="modal-body">
        <h5>Área: {{$matsu->descrip_area}}</h5>
        <h5>Unidad de Compra: {{$matsu->descrip_unidad_compra}}</h5>
        <h5>Valor Unitario: S/.{{$matsu->costo_sin_igv_material}}</h5>
        <h5>Consumo: {{$matsu->consumo}}</h5>
        <h5>Meses de Duración: {{$matsu->meses_duracion}} meses</h5>
        <h5>Gasto Mensual: S/.{{$matsu->gasto_mensual_suministro}}</h5>
			</div>
		</div>
	</div>
</div>
