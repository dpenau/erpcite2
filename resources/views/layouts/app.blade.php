<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ERP Cite</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/estilos.css')}}">
            <link rel="stylesheet" href="{{asset('css/loader.css')}}">
    <link rel="image_src" href="{{asset('/img/logo_cite_v2.2.png')}}">

      <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.css')}}">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark entrar2">
            <div class="container ">
                <a class="navbar-brand " href="{{ url('/') }}">
                    <img src="/img/logo_cite_v3.2.png" height="30" class="d-inline-block align-top"
                        alt="mdb logo">
                    ERP CITECCAL
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                      @guest
                      @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Mantenimiento</a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="{{url('Mantenimiento/configuracion')}}">
                               Configuracion
                           </a>
                             <a class="dropdown-item" href="{{url('Mantenimiento/area')}}">
                                 Area
                             </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Categoria')}}">
                                  Categoría
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Subcategoria')}}">
                                  Subcategoría
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Roles')}}">
                                  Roles
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Contribuyente')}}">
                                  Contribuyentes
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Regimen_Renta')}}">
                                  Régimen Renta
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Regimen_Laboral')}}">
                                  Régimen Laboral
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Tipo_Trabajador')}}">
                                  Tipo Trabajadores
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/Unidad_Medida')}}">
                                  Unidad de Medida
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/recuperar_clave')}}">
                                  Recuperar Contraseña
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/noticias')}}">
                                  Noticias
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/comentarios')}}">
                                  Comentarios
                              </a>
                              <a class="dropdown-item" href="{{url('Mantenimiento/contacto')}}">
                                  Contacto
                              </a>
                          </div>

                      </li>
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Indicadores</a>

                         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="{{url('Admin/indicador/operacion')}}">
                                Operaciones
                           </a>

                          </div>

                      </li>
                      @endguest
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest

                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Salir') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest

                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @include('flash-message')
            @yield('content')
        </main>
        <footer class="main-footer">
                    Copyright © Todos los Derechos Reservados<b> - Instituto Tecnologico de la Produccion</b>
                    <div class="pull-right hidden-xs">
                      <b>Version</b>
                      1.1.2
                    </div>
        </footer>
    </div>
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script type="text/javascript" language="javascript" src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('js/loader.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/validacion.js')}}"></script>
    <script type="text/javascript" language="javascript" class="init">
    $(document).ready(function() {
      $('#example').DataTable( {
      'columnDefs': [
           {
              'targets': 0,
              'checkboxes': {
                 'selectRow': true
              }
           }
        ],
          "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
          }
      } );
  } );
    </script>
</body>
</html>
