-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2019 a las 23:05:06
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cite_daniel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `descrip_almacen` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_almacen` tinyint(1) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `DNI_trabajador` varchar(13) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nom_almacen` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`descrip_almacen`, `estado_almacen`, `RUC_empresa`, `DNI_trabajador`, `nom_almacen`, `cod_almacen`) VALUES
('sad', 1, '15849643032', 'PRU-75765676', 'Materia Prima', 14011),
('Almacén solo de insumos', 1, '20134956777', '66631231', 'Almacén Insumos', 21853),
('Recepción, entrega e inspección de repuestos', 1, '20112233441', '07227131', 'Almacén Materiales Indirectos', 23858),
('Recepción, entrega e inspección de cueros y badanas', 1, '20112233441', '07227131', 'Almacén de Cueros', 41241),
('Aca se almacenaran solo los cueros', 1, '20134956777', '66631231', 'Almacen Cueros', 44005),
('Pisos', 1, '15849643032', 'PRU-75765676', 'Pisos', 71161),
('Recepción, entrega e inspección de pisos', 1, '20112233441', '18095992', 'Almacén de Pisos', 72104),
('Recepción, entrega e inspección de insumos de fabricación', 1, '20112233441', '07639874', 'Almacén de Insumos', 94137);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `descrip_area` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_area` tinyint(1) NOT NULL,
  `cod_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`descrip_area`, `estado_area`, `cod_area`) VALUES
('P-Acabado', 1, 11297),
('P-Aparado', 1, 27188),
('P-Corte', 1, 27405),
('P-Habilitado', 1, 30844),
('Administración', 1, 36637),
('Logística', 1, 47812),
('Ventas y Marketing', 1, 53586),
('Seguridad y Limpieza', 1, 63488),
('Marketing', 0, 70871),
('General', 1, 75359),
('P-Alistado', 1, 79770),
('P-Montaje', 1, 83205),
('Desarrollo de Producto', 1, 83307),
('Finanzas', 1, 84212),
('Producción', 1, 87883);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capellada`
--

CREATE TABLE `capellada` (
  `cod_capellada` int(11) NOT NULL,
  `descrip_capellada` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_capellada` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `capellada`
--

INSERT INTO `capellada` (`cod_capellada`, `descrip_capellada`, `estado_capellada`) VALUES
(1, 'Cuero', 1),
(2, 'Cuero con Recubrimiento', 1),
(3, 'Textiles Naturales y/o Sintéticos, tejidos o no', 1),
(4, 'Otros Materiales', 1),
(5, 'Combinado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_precios`
--

CREATE TABLE `catalogo_precios` (
  `cod_articulo` char(11) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` double(6,2) NOT NULL,
  `ultima_actualizacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usuario` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `catalogo_precios`
--

INSERT INTO `catalogo_precios` (`cod_articulo`, `precio`, `ultima_actualizacion`, `usuario`) VALUES
('PRU354420', 47.20, '2019-06-19 15:32:03', '1111'),
('PRU42252', 873.20, '2019-09-05 16:27:14', '1111'),
('PRU445864', 283.20, '2019-09-23 02:37:06', '1111'),
('PRU493392', 165.20, '2019-09-23 02:15:33', '1111'),
('PRU546844', 59.00, '2019-06-19 15:32:14', '1111'),
('PRU72225', 59.00, '2019-06-19 15:07:13', '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `nom_categoria` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `estado_categoria` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`nom_categoria`, `cod_categoria`, `estado_categoria`) VALUES
('Insumos', 306, 1),
('Suministros', 634, 1),
('Materias Primas', 969, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_noticias`
--

CREATE TABLE `categoria_noticias` (
  `cod_categoria` int(1) NOT NULL,
  `nombre_categoria` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria_noticias`
--

INSERT INTO `categoria_noticias` (`cod_categoria`, `nombre_categoria`) VALUES
(1, 'Capacitacion'),
(2, 'Conferencia'),
(3, 'Charlas'),
(4, 'Congresos'),
(5, 'Ferias'),
(6, 'Servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `cod_ciudad` char(6) COLLATE utf8_spanish2_ci NOT NULL,
  `nomb_ciudad` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`cod_ciudad`, `nomb_ciudad`) VALUES
('1', 'Arequipa'),
('2', 'Lima'),
('3', 'Trujillo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `codigo` char(10) COLLATE utf8_spanish2_ci NOT NULL,
  `documento` char(11) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `destino` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` char(9) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` char(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1',
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`codigo`, `documento`, `nombre`, `destino`, `telefono`, `direccion`, `estado`, `RUC_empresa`) VALUES
('PRU-35340', '19023460185', 'MIGUEL DIAZ AGUERO', 'PUNO', '054123455', 'AV. TORRES PARNO 567', '1', '15849643032'),
('PRU-52326', '10778844551', 'JOSE LOPEZ CORRALES', 'DESAGUADERO', '054445454', 'DESAGUADERO', '1', '15849643032');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coleccion`
--

CREATE TABLE `coleccion` (
  `codigo_coleccion` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre_coleccion` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_coleccion` char(1) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `coleccion`
--

INSERT INTO `coleccion` (`codigo_coleccion`, `nombre_coleccion`, `RUC_empresa`, `estado_coleccion`) VALUES
('PRU-19684', 'verano 2019', '15849643032', '1'),
('PRU-32144', 'verano 2020', '15849643032', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `codigo_comentario` char(6) COLLATE utf8_spanish2_ci NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `titulo` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `comentario` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_comentario` char(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`codigo_comentario`, `RUC_empresa`, `titulo`, `comentario`, `estado_comentario`) VALUES
('962232', '20112233441', 'Mejora del modulo de logistica', 'Aumentar el tamaño de ..', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion_valores`
--

CREATE TABLE `configuracion_valores` (
  `codigo_valor` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `valor` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `configuracion_valores`
--

INSERT INTO `configuracion_valores` (`codigo_valor`, `nombre`, `valor`) VALUES
(1, 'Dolar', 3.33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo_modelo`
--

CREATE TABLE `costo_modelo` (
  `cod_costo_modelo` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `total_materiales` double(10,6) NOT NULL DEFAULT 0.000000,
  `total_obra` double(10,6) NOT NULL DEFAULT 0.000000,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `modelo_serie` char(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `costo_modelo`
--

INSERT INTO `costo_modelo` (`cod_costo_modelo`, `RUC_empresa`, `total_materiales`, `total_obra`, `estado`, `modelo_serie`) VALUES
('145431', '15849643032', 79.600000, 61.643185, 1, 'PRU84819'),
('148895', '15849643032', 79.600000, 61.643185, 3, 'PRU493392'),
('360406', '15849643032', 0.000000, 0.000000, 3, 'PRU445864'),
('482562', '15849643032', 1.800000, 3.883614, 3, 'PRU42252'),
('772847', '15849643032', 1.350000, 1.469329, 1, 'PRU95556'),
('922345', '15849643032', 0.000000, 0.000000, 1, 'PRU60638');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_generales`
--

CREATE TABLE `datos_generales` (
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_retencion_utilidades` int(11) DEFAULT NULL,
  `TCEA_capital` double(5,2) DEFAULT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `porcentaje_reproceso` double(5,2) DEFAULT NULL,
  `TCEA_credito` decimal(5,2) DEFAULT NULL,
  `porcentaje_comision_ventas` double(5,2) DEFAULT NULL,
  `politica_desarrollo_producto` int(11) DEFAULT NULL,
  `politica_desarrollo_horma` int(11) DEFAULT NULL,
  `politica_desarrollo_troqueles` int(11) DEFAULT NULL,
  `produccion_promedio` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `datos_generales`
--

INSERT INTO `datos_generales` (`RUC_empresa`, `cod_retencion_utilidades`, `TCEA_capital`, `lead_time`, `porcentaje_reproceso`, `TCEA_credito`, `porcentaje_comision_ventas`, `politica_desarrollo_producto`, `politica_desarrollo_horma`, `politica_desarrollo_troqueles`, `produccion_promedio`) VALUES
('15849643032', 1, 12.00, 20, 0.50, '14.00', 0.20, 10000, 10000, 10000, 700),
('20112233441', 2, 17.00, 5, 5.00, '17.00', 2.00, 2000, 10000, 10000, 847);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_costo_modelo_manoobra`
--

CREATE TABLE `detalle_costo_modelo_manoobra` (
  `cod_costo_modelo` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_area` int(11) NOT NULL,
  `cod_tipo_trabajador` int(11) NOT NULL,
  `operacion` varchar(75) COLLATE utf8mb4_spanish_ci NOT NULL,
  `costo` double(10,6) NOT NULL,
  `beneficio` double(10,6) DEFAULT NULL,
  `costo_por_par` double(10,6) NOT NULL,
  `porcentaje` double(5,2) DEFAULT NULL,
  `estado_trabajador` int(1) NOT NULL DEFAULT 1,
  `cod_detalle_costo_mano` char(16) COLLATE utf8mb4_spanish_ci NOT NULL,
  `otros_costos` double(11,6) NOT NULL DEFAULT 0.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_costo_modelo_manoobra`
--

INSERT INTO `detalle_costo_modelo_manoobra` (`cod_costo_modelo`, `cod_area`, `cod_tipo_trabajador`, `operacion`, `costo`, `beneficio`, `costo_por_par`, `porcentaje`, `estado_trabajador`, `cod_detalle_costo_mano`, `otros_costos`) VALUES
('148895', 79770, 2, 'ewrrrr', 400.000000, 0.000000, 2.285714, NULL, 1, '222577', 0.000000),
('148895', 30844, 2, 'fhhgh', 200.000000, 0.000000, 1.142857, NULL, 1, '339546', 0.000000),
('148895', 27405, 1, 'corte', 900.000000, 118.530000, 1.455043, NULL, 1, '355002', 0.000000),
('148895', 83205, 1, 'oper4', 500.000000, 65.850000, 0.808357, NULL, 1, '447715', 0.000000),
('148895', 11297, 3, 'weyuuiii', 600.000000, 0.000000, 50.000000, NULL, 1, '615436', 0.000000),
('148895', 27405, 2, 'dos', 200.000000, 0.000000, 1.142857, NULL, 1, '655595', 0.000000),
('148895', 27188, 2, 'ghgh', 700.000000, 0.000000, 4.000000, NULL, 1, '860753', 0.000000),
('148895', 30844, 1, 'dfdf', 500.000000, 65.850000, 0.808357, NULL, 1, '971703', 0.000000),
('482562', 27405, 1, 'a', 900.000000, 118.530000, 1.597900, NULL, 1, 'PRU42252-424396', 100.000000),
('482562', 30844, 2, 'a', 200.000000, 0.000000, 1.142857, NULL, 1, 'PRU42252-470667', 0.000000),
('482562', 27188, 2, 's', 200.000000, 0.000000, 1.142857, NULL, 1, 'PRU42252-868444', 0.000000),
('145431', 27405, 1, 'corte', 900.000000, 118.530000, 1.455043, NULL, 1, 'PRU84819-118074', 0.000000),
('145431', 11297, 3, 'weyuuiii', 600.000000, 0.000000, 50.000000, NULL, 1, 'PRU84819-250446', 0.000000),
('145431', 30844, 2, 'fhhgh', 200.000000, 0.000000, 1.142857, NULL, 1, 'PRU84819-282484', 0.000000),
('145431', 30844, 1, 'dfdf', 500.000000, 65.850000, 0.808357, NULL, 1, 'PRU84819-318666', 0.000000),
('145431', 83205, 1, 'oper4', 500.000000, 65.850000, 0.808357, NULL, 1, 'PRU84819-516901', 0.000000),
('145431', 27405, 2, 'dos', 200.000000, 0.000000, 1.142857, NULL, 1, 'PRU84819-713944', 0.000000),
('145431', 79770, 2, 'ewrrrr', 400.000000, 0.000000, 2.285714, NULL, 1, 'PRU84819-754709', 0.000000),
('145431', 27188, 2, 'ghgh', 700.000000, 0.000000, 4.000000, NULL, 1, 'PRU84819-830654', 0.000000),
('772847', 27405, 1, 'as', 900.000000, 118.530000, 1.469329, NULL, 1, 'PRU95556-135380', 10.000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_costo_modelo_materiales`
--

CREATE TABLE `detalle_costo_modelo_materiales` (
  `cod_costo_modelo` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_area` int(11) NOT NULL,
  `consumo_por_par` double(10,6) NOT NULL,
  `valor_unitario` double(10,6) NOT NULL,
  `total` double(10,6) NOT NULL,
  `estado_material_costos` int(1) NOT NULL DEFAULT 1,
  `cod_detalle_costo_material` char(16) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_costo_modelo_materiales`
--

INSERT INTO `detalle_costo_modelo_materiales` (`cod_costo_modelo`, `cod_material`, `cod_area`, `consumo_por_par`, `valor_unitario`, `total`, `estado_material_costos`, `cod_detalle_costo_material`) VALUES
('148895', 'PRU720158', 27405, 4.000000, 16.650000, 0.800000, 1, '190409'),
('148895', 'PRU720158', 83205, 1.000000, 16.650000, 0.200000, 1, '197137'),
('148895', 'PRU6251418', 27405, 2.000000, 11.000000, 22.000000, 1, '322984'),
('148895', 'PRU720158', 30844, 2.000000, 16.650000, 0.400000, 1, '323966'),
('148895', 'PRU720158', 79770, 7.000000, 16.650000, 1.400000, 1, '587531'),
('148895', 'PRU971659', 27405, 5.000000, 10.252520, 45.000000, 1, '593320'),
('148895', 'PRU720158', 27188, 4.000000, 16.650000, 0.800000, 1, '662732'),
('148895', 'PRU512239', 27188, 10.000000, 10.000000, 8.000000, 1, '778822'),
('148895', 'PRU720158', 11297, 5.000000, 16.650000, 1.000000, 1, '808129'),
('482562', 'PRU971659', 27405, 0.200000, 9.000000, 1.800000, 1, 'PRU42252-668974'),
('145431', 'PRU720158', 27405, 4.000000, 16.650000, 0.800000, 1, 'PRU84819-234318'),
('145431', 'PRU720158', 27188, 4.000000, 16.650000, 0.800000, 1, 'PRU84819-402100'),
('145431', 'PRU720158', 83205, 1.000000, 16.650000, 0.200000, 1, 'PRU84819-655304'),
('145431', 'PRU720158', 30844, 2.000000, 16.650000, 0.400000, 1, 'PRU84819-731426'),
('145431', 'PRU971659', 27405, 5.000000, 10.252520, 45.000000, 1, 'PRU84819-733626'),
('145431', 'PRU720158', 79770, 7.000000, 16.650000, 1.400000, 1, 'PRU84819-749869'),
('145431', 'PRU6251418', 27405, 2.000000, 11.000000, 22.000000, 1, 'PRU84819-869258'),
('145431', 'PRU512239', 27188, 10.000000, 10.000000, 8.000000, 1, 'PRU84819-963127'),
('145431', 'PRU720158', 11297, 5.000000, 16.650000, 1.000000, 1, 'PRU84819-979614'),
('772847', 'PRU971659', 27405, 0.150000, 9.627129, 1.350000, 1, 'PRU95556-651964');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_kardex_material`
--

CREATE TABLE `detalle_kardex_material` (
  `id_detalle` int(11) NOT NULL,
  `fecha_ingreso` timestamp NULL DEFAULT NULL,
  `fecha_salida` timestamp NULL DEFAULT NULL,
  `fecha_devolucion` timestamp NULL DEFAULT NULL,
  `stock` double(10,2) NOT NULL,
  `cantidad_ingresada` double(10,2) DEFAULT NULL,
  `cantidad_salida` double(10,2) DEFAULT NULL,
  `trasladador_material` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_kardex_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `comentario_devolucion` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cod_area` int(11) NOT NULL,
  `estado_detalle_kardex` char(1) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '0',
  `restante_detalle` double(10,2) NOT NULL DEFAULT 0.00,
  `costo_material` double(9,6) NOT NULL,
  `dni_usuario` char(10) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_kardex_material`
--

INSERT INTO `detalle_kardex_material` (`id_detalle`, `fecha_ingreso`, `fecha_salida`, `fecha_devolucion`, `stock`, `cantidad_ingresada`, `cantidad_salida`, `trasladador_material`, `cod_kardex_material`, `comentario_devolucion`, `cod_area`, `estado_detalle_kardex`, `restante_detalle`, `costo_material`, `dni_usuario`) VALUES
(1, '2019-04-26 14:24:57', NULL, NULL, 5.00, 5.00, NULL, 'pedro', 'CIT428538', NULL, 47812, '0', 0.00, 15.500000, '1111'),
(2, '2019-04-26 14:44:32', NULL, NULL, 7.00, 2.00, NULL, 'Pedro', 'CIT428538', NULL, 47812, '0', 0.00, 15.500000, '1111'),
(3, '2019-04-26 14:44:52', NULL, NULL, 17.00, 10.00, NULL, 'jose', 'CIT428538', NULL, 47812, '0', 0.00, 20.520000, '1111'),
(4, '2019-04-26 14:45:51', NULL, NULL, 20.00, 3.00, NULL, 'Pedro', 'CIT428538', NULL, 47812, '0', 0.00, 15.500000, '1111'),
(5, '2019-04-26 14:49:55', NULL, NULL, 5.00, 5.00, NULL, 'Jose', 'CIT3908422', NULL, 47812, '1', 5.00, 2.750000, '1111'),
(6, '2019-04-26 14:49:55', NULL, NULL, 5.00, 5.00, NULL, 'Jose', 'CIT9072887', NULL, 47812, '1', 5.00, 2.750000, '1111'),
(14, NULL, '2019-05-06 19:34:13', NULL, 15.00, NULL, 5.00, 'pedro', 'CIT428538', '48074', 27405, '0', 0.00, 15.500000, '1111'),
(15, NULL, '2019-05-06 19:34:13', NULL, 13.00, NULL, 2.00, 'pedro', 'CIT428538', '48074', 27405, '0', 0.00, 15.500000, '1111'),
(16, NULL, '2019-05-06 19:34:13', NULL, 12.00, NULL, 1.00, 'pedro', 'CIT428538', '48074', 27405, '0', 0.00, 20.520000, '1111'),
(17, '2019-05-06 19:36:59', NULL, NULL, 10.00, 10.00, NULL, 'pedro', '611022', NULL, 47812, '1', 10.00, 8.500000, '1111'),
(22, NULL, '2019-05-06 21:57:44', NULL, 3.00, NULL, 9.00, 'pedro', 'CIT428538', '94224', 27405, '0', 0.00, 20.520000, '1111'),
(23, NULL, '2019-05-06 21:57:44', NULL, 2.00, NULL, 1.00, 'pedro', 'CIT428538', '94224', 27405, '0', 0.00, 15.500000, '1111'),
(24, '2019-05-06 21:58:21', NULL, NULL, 502.00, 500.00, NULL, 'pedro', 'CIT428538', NULL, 47812, '1', 497.00, 15.500000, '1111'),
(27, NULL, '2019-05-06 22:02:08', NULL, 500.00, NULL, 2.00, 'pedro', 'CIT428538', '57440', 27405, '0', 0.00, 15.500000, '1111'),
(28, NULL, '2019-05-06 22:02:08', NULL, 497.00, NULL, 3.00, 'pedro', 'CIT428538', '57440', 27405, '0', 0.00, 15.500000, '1111'),
(29, '2019-05-06 22:19:54', NULL, NULL, 25.00, 25.00, NULL, 'pedro', '7249312', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(30, '2019-05-06 22:19:54', NULL, NULL, 25.00, 25.00, NULL, 'pedro', '7919626', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(31, '2019-05-06 22:19:54', NULL, NULL, 25.00, 25.00, NULL, 'pedro', '5997853', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(32, '2019-05-06 22:19:54', NULL, NULL, 25.00, 25.00, NULL, 'pedro', '3211700', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(33, '2019-05-06 22:19:54', NULL, NULL, 25.00, 25.00, NULL, 'pedro', '4465557', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(34, '2019-05-06 22:22:00', NULL, NULL, 50.00, 25.00, NULL, 'pedro', '7249312', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(35, '2019-05-06 22:22:00', NULL, NULL, 50.00, 25.00, NULL, 'pedro', '7919626', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(36, '2019-05-06 22:22:00', NULL, NULL, 50.00, 25.00, NULL, 'pedro', '5997853', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(37, '2019-05-06 22:22:00', NULL, NULL, 50.00, 25.00, NULL, 'pedro', '3211700', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(38, '2019-05-06 22:22:00', NULL, NULL, 50.00, 25.00, NULL, 'pedro', '4465557', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(39, NULL, '2019-05-06 22:22:18', NULL, 25.00, NULL, 25.00, 'pedro', '4465557', '26267', 83205, '0', 0.00, 15.000000, '1111'),
(40, NULL, '2019-05-06 22:22:18', NULL, 20.00, NULL, 5.00, 'pedro', '4465557', '26267', 83205, '0', 0.00, 15.000000, '1111'),
(41, NULL, '2019-05-06 22:23:53', NULL, 25.00, NULL, 25.00, 'pdro', '7249312', '42559', 83205, '0', 0.00, 15.000000, '1111'),
(42, NULL, '2019-05-06 22:23:53', NULL, 25.00, NULL, 25.00, 'pdro', '7919626', '42559', 83205, '0', 0.00, 15.000000, '1111'),
(43, NULL, '2019-05-06 22:23:53', NULL, 25.00, NULL, 25.00, 'pdro', '5997853', '42559', 83205, '0', 0.00, 15.000000, '1111'),
(44, NULL, '2019-05-06 22:23:53', NULL, 25.00, NULL, 25.00, 'pdro', '3211700', '42559', 83205, '0', 0.00, 15.000000, '1111'),
(45, NULL, '2019-05-06 22:24:24', NULL, 20.00, NULL, 5.00, 'pedro', '7249312', '40771', 83205, '0', 0.00, 15.000000, '1111'),
(46, NULL, '2019-05-06 22:24:24', NULL, 20.00, NULL, 5.00, 'pedro', '7919626', '40771', 83205, '0', 0.00, 15.000000, '1111'),
(47, NULL, '2019-05-06 22:24:24', NULL, 20.00, NULL, 5.00, 'pedro', '5997853', '40771', 83205, '0', 0.00, 15.000000, '1111'),
(48, NULL, '2019-05-06 22:24:24', NULL, 20.00, NULL, 5.00, 'pedro', '3211700', '40771', 83205, '0', 0.00, 15.000000, '1111'),
(49, '2019-05-06 22:24:53', NULL, NULL, 45.00, 25.00, NULL, 'jose', '7249312', NULL, 47812, '1', 15.00, 15.000000, '1111'),
(50, '2019-05-06 22:24:53', NULL, NULL, 45.00, 25.00, NULL, 'jose', '7919626', NULL, 47812, '1', 15.00, 15.000000, '1111'),
(51, '2019-05-06 22:24:53', NULL, NULL, 45.00, 25.00, NULL, 'jose', '5997853', NULL, 47812, '1', 15.00, 15.000000, '1111'),
(52, '2019-05-06 22:24:53', NULL, NULL, 45.00, 25.00, NULL, 'jose', '3211700', NULL, 47812, '1', 15.00, 15.000000, '1111'),
(53, '2019-05-06 22:24:53', NULL, NULL, 45.00, 25.00, NULL, 'jose', '4465557', NULL, 47812, '1', 15.00, 15.000000, '1111'),
(54, '2019-05-06 22:26:16', NULL, NULL, 70.00, 25.00, NULL, 'juan', '7249312', NULL, 47812, '1', 25.00, 15.000000, '1111'),
(55, '2019-05-06 22:26:16', NULL, NULL, 70.00, 25.00, NULL, 'juan', '7919626', NULL, 47812, '1', 25.00, 15.000000, '1111'),
(56, '2019-05-06 22:26:16', NULL, NULL, 70.00, 25.00, NULL, 'juan', '5997853', NULL, 47812, '1', 25.00, 15.000000, '1111'),
(57, '2019-05-06 22:26:16', NULL, NULL, 70.00, 25.00, NULL, 'juan', '3211700', NULL, 47812, '1', 25.00, 15.000000, '1111'),
(58, '2019-05-06 22:26:16', NULL, NULL, 70.00, 25.00, NULL, 'juan', '4465557', NULL, 47812, '1', 25.00, 15.000000, '1111'),
(59, NULL, '2019-05-06 22:27:02', NULL, 50.00, NULL, 20.00, 'jose', '7249312', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(60, NULL, '2019-05-06 22:27:02', NULL, 40.00, NULL, 10.00, 'jose', '7249312', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(61, NULL, '2019-05-06 22:27:02', NULL, 50.00, NULL, 20.00, 'jose', '7919626', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(62, NULL, '2019-05-06 22:27:02', NULL, 40.00, NULL, 10.00, 'jose', '7919626', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(63, NULL, '2019-05-06 22:27:02', NULL, 50.00, NULL, 20.00, 'jose', '5997853', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(64, NULL, '2019-05-06 22:27:02', NULL, 40.00, NULL, 10.00, 'jose', '5997853', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(65, NULL, '2019-05-06 22:27:02', NULL, 50.00, NULL, 20.00, 'jose', '3211700', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(66, NULL, '2019-05-06 22:27:02', NULL, 40.00, NULL, 10.00, 'jose', '3211700', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(67, NULL, '2019-05-06 22:27:02', NULL, 50.00, NULL, 20.00, 'jose', '4465557', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(68, NULL, '2019-05-06 22:27:02', NULL, 40.00, NULL, 10.00, 'jose', '4465557', '37181', 83205, '0', 0.00, 15.000000, '1111'),
(69, NULL, NULL, '2019-05-07 13:38:54', 500.00, 3.00, NULL, 'Pedro', 'CIT428538', 'sobrante', 47812, '1', 3.00, 15.500000, '1111'),
(70, '2019-05-14 14:53:18', NULL, NULL, 100.00, 100.00, NULL, 'pedro', 'PRU971659', NULL, 47812, '1', 6.00, 7.630000, '1111'),
(71, '2019-05-30 19:14:45', NULL, NULL, 20.00, 20.00, NULL, 'asd', '6585644', NULL, 47812, '1', 20.00, 12.000000, '1111'),
(72, '2019-05-30 19:14:45', NULL, NULL, 20.00, 20.00, NULL, 'asd', '9217658', NULL, 47812, '1', 20.00, 12.000000, '1111'),
(73, '2019-05-30 19:14:57', NULL, NULL, 20.00, 20.00, NULL, 'ads', '9193584', NULL, 47812, '1', 20.00, 12.000000, '1111'),
(74, '2019-05-30 19:14:57', NULL, NULL, 40.00, 20.00, NULL, 'ads', '6585644', NULL, 47812, '1', 20.00, 12.000000, '1111'),
(75, '2019-05-30 19:15:09', NULL, NULL, 30.00, 10.00, NULL, 'asd', '9217658', NULL, 47812, '1', 10.00, 12.000000, '1111'),
(76, '2019-05-30 19:15:09', NULL, NULL, 10.00, 10.00, NULL, 'asd', '4718216', NULL, 47812, '1', 10.00, 12.000000, '1111'),
(77, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU4393533', NULL, 47812, '1', 37.00, 12.000000, '1111'),
(78, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU1691607', NULL, 47812, '1', 38.00, 12.000000, '1111'),
(79, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU4826616', NULL, 47812, '1', 39.00, 12.000000, '1111'),
(80, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU8409456', NULL, 47812, '1', 20.00, 12.000000, '1111'),
(81, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU7794082', NULL, 47812, '1', 41.00, 12.000000, '1111'),
(82, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU6934667', NULL, 47812, '1', 42.00, 12.000000, '1111'),
(83, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU1966258', NULL, 47812, '1', 43.00, 12.000000, '1111'),
(84, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU7545757', NULL, 47812, '1', 44.00, 12.000000, '1111'),
(85, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU2971377', NULL, 47812, '1', 45.00, 12.000000, '1111'),
(86, '2019-05-30 20:51:57', NULL, NULL, 100.00, 100.00, NULL, 'pedrito', 'PRU1317125', NULL, 47812, '1', 46.00, 12.000000, '1111'),
(87, NULL, '2019-05-30 21:30:49', NULL, 99.00, NULL, 1.00, 'JOSE', 'PRU4393533', '54958', 83205, '0', 0.00, 12.000000, '1111'),
(88, NULL, '2019-05-30 21:30:49', NULL, 98.00, NULL, 2.00, 'JOSE', 'PRU1691607', '27778', 83205, '0', 0.00, 12.000000, '1111'),
(89, NULL, '2019-05-30 21:30:49', NULL, 97.00, NULL, 3.00, 'JOSE', 'PRU4826616', '47325', 83205, '0', 0.00, 12.000000, '1111'),
(90, NULL, '2019-05-30 21:30:49', NULL, 96.00, NULL, 4.00, 'JOSE', 'PRU8409456', '57088', 83205, '0', 0.00, 12.000000, '1111'),
(91, NULL, '2019-05-30 21:30:49', NULL, 95.00, NULL, 5.00, 'JOSE', 'PRU7794082', '15849', 83205, '0', 0.00, 12.000000, '1111'),
(92, NULL, '2019-05-30 21:30:49', NULL, 94.00, NULL, 6.00, 'JOSE', 'PRU6934667', '12181', 83205, '0', 0.00, 12.000000, '1111'),
(93, NULL, '2019-05-30 21:30:49', NULL, 93.00, NULL, 7.00, 'JOSE', 'PRU1966258', '60240', 83205, '0', 0.00, 12.000000, '1111'),
(94, NULL, '2019-05-30 21:30:49', NULL, 92.00, NULL, 8.00, 'JOSE', 'PRU7545757', '33808', 83205, '0', 0.00, 12.000000, '1111'),
(95, NULL, '2019-05-30 21:30:49', NULL, 91.00, NULL, 9.00, 'JOSE', 'PRU2971377', '49546', 83205, '0', 0.00, 12.000000, '1111'),
(96, NULL, '2019-05-30 21:30:49', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU1317125', '84945', 83205, '0', 0.00, 12.000000, '1111'),
(97, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 9.00, 'JOSE', 'PRU4393533', '79354', 83205, '0', 0.00, 12.000000, '1111'),
(98, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 8.00, 'JOSE', 'PRU1691607', '64568', 83205, '0', 0.00, 12.000000, '1111'),
(99, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 7.00, 'JOSE', 'PRU4826616', '49073', 83205, '0', 0.00, 12.000000, '1111'),
(100, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 6.00, 'JOSE', 'PRU8409456', '58267', 83205, '0', 0.00, 12.000000, '1111'),
(101, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 5.00, 'JOSE', 'PRU7794082', '37051', 83205, '0', 0.00, 12.000000, '1111'),
(102, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 4.00, 'JOSE', 'PRU6934667', '36608', 83205, '0', 0.00, 12.000000, '1111'),
(103, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 3.00, 'JOSE', 'PRU1966258', '96550', 83205, '0', 0.00, 12.000000, '1111'),
(104, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 2.00, 'JOSE', 'PRU7545757', '24522', 83205, '0', 0.00, 12.000000, '1111'),
(105, NULL, '2019-05-30 21:37:48', NULL, 90.00, NULL, 1.00, 'JOSE', 'PRU2971377', '35647', 83205, '0', 0.00, 12.000000, '1111'),
(106, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU4393533', '56167', 83205, '0', 0.00, 12.000000, '1111'),
(107, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU1691607', '88482', 83205, '0', 0.00, 12.000000, '1111'),
(108, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU4826616', '96377', 83205, '0', 0.00, 12.000000, '1111'),
(109, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU8409456', '96781', 83205, '0', 0.00, 12.000000, '1111'),
(110, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU7794082', '91318', 83205, '0', 0.00, 12.000000, '1111'),
(111, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU6934667', '20382', 83205, '0', 0.00, 12.000000, '1111'),
(112, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU1966258', '51630', 83205, '0', 0.00, 12.000000, '1111'),
(113, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU7545757', '38910', 83205, '0', 0.00, 12.000000, '1111'),
(114, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU2971377', '60703', 83205, '0', 0.00, 12.000000, '1111'),
(115, NULL, '2019-05-31 13:28:12', NULL, 80.00, NULL, 10.00, 'JOSE', 'PRU1317125', '93585', 83205, '0', 0.00, 12.000000, '1111'),
(116, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU8527333', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(117, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU5202290', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(118, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU9289152', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(119, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU9637844', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(120, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU8451945', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(121, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU1627293', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(122, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU2464518', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(123, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU8303033', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(124, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU1765526', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(125, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU3522135', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(126, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU3102621', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(127, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU6705771', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(128, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU8323795', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(129, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU8142419', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(130, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU7618406', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(131, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU4123941', NULL, 47812, '0', 0.00, 15.000000, '1111'),
(132, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU6855268', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(133, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU3958481', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(134, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU1610155', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(135, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU1227927', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(136, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU1927090', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(137, '2019-05-31 13:30:49', NULL, NULL, 10.00, 10.00, NULL, 'pedro', 'PRU2014292', NULL, 47812, '0', 0.00, 14.000000, '1111'),
(138, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU8527333', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(139, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU5202290', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(140, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU9289152', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(141, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU9637844', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(142, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU8451945', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(143, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU1627293', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(144, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU2464518', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(145, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU8303033', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(146, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU1765526', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(147, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU3522135', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(148, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU3102621', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(149, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU6705771', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(150, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU8323795', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(151, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU8142419', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(152, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU7618406', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(153, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU4123941', NULL, 47812, '1', 42.00, 15.000000, '1111'),
(154, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU6855268', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(155, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU3958481', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(156, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU1610155', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(157, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU1227927', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(158, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU1927090', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(159, '2019-05-31 13:31:17', NULL, NULL, 100.00, 90.00, NULL, 'pedrito', 'PRU2014292', NULL, 47812, '1', 42.00, 14.000000, '1111'),
(160, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU4393533', '53697', 83205, '0', 0.00, 12.000000, '1111'),
(161, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU1691607', '69789', 83205, '0', 0.00, 12.000000, '1111'),
(162, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU4826616', '51337', 83205, '0', 0.00, 12.000000, '1111'),
(163, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU8409456', '43527', 83205, '0', 0.00, 12.000000, '1111'),
(164, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU7794082', '93774', 83205, '0', 0.00, 12.000000, '1111'),
(165, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU6934667', '63790', 83205, '0', 0.00, 12.000000, '1111'),
(166, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU1966258', '86064', 83205, '0', 0.00, 12.000000, '1111'),
(167, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU7545757', '55379', 83205, '0', 0.00, 12.000000, '1111'),
(168, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU2971377', '43472', 83205, '0', 0.00, 12.000000, '1111'),
(169, NULL, '2019-05-31 13:33:42', NULL, 70.00, NULL, 10.00, 'JOSE', 'PRU1317125', '90658', 83205, '0', 0.00, 12.000000, '1111'),
(170, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU6855268', '98324', 83205, '0', 0.00, 14.000000, '1111'),
(171, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU6855268', '98324', 83205, '0', 0.00, 14.000000, '1111'),
(172, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU3958481', '48534', 83205, '0', 0.00, 14.000000, '1111'),
(173, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU3958481', '48534', 83205, '0', 0.00, 14.000000, '1111'),
(174, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU1610155', '29420', 83205, '0', 0.00, 14.000000, '1111'),
(175, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU1610155', '29420', 83205, '0', 0.00, 14.000000, '1111'),
(176, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU1227927', '99248', 83205, '0', 0.00, 14.000000, '1111'),
(177, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU1227927', '99248', 83205, '0', 0.00, 14.000000, '1111'),
(178, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU1927090', '17717', 83205, '0', 0.00, 14.000000, '1111'),
(179, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU1927090', '17717', 83205, '0', 0.00, 14.000000, '1111'),
(180, NULL, '2019-05-31 13:33:43', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2014292', '83734', 83205, '0', 0.00, 14.000000, '1111'),
(181, NULL, '2019-05-31 13:33:43', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2014292', '83734', 83205, '0', 0.00, 14.000000, '1111'),
(182, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8527333', '82413', 83205, '0', 0.00, 15.000000, '1111'),
(183, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8527333', '82413', 83205, '0', 0.00, 15.000000, '1111'),
(184, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU5202290', '34692', 83205, '0', 0.00, 15.000000, '1111'),
(185, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU5202290', '34692', 83205, '0', 0.00, 15.000000, '1111'),
(186, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU9289152', '40625', 83205, '0', 0.00, 15.000000, '1111'),
(187, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU9289152', '40625', 83205, '0', 0.00, 15.000000, '1111'),
(188, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU9637844', '63664', 83205, '0', 0.00, 15.000000, '1111'),
(189, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU9637844', '63664', 83205, '0', 0.00, 15.000000, '1111'),
(190, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8451945', '87140', 83205, '0', 0.00, 15.000000, '1111'),
(191, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8451945', '87140', 83205, '0', 0.00, 15.000000, '1111'),
(192, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU1627293', '42083', 83205, '0', 0.00, 15.000000, '1111'),
(193, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU1627293', '42083', 83205, '0', 0.00, 15.000000, '1111'),
(194, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU2464518', '80231', 83205, '0', 0.00, 15.000000, '1111'),
(195, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU2464518', '80231', 83205, '0', 0.00, 15.000000, '1111'),
(196, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8303033', '48564', 83205, '0', 0.00, 15.000000, '1111'),
(197, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8303033', '48564', 83205, '0', 0.00, 15.000000, '1111'),
(198, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU1765526', '40654', 83205, '0', 0.00, 15.000000, '1111'),
(199, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU1765526', '40654', 83205, '0', 0.00, 15.000000, '1111'),
(200, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU3522135', '41368', 83205, '0', 0.00, 15.000000, '1111'),
(201, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU3522135', '41368', 83205, '0', 0.00, 15.000000, '1111'),
(202, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU3102621', '31073', 83205, '0', 0.00, 15.000000, '1111'),
(203, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU3102621', '31073', 83205, '0', 0.00, 15.000000, '1111'),
(204, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU6705771', '38709', 83205, '0', 0.00, 15.000000, '1111'),
(205, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU6705771', '38709', 83205, '0', 0.00, 15.000000, '1111'),
(206, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8323795', '39612', 83205, '0', 0.00, 15.000000, '1111'),
(207, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8323795', '39612', 83205, '0', 0.00, 15.000000, '1111'),
(208, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8142419', '80064', 83205, '0', 0.00, 15.000000, '1111'),
(209, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8142419', '80064', 83205, '0', 0.00, 15.000000, '1111'),
(210, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU7618406', '88860', 83205, '0', 0.00, 15.000000, '1111'),
(211, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU7618406', '88860', 83205, '0', 0.00, 15.000000, '1111'),
(212, NULL, '2019-05-31 13:33:44', NULL, 90.00, NULL, 10.00, 'luis', 'PRU4123941', '59522', 83205, '0', 0.00, 15.000000, '1111'),
(213, NULL, '2019-05-31 13:33:44', NULL, 85.00, NULL, 5.00, 'luis', 'PRU4123941', '59522', 83205, '0', 0.00, 15.000000, '1111'),
(214, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU7295906', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(215, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU4025245', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(216, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU8071531', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(217, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU4645427', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(218, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU4994313', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(219, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU9944152', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(220, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU5898679', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(221, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU4860669', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(222, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6502475', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(223, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU9595592', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(224, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU9086300', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(225, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU2912069', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(226, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU1796790', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(227, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU8053640', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(228, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU4753172', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(229, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'JOSE', 'PRU5405157', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(230, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU3189337', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(231, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU2146598', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(232, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU1221862', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(233, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU8085569', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(234, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6064836', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(235, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU2705300', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(236, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU2715477', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(237, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU2553449', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(238, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU2780448', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(239, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU3943870', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(240, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU8090177', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(241, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU9973923', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(242, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6583044', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(243, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU5022807', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(244, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU9772816', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(245, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6597426', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(246, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU8866631', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(247, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6755509', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(248, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6573249', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(249, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU4710897', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(250, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU6567800', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(251, '2019-05-31 13:41:58', NULL, NULL, 10.00, 10.00, NULL, 'PEDRO', 'PRU5988061', NULL, 47812, '0', 0.00, 12.000000, '1111'),
(252, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU7295906', NULL, 47812, '1', 34.00, 12.000000, '1111'),
(253, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU4025245', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(254, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU8071531', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(255, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU4645427', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(256, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU4994313', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(257, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU9944152', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(258, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU5898679', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(259, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU4860669', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(260, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU6502475', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(261, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU9595592', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(262, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU9086300', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(263, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU2912069', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(264, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU1796790', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(265, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU8053640', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(266, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU4753172', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(267, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'PEDRO', 'PRU5405157', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(268, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU3189337', NULL, 47812, '1', 56.00, 12.000000, '1111'),
(269, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU2146598', NULL, 47812, '1', 56.00, 12.000000, '1111'),
(270, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU1221862', NULL, 47812, '1', 56.00, 12.000000, '1111'),
(271, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU8085569', NULL, 47812, '1', 56.00, 12.000000, '1111'),
(272, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU6064836', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(273, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU2705300', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(274, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU2715477', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(275, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU2553449', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(276, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU2780448', NULL, 47812, '1', 56.00, 12.000000, '1111'),
(277, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU3943870', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(278, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU8090177', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(279, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU9973923', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(280, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU6583044', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(281, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU5022807', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(282, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU9772816', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(283, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'LUIS', 'PRU6597426', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(284, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU8866631', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(285, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU6755509', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(286, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU6573249', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(287, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU4710897', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(288, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU6567800', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(289, '2019-05-31 13:42:53', NULL, NULL, 100.00, 90.00, NULL, 'JOSE', 'PRU5988061', NULL, 47812, '1', 57.00, 12.000000, '1111'),
(290, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU4393533', '25988', 83205, '0', 0.00, 12.000000, '1111'),
(291, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU1691607', '99858', 83205, '0', 0.00, 12.000000, '1111'),
(292, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU4826616', '84747', 83205, '0', 0.00, 12.000000, '1111'),
(293, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU8409456', '67623', 83205, '0', 0.00, 12.000000, '1111'),
(294, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU7794082', '81493', 83205, '0', 0.00, 12.000000, '1111'),
(295, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU6934667', '51504', 83205, '0', 0.00, 12.000000, '1111'),
(296, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU1966258', '75473', 83205, '0', 0.00, 12.000000, '1111'),
(297, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU7545757', '87215', 83205, '0', 0.00, 12.000000, '1111'),
(298, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU2971377', '63454', 83205, '0', 0.00, 12.000000, '1111'),
(299, NULL, '2019-05-31 13:45:18', NULL, 60.00, NULL, 10.00, 'JOSE', 'PRU1317125', '10046', 83205, '0', 0.00, 12.000000, '1111'),
(300, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU6855268', '89588', 83205, '0', 0.00, 14.000000, '1111'),
(301, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU3958481', '82374', 83205, '0', 0.00, 14.000000, '1111'),
(302, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU1610155', '58255', 83205, '0', 0.00, 14.000000, '1111'),
(303, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU1227927', '23573', 83205, '0', 0.00, 14.000000, '1111'),
(304, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU1927090', '20019', 83205, '0', 0.00, 14.000000, '1111'),
(305, NULL, '2019-05-31 13:45:19', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2014292', '97565', 83205, '0', 0.00, 14.000000, '1111'),
(306, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8527333', '42966', 83205, '0', 0.00, 15.000000, '1111'),
(307, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU5202290', '47578', 83205, '0', 0.00, 15.000000, '1111'),
(308, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU9289152', '12464', 83205, '0', 0.00, 15.000000, '1111'),
(309, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU9637844', '95229', 83205, '0', 0.00, 15.000000, '1111'),
(310, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8451945', '11870', 83205, '0', 0.00, 15.000000, '1111'),
(311, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU1627293', '64001', 83205, '0', 0.00, 15.000000, '1111'),
(312, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU2464518', '64829', 83205, '0', 0.00, 15.000000, '1111'),
(313, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8303033', '30242', 83205, '0', 0.00, 15.000000, '1111'),
(314, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU1765526', '89672', 83205, '0', 0.00, 15.000000, '1111'),
(315, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU3522135', '96206', 83205, '0', 0.00, 15.000000, '1111'),
(316, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU3102621', '87284', 83205, '0', 0.00, 15.000000, '1111'),
(317, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU6705771', '68738', 83205, '0', 0.00, 15.000000, '1111'),
(318, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8323795', '80758', 83205, '0', 0.00, 15.000000, '1111'),
(319, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8142419', '62285', 83205, '0', 0.00, 15.000000, '1111'),
(320, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU7618406', '96889', 83205, '0', 0.00, 15.000000, '1111'),
(321, NULL, '2019-05-31 13:45:20', NULL, 70.00, NULL, 15.00, 'luis', 'PRU4123941', '27522', 83205, '0', 0.00, 15.000000, '1111'),
(322, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU8866631', '44991', 83205, '0', 0.00, 12.000000, '1111'),
(323, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU8866631', '44991', 83205, '0', 0.00, 12.000000, '1111'),
(324, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU6755509', '64677', 83205, '0', 0.00, 12.000000, '1111'),
(325, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU6755509', '64677', 83205, '0', 0.00, 12.000000, '1111'),
(326, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU6573249', '26431', 83205, '0', 0.00, 12.000000, '1111'),
(327, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU6573249', '26431', 83205, '0', 0.00, 12.000000, '1111'),
(328, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU4710897', '63138', 83205, '0', 0.00, 12.000000, '1111'),
(329, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU4710897', '63138', 83205, '0', 0.00, 12.000000, '1111'),
(330, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU6567800', '22957', 83205, '0', 0.00, 12.000000, '1111'),
(331, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU6567800', '22957', 83205, '0', 0.00, 12.000000, '1111'),
(332, NULL, '2019-05-31 13:45:22', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU5988061', '34219', 83205, '0', 0.00, 12.000000, '1111'),
(333, NULL, '2019-05-31 13:45:22', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU5988061', '34219', 83205, '0', 0.00, 12.000000, '1111'),
(334, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU3943870', '73289', 83205, '0', 0.00, 12.000000, '1111'),
(335, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU3943870', '73289', 83205, '0', 0.00, 12.000000, '1111'),
(336, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU8090177', '76321', 83205, '0', 0.00, 12.000000, '1111'),
(337, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU8090177', '76321', 83205, '0', 0.00, 12.000000, '1111'),
(338, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU9973923', '29417', 83205, '0', 0.00, 12.000000, '1111'),
(339, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU9973923', '29417', 83205, '0', 0.00, 12.000000, '1111'),
(340, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU6583044', '27631', 83205, '0', 0.00, 12.000000, '1111'),
(341, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU6583044', '27631', 83205, '0', 0.00, 12.000000, '1111'),
(342, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU5022807', '57659', 83205, '0', 0.00, 12.000000, '1111'),
(343, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU5022807', '57659', 83205, '0', 0.00, 12.000000, '1111'),
(344, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU9772816', '20336', 83205, '0', 0.00, 12.000000, '1111'),
(345, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU9772816', '20336', 83205, '0', 0.00, 12.000000, '1111'),
(346, NULL, '2019-05-31 13:45:24', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU6597426', '57149', 83205, '0', 0.00, 12.000000, '1111'),
(347, NULL, '2019-05-31 13:45:24', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU6597426', '57149', 83205, '0', 0.00, 12.000000, '1111'),
(348, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU7295906', '90159', 83205, '0', 0.00, 12.000000, '1111'),
(349, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU7295906', '90159', 83205, '0', 0.00, 12.000000, '1111'),
(350, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU4025245', '12176', 83205, '0', 0.00, 12.000000, '1111'),
(351, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU4025245', '12176', 83205, '0', 0.00, 12.000000, '1111'),
(352, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU8071531', '20359', 83205, '0', 0.00, 12.000000, '1111'),
(353, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU8071531', '20359', 83205, '0', 0.00, 12.000000, '1111'),
(354, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU4645427', '71483', 83205, '0', 0.00, 12.000000, '1111'),
(355, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU4645427', '71483', 83205, '0', 0.00, 12.000000, '1111'),
(356, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU4994313', '71571', 83205, '0', 0.00, 12.000000, '1111'),
(357, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU4994313', '71571', 83205, '0', 0.00, 12.000000, '1111'),
(358, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU9944152', '64530', 83205, '0', 0.00, 12.000000, '1111'),
(359, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU9944152', '64530', 83205, '0', 0.00, 12.000000, '1111'),
(360, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU5898679', '81444', 83205, '0', 0.00, 12.000000, '1111'),
(361, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU5898679', '81444', 83205, '0', 0.00, 12.000000, '1111'),
(362, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU4860669', '35272', 83205, '0', 0.00, 12.000000, '1111'),
(363, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU4860669', '35272', 83205, '0', 0.00, 12.000000, '1111'),
(364, NULL, '2019-05-31 13:45:26', NULL, 90.00, NULL, 10.00, 'luis', 'PRU6502475', '85325', 83205, '0', 0.00, 12.000000, '1111'),
(365, NULL, '2019-05-31 13:45:26', NULL, 85.00, NULL, 5.00, 'luis', 'PRU6502475', '85325', 83205, '0', 0.00, 12.000000, '1111'),
(366, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU9595592', '28558', 83205, '0', 0.00, 12.000000, '1111'),
(367, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU9595592', '28558', 83205, '0', 0.00, 12.000000, '1111'),
(368, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU9086300', '70350', 83205, '0', 0.00, 12.000000, '1111'),
(369, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU9086300', '70350', 83205, '0', 0.00, 12.000000, '1111'),
(370, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU2912069', '60874', 83205, '0', 0.00, 12.000000, '1111'),
(371, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU2912069', '60874', 83205, '0', 0.00, 12.000000, '1111'),
(372, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU1796790', '74728', 83205, '0', 0.00, 12.000000, '1111'),
(373, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU1796790', '74728', 83205, '0', 0.00, 12.000000, '1111'),
(374, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU8053640', '90881', 83205, '0', 0.00, 12.000000, '1111'),
(375, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU8053640', '90881', 83205, '0', 0.00, 12.000000, '1111'),
(376, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU4753172', '70441', 83205, '0', 0.00, 12.000000, '1111'),
(377, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU4753172', '70441', 83205, '0', 0.00, 12.000000, '1111'),
(378, NULL, '2019-05-31 13:45:29', NULL, 90.00, NULL, 10.00, 'JOSE', 'PRU5405157', '26659', 83205, '0', 0.00, 12.000000, '1111'),
(379, NULL, '2019-05-31 13:45:29', NULL, 85.00, NULL, 5.00, 'JOSE', 'PRU5405157', '26659', 83205, '0', 0.00, 12.000000, '1111'),
(380, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU3189337', '79745', 83205, '0', 0.00, 12.000000, '1111'),
(381, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU3189337', '79745', 83205, '0', 0.00, 12.000000, '1111'),
(382, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2146598', '96200', 83205, '0', 0.00, 12.000000, '1111'),
(383, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2146598', '96200', 83205, '0', 0.00, 12.000000, '1111'),
(384, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU1221862', '57604', 83205, '0', 0.00, 12.000000, '1111'),
(385, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU1221862', '57604', 83205, '0', 0.00, 12.000000, '1111'),
(386, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU8085569', '70687', 83205, '0', 0.00, 12.000000, '1111'),
(387, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU8085569', '70687', 83205, '0', 0.00, 12.000000, '1111'),
(388, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU6064836', '88876', 83205, '0', 0.00, 12.000000, '1111'),
(389, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU6064836', '88876', 83205, '0', 0.00, 12.000000, '1111'),
(390, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2705300', '93535', 83205, '0', 0.00, 12.000000, '1111'),
(391, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2705300', '93535', 83205, '0', 0.00, 12.000000, '1111'),
(392, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2715477', '83969', 83205, '0', 0.00, 12.000000, '1111'),
(393, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2715477', '83969', 83205, '0', 0.00, 12.000000, '1111'),
(394, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2553449', '48456', 83205, '0', 0.00, 12.000000, '1111'),
(395, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2553449', '48456', 83205, '0', 0.00, 12.000000, '1111'),
(396, NULL, '2019-05-31 13:45:31', NULL, 90.00, NULL, 10.00, 'Pedro', 'PRU2780448', '77250', 83205, '0', 0.00, 12.000000, '1111'),
(397, NULL, '2019-05-31 13:45:31', NULL, 85.00, NULL, 5.00, 'Pedro', 'PRU2780448', '77250', 83205, '0', 0.00, 12.000000, '1111'),
(398, NULL, '2019-05-31 13:48:05', NULL, 50.00, NULL, 10.00, 'JOSE', 'PRU4393533', '89011', 83205, '0', 0.00, 12.000000, '1111'),
(399, NULL, '2019-05-31 13:48:05', NULL, 51.00, NULL, 9.00, 'JOSE', 'PRU1691607', '49854', 83205, '0', 0.00, 12.000000, '1111'),
(400, NULL, '2019-05-31 13:48:05', NULL, 52.00, NULL, 8.00, 'JOSE', 'PRU4826616', '90712', 83205, '0', 0.00, 12.000000, '1111'),
(401, NULL, '2019-05-31 13:48:05', NULL, 53.00, NULL, 7.00, 'JOSE', 'PRU8409456', '89539', 83205, '0', 0.00, 12.000000, '1111'),
(402, NULL, '2019-05-31 13:48:05', NULL, 54.00, NULL, 6.00, 'JOSE', 'PRU7794082', '10969', 83205, '0', 0.00, 12.000000, '1111'),
(403, NULL, '2019-05-31 13:48:05', NULL, 55.00, NULL, 5.00, 'JOSE', 'PRU6934667', '77932', 83205, '0', 0.00, 12.000000, '1111'),
(404, NULL, '2019-05-31 13:48:05', NULL, 56.00, NULL, 4.00, 'JOSE', 'PRU1966258', '62030', 83205, '0', 0.00, 12.000000, '1111');
INSERT INTO `detalle_kardex_material` (`id_detalle`, `fecha_ingreso`, `fecha_salida`, `fecha_devolucion`, `stock`, `cantidad_ingresada`, `cantidad_salida`, `trasladador_material`, `cod_kardex_material`, `comentario_devolucion`, `cod_area`, `estado_detalle_kardex`, `restante_detalle`, `costo_material`, `dni_usuario`) VALUES
(405, NULL, '2019-05-31 13:48:05', NULL, 57.00, NULL, 3.00, 'JOSE', 'PRU7545757', '79085', 83205, '0', 0.00, 12.000000, '1111'),
(406, NULL, '2019-05-31 13:48:05', NULL, 58.00, NULL, 2.00, 'JOSE', 'PRU2971377', '81669', 83205, '0', 0.00, 12.000000, '1111'),
(407, NULL, '2019-05-31 13:48:05', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU1317125', '12331', 83205, '0', 0.00, 12.000000, '1111'),
(408, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU6855268', '19314', 83205, '0', 0.00, 14.000000, '1111'),
(409, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU3958481', '42814', 83205, '0', 0.00, 14.000000, '1111'),
(410, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU1610155', '82686', 83205, '0', 0.00, 14.000000, '1111'),
(411, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU1227927', '54024', 83205, '0', 0.00, 14.000000, '1111'),
(412, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU1927090', '74003', 83205, '0', 0.00, 14.000000, '1111'),
(413, NULL, '2019-05-31 13:48:06', NULL, 55.00, NULL, 15.00, 'Pedro', 'PRU2014292', '26026', 83205, '0', 0.00, 14.000000, '1111'),
(414, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU8527333', '31408', 83205, '0', 0.00, 15.000000, '1111'),
(415, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU5202290', '36667', 83205, '0', 0.00, 15.000000, '1111'),
(416, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU9289152', '53608', 83205, '0', 0.00, 15.000000, '1111'),
(417, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU9637844', '28397', 83205, '0', 0.00, 15.000000, '1111'),
(418, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU8451945', '28337', 83205, '0', 0.00, 15.000000, '1111'),
(419, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU1627293', '46224', 83205, '0', 0.00, 15.000000, '1111'),
(420, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU2464518', '41702', 83205, '0', 0.00, 15.000000, '1111'),
(421, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU8303033', '78196', 83205, '0', 0.00, 15.000000, '1111'),
(422, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU1765526', '85037', 83205, '0', 0.00, 15.000000, '1111'),
(423, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU3522135', '72736', 83205, '0', 0.00, 15.000000, '1111'),
(424, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU3102621', '66017', 83205, '0', 0.00, 15.000000, '1111'),
(425, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU6705771', '58911', 83205, '0', 0.00, 15.000000, '1111'),
(426, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU8323795', '74559', 83205, '0', 0.00, 15.000000, '1111'),
(427, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU8142419', '79890', 83205, '0', 0.00, 15.000000, '1111'),
(428, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU7618406', '95751', 83205, '0', 0.00, 15.000000, '1111'),
(429, NULL, '2019-05-31 13:48:07', NULL, 55.00, NULL, 15.00, 'luis', 'PRU4123941', '98177', 83205, '0', 0.00, 15.000000, '1111'),
(430, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU8866631', '34402', 83205, '0', 0.00, 12.000000, '1111'),
(431, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU6755509', '55407', 83205, '0', 0.00, 12.000000, '1111'),
(432, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU6573249', '66384', 83205, '0', 0.00, 12.000000, '1111'),
(433, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU4710897', '67480', 83205, '0', 0.00, 12.000000, '1111'),
(434, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU6567800', '98354', 83205, '0', 0.00, 12.000000, '1111'),
(435, NULL, '2019-05-31 13:48:09', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU5988061', '66337', 83205, '0', 0.00, 12.000000, '1111'),
(436, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU3943870', '37661', 83205, '0', 0.00, 12.000000, '1111'),
(437, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU8090177', '98039', 83205, '0', 0.00, 12.000000, '1111'),
(438, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU9973923', '86667', 83205, '0', 0.00, 12.000000, '1111'),
(439, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU6583044', '13500', 83205, '0', 0.00, 12.000000, '1111'),
(440, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU5022807', '69168', 83205, '0', 0.00, 12.000000, '1111'),
(441, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU9772816', '18049', 83205, '0', 0.00, 12.000000, '1111'),
(442, NULL, '2019-05-31 13:48:10', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU6597426', '55866', 83205, '0', 0.00, 12.000000, '1111'),
(443, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU7295906', '51245', 83205, '0', 0.00, 12.000000, '1111'),
(444, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU4025245', '75845', 83205, '0', 0.00, 12.000000, '1111'),
(445, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU8071531', '67209', 83205, '0', 0.00, 12.000000, '1111'),
(446, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU4645427', '71759', 83205, '0', 0.00, 12.000000, '1111'),
(447, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU4994313', '17108', 83205, '0', 0.00, 12.000000, '1111'),
(448, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU9944152', '98403', 83205, '0', 0.00, 12.000000, '1111'),
(449, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU5898679', '90740', 83205, '0', 0.00, 12.000000, '1111'),
(450, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU4860669', '47466', 83205, '0', 0.00, 12.000000, '1111'),
(451, NULL, '2019-05-31 13:48:11', NULL, 70.00, NULL, 15.00, 'luis', 'PRU6502475', '42429', 83205, '0', 0.00, 12.000000, '1111'),
(452, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU9595592', '31184', 83205, '0', 0.00, 12.000000, '1111'),
(453, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU9086300', '30899', 83205, '0', 0.00, 12.000000, '1111'),
(454, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU2912069', '93832', 83205, '0', 0.00, 12.000000, '1111'),
(455, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU1796790', '31286', 83205, '0', 0.00, 12.000000, '1111'),
(456, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU8053640', '36497', 83205, '0', 0.00, 12.000000, '1111'),
(457, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU4753172', '20640', 83205, '0', 0.00, 12.000000, '1111'),
(458, NULL, '2019-05-31 13:48:12', NULL, 70.00, NULL, 15.00, 'JOSE', 'PRU5405157', '71146', 83205, '0', 0.00, 12.000000, '1111'),
(459, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU3189337', '55606', 83205, '0', 0.00, 12.000000, '1111'),
(460, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2146598', '82259', 83205, '0', 0.00, 12.000000, '1111'),
(461, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU1221862', '80744', 83205, '0', 0.00, 12.000000, '1111'),
(462, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU8085569', '11890', 83205, '0', 0.00, 12.000000, '1111'),
(463, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU6064836', '70770', 83205, '0', 0.00, 12.000000, '1111'),
(464, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2705300', '67168', 83205, '0', 0.00, 12.000000, '1111'),
(465, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2715477', '37451', 83205, '0', 0.00, 12.000000, '1111'),
(466, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2553449', '54370', 83205, '0', 0.00, 12.000000, '1111'),
(467, NULL, '2019-05-31 13:48:13', NULL, 70.00, NULL, 15.00, 'Pedro', 'PRU2780448', '18873', 83205, '0', 0.00, 12.000000, '1111'),
(468, NULL, '2019-05-31 13:50:59', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU4393533', '23465', 83205, '0', 0.00, 12.000000, '1111'),
(469, NULL, '2019-05-31 13:50:59', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU1691607', '32876', 83205, '0', 0.00, 12.000000, '1111'),
(470, NULL, '2019-05-31 13:50:59', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU4826616', '59964', 83205, '0', 0.00, 12.000000, '1111'),
(471, NULL, '2019-05-31 13:50:59', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU8409456', '29266', 83205, '0', 0.00, 12.000000, '1111'),
(472, NULL, '2019-05-31 13:50:59', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU7794082', '94048', 83205, '0', 0.00, 12.000000, '1111'),
(473, NULL, '2019-05-31 13:50:59', NULL, 54.00, NULL, 1.00, 'JOSE', 'PRU6934667', '91023', 83205, '0', 0.00, 12.000000, '1111'),
(474, NULL, '2019-05-31 13:50:59', NULL, 55.00, NULL, 1.00, 'JOSE', 'PRU1966258', '46011', 83205, '0', 0.00, 12.000000, '1111'),
(475, NULL, '2019-05-31 13:50:59', NULL, 56.00, NULL, 1.00, 'JOSE', 'PRU7545757', '63897', 83205, '0', 0.00, 12.000000, '1111'),
(476, NULL, '2019-05-31 13:50:59', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU2971377', '34721', 83205, '0', 0.00, 12.000000, '1111'),
(477, NULL, '2019-05-31 13:50:59', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU1317125', '56994', 83205, '0', 0.00, 12.000000, '1111'),
(478, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU6855268', '29291', 83205, '0', 0.00, 14.000000, '1111'),
(479, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU3958481', '75565', 83205, '0', 0.00, 14.000000, '1111'),
(480, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU1610155', '11636', 83205, '0', 0.00, 14.000000, '1111'),
(481, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU1227927', '46931', 83205, '0', 0.00, 14.000000, '1111'),
(482, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU1927090', '50947', 83205, '0', 0.00, 14.000000, '1111'),
(483, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'Pedro', 'PRU2014292', '51317', 83205, '0', 0.00, 14.000000, '1111'),
(484, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU8527333', '84109', 83205, '0', 0.00, 15.000000, '1111'),
(485, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU5202290', '23686', 83205, '0', 0.00, 15.000000, '1111'),
(486, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU9289152', '61122', 83205, '0', 0.00, 15.000000, '1111'),
(487, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU9637844', '99820', 83205, '0', 0.00, 15.000000, '1111'),
(488, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU8451945', '28891', 83205, '0', 0.00, 15.000000, '1111'),
(489, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU1627293', '68490', 83205, '0', 0.00, 15.000000, '1111'),
(490, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU2464518', '77737', 83205, '0', 0.00, 15.000000, '1111'),
(491, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU8303033', '83072', 83205, '0', 0.00, 15.000000, '1111'),
(492, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU1765526', '72659', 83205, '0', 0.00, 15.000000, '1111'),
(493, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU3522135', '80046', 83205, '0', 0.00, 15.000000, '1111'),
(494, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU3102621', '50464', 83205, '0', 0.00, 15.000000, '1111'),
(495, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU6705771', '63214', 83205, '0', 0.00, 15.000000, '1111'),
(496, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU8323795', '45074', 83205, '0', 0.00, 15.000000, '1111'),
(497, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU8142419', '48976', 83205, '0', 0.00, 15.000000, '1111'),
(498, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU7618406', '93448', 83205, '0', 0.00, 15.000000, '1111'),
(499, NULL, '2019-05-31 13:51:01', NULL, 54.00, NULL, 1.00, 'luis', 'PRU4123941', '67406', 83205, '0', 0.00, 15.000000, '1111'),
(500, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU8866631', '93271', 83205, '0', 0.00, 12.000000, '1111'),
(501, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU6755509', '86610', 83205, '0', 0.00, 12.000000, '1111'),
(502, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU6573249', '16283', 83205, '0', 0.00, 12.000000, '1111'),
(503, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU4710897', '66885', 83205, '0', 0.00, 12.000000, '1111'),
(504, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU6567800', '32197', 83205, '0', 0.00, 12.000000, '1111'),
(505, NULL, '2019-05-31 13:51:04', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU5988061', '59707', 83205, '0', 0.00, 12.000000, '1111'),
(506, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU3943870', '51676', 83205, '0', 0.00, 12.000000, '1111'),
(507, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU8090177', '15918', 83205, '0', 0.00, 12.000000, '1111'),
(508, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU9973923', '86554', 83205, '0', 0.00, 12.000000, '1111'),
(509, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU6583044', '38140', 83205, '0', 0.00, 12.000000, '1111'),
(510, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU5022807', '12705', 83205, '0', 0.00, 12.000000, '1111'),
(511, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU9772816', '90208', 83205, '0', 0.00, 12.000000, '1111'),
(512, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU6597426', '66887', 83205, '0', 0.00, 12.000000, '1111'),
(513, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU7295906', '14220', 83205, '0', 0.00, 12.000000, '1111'),
(514, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU4025245', '93449', 83205, '0', 0.00, 12.000000, '1111'),
(515, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU8071531', '39243', 83205, '0', 0.00, 12.000000, '1111'),
(516, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU4645427', '58674', 83205, '0', 0.00, 12.000000, '1111'),
(517, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU4994313', '26115', 83205, '0', 0.00, 12.000000, '1111'),
(518, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU9944152', '16941', 83205, '0', 0.00, 12.000000, '1111'),
(519, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU5898679', '98664', 83205, '0', 0.00, 12.000000, '1111'),
(520, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU4860669', '26096', 83205, '0', 0.00, 12.000000, '1111'),
(521, NULL, '2019-05-31 13:51:05', NULL, 69.00, NULL, 1.00, 'luis', 'PRU6502475', '93655', 83205, '0', 0.00, 12.000000, '1111'),
(522, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU9595592', '20190', 83205, '0', 0.00, 12.000000, '1111'),
(523, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU9086300', '67682', 83205, '0', 0.00, 12.000000, '1111'),
(524, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU2912069', '29069', 83205, '0', 0.00, 12.000000, '1111'),
(525, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU1796790', '43293', 83205, '0', 0.00, 12.000000, '1111'),
(526, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU8053640', '22484', 83205, '0', 0.00, 12.000000, '1111'),
(527, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU4753172', '52354', 83205, '0', 0.00, 12.000000, '1111'),
(528, NULL, '2019-05-31 13:51:07', NULL, 69.00, NULL, 1.00, 'JOSE', 'PRU5405157', '95817', 83205, '0', 0.00, 12.000000, '1111'),
(529, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU3189337', '32336', 83205, '0', 0.00, 12.000000, '1111'),
(530, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU2146598', '35790', 83205, '0', 0.00, 12.000000, '1111'),
(531, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU1221862', '95197', 83205, '0', 0.00, 12.000000, '1111'),
(532, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU8085569', '32386', 83205, '0', 0.00, 12.000000, '1111'),
(533, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU6064836', '19695', 83205, '0', 0.00, 12.000000, '1111'),
(534, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU2705300', '53927', 83205, '0', 0.00, 12.000000, '1111'),
(535, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU2715477', '73179', 83205, '0', 0.00, 12.000000, '1111'),
(536, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU2553449', '83342', 83205, '0', 0.00, 12.000000, '1111'),
(537, NULL, '2019-05-31 13:51:08', NULL, 69.00, NULL, 1.00, 'Pedro', 'PRU2780448', '67536', 83205, '0', 0.00, 12.000000, '1111'),
(538, NULL, '2019-05-31 13:51:09', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU4393533', '39647', 83205, '0', 0.00, 12.000000, '1111'),
(539, NULL, '2019-05-31 13:51:09', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU1691607', '69506', 83205, '0', 0.00, 12.000000, '1111'),
(540, NULL, '2019-05-31 13:51:09', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU4826616', '68039', 83205, '0', 0.00, 12.000000, '1111'),
(541, NULL, '2019-05-31 13:51:09', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU8409456', '20762', 83205, '0', 0.00, 12.000000, '1111'),
(542, NULL, '2019-05-31 13:51:09', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU7794082', '85833', 83205, '0', 0.00, 12.000000, '1111'),
(543, NULL, '2019-05-31 13:51:09', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU6934667', '75118', 83205, '0', 0.00, 12.000000, '1111'),
(544, NULL, '2019-05-31 13:51:09', NULL, 54.00, NULL, 1.00, 'JOSE', 'PRU1966258', '50142', 83205, '0', 0.00, 12.000000, '1111'),
(545, NULL, '2019-05-31 13:51:09', NULL, 55.00, NULL, 1.00, 'JOSE', 'PRU7545757', '97810', 83205, '0', 0.00, 12.000000, '1111'),
(546, NULL, '2019-05-31 13:51:09', NULL, 56.00, NULL, 1.00, 'JOSE', 'PRU2971377', '81531', 83205, '0', 0.00, 12.000000, '1111'),
(547, NULL, '2019-05-31 13:51:09', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU1317125', '24930', 83205, '0', 0.00, 12.000000, '1111'),
(548, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU6855268', '66837', 83205, '0', 0.00, 14.000000, '1111'),
(549, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU3958481', '96137', 83205, '0', 0.00, 14.000000, '1111'),
(550, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU1610155', '83516', 83205, '0', 0.00, 14.000000, '1111'),
(551, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU1227927', '19502', 83205, '0', 0.00, 14.000000, '1111'),
(552, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU1927090', '95829', 83205, '0', 0.00, 14.000000, '1111'),
(553, NULL, '2019-05-31 13:51:10', NULL, 53.00, NULL, 1.00, 'Pedro', 'PRU2014292', '76456', 83205, '0', 0.00, 14.000000, '1111'),
(554, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU8527333', '68107', 83205, '0', 0.00, 15.000000, '1111'),
(555, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU5202290', '41131', 83205, '0', 0.00, 15.000000, '1111'),
(556, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU9289152', '83009', 83205, '0', 0.00, 15.000000, '1111'),
(557, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU9637844', '21379', 83205, '0', 0.00, 15.000000, '1111'),
(558, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU8451945', '66356', 83205, '0', 0.00, 15.000000, '1111'),
(559, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU1627293', '31900', 83205, '0', 0.00, 15.000000, '1111'),
(560, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU2464518', '40249', 83205, '0', 0.00, 15.000000, '1111'),
(561, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU8303033', '58706', 83205, '0', 0.00, 15.000000, '1111'),
(562, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU1765526', '76309', 83205, '0', 0.00, 15.000000, '1111'),
(563, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU3522135', '68882', 83205, '0', 0.00, 15.000000, '1111'),
(564, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU3102621', '86439', 83205, '0', 0.00, 15.000000, '1111'),
(565, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU6705771', '98752', 83205, '0', 0.00, 15.000000, '1111'),
(566, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU8323795', '51333', 83205, '0', 0.00, 15.000000, '1111'),
(567, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU8142419', '72628', 83205, '0', 0.00, 15.000000, '1111'),
(568, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU7618406', '64944', 83205, '0', 0.00, 15.000000, '1111'),
(569, NULL, '2019-05-31 13:51:11', NULL, 53.00, NULL, 1.00, 'luis', 'PRU4123941', '17893', 83205, '0', 0.00, 15.000000, '1111'),
(570, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU8866631', '74990', 83205, '0', 0.00, 12.000000, '1111'),
(571, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU6755509', '67729', 83205, '0', 0.00, 12.000000, '1111'),
(572, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU6573249', '79864', 83205, '0', 0.00, 12.000000, '1111'),
(573, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU4710897', '64782', 83205, '0', 0.00, 12.000000, '1111'),
(574, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU6567800', '11441', 83205, '0', 0.00, 12.000000, '1111'),
(575, NULL, '2019-05-31 13:51:13', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU5988061', '97114', 83205, '0', 0.00, 12.000000, '1111'),
(576, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU3943870', '70229', 83205, '0', 0.00, 12.000000, '1111'),
(577, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU8090177', '88381', 83205, '0', 0.00, 12.000000, '1111'),
(578, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU9973923', '41974', 83205, '0', 0.00, 12.000000, '1111'),
(579, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU6583044', '64179', 83205, '0', 0.00, 12.000000, '1111'),
(580, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU5022807', '22588', 83205, '0', 0.00, 12.000000, '1111'),
(581, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU9772816', '52746', 83205, '0', 0.00, 12.000000, '1111'),
(582, NULL, '2019-05-31 13:51:14', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU6597426', '41424', 83205, '0', 0.00, 12.000000, '1111'),
(583, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU7295906', '19524', 83205, '0', 0.00, 12.000000, '1111'),
(584, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU4025245', '45161', 83205, '0', 0.00, 12.000000, '1111'),
(585, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU8071531', '73355', 83205, '0', 0.00, 12.000000, '1111'),
(586, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU4645427', '11913', 83205, '0', 0.00, 12.000000, '1111'),
(587, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU4994313', '44122', 83205, '0', 0.00, 12.000000, '1111'),
(588, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU9944152', '74301', 83205, '0', 0.00, 12.000000, '1111'),
(589, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU5898679', '73193', 83205, '0', 0.00, 12.000000, '1111'),
(590, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU4860669', '42981', 83205, '0', 0.00, 12.000000, '1111'),
(591, NULL, '2019-05-31 13:51:15', NULL, 68.00, NULL, 1.00, 'luis', 'PRU6502475', '42376', 83205, '0', 0.00, 12.000000, '1111'),
(592, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU9595592', '97439', 83205, '0', 0.00, 12.000000, '1111'),
(593, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU9086300', '16217', 83205, '0', 0.00, 12.000000, '1111'),
(594, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU2912069', '61196', 83205, '0', 0.00, 12.000000, '1111'),
(595, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU1796790', '23241', 83205, '0', 0.00, 12.000000, '1111'),
(596, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU8053640', '40575', 83205, '0', 0.00, 12.000000, '1111'),
(597, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU4753172', '15359', 83205, '0', 0.00, 12.000000, '1111'),
(598, NULL, '2019-05-31 13:51:16', NULL, 68.00, NULL, 1.00, 'JOSE', 'PRU5405157', '82565', 83205, '0', 0.00, 12.000000, '1111'),
(599, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU3189337', '28721', 83205, '0', 0.00, 12.000000, '1111'),
(600, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU2146598', '78556', 83205, '0', 0.00, 12.000000, '1111'),
(601, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU1221862', '12055', 83205, '0', 0.00, 12.000000, '1111'),
(602, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU8085569', '23884', 83205, '0', 0.00, 12.000000, '1111'),
(603, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU6064836', '85326', 83205, '0', 0.00, 12.000000, '1111'),
(604, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU2705300', '64827', 83205, '0', 0.00, 12.000000, '1111'),
(605, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU2715477', '67198', 83205, '0', 0.00, 12.000000, '1111'),
(606, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU2553449', '64112', 83205, '0', 0.00, 12.000000, '1111'),
(607, NULL, '2019-05-31 13:51:17', NULL, 68.00, NULL, 1.00, 'Pedro', 'PRU2780448', '70595', 83205, '0', 0.00, 12.000000, '1111'),
(608, NULL, '2019-05-31 13:51:18', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU4393533', '40098', 83205, '0', 0.00, 12.000000, '1111'),
(609, NULL, '2019-05-31 13:51:18', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU1691607', '40299', 83205, '0', 0.00, 12.000000, '1111'),
(610, NULL, '2019-05-31 13:51:18', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU4826616', '47863', 83205, '0', 0.00, 12.000000, '1111'),
(611, NULL, '2019-05-31 13:51:18', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU8409456', '43437', 83205, '0', 0.00, 12.000000, '1111'),
(612, NULL, '2019-05-31 13:51:18', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU7794082', '96935', 83205, '0', 0.00, 12.000000, '1111'),
(613, NULL, '2019-05-31 13:51:18', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU6934667', '81409', 83205, '0', 0.00, 12.000000, '1111'),
(614, NULL, '2019-05-31 13:51:18', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU1966258', '37197', 83205, '0', 0.00, 12.000000, '1111'),
(615, NULL, '2019-05-31 13:51:18', NULL, 54.00, NULL, 1.00, 'JOSE', 'PRU7545757', '21291', 83205, '0', 0.00, 12.000000, '1111'),
(616, NULL, '2019-05-31 13:51:18', NULL, 55.00, NULL, 1.00, 'JOSE', 'PRU2971377', '64834', 83205, '0', 0.00, 12.000000, '1111'),
(617, NULL, '2019-05-31 13:51:18', NULL, 56.00, NULL, 1.00, 'JOSE', 'PRU1317125', '24623', 83205, '0', 0.00, 12.000000, '1111'),
(618, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU6855268', '62760', 83205, '0', 0.00, 14.000000, '1111'),
(619, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU3958481', '62307', 83205, '0', 0.00, 14.000000, '1111'),
(620, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU1610155', '88417', 83205, '0', 0.00, 14.000000, '1111'),
(621, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU1227927', '82411', 83205, '0', 0.00, 14.000000, '1111'),
(622, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU1927090', '15750', 83205, '0', 0.00, 14.000000, '1111'),
(623, NULL, '2019-05-31 13:51:19', NULL, 52.00, NULL, 1.00, 'Pedro', 'PRU2014292', '68073', 83205, '0', 0.00, 14.000000, '1111'),
(624, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU8527333', '18213', 83205, '0', 0.00, 15.000000, '1111'),
(625, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU5202290', '68319', 83205, '0', 0.00, 15.000000, '1111'),
(626, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU9289152', '93548', 83205, '0', 0.00, 15.000000, '1111'),
(627, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU9637844', '96054', 83205, '0', 0.00, 15.000000, '1111'),
(628, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU8451945', '24912', 83205, '0', 0.00, 15.000000, '1111'),
(629, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU1627293', '55477', 83205, '0', 0.00, 15.000000, '1111'),
(630, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU2464518', '58543', 83205, '0', 0.00, 15.000000, '1111'),
(631, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU8303033', '58814', 83205, '0', 0.00, 15.000000, '1111'),
(632, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU1765526', '17378', 83205, '0', 0.00, 15.000000, '1111'),
(633, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU3522135', '14772', 83205, '0', 0.00, 15.000000, '1111'),
(634, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU3102621', '82318', 83205, '0', 0.00, 15.000000, '1111'),
(635, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU6705771', '51705', 83205, '0', 0.00, 15.000000, '1111'),
(636, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU8323795', '33948', 83205, '0', 0.00, 15.000000, '1111'),
(637, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU8142419', '67407', 83205, '0', 0.00, 15.000000, '1111'),
(638, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU7618406', '94737', 83205, '0', 0.00, 15.000000, '1111'),
(639, NULL, '2019-05-31 13:51:20', NULL, 52.00, NULL, 1.00, 'luis', 'PRU4123941', '46046', 83205, '0', 0.00, 15.000000, '1111'),
(640, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU8866631', '24669', 83205, '0', 0.00, 12.000000, '1111'),
(641, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU6755509', '10624', 83205, '0', 0.00, 12.000000, '1111'),
(642, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU6573249', '16324', 83205, '0', 0.00, 12.000000, '1111'),
(643, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU4710897', '18196', 83205, '0', 0.00, 12.000000, '1111'),
(644, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU6567800', '88474', 83205, '0', 0.00, 12.000000, '1111'),
(645, NULL, '2019-05-31 13:51:23', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU5988061', '76671', 83205, '0', 0.00, 12.000000, '1111'),
(646, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU3943870', '91572', 83205, '0', 0.00, 12.000000, '1111'),
(647, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU8090177', '92163', 83205, '0', 0.00, 12.000000, '1111'),
(648, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU9973923', '65068', 83205, '0', 0.00, 12.000000, '1111'),
(649, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU6583044', '51005', 83205, '0', 0.00, 12.000000, '1111'),
(650, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU5022807', '36196', 83205, '0', 0.00, 12.000000, '1111'),
(651, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU9772816', '76003', 83205, '0', 0.00, 12.000000, '1111'),
(652, NULL, '2019-05-31 13:51:24', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU6597426', '40891', 83205, '0', 0.00, 12.000000, '1111'),
(653, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU7295906', '65297', 83205, '0', 0.00, 12.000000, '1111'),
(654, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU4025245', '16784', 83205, '0', 0.00, 12.000000, '1111'),
(655, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU8071531', '26558', 83205, '0', 0.00, 12.000000, '1111'),
(656, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU4645427', '36953', 83205, '0', 0.00, 12.000000, '1111'),
(657, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU4994313', '49790', 83205, '0', 0.00, 12.000000, '1111'),
(658, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU9944152', '66234', 83205, '0', 0.00, 12.000000, '1111'),
(659, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU5898679', '13171', 83205, '0', 0.00, 12.000000, '1111'),
(660, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU4860669', '34937', 83205, '0', 0.00, 12.000000, '1111'),
(661, NULL, '2019-05-31 13:51:25', NULL, 67.00, NULL, 1.00, 'luis', 'PRU6502475', '76848', 83205, '0', 0.00, 12.000000, '1111'),
(662, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU9595592', '60824', 83205, '0', 0.00, 12.000000, '1111'),
(663, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU9086300', '17978', 83205, '0', 0.00, 12.000000, '1111'),
(664, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU2912069', '24827', 83205, '0', 0.00, 12.000000, '1111'),
(665, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU1796790', '23603', 83205, '0', 0.00, 12.000000, '1111'),
(666, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU8053640', '65531', 83205, '0', 0.00, 12.000000, '1111'),
(667, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU4753172', '66979', 83205, '0', 0.00, 12.000000, '1111'),
(668, NULL, '2019-05-31 13:51:26', NULL, 67.00, NULL, 1.00, 'JOSE', 'PRU5405157', '40979', 83205, '0', 0.00, 12.000000, '1111'),
(669, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU3189337', '52204', 83205, '0', 0.00, 12.000000, '1111'),
(670, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU2146598', '24060', 83205, '0', 0.00, 12.000000, '1111'),
(671, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU1221862', '32933', 83205, '0', 0.00, 12.000000, '1111'),
(672, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU8085569', '77793', 83205, '0', 0.00, 12.000000, '1111'),
(673, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU6064836', '24739', 83205, '0', 0.00, 12.000000, '1111'),
(674, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU2705300', '44903', 83205, '0', 0.00, 12.000000, '1111'),
(675, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU2715477', '46447', 83205, '0', 0.00, 12.000000, '1111'),
(676, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU2553449', '87452', 83205, '0', 0.00, 12.000000, '1111'),
(677, NULL, '2019-05-31 13:51:27', NULL, 67.00, NULL, 1.00, 'Pedro', 'PRU2780448', '30557', 83205, '0', 0.00, 12.000000, '1111'),
(678, NULL, '2019-05-31 13:51:28', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU4393533', '28190', 83205, '0', 0.00, 12.000000, '1111'),
(679, NULL, '2019-05-31 13:51:28', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU1691607', '53027', 83205, '0', 0.00, 12.000000, '1111'),
(680, NULL, '2019-05-31 13:51:28', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU4826616', '99580', 83205, '0', 0.00, 12.000000, '1111'),
(681, NULL, '2019-05-31 13:51:28', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU8409456', '95267', 83205, '0', 0.00, 12.000000, '1111'),
(682, NULL, '2019-05-31 13:51:28', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU7794082', '27827', 83205, '0', 0.00, 12.000000, '1111'),
(683, NULL, '2019-05-31 13:51:28', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU6934667', '39933', 83205, '0', 0.00, 12.000000, '1111'),
(684, NULL, '2019-05-31 13:51:28', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU1966258', '33418', 83205, '0', 0.00, 12.000000, '1111'),
(685, NULL, '2019-05-31 13:51:28', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU7545757', '62407', 83205, '0', 0.00, 12.000000, '1111'),
(686, NULL, '2019-05-31 13:51:28', NULL, 54.00, NULL, 1.00, 'JOSE', 'PRU2971377', '39186', 83205, '0', 0.00, 12.000000, '1111'),
(687, NULL, '2019-05-31 13:51:28', NULL, 55.00, NULL, 1.00, 'JOSE', 'PRU1317125', '98409', 83205, '0', 0.00, 12.000000, '1111'),
(688, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU6855268', '38866', 83205, '0', 0.00, 14.000000, '1111'),
(689, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU3958481', '89550', 83205, '0', 0.00, 14.000000, '1111'),
(690, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU1610155', '44884', 83205, '0', 0.00, 14.000000, '1111'),
(691, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU1227927', '26233', 83205, '0', 0.00, 14.000000, '1111'),
(692, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU1927090', '28242', 83205, '0', 0.00, 14.000000, '1111'),
(693, NULL, '2019-05-31 13:51:29', NULL, 51.00, NULL, 1.00, 'Pedro', 'PRU2014292', '23409', 83205, '0', 0.00, 14.000000, '1111'),
(694, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU8527333', '11914', 83205, '0', 0.00, 15.000000, '1111'),
(695, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU5202290', '37897', 83205, '0', 0.00, 15.000000, '1111'),
(696, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU9289152', '23524', 83205, '0', 0.00, 15.000000, '1111'),
(697, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU9637844', '21585', 83205, '0', 0.00, 15.000000, '1111'),
(698, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU8451945', '31874', 83205, '0', 0.00, 15.000000, '1111'),
(699, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU1627293', '81234', 83205, '0', 0.00, 15.000000, '1111'),
(700, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU2464518', '33986', 83205, '0', 0.00, 15.000000, '1111'),
(701, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU8303033', '15351', 83205, '0', 0.00, 15.000000, '1111'),
(702, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU1765526', '15863', 83205, '0', 0.00, 15.000000, '1111'),
(703, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU3522135', '22270', 83205, '0', 0.00, 15.000000, '1111'),
(704, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU3102621', '28627', 83205, '0', 0.00, 15.000000, '1111'),
(705, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU6705771', '91037', 83205, '0', 0.00, 15.000000, '1111'),
(706, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU8323795', '79661', 83205, '0', 0.00, 15.000000, '1111'),
(707, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU8142419', '73567', 83205, '0', 0.00, 15.000000, '1111'),
(708, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU7618406', '95836', 83205, '0', 0.00, 15.000000, '1111'),
(709, NULL, '2019-05-31 13:51:30', NULL, 51.00, NULL, 1.00, 'luis', 'PRU4123941', '83778', 83205, '0', 0.00, 15.000000, '1111'),
(710, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU8866631', '43740', 83205, '0', 0.00, 12.000000, '1111'),
(711, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU6755509', '74909', 83205, '0', 0.00, 12.000000, '1111'),
(712, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU6573249', '87390', 83205, '0', 0.00, 12.000000, '1111'),
(713, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU4710897', '27080', 83205, '0', 0.00, 12.000000, '1111'),
(714, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU6567800', '26883', 83205, '0', 0.00, 12.000000, '1111'),
(715, NULL, '2019-05-31 13:51:33', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU5988061', '93718', 83205, '0', 0.00, 12.000000, '1111'),
(716, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU3943870', '64546', 83205, '0', 0.00, 12.000000, '1111'),
(717, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU8090177', '27701', 83205, '0', 0.00, 12.000000, '1111'),
(718, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU9973923', '20496', 83205, '0', 0.00, 12.000000, '1111'),
(719, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU6583044', '93481', 83205, '0', 0.00, 12.000000, '1111'),
(720, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU5022807', '85708', 83205, '0', 0.00, 12.000000, '1111'),
(721, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU9772816', '89412', 83205, '0', 0.00, 12.000000, '1111'),
(722, NULL, '2019-05-31 13:51:34', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU6597426', '31829', 83205, '0', 0.00, 12.000000, '1111'),
(723, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU7295906', '43868', 83205, '0', 0.00, 12.000000, '1111'),
(724, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU4025245', '87430', 83205, '0', 0.00, 12.000000, '1111'),
(725, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU8071531', '66445', 83205, '0', 0.00, 12.000000, '1111'),
(726, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU4645427', '22025', 83205, '0', 0.00, 12.000000, '1111'),
(727, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU4994313', '91740', 83205, '0', 0.00, 12.000000, '1111'),
(728, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU9944152', '48251', 83205, '0', 0.00, 12.000000, '1111'),
(729, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU5898679', '48602', 83205, '0', 0.00, 12.000000, '1111'),
(730, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU4860669', '67803', 83205, '0', 0.00, 12.000000, '1111'),
(731, NULL, '2019-05-31 13:51:35', NULL, 66.00, NULL, 1.00, 'luis', 'PRU6502475', '29874', 83205, '0', 0.00, 12.000000, '1111'),
(732, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU9595592', '53769', 83205, '0', 0.00, 12.000000, '1111'),
(733, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU9086300', '32552', 83205, '0', 0.00, 12.000000, '1111'),
(734, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU2912069', '92854', 83205, '0', 0.00, 12.000000, '1111'),
(735, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU1796790', '78337', 83205, '0', 0.00, 12.000000, '1111'),
(736, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU8053640', '75646', 83205, '0', 0.00, 12.000000, '1111'),
(737, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU4753172', '25143', 83205, '0', 0.00, 12.000000, '1111'),
(738, NULL, '2019-05-31 13:51:37', NULL, 66.00, NULL, 1.00, 'JOSE', 'PRU5405157', '50114', 83205, '0', 0.00, 12.000000, '1111'),
(739, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU3189337', '84997', 83205, '0', 0.00, 12.000000, '1111'),
(740, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU2146598', '66389', 83205, '0', 0.00, 12.000000, '1111'),
(741, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU1221862', '24766', 83205, '0', 0.00, 12.000000, '1111'),
(742, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU8085569', '23907', 83205, '0', 0.00, 12.000000, '1111'),
(743, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU6064836', '24231', 83205, '0', 0.00, 12.000000, '1111'),
(744, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU2705300', '68494', 83205, '0', 0.00, 12.000000, '1111'),
(745, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU2715477', '68791', 83205, '0', 0.00, 12.000000, '1111'),
(746, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU2553449', '65824', 83205, '0', 0.00, 12.000000, '1111'),
(747, NULL, '2019-05-31 13:51:38', NULL, 66.00, NULL, 1.00, 'Pedro', 'PRU2780448', '45737', 83205, '0', 0.00, 12.000000, '1111'),
(748, NULL, '2019-05-31 13:51:39', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU4393533', '51717', 83205, '0', 0.00, 12.000000, '1111'),
(749, NULL, '2019-05-31 13:51:39', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU1691607', '58179', 83205, '0', 0.00, 12.000000, '1111'),
(750, NULL, '2019-05-31 13:51:39', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU4826616', '29675', 83205, '0', 0.00, 12.000000, '1111'),
(751, NULL, '2019-05-31 13:51:39', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU8409456', '95706', 83205, '0', 0.00, 12.000000, '1111'),
(752, NULL, '2019-05-31 13:51:39', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU7794082', '53446', 83205, '0', 0.00, 12.000000, '1111'),
(753, NULL, '2019-05-31 13:51:39', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU6934667', '62510', 83205, '0', 0.00, 12.000000, '1111'),
(754, NULL, '2019-05-31 13:51:39', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU1966258', '36737', 83205, '0', 0.00, 12.000000, '1111'),
(755, NULL, '2019-05-31 13:51:39', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU7545757', '82807', 83205, '0', 0.00, 12.000000, '1111'),
(756, NULL, '2019-05-31 13:51:39', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU2971377', '85293', 83205, '0', 0.00, 12.000000, '1111'),
(757, NULL, '2019-05-31 13:51:39', NULL, 54.00, NULL, 1.00, 'JOSE', 'PRU1317125', '52662', 83205, '0', 0.00, 12.000000, '1111'),
(758, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU6855268', '54205', 83205, '0', 0.00, 14.000000, '1111'),
(759, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU3958481', '57682', 83205, '0', 0.00, 14.000000, '1111'),
(760, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU1610155', '24847', 83205, '0', 0.00, 14.000000, '1111'),
(761, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU1227927', '60147', 83205, '0', 0.00, 14.000000, '1111'),
(762, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU1927090', '79200', 83205, '0', 0.00, 14.000000, '1111'),
(763, NULL, '2019-05-31 13:51:41', NULL, 50.00, NULL, 1.00, 'Pedro', 'PRU2014292', '31432', 83205, '0', 0.00, 14.000000, '1111'),
(764, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU8527333', '83444', 83205, '0', 0.00, 15.000000, '1111'),
(765, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU5202290', '79704', 83205, '0', 0.00, 15.000000, '1111'),
(766, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU9289152', '62795', 83205, '0', 0.00, 15.000000, '1111'),
(767, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU9637844', '86920', 83205, '0', 0.00, 15.000000, '1111'),
(768, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU8451945', '24228', 83205, '0', 0.00, 15.000000, '1111'),
(769, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU1627293', '30430', 83205, '0', 0.00, 15.000000, '1111'),
(770, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU2464518', '42434', 83205, '0', 0.00, 15.000000, '1111'),
(771, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU8303033', '86162', 83205, '0', 0.00, 15.000000, '1111'),
(772, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU1765526', '95413', 83205, '0', 0.00, 15.000000, '1111'),
(773, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU3522135', '35976', 83205, '0', 0.00, 15.000000, '1111'),
(774, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU3102621', '77865', 83205, '0', 0.00, 15.000000, '1111'),
(775, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU6705771', '64646', 83205, '0', 0.00, 15.000000, '1111'),
(776, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU8323795', '81887', 83205, '0', 0.00, 15.000000, '1111'),
(777, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU8142419', '62248', 83205, '0', 0.00, 15.000000, '1111'),
(778, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU7618406', '57764', 83205, '0', 0.00, 15.000000, '1111'),
(779, NULL, '2019-05-31 13:51:42', NULL, 50.00, NULL, 1.00, 'luis', 'PRU4123941', '19389', 83205, '0', 0.00, 15.000000, '1111'),
(780, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU8866631', '38685', 83205, '0', 0.00, 12.000000, '1111'),
(781, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU6755509', '54277', 83205, '0', 0.00, 12.000000, '1111'),
(782, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU6573249', '61914', 83205, '0', 0.00, 12.000000, '1111'),
(783, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU4710897', '52615', 83205, '0', 0.00, 12.000000, '1111'),
(784, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU6567800', '32344', 83205, '0', 0.00, 12.000000, '1111'),
(785, NULL, '2019-05-31 13:51:44', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU5988061', '13322', 83205, '0', 0.00, 12.000000, '1111'),
(786, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU3943870', '27273', 83205, '0', 0.00, 12.000000, '1111'),
(787, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU8090177', '71864', 83205, '0', 0.00, 12.000000, '1111'),
(788, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU9973923', '73314', 83205, '0', 0.00, 12.000000, '1111'),
(789, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU6583044', '58441', 83205, '0', 0.00, 12.000000, '1111'),
(790, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU5022807', '36667', 83205, '0', 0.00, 12.000000, '1111'),
(791, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU9772816', '49327', 83205, '0', 0.00, 12.000000, '1111'),
(792, NULL, '2019-05-31 13:51:45', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU6597426', '55025', 83205, '0', 0.00, 12.000000, '1111'),
(793, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU7295906', '83923', 83205, '0', 0.00, 12.000000, '1111');
INSERT INTO `detalle_kardex_material` (`id_detalle`, `fecha_ingreso`, `fecha_salida`, `fecha_devolucion`, `stock`, `cantidad_ingresada`, `cantidad_salida`, `trasladador_material`, `cod_kardex_material`, `comentario_devolucion`, `cod_area`, `estado_detalle_kardex`, `restante_detalle`, `costo_material`, `dni_usuario`) VALUES
(794, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU4025245', '19519', 83205, '0', 0.00, 12.000000, '1111'),
(795, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU8071531', '58047', 83205, '0', 0.00, 12.000000, '1111'),
(796, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU4645427', '41568', 83205, '0', 0.00, 12.000000, '1111'),
(797, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU4994313', '49842', 83205, '0', 0.00, 12.000000, '1111'),
(798, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU9944152', '99161', 83205, '0', 0.00, 12.000000, '1111'),
(799, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU5898679', '60559', 83205, '0', 0.00, 12.000000, '1111'),
(800, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU4860669', '12979', 83205, '0', 0.00, 12.000000, '1111'),
(801, NULL, '2019-05-31 13:51:46', NULL, 65.00, NULL, 1.00, 'luis', 'PRU6502475', '83786', 83205, '0', 0.00, 12.000000, '1111'),
(802, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU9595592', '14353', 83205, '0', 0.00, 12.000000, '1111'),
(803, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU9086300', '22820', 83205, '0', 0.00, 12.000000, '1111'),
(804, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU2912069', '54977', 83205, '0', 0.00, 12.000000, '1111'),
(805, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU1796790', '95866', 83205, '0', 0.00, 12.000000, '1111'),
(806, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU8053640', '32642', 83205, '0', 0.00, 12.000000, '1111'),
(807, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU4753172', '70455', 83205, '0', 0.00, 12.000000, '1111'),
(808, NULL, '2019-05-31 13:51:48', NULL, 65.00, NULL, 1.00, 'JOSE', 'PRU5405157', '86762', 83205, '0', 0.00, 12.000000, '1111'),
(809, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU3189337', '54131', 83205, '0', 0.00, 12.000000, '1111'),
(810, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU2146598', '79424', 83205, '0', 0.00, 12.000000, '1111'),
(811, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU1221862', '72923', 83205, '0', 0.00, 12.000000, '1111'),
(812, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU8085569', '98344', 83205, '0', 0.00, 12.000000, '1111'),
(813, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU6064836', '21617', 83205, '0', 0.00, 12.000000, '1111'),
(814, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU2705300', '35748', 83205, '0', 0.00, 12.000000, '1111'),
(815, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU2715477', '64644', 83205, '0', 0.00, 12.000000, '1111'),
(816, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU2553449', '22028', 83205, '0', 0.00, 12.000000, '1111'),
(817, NULL, '2019-05-31 13:51:49', NULL, 65.00, NULL, 1.00, 'Pedro', 'PRU2780448', '33080', 83205, '0', 0.00, 12.000000, '1111'),
(818, NULL, '2019-05-31 13:51:50', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU4393533', '63939', 83205, '0', 0.00, 12.000000, '1111'),
(819, NULL, '2019-05-31 13:51:50', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU1691607', '61975', 83205, '0', 0.00, 12.000000, '1111'),
(820, NULL, '2019-05-31 13:51:50', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU4826616', '52811', 83205, '0', 0.00, 12.000000, '1111'),
(821, NULL, '2019-05-31 13:51:50', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU8409456', '86416', 83205, '0', 0.00, 12.000000, '1111'),
(822, NULL, '2019-05-31 13:51:50', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU7794082', '49935', 83205, '0', 0.00, 12.000000, '1111'),
(823, NULL, '2019-05-31 13:51:50', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU6934667', '93899', 83205, '0', 0.00, 12.000000, '1111'),
(824, NULL, '2019-05-31 13:51:50', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU1966258', '71112', 83205, '0', 0.00, 12.000000, '1111'),
(825, NULL, '2019-05-31 13:51:50', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU7545757', '39134', 83205, '0', 0.00, 12.000000, '1111'),
(826, NULL, '2019-05-31 13:51:50', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU2971377', '93626', 83205, '0', 0.00, 12.000000, '1111'),
(827, NULL, '2019-05-31 13:51:50', NULL, 53.00, NULL, 1.00, 'JOSE', 'PRU1317125', '13255', 83205, '0', 0.00, 12.000000, '1111'),
(828, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU6855268', '67973', 83205, '0', 0.00, 14.000000, '1111'),
(829, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU3958481', '43496', 83205, '0', 0.00, 14.000000, '1111'),
(830, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU1610155', '66584', 83205, '0', 0.00, 14.000000, '1111'),
(831, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU1227927', '28177', 83205, '0', 0.00, 14.000000, '1111'),
(832, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU1927090', '28078', 83205, '0', 0.00, 14.000000, '1111'),
(833, NULL, '2019-05-31 13:51:52', NULL, 49.00, NULL, 1.00, 'Pedro', 'PRU2014292', '93164', 83205, '0', 0.00, 14.000000, '1111'),
(834, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU8527333', '92641', 83205, '0', 0.00, 15.000000, '1111'),
(835, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU5202290', '24703', 83205, '0', 0.00, 15.000000, '1111'),
(836, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU9289152', '70074', 83205, '0', 0.00, 15.000000, '1111'),
(837, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU9637844', '46761', 83205, '0', 0.00, 15.000000, '1111'),
(838, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU8451945', '97754', 83205, '0', 0.00, 15.000000, '1111'),
(839, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU1627293', '69668', 83205, '0', 0.00, 15.000000, '1111'),
(840, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU2464518', '83245', 83205, '0', 0.00, 15.000000, '1111'),
(841, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU8303033', '77895', 83205, '0', 0.00, 15.000000, '1111'),
(842, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU1765526', '78555', 83205, '0', 0.00, 15.000000, '1111'),
(843, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU3522135', '95261', 83205, '0', 0.00, 15.000000, '1111'),
(844, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU3102621', '84802', 83205, '0', 0.00, 15.000000, '1111'),
(845, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU6705771', '42831', 83205, '0', 0.00, 15.000000, '1111'),
(846, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU8323795', '36058', 83205, '0', 0.00, 15.000000, '1111'),
(847, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU8142419', '34500', 83205, '0', 0.00, 15.000000, '1111'),
(848, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU7618406', '81271', 83205, '0', 0.00, 15.000000, '1111'),
(849, NULL, '2019-05-31 13:51:53', NULL, 49.00, NULL, 1.00, 'luis', 'PRU4123941', '81066', 83205, '0', 0.00, 15.000000, '1111'),
(850, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU8866631', '73476', 83205, '0', 0.00, 12.000000, '1111'),
(851, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU6755509', '80663', 83205, '0', 0.00, 12.000000, '1111'),
(852, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU6573249', '62183', 83205, '0', 0.00, 12.000000, '1111'),
(853, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU4710897', '65783', 83205, '0', 0.00, 12.000000, '1111'),
(854, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU6567800', '18016', 83205, '0', 0.00, 12.000000, '1111'),
(855, NULL, '2019-05-31 13:51:56', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU5988061', '29142', 83205, '0', 0.00, 12.000000, '1111'),
(856, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU3943870', '89342', 83205, '0', 0.00, 12.000000, '1111'),
(857, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU8090177', '92533', 83205, '0', 0.00, 12.000000, '1111'),
(858, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU9973923', '79744', 83205, '0', 0.00, 12.000000, '1111'),
(859, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU6583044', '55231', 83205, '0', 0.00, 12.000000, '1111'),
(860, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU5022807', '59953', 83205, '0', 0.00, 12.000000, '1111'),
(861, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU9772816', '82829', 83205, '0', 0.00, 12.000000, '1111'),
(862, NULL, '2019-05-31 13:51:57', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU6597426', '49540', 83205, '0', 0.00, 12.000000, '1111'),
(863, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU7295906', '88159', 83205, '0', 0.00, 12.000000, '1111'),
(864, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU4025245', '27093', 83205, '0', 0.00, 12.000000, '1111'),
(865, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU8071531', '58775', 83205, '0', 0.00, 12.000000, '1111'),
(866, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU4645427', '44560', 83205, '0', 0.00, 12.000000, '1111'),
(867, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU4994313', '61151', 83205, '0', 0.00, 12.000000, '1111'),
(868, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU9944152', '80234', 83205, '0', 0.00, 12.000000, '1111'),
(869, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU5898679', '73828', 83205, '0', 0.00, 12.000000, '1111'),
(870, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU4860669', '14505', 83205, '0', 0.00, 12.000000, '1111'),
(871, NULL, '2019-05-31 13:51:58', NULL, 64.00, NULL, 1.00, 'luis', 'PRU6502475', '60084', 83205, '0', 0.00, 12.000000, '1111'),
(872, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU9595592', '71731', 83205, '0', 0.00, 12.000000, '1111'),
(873, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU9086300', '93164', 83205, '0', 0.00, 12.000000, '1111'),
(874, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU2912069', '96577', 83205, '0', 0.00, 12.000000, '1111'),
(875, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU1796790', '97155', 83205, '0', 0.00, 12.000000, '1111'),
(876, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU8053640', '73045', 83205, '0', 0.00, 12.000000, '1111'),
(877, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU4753172', '35377', 83205, '0', 0.00, 12.000000, '1111'),
(878, NULL, '2019-05-31 13:52:00', NULL, 64.00, NULL, 1.00, 'JOSE', 'PRU5405157', '18513', 83205, '0', 0.00, 12.000000, '1111'),
(879, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU3189337', '57227', 83205, '0', 0.00, 12.000000, '1111'),
(880, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU2146598', '49955', 83205, '0', 0.00, 12.000000, '1111'),
(881, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU1221862', '45956', 83205, '0', 0.00, 12.000000, '1111'),
(882, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU8085569', '74305', 83205, '0', 0.00, 12.000000, '1111'),
(883, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU6064836', '85297', 83205, '0', 0.00, 12.000000, '1111'),
(884, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU2705300', '23806', 83205, '0', 0.00, 12.000000, '1111'),
(885, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU2715477', '45356', 83205, '0', 0.00, 12.000000, '1111'),
(886, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU2553449', '83593', 83205, '0', 0.00, 12.000000, '1111'),
(887, NULL, '2019-05-31 13:52:01', NULL, 64.00, NULL, 1.00, 'Pedro', 'PRU2780448', '74194', 83205, '0', 0.00, 12.000000, '1111'),
(888, NULL, '2019-05-31 13:52:02', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU4393533', '45871', 83205, '0', 0.00, 12.000000, '1111'),
(889, NULL, '2019-05-31 13:52:02', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU1691607', '23424', 83205, '0', 0.00, 12.000000, '1111'),
(890, NULL, '2019-05-31 13:52:02', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU4826616', '56394', 83205, '0', 0.00, 12.000000, '1111'),
(891, NULL, '2019-05-31 13:52:02', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU8409456', '22234', 83205, '0', 0.00, 12.000000, '1111'),
(892, NULL, '2019-05-31 13:52:02', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU7794082', '24745', 83205, '0', 0.00, 12.000000, '1111'),
(893, NULL, '2019-05-31 13:52:02', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU6934667', '99486', 83205, '0', 0.00, 12.000000, '1111'),
(894, NULL, '2019-05-31 13:52:02', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU1966258', '98709', 83205, '0', 0.00, 12.000000, '1111'),
(895, NULL, '2019-05-31 13:52:02', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU7545757', '76745', 83205, '0', 0.00, 12.000000, '1111'),
(896, NULL, '2019-05-31 13:52:02', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU2971377', '20244', 83205, '0', 0.00, 12.000000, '1111'),
(897, NULL, '2019-05-31 13:52:02', NULL, 52.00, NULL, 1.00, 'JOSE', 'PRU1317125', '21917', 83205, '0', 0.00, 12.000000, '1111'),
(898, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU6855268', '15384', 83205, '0', 0.00, 14.000000, '1111'),
(899, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU3958481', '61204', 83205, '0', 0.00, 14.000000, '1111'),
(900, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU1610155', '55203', 83205, '0', 0.00, 14.000000, '1111'),
(901, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU1227927', '66547', 83205, '0', 0.00, 14.000000, '1111'),
(902, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU1927090', '15719', 83205, '0', 0.00, 14.000000, '1111'),
(903, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'Pedro', 'PRU2014292', '58985', 83205, '0', 0.00, 14.000000, '1111'),
(904, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU8527333', '48106', 83205, '0', 0.00, 15.000000, '1111'),
(905, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU5202290', '19346', 83205, '0', 0.00, 15.000000, '1111'),
(906, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU9289152', '90812', 83205, '0', 0.00, 15.000000, '1111'),
(907, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU9637844', '76487', 83205, '0', 0.00, 15.000000, '1111'),
(908, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU8451945', '41709', 83205, '0', 0.00, 15.000000, '1111'),
(909, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU1627293', '48861', 83205, '0', 0.00, 15.000000, '1111'),
(910, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU2464518', '31525', 83205, '0', 0.00, 15.000000, '1111'),
(911, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU8303033', '58327', 83205, '0', 0.00, 15.000000, '1111'),
(912, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU1765526', '26170', 83205, '0', 0.00, 15.000000, '1111'),
(913, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU3522135', '97205', 83205, '0', 0.00, 15.000000, '1111'),
(914, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU3102621', '50016', 83205, '0', 0.00, 15.000000, '1111'),
(915, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU6705771', '97749', 83205, '0', 0.00, 15.000000, '1111'),
(916, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU8323795', '60622', 83205, '0', 0.00, 15.000000, '1111'),
(917, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU8142419', '81410', 83205, '0', 0.00, 15.000000, '1111'),
(918, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU7618406', '28862', 83205, '0', 0.00, 15.000000, '1111'),
(919, NULL, '2019-05-31 13:52:04', NULL, 48.00, NULL, 1.00, 'luis', 'PRU4123941', '76994', 83205, '0', 0.00, 15.000000, '1111'),
(920, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU8866631', '53293', 83205, '0', 0.00, 12.000000, '1111'),
(921, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU6755509', '42067', 83205, '0', 0.00, 12.000000, '1111'),
(922, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU6573249', '65534', 83205, '0', 0.00, 12.000000, '1111'),
(923, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU4710897', '69445', 83205, '0', 0.00, 12.000000, '1111'),
(924, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU6567800', '79865', 83205, '0', 0.00, 12.000000, '1111'),
(925, NULL, '2019-05-31 13:52:06', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU5988061', '65193', 83205, '0', 0.00, 12.000000, '1111'),
(926, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU3943870', '25754', 83205, '0', 0.00, 12.000000, '1111'),
(927, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU8090177', '24105', 83205, '0', 0.00, 12.000000, '1111'),
(928, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU9973923', '10170', 83205, '0', 0.00, 12.000000, '1111'),
(929, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU6583044', '29253', 83205, '0', 0.00, 12.000000, '1111'),
(930, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU5022807', '58430', 83205, '0', 0.00, 12.000000, '1111'),
(931, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU9772816', '89216', 83205, '0', 0.00, 12.000000, '1111'),
(932, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU6597426', '24002', 83205, '0', 0.00, 12.000000, '1111'),
(933, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU7295906', '60443', 83205, '0', 0.00, 12.000000, '1111'),
(934, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU4025245', '40864', 83205, '0', 0.00, 12.000000, '1111'),
(935, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU8071531', '34189', 83205, '0', 0.00, 12.000000, '1111'),
(936, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU4645427', '88502', 83205, '0', 0.00, 12.000000, '1111'),
(937, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU4994313', '39861', 83205, '0', 0.00, 12.000000, '1111'),
(938, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU9944152', '92550', 83205, '0', 0.00, 12.000000, '1111'),
(939, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU5898679', '19441', 83205, '0', 0.00, 12.000000, '1111'),
(940, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU4860669', '23817', 83205, '0', 0.00, 12.000000, '1111'),
(941, NULL, '2019-05-31 13:52:07', NULL, 63.00, NULL, 1.00, 'luis', 'PRU6502475', '90136', 83205, '0', 0.00, 12.000000, '1111'),
(942, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU9595592', '73949', 83205, '0', 0.00, 12.000000, '1111'),
(943, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU9086300', '23851', 83205, '0', 0.00, 12.000000, '1111'),
(944, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU2912069', '11915', 83205, '0', 0.00, 12.000000, '1111'),
(945, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU1796790', '13765', 83205, '0', 0.00, 12.000000, '1111'),
(946, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU8053640', '20771', 83205, '0', 0.00, 12.000000, '1111'),
(947, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU4753172', '73154', 83205, '0', 0.00, 12.000000, '1111'),
(948, NULL, '2019-05-31 13:52:09', NULL, 63.00, NULL, 1.00, 'JOSE', 'PRU5405157', '51106', 83205, '0', 0.00, 12.000000, '1111'),
(949, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU3189337', '70187', 83205, '0', 0.00, 12.000000, '1111'),
(950, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU2146598', '60474', 83205, '0', 0.00, 12.000000, '1111'),
(951, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU1221862', '58853', 83205, '0', 0.00, 12.000000, '1111'),
(952, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU8085569', '55797', 83205, '0', 0.00, 12.000000, '1111'),
(953, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU6064836', '53369', 83205, '0', 0.00, 12.000000, '1111'),
(954, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU2705300', '87847', 83205, '0', 0.00, 12.000000, '1111'),
(955, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU2715477', '38635', 83205, '0', 0.00, 12.000000, '1111'),
(956, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU2553449', '40185', 83205, '0', 0.00, 12.000000, '1111'),
(957, NULL, '2019-05-31 13:52:10', NULL, 63.00, NULL, 1.00, 'Pedro', 'PRU2780448', '53550', 83205, '0', 0.00, 12.000000, '1111'),
(958, NULL, '2019-05-31 13:52:11', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU4393533', '79287', 83205, '0', 0.00, 12.000000, '1111'),
(959, NULL, '2019-05-31 13:52:11', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU1691607', '41425', 83205, '0', 0.00, 12.000000, '1111'),
(960, NULL, '2019-05-31 13:52:11', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU4826616', '57936', 83205, '0', 0.00, 12.000000, '1111'),
(961, NULL, '2019-05-31 13:52:11', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU8409456', '44917', 83205, '0', 0.00, 12.000000, '1111'),
(962, NULL, '2019-05-31 13:52:11', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU7794082', '86630', 83205, '0', 0.00, 12.000000, '1111'),
(963, NULL, '2019-05-31 13:52:11', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU6934667', '81534', 83205, '0', 0.00, 12.000000, '1111'),
(964, NULL, '2019-05-31 13:52:11', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU1966258', '23210', 83205, '0', 0.00, 12.000000, '1111'),
(965, NULL, '2019-05-31 13:52:11', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU7545757', '35678', 83205, '0', 0.00, 12.000000, '1111'),
(966, NULL, '2019-05-31 13:52:11', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU2971377', '82517', 83205, '0', 0.00, 12.000000, '1111'),
(967, NULL, '2019-05-31 13:52:11', NULL, 51.00, NULL, 1.00, 'JOSE', 'PRU1317125', '96747', 83205, '0', 0.00, 12.000000, '1111'),
(968, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU6855268', '98119', 83205, '0', 0.00, 14.000000, '1111'),
(969, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU3958481', '18563', 83205, '0', 0.00, 14.000000, '1111'),
(970, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU1610155', '80667', 83205, '0', 0.00, 14.000000, '1111'),
(971, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU1227927', '32050', 83205, '0', 0.00, 14.000000, '1111'),
(972, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU1927090', '17979', 83205, '0', 0.00, 14.000000, '1111'),
(973, NULL, '2019-05-31 13:52:13', NULL, 47.00, NULL, 1.00, 'Pedro', 'PRU2014292', '72365', 83205, '0', 0.00, 14.000000, '1111'),
(974, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU8527333', '47031', 83205, '0', 0.00, 15.000000, '1111'),
(975, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU5202290', '21122', 83205, '0', 0.00, 15.000000, '1111'),
(976, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU9289152', '82455', 83205, '0', 0.00, 15.000000, '1111'),
(977, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU9637844', '86882', 83205, '0', 0.00, 15.000000, '1111'),
(978, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU8451945', '58462', 83205, '0', 0.00, 15.000000, '1111'),
(979, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU1627293', '97392', 83205, '0', 0.00, 15.000000, '1111'),
(980, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU2464518', '99157', 83205, '0', 0.00, 15.000000, '1111'),
(981, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU8303033', '34708', 83205, '0', 0.00, 15.000000, '1111'),
(982, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU1765526', '98330', 83205, '0', 0.00, 15.000000, '1111'),
(983, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU3522135', '30062', 83205, '0', 0.00, 15.000000, '1111'),
(984, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU3102621', '18877', 83205, '0', 0.00, 15.000000, '1111'),
(985, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU6705771', '61694', 83205, '0', 0.00, 15.000000, '1111'),
(986, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU8323795', '93876', 83205, '0', 0.00, 15.000000, '1111'),
(987, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU8142419', '30618', 83205, '0', 0.00, 15.000000, '1111'),
(988, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU7618406', '75515', 83205, '0', 0.00, 15.000000, '1111'),
(989, NULL, '2019-05-31 13:52:14', NULL, 47.00, NULL, 1.00, 'luis', 'PRU4123941', '92869', 83205, '0', 0.00, 15.000000, '1111'),
(990, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU8866631', '61063', 83205, '0', 0.00, 12.000000, '1111'),
(991, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU6755509', '45837', 83205, '0', 0.00, 12.000000, '1111'),
(992, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU6573249', '53448', 83205, '0', 0.00, 12.000000, '1111'),
(993, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU4710897', '72208', 83205, '0', 0.00, 12.000000, '1111'),
(994, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU6567800', '52129', 83205, '0', 0.00, 12.000000, '1111'),
(995, NULL, '2019-05-31 13:52:16', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU5988061', '53220', 83205, '0', 0.00, 12.000000, '1111'),
(996, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU3943870', '77391', 83205, '0', 0.00, 12.000000, '1111'),
(997, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU8090177', '42484', 83205, '0', 0.00, 12.000000, '1111'),
(998, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU9973923', '21163', 83205, '0', 0.00, 12.000000, '1111'),
(999, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU6583044', '94645', 83205, '0', 0.00, 12.000000, '1111'),
(1000, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU5022807', '98354', 83205, '0', 0.00, 12.000000, '1111'),
(1001, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU9772816', '68618', 83205, '0', 0.00, 12.000000, '1111'),
(1002, NULL, '2019-05-31 13:52:17', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU6597426', '51022', 83205, '0', 0.00, 12.000000, '1111'),
(1003, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU7295906', '68730', 83205, '0', 0.00, 12.000000, '1111'),
(1004, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU4025245', '52734', 83205, '0', 0.00, 12.000000, '1111'),
(1005, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU8071531', '17914', 83205, '0', 0.00, 12.000000, '1111'),
(1006, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU4645427', '23539', 83205, '0', 0.00, 12.000000, '1111'),
(1007, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU4994313', '33740', 83205, '0', 0.00, 12.000000, '1111'),
(1008, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU9944152', '67325', 83205, '0', 0.00, 12.000000, '1111'),
(1009, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU5898679', '21948', 83205, '0', 0.00, 12.000000, '1111'),
(1010, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU4860669', '91866', 83205, '0', 0.00, 12.000000, '1111'),
(1011, NULL, '2019-05-31 13:52:18', NULL, 62.00, NULL, 1.00, 'luis', 'PRU6502475', '41005', 83205, '0', 0.00, 12.000000, '1111'),
(1012, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU9595592', '44897', 83205, '0', 0.00, 12.000000, '1111'),
(1013, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU9086300', '67088', 83205, '0', 0.00, 12.000000, '1111'),
(1014, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU2912069', '89381', 83205, '0', 0.00, 12.000000, '1111'),
(1015, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU1796790', '90009', 83205, '0', 0.00, 12.000000, '1111'),
(1016, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU8053640', '31826', 83205, '0', 0.00, 12.000000, '1111'),
(1017, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU4753172', '44372', 83205, '0', 0.00, 12.000000, '1111'),
(1018, NULL, '2019-05-31 13:52:20', NULL, 62.00, NULL, 1.00, 'JOSE', 'PRU5405157', '31052', 83205, '0', 0.00, 12.000000, '1111'),
(1019, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU3189337', '85164', 83205, '0', 0.00, 12.000000, '1111'),
(1020, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU2146598', '24760', 83205, '0', 0.00, 12.000000, '1111'),
(1021, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU1221862', '85158', 83205, '0', 0.00, 12.000000, '1111'),
(1022, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU8085569', '74514', 83205, '0', 0.00, 12.000000, '1111'),
(1023, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU6064836', '82127', 83205, '0', 0.00, 12.000000, '1111'),
(1024, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU2705300', '82238', 83205, '0', 0.00, 12.000000, '1111'),
(1025, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU2715477', '21549', 83205, '0', 0.00, 12.000000, '1111'),
(1026, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU2553449', '41792', 83205, '0', 0.00, 12.000000, '1111'),
(1027, NULL, '2019-05-31 13:52:21', NULL, 62.00, NULL, 1.00, 'Pedro', 'PRU2780448', '42179', 83205, '0', 0.00, 12.000000, '1111'),
(1028, NULL, '2019-05-31 13:52:22', NULL, 41.00, NULL, 1.00, 'JOSE', 'PRU4393533', '36420', 83205, '0', 0.00, 12.000000, '1111'),
(1029, NULL, '2019-05-31 13:52:22', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU1691607', '72972', 83205, '0', 0.00, 12.000000, '1111'),
(1030, NULL, '2019-05-31 13:52:22', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU4826616', '28493', 83205, '0', 0.00, 12.000000, '1111'),
(1031, NULL, '2019-05-31 13:52:22', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU8409456', '78371', 83205, '0', 0.00, 12.000000, '1111'),
(1032, NULL, '2019-05-31 13:52:22', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU7794082', '28640', 83205, '0', 0.00, 12.000000, '1111'),
(1033, NULL, '2019-05-31 13:52:22', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU6934667', '26616', 83205, '0', 0.00, 12.000000, '1111'),
(1034, NULL, '2019-05-31 13:52:22', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU1966258', '18003', 83205, '0', 0.00, 12.000000, '1111'),
(1035, NULL, '2019-05-31 13:52:22', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU7545757', '23029', 83205, '0', 0.00, 12.000000, '1111'),
(1036, NULL, '2019-05-31 13:52:22', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU2971377', '37272', 83205, '0', 0.00, 12.000000, '1111'),
(1037, NULL, '2019-05-31 13:52:22', NULL, 50.00, NULL, 1.00, 'JOSE', 'PRU1317125', '67786', 83205, '0', 0.00, 12.000000, '1111'),
(1038, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU6855268', '66232', 83205, '0', 0.00, 14.000000, '1111'),
(1039, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU3958481', '81335', 83205, '0', 0.00, 14.000000, '1111'),
(1040, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU1610155', '22205', 83205, '0', 0.00, 14.000000, '1111'),
(1041, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU1227927', '93174', 83205, '0', 0.00, 14.000000, '1111'),
(1042, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU1927090', '17854', 83205, '0', 0.00, 14.000000, '1111'),
(1043, NULL, '2019-05-31 13:52:24', NULL, 46.00, NULL, 1.00, 'Pedro', 'PRU2014292', '89757', 83205, '0', 0.00, 14.000000, '1111'),
(1044, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU8527333', '46845', 83205, '0', 0.00, 15.000000, '1111'),
(1045, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU5202290', '93957', 83205, '0', 0.00, 15.000000, '1111'),
(1046, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU9289152', '30635', 83205, '0', 0.00, 15.000000, '1111'),
(1047, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU9637844', '47497', 83205, '0', 0.00, 15.000000, '1111'),
(1048, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU8451945', '59528', 83205, '0', 0.00, 15.000000, '1111'),
(1049, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU1627293', '92886', 83205, '0', 0.00, 15.000000, '1111'),
(1050, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU2464518', '82000', 83205, '0', 0.00, 15.000000, '1111'),
(1051, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU8303033', '15655', 83205, '0', 0.00, 15.000000, '1111'),
(1052, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU1765526', '53483', 83205, '0', 0.00, 15.000000, '1111'),
(1053, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU3522135', '60961', 83205, '0', 0.00, 15.000000, '1111'),
(1054, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU3102621', '22924', 83205, '0', 0.00, 15.000000, '1111'),
(1055, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU6705771', '98639', 83205, '0', 0.00, 15.000000, '1111'),
(1056, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU8323795', '64880', 83205, '0', 0.00, 15.000000, '1111'),
(1057, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU8142419', '82001', 83205, '0', 0.00, 15.000000, '1111'),
(1058, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU7618406', '93441', 83205, '0', 0.00, 15.000000, '1111'),
(1059, NULL, '2019-05-31 13:52:25', NULL, 46.00, NULL, 1.00, 'luis', 'PRU4123941', '58387', 83205, '0', 0.00, 15.000000, '1111'),
(1060, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU8866631', '49375', 83205, '0', 0.00, 12.000000, '1111'),
(1061, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU6755509', '46678', 83205, '0', 0.00, 12.000000, '1111'),
(1062, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU6573249', '38488', 83205, '0', 0.00, 12.000000, '1111'),
(1063, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU4710897', '37472', 83205, '0', 0.00, 12.000000, '1111'),
(1064, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU6567800', '64584', 83205, '0', 0.00, 12.000000, '1111'),
(1065, NULL, '2019-05-31 13:52:27', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU5988061', '83429', 83205, '0', 0.00, 12.000000, '1111'),
(1066, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU3943870', '41803', 83205, '0', 0.00, 12.000000, '1111'),
(1067, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU8090177', '40155', 83205, '0', 0.00, 12.000000, '1111'),
(1068, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU9973923', '19182', 83205, '0', 0.00, 12.000000, '1111'),
(1069, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU6583044', '84097', 83205, '0', 0.00, 12.000000, '1111'),
(1070, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU5022807', '12884', 83205, '0', 0.00, 12.000000, '1111'),
(1071, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU9772816', '37298', 83205, '0', 0.00, 12.000000, '1111'),
(1072, NULL, '2019-05-31 13:52:28', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU6597426', '80606', 83205, '0', 0.00, 12.000000, '1111'),
(1073, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU7295906', '16205', 83205, '0', 0.00, 12.000000, '1111'),
(1074, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU4025245', '46859', 83205, '0', 0.00, 12.000000, '1111'),
(1075, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU8071531', '25612', 83205, '0', 0.00, 12.000000, '1111'),
(1076, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU4645427', '86199', 83205, '0', 0.00, 12.000000, '1111'),
(1077, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU4994313', '71016', 83205, '0', 0.00, 12.000000, '1111'),
(1078, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU9944152', '88677', 83205, '0', 0.00, 12.000000, '1111'),
(1079, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU5898679', '85027', 83205, '0', 0.00, 12.000000, '1111'),
(1080, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU4860669', '48814', 83205, '0', 0.00, 12.000000, '1111'),
(1081, NULL, '2019-05-31 13:52:29', NULL, 61.00, NULL, 1.00, 'luis', 'PRU6502475', '27455', 83205, '0', 0.00, 12.000000, '1111'),
(1082, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU9595592', '56773', 83205, '0', 0.00, 12.000000, '1111'),
(1083, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU9086300', '87955', 83205, '0', 0.00, 12.000000, '1111'),
(1084, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU2912069', '40210', 83205, '0', 0.00, 12.000000, '1111'),
(1085, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU1796790', '86940', 83205, '0', 0.00, 12.000000, '1111'),
(1086, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU8053640', '89543', 83205, '0', 0.00, 12.000000, '1111'),
(1087, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU4753172', '33380', 83205, '0', 0.00, 12.000000, '1111'),
(1088, NULL, '2019-05-31 13:52:31', NULL, 61.00, NULL, 1.00, 'JOSE', 'PRU5405157', '95992', 83205, '0', 0.00, 12.000000, '1111'),
(1089, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU3189337', '79288', 83205, '0', 0.00, 12.000000, '1111'),
(1090, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU2146598', '30923', 83205, '0', 0.00, 12.000000, '1111'),
(1091, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU1221862', '59334', 83205, '0', 0.00, 12.000000, '1111'),
(1092, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU8085569', '27708', 83205, '0', 0.00, 12.000000, '1111'),
(1093, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU6064836', '71194', 83205, '0', 0.00, 12.000000, '1111'),
(1094, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU2705300', '88074', 83205, '0', 0.00, 12.000000, '1111'),
(1095, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU2715477', '53402', 83205, '0', 0.00, 12.000000, '1111'),
(1096, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU2553449', '42227', 83205, '0', 0.00, 12.000000, '1111'),
(1097, NULL, '2019-05-31 13:52:32', NULL, 61.00, NULL, 1.00, 'Pedro', 'PRU2780448', '93031', 83205, '0', 0.00, 12.000000, '1111'),
(1098, NULL, '2019-05-31 13:52:34', NULL, 40.00, NULL, 1.00, 'JOSE', 'PRU4393533', '39968', 83205, '0', 0.00, 12.000000, '1111'),
(1099, NULL, '2019-05-31 13:52:34', NULL, 41.00, NULL, 1.00, 'JOSE', 'PRU1691607', '35345', 83205, '0', 0.00, 12.000000, '1111'),
(1100, NULL, '2019-05-31 13:52:34', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU4826616', '61641', 83205, '0', 0.00, 12.000000, '1111'),
(1101, NULL, '2019-05-31 13:52:34', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU8409456', '83039', 83205, '0', 0.00, 12.000000, '1111'),
(1102, NULL, '2019-05-31 13:52:34', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU7794082', '75468', 83205, '0', 0.00, 12.000000, '1111'),
(1103, NULL, '2019-05-31 13:52:34', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU6934667', '48063', 83205, '0', 0.00, 12.000000, '1111'),
(1104, NULL, '2019-05-31 13:52:34', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU1966258', '84635', 83205, '0', 0.00, 12.000000, '1111'),
(1105, NULL, '2019-05-31 13:52:34', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU7545757', '35572', 83205, '0', 0.00, 12.000000, '1111'),
(1106, NULL, '2019-05-31 13:52:34', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU2971377', '90370', 83205, '0', 0.00, 12.000000, '1111'),
(1107, NULL, '2019-05-31 13:52:34', NULL, 49.00, NULL, 1.00, 'JOSE', 'PRU1317125', '85644', 83205, '0', 0.00, 12.000000, '1111'),
(1108, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU6855268', '12207', 83205, '0', 0.00, 14.000000, '1111'),
(1109, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU3958481', '61642', 83205, '0', 0.00, 14.000000, '1111'),
(1110, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU1610155', '36675', 83205, '0', 0.00, 14.000000, '1111'),
(1111, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU1227927', '89562', 83205, '0', 0.00, 14.000000, '1111'),
(1112, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU1927090', '79341', 83205, '0', 0.00, 14.000000, '1111'),
(1113, NULL, '2019-05-31 13:52:35', NULL, 45.00, NULL, 1.00, 'Pedro', 'PRU2014292', '72626', 83205, '0', 0.00, 14.000000, '1111'),
(1114, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU8527333', '24281', 83205, '0', 0.00, 15.000000, '1111'),
(1115, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU5202290', '76554', 83205, '0', 0.00, 15.000000, '1111'),
(1116, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU9289152', '45221', 83205, '0', 0.00, 15.000000, '1111'),
(1117, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU9637844', '33610', 83205, '0', 0.00, 15.000000, '1111'),
(1118, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU8451945', '87621', 83205, '0', 0.00, 15.000000, '1111'),
(1119, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU1627293', '39029', 83205, '0', 0.00, 15.000000, '1111'),
(1120, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU2464518', '55107', 83205, '0', 0.00, 15.000000, '1111'),
(1121, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU8303033', '80243', 83205, '0', 0.00, 15.000000, '1111'),
(1122, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU1765526', '29669', 83205, '0', 0.00, 15.000000, '1111'),
(1123, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU3522135', '13910', 83205, '0', 0.00, 15.000000, '1111'),
(1124, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU3102621', '49541', 83205, '0', 0.00, 15.000000, '1111'),
(1125, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU6705771', '23937', 83205, '0', 0.00, 15.000000, '1111'),
(1126, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU8323795', '47921', 83205, '0', 0.00, 15.000000, '1111'),
(1127, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU8142419', '38091', 83205, '0', 0.00, 15.000000, '1111'),
(1128, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU7618406', '70947', 83205, '0', 0.00, 15.000000, '1111'),
(1129, NULL, '2019-05-31 13:52:36', NULL, 45.00, NULL, 1.00, 'luis', 'PRU4123941', '97178', 83205, '0', 0.00, 15.000000, '1111'),
(1130, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU8866631', '31347', 83205, '0', 0.00, 12.000000, '1111'),
(1131, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU6755509', '93926', 83205, '0', 0.00, 12.000000, '1111'),
(1132, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU6573249', '19264', 83205, '0', 0.00, 12.000000, '1111'),
(1133, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU4710897', '92477', 83205, '0', 0.00, 12.000000, '1111'),
(1134, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU6567800', '93582', 83205, '0', 0.00, 12.000000, '1111'),
(1135, NULL, '2019-05-31 13:52:38', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU5988061', '92844', 83205, '0', 0.00, 12.000000, '1111'),
(1136, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU3943870', '57596', 83205, '0', 0.00, 12.000000, '1111'),
(1137, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU8090177', '12136', 83205, '0', 0.00, 12.000000, '1111'),
(1138, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU9973923', '31439', 83205, '0', 0.00, 12.000000, '1111'),
(1139, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU6583044', '84859', 83205, '0', 0.00, 12.000000, '1111'),
(1140, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU5022807', '81406', 83205, '0', 0.00, 12.000000, '1111'),
(1141, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU9772816', '38023', 83205, '0', 0.00, 12.000000, '1111'),
(1142, NULL, '2019-05-31 13:52:39', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU6597426', '61770', 83205, '0', 0.00, 12.000000, '1111'),
(1143, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU7295906', '29646', 83205, '0', 0.00, 12.000000, '1111'),
(1144, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU4025245', '75081', 83205, '0', 0.00, 12.000000, '1111'),
(1145, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU8071531', '48330', 83205, '0', 0.00, 12.000000, '1111'),
(1146, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU4645427', '21172', 83205, '0', 0.00, 12.000000, '1111'),
(1147, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU4994313', '21428', 83205, '0', 0.00, 12.000000, '1111'),
(1148, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU9944152', '96045', 83205, '0', 0.00, 12.000000, '1111'),
(1149, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU5898679', '19402', 83205, '0', 0.00, 12.000000, '1111'),
(1150, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU4860669', '38709', 83205, '0', 0.00, 12.000000, '1111'),
(1151, NULL, '2019-05-31 13:52:41', NULL, 60.00, NULL, 1.00, 'luis', 'PRU6502475', '15735', 83205, '0', 0.00, 12.000000, '1111'),
(1152, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU9595592', '89407', 83205, '0', 0.00, 12.000000, '1111'),
(1153, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU9086300', '73254', 83205, '0', 0.00, 12.000000, '1111'),
(1154, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU2912069', '82629', 83205, '0', 0.00, 12.000000, '1111'),
(1155, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU1796790', '77073', 83205, '0', 0.00, 12.000000, '1111'),
(1156, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU8053640', '68560', 83205, '0', 0.00, 12.000000, '1111'),
(1157, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU4753172', '40599', 83205, '0', 0.00, 12.000000, '1111'),
(1158, NULL, '2019-05-31 13:52:42', NULL, 60.00, NULL, 1.00, 'JOSE', 'PRU5405157', '47029', 83205, '0', 0.00, 12.000000, '1111'),
(1159, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU3189337', '42644', 83205, '0', 0.00, 12.000000, '1111'),
(1160, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU2146598', '71730', 83205, '0', 0.00, 12.000000, '1111'),
(1161, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU1221862', '91363', 83205, '0', 0.00, 12.000000, '1111'),
(1162, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU8085569', '22182', 83205, '0', 0.00, 12.000000, '1111'),
(1163, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU6064836', '61107', 83205, '0', 0.00, 12.000000, '1111'),
(1164, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU2705300', '24636', 83205, '0', 0.00, 12.000000, '1111'),
(1165, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU2715477', '58223', 83205, '0', 0.00, 12.000000, '1111'),
(1166, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU2553449', '55856', 83205, '0', 0.00, 12.000000, '1111'),
(1167, NULL, '2019-05-31 13:52:44', NULL, 60.00, NULL, 1.00, 'Pedro', 'PRU2780448', '20341', 83205, '0', 0.00, 12.000000, '1111'),
(1168, NULL, '2019-05-31 13:52:46', NULL, 39.00, NULL, 1.00, 'JOSE', 'PRU4393533', '38558', 83205, '0', 0.00, 12.000000, '1111'),
(1169, NULL, '2019-05-31 13:52:46', NULL, 40.00, NULL, 1.00, 'JOSE', 'PRU1691607', '44199', 83205, '0', 0.00, 12.000000, '1111'),
(1170, NULL, '2019-05-31 13:52:46', NULL, 41.00, NULL, 1.00, 'JOSE', 'PRU4826616', '27924', 83205, '0', 0.00, 12.000000, '1111'),
(1171, NULL, '2019-05-31 13:52:46', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU8409456', '63564', 83205, '0', 0.00, 12.000000, '1111'),
(1172, NULL, '2019-05-31 13:52:46', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU7794082', '74292', 83205, '0', 0.00, 12.000000, '1111'),
(1173, NULL, '2019-05-31 13:52:46', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU6934667', '44372', 83205, '0', 0.00, 12.000000, '1111'),
(1174, NULL, '2019-05-31 13:52:46', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU1966258', '33056', 83205, '0', 0.00, 12.000000, '1111'),
(1175, NULL, '2019-05-31 13:52:46', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU7545757', '83896', 83205, '0', 0.00, 12.000000, '1111'),
(1176, NULL, '2019-05-31 13:52:46', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU2971377', '36756', 83205, '0', 0.00, 12.000000, '1111'),
(1177, NULL, '2019-05-31 13:52:46', NULL, 48.00, NULL, 1.00, 'JOSE', 'PRU1317125', '81174', 83205, '0', 0.00, 12.000000, '1111'),
(1178, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU6855268', '30506', 83205, '0', 0.00, 14.000000, '1111'),
(1179, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU3958481', '75236', 83205, '0', 0.00, 14.000000, '1111'),
(1180, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU1610155', '94758', 83205, '0', 0.00, 14.000000, '1111'),
(1181, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU1227927', '34892', 83205, '0', 0.00, 14.000000, '1111');
INSERT INTO `detalle_kardex_material` (`id_detalle`, `fecha_ingreso`, `fecha_salida`, `fecha_devolucion`, `stock`, `cantidad_ingresada`, `cantidad_salida`, `trasladador_material`, `cod_kardex_material`, `comentario_devolucion`, `cod_area`, `estado_detalle_kardex`, `restante_detalle`, `costo_material`, `dni_usuario`) VALUES
(1182, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU1927090', '79158', 83205, '0', 0.00, 14.000000, '1111'),
(1183, NULL, '2019-05-31 13:52:48', NULL, 44.00, NULL, 1.00, 'Pedro', 'PRU2014292', '12247', 83205, '0', 0.00, 14.000000, '1111'),
(1184, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU8527333', '52909', 83205, '0', 0.00, 15.000000, '1111'),
(1185, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU5202290', '83414', 83205, '0', 0.00, 15.000000, '1111'),
(1186, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU9289152', '79685', 83205, '0', 0.00, 15.000000, '1111'),
(1187, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU9637844', '84410', 83205, '0', 0.00, 15.000000, '1111'),
(1188, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU8451945', '13097', 83205, '0', 0.00, 15.000000, '1111'),
(1189, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU1627293', '93399', 83205, '0', 0.00, 15.000000, '1111'),
(1190, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU2464518', '80869', 83205, '0', 0.00, 15.000000, '1111'),
(1191, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU8303033', '24911', 83205, '0', 0.00, 15.000000, '1111'),
(1192, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU1765526', '33527', 83205, '0', 0.00, 15.000000, '1111'),
(1193, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU3522135', '81351', 83205, '0', 0.00, 15.000000, '1111'),
(1194, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU3102621', '44758', 83205, '0', 0.00, 15.000000, '1111'),
(1195, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU6705771', '22539', 83205, '0', 0.00, 15.000000, '1111'),
(1196, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU8323795', '90853', 83205, '0', 0.00, 15.000000, '1111'),
(1197, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU8142419', '67396', 83205, '0', 0.00, 15.000000, '1111'),
(1198, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU7618406', '25409', 83205, '0', 0.00, 15.000000, '1111'),
(1199, NULL, '2019-05-31 13:52:49', NULL, 44.00, NULL, 1.00, 'luis', 'PRU4123941', '95397', 83205, '0', 0.00, 15.000000, '1111'),
(1200, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU8866631', '71767', 83205, '0', 0.00, 12.000000, '1111'),
(1201, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU6755509', '34469', 83205, '0', 0.00, 12.000000, '1111'),
(1202, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU6573249', '89652', 83205, '0', 0.00, 12.000000, '1111'),
(1203, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU4710897', '21242', 83205, '0', 0.00, 12.000000, '1111'),
(1204, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU6567800', '93897', 83205, '0', 0.00, 12.000000, '1111'),
(1205, NULL, '2019-05-31 13:52:51', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU5988061', '52137', 83205, '0', 0.00, 12.000000, '1111'),
(1206, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU3943870', '82015', 83205, '0', 0.00, 12.000000, '1111'),
(1207, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU8090177', '79288', 83205, '0', 0.00, 12.000000, '1111'),
(1208, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU9973923', '73878', 83205, '0', 0.00, 12.000000, '1111'),
(1209, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU6583044', '82151', 83205, '0', 0.00, 12.000000, '1111'),
(1210, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU5022807', '64279', 83205, '0', 0.00, 12.000000, '1111'),
(1211, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU9772816', '68426', 83205, '0', 0.00, 12.000000, '1111'),
(1212, NULL, '2019-05-31 13:52:52', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU6597426', '56196', 83205, '0', 0.00, 12.000000, '1111'),
(1213, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU7295906', '23770', 83205, '0', 0.00, 12.000000, '1111'),
(1214, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU4025245', '90344', 83205, '0', 0.00, 12.000000, '1111'),
(1215, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU8071531', '13936', 83205, '0', 0.00, 12.000000, '1111'),
(1216, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU4645427', '82320', 83205, '0', 0.00, 12.000000, '1111'),
(1217, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU4994313', '12820', 83205, '0', 0.00, 12.000000, '1111'),
(1218, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU9944152', '11390', 83205, '0', 0.00, 12.000000, '1111'),
(1219, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU5898679', '36326', 83205, '0', 0.00, 12.000000, '1111'),
(1220, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU4860669', '17160', 83205, '0', 0.00, 12.000000, '1111'),
(1221, NULL, '2019-05-31 13:52:54', NULL, 59.00, NULL, 1.00, 'luis', 'PRU6502475', '90482', 83205, '0', 0.00, 12.000000, '1111'),
(1222, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU9595592', '11481', 83205, '0', 0.00, 12.000000, '1111'),
(1223, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU9086300', '83898', 83205, '0', 0.00, 12.000000, '1111'),
(1224, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU2912069', '67119', 83205, '0', 0.00, 12.000000, '1111'),
(1225, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU1796790', '36167', 83205, '0', 0.00, 12.000000, '1111'),
(1226, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU8053640', '85226', 83205, '0', 0.00, 12.000000, '1111'),
(1227, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU4753172', '47463', 83205, '0', 0.00, 12.000000, '1111'),
(1228, NULL, '2019-05-31 13:52:56', NULL, 59.00, NULL, 1.00, 'JOSE', 'PRU5405157', '25913', 83205, '0', 0.00, 12.000000, '1111'),
(1229, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU3189337', '88611', 83205, '0', 0.00, 12.000000, '1111'),
(1230, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU2146598', '75061', 83205, '0', 0.00, 12.000000, '1111'),
(1231, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU1221862', '41122', 83205, '0', 0.00, 12.000000, '1111'),
(1232, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU8085569', '69101', 83205, '0', 0.00, 12.000000, '1111'),
(1233, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU6064836', '77924', 83205, '0', 0.00, 12.000000, '1111'),
(1234, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU2705300', '48842', 83205, '0', 0.00, 12.000000, '1111'),
(1235, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU2715477', '48400', 83205, '0', 0.00, 12.000000, '1111'),
(1236, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU2553449', '33399', 83205, '0', 0.00, 12.000000, '1111'),
(1237, NULL, '2019-05-31 13:52:57', NULL, 59.00, NULL, 1.00, 'Pedro', 'PRU2780448', '81189', 83205, '0', 0.00, 12.000000, '1111'),
(1238, NULL, '2019-05-31 13:52:58', NULL, 38.00, NULL, 1.00, 'JOSE', 'PRU4393533', '95915', 83205, '0', 0.00, 12.000000, '1111'),
(1239, NULL, '2019-05-31 13:52:58', NULL, 39.00, NULL, 1.00, 'JOSE', 'PRU1691607', '21628', 83205, '0', 0.00, 12.000000, '1111'),
(1240, NULL, '2019-05-31 13:52:58', NULL, 40.00, NULL, 1.00, 'JOSE', 'PRU4826616', '68700', 83205, '0', 0.00, 12.000000, '1111'),
(1241, NULL, '2019-05-31 13:52:58', NULL, 41.00, NULL, 1.00, 'JOSE', 'PRU8409456', '81199', 83205, '0', 0.00, 12.000000, '1111'),
(1242, NULL, '2019-05-31 13:52:58', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU7794082', '31978', 83205, '0', 0.00, 12.000000, '1111'),
(1243, NULL, '2019-05-31 13:52:58', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU6934667', '60763', 83205, '0', 0.00, 12.000000, '1111'),
(1244, NULL, '2019-05-31 13:52:58', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU1966258', '32294', 83205, '0', 0.00, 12.000000, '1111'),
(1245, NULL, '2019-05-31 13:52:58', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU7545757', '74613', 83205, '0', 0.00, 12.000000, '1111'),
(1246, NULL, '2019-05-31 13:52:58', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU2971377', '61731', 83205, '0', 0.00, 12.000000, '1111'),
(1247, NULL, '2019-05-31 13:52:58', NULL, 47.00, NULL, 1.00, 'JOSE', 'PRU1317125', '86678', 83205, '0', 0.00, 12.000000, '1111'),
(1248, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU6855268', '83119', 83205, '0', 0.00, 14.000000, '1111'),
(1249, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU3958481', '21284', 83205, '0', 0.00, 14.000000, '1111'),
(1250, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU1610155', '15163', 83205, '0', 0.00, 14.000000, '1111'),
(1251, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU1227927', '55240', 83205, '0', 0.00, 14.000000, '1111'),
(1252, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU1927090', '75340', 83205, '0', 0.00, 14.000000, '1111'),
(1253, NULL, '2019-05-31 13:52:59', NULL, 43.00, NULL, 1.00, 'Pedro', 'PRU2014292', '24852', 83205, '0', 0.00, 14.000000, '1111'),
(1254, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU8527333', '10668', 83205, '0', 0.00, 15.000000, '1111'),
(1255, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU5202290', '24958', 83205, '0', 0.00, 15.000000, '1111'),
(1256, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU9289152', '53436', 83205, '0', 0.00, 15.000000, '1111'),
(1257, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU9637844', '40354', 83205, '0', 0.00, 15.000000, '1111'),
(1258, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU8451945', '77404', 83205, '0', 0.00, 15.000000, '1111'),
(1259, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU1627293', '82780', 83205, '0', 0.00, 15.000000, '1111'),
(1260, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU2464518', '14885', 83205, '0', 0.00, 15.000000, '1111'),
(1261, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU8303033', '62224', 83205, '0', 0.00, 15.000000, '1111'),
(1262, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU1765526', '31547', 83205, '0', 0.00, 15.000000, '1111'),
(1263, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU3522135', '73748', 83205, '0', 0.00, 15.000000, '1111'),
(1264, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU3102621', '64407', 83205, '0', 0.00, 15.000000, '1111'),
(1265, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU6705771', '30484', 83205, '0', 0.00, 15.000000, '1111'),
(1266, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU8323795', '58772', 83205, '0', 0.00, 15.000000, '1111'),
(1267, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU8142419', '87997', 83205, '0', 0.00, 15.000000, '1111'),
(1268, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU7618406', '33748', 83205, '0', 0.00, 15.000000, '1111'),
(1269, NULL, '2019-05-31 13:53:00', NULL, 43.00, NULL, 1.00, 'luis', 'PRU4123941', '67448', 83205, '0', 0.00, 15.000000, '1111'),
(1270, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU8866631', '91162', 83205, '0', 0.00, 12.000000, '1111'),
(1271, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU6755509', '34443', 83205, '0', 0.00, 12.000000, '1111'),
(1272, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU6573249', '60838', 83205, '0', 0.00, 12.000000, '1111'),
(1273, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU4710897', '78906', 83205, '0', 0.00, 12.000000, '1111'),
(1274, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU6567800', '86557', 83205, '0', 0.00, 12.000000, '1111'),
(1275, NULL, '2019-05-31 13:53:03', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU5988061', '45228', 83205, '0', 0.00, 12.000000, '1111'),
(1276, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU3943870', '84260', 83205, '0', 0.00, 12.000000, '1111'),
(1277, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU8090177', '72482', 83205, '0', 0.00, 12.000000, '1111'),
(1278, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU9973923', '26270', 83205, '0', 0.00, 12.000000, '1111'),
(1279, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU6583044', '59184', 83205, '0', 0.00, 12.000000, '1111'),
(1280, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU5022807', '30846', 83205, '0', 0.00, 12.000000, '1111'),
(1281, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU9772816', '38044', 83205, '0', 0.00, 12.000000, '1111'),
(1282, NULL, '2019-05-31 13:53:04', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU6597426', '21952', 83205, '0', 0.00, 12.000000, '1111'),
(1283, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU7295906', '33538', 83205, '0', 0.00, 12.000000, '1111'),
(1284, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU4025245', '45381', 83205, '0', 0.00, 12.000000, '1111'),
(1285, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU8071531', '77077', 83205, '0', 0.00, 12.000000, '1111'),
(1286, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU4645427', '22364', 83205, '0', 0.00, 12.000000, '1111'),
(1287, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU4994313', '37330', 83205, '0', 0.00, 12.000000, '1111'),
(1288, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU9944152', '47891', 83205, '0', 0.00, 12.000000, '1111'),
(1289, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU5898679', '31397', 83205, '0', 0.00, 12.000000, '1111'),
(1290, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU4860669', '85387', 83205, '0', 0.00, 12.000000, '1111'),
(1291, NULL, '2019-05-31 13:53:05', NULL, 58.00, NULL, 1.00, 'luis', 'PRU6502475', '81139', 83205, '0', 0.00, 12.000000, '1111'),
(1292, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU9595592', '37459', 83205, '0', 0.00, 12.000000, '1111'),
(1293, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU9086300', '81078', 83205, '0', 0.00, 12.000000, '1111'),
(1294, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU2912069', '87504', 83205, '0', 0.00, 12.000000, '1111'),
(1295, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU1796790', '57386', 83205, '0', 0.00, 12.000000, '1111'),
(1296, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU8053640', '40842', 83205, '0', 0.00, 12.000000, '1111'),
(1297, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU4753172', '51061', 83205, '0', 0.00, 12.000000, '1111'),
(1298, NULL, '2019-05-31 13:53:07', NULL, 58.00, NULL, 1.00, 'JOSE', 'PRU5405157', '79927', 83205, '0', 0.00, 12.000000, '1111'),
(1299, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU3189337', '99695', 83205, '0', 0.00, 12.000000, '1111'),
(1300, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU2146598', '98016', 83205, '0', 0.00, 12.000000, '1111'),
(1301, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU1221862', '66432', 83205, '0', 0.00, 12.000000, '1111'),
(1302, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU8085569', '41346', 83205, '0', 0.00, 12.000000, '1111'),
(1303, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU6064836', '97109', 83205, '0', 0.00, 12.000000, '1111'),
(1304, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU2705300', '21179', 83205, '0', 0.00, 12.000000, '1111'),
(1305, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU2715477', '20824', 83205, '0', 0.00, 12.000000, '1111'),
(1306, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU2553449', '64851', 83205, '0', 0.00, 12.000000, '1111'),
(1307, NULL, '2019-05-31 13:53:08', NULL, 58.00, NULL, 1.00, 'Pedro', 'PRU2780448', '31131', 83205, '0', 0.00, 12.000000, '1111'),
(1308, NULL, '2019-05-31 13:53:10', NULL, 37.00, NULL, 1.00, 'JOSE', 'PRU4393533', '70577', 83205, '0', 0.00, 12.000000, '1111'),
(1309, NULL, '2019-05-31 13:53:10', NULL, 38.00, NULL, 1.00, 'JOSE', 'PRU1691607', '36770', 83205, '0', 0.00, 12.000000, '1111'),
(1310, NULL, '2019-05-31 13:53:10', NULL, 39.00, NULL, 1.00, 'JOSE', 'PRU4826616', '46085', 83205, '0', 0.00, 12.000000, '1111'),
(1311, NULL, '2019-05-31 13:53:10', NULL, 40.00, NULL, 1.00, 'JOSE', 'PRU8409456', '27668', 83205, '0', 0.00, 12.000000, '1111'),
(1312, NULL, '2019-05-31 13:53:10', NULL, 41.00, NULL, 1.00, 'JOSE', 'PRU7794082', '49321', 83205, '0', 0.00, 12.000000, '1111'),
(1313, NULL, '2019-05-31 13:53:10', NULL, 42.00, NULL, 1.00, 'JOSE', 'PRU6934667', '33830', 83205, '0', 0.00, 12.000000, '1111'),
(1314, NULL, '2019-05-31 13:53:10', NULL, 43.00, NULL, 1.00, 'JOSE', 'PRU1966258', '20154', 83205, '0', 0.00, 12.000000, '1111'),
(1315, NULL, '2019-05-31 13:53:10', NULL, 44.00, NULL, 1.00, 'JOSE', 'PRU7545757', '79648', 83205, '0', 0.00, 12.000000, '1111'),
(1316, NULL, '2019-05-31 13:53:10', NULL, 45.00, NULL, 1.00, 'JOSE', 'PRU2971377', '58450', 83205, '0', 0.00, 12.000000, '1111'),
(1317, NULL, '2019-05-31 13:53:10', NULL, 46.00, NULL, 1.00, 'JOSE', 'PRU1317125', '36579', 83205, '0', 0.00, 12.000000, '1111'),
(1318, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU6855268', '78986', 83205, '0', 0.00, 14.000000, '1111'),
(1319, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU3958481', '65786', 83205, '0', 0.00, 14.000000, '1111'),
(1320, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU1610155', '10024', 83205, '0', 0.00, 14.000000, '1111'),
(1321, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU1227927', '92031', 83205, '0', 0.00, 14.000000, '1111'),
(1322, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU1927090', '67534', 83205, '0', 0.00, 14.000000, '1111'),
(1323, NULL, '2019-05-31 13:53:11', NULL, 42.00, NULL, 1.00, 'Pedro', 'PRU2014292', '72821', 83205, '0', 0.00, 14.000000, '1111'),
(1324, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU8527333', '44066', 83205, '0', 0.00, 15.000000, '1111'),
(1325, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU5202290', '80678', 83205, '0', 0.00, 15.000000, '1111'),
(1326, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU9289152', '52556', 83205, '0', 0.00, 15.000000, '1111'),
(1327, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU9637844', '35383', 83205, '0', 0.00, 15.000000, '1111'),
(1328, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU8451945', '31903', 83205, '0', 0.00, 15.000000, '1111'),
(1329, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU1627293', '67239', 83205, '0', 0.00, 15.000000, '1111'),
(1330, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU2464518', '38619', 83205, '0', 0.00, 15.000000, '1111'),
(1331, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU8303033', '15171', 83205, '0', 0.00, 15.000000, '1111'),
(1332, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU1765526', '49617', 83205, '0', 0.00, 15.000000, '1111'),
(1333, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU3522135', '95909', 83205, '0', 0.00, 15.000000, '1111'),
(1334, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU3102621', '50075', 83205, '0', 0.00, 15.000000, '1111'),
(1335, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU6705771', '36605', 83205, '0', 0.00, 15.000000, '1111'),
(1336, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU8323795', '33241', 83205, '0', 0.00, 15.000000, '1111'),
(1337, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU8142419', '79972', 83205, '0', 0.00, 15.000000, '1111'),
(1338, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU7618406', '93722', 83205, '0', 0.00, 15.000000, '1111'),
(1339, NULL, '2019-05-31 13:53:12', NULL, 42.00, NULL, 1.00, 'luis', 'PRU4123941', '85302', 83205, '0', 0.00, 15.000000, '1111'),
(1340, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU8866631', '54648', 83205, '0', 0.00, 12.000000, '1111'),
(1341, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU6755509', '13108', 83205, '0', 0.00, 12.000000, '1111'),
(1342, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU6573249', '89201', 83205, '0', 0.00, 12.000000, '1111'),
(1343, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU4710897', '72588', 83205, '0', 0.00, 12.000000, '1111'),
(1344, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU6567800', '47130', 83205, '0', 0.00, 12.000000, '1111'),
(1345, NULL, '2019-05-31 13:53:14', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU5988061', '43608', 83205, '0', 0.00, 12.000000, '1111'),
(1346, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU3943870', '21091', 83205, '0', 0.00, 12.000000, '1111'),
(1347, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU8090177', '30153', 83205, '0', 0.00, 12.000000, '1111'),
(1348, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU9973923', '95725', 83205, '0', 0.00, 12.000000, '1111'),
(1349, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU6583044', '27052', 83205, '0', 0.00, 12.000000, '1111'),
(1350, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU5022807', '36479', 83205, '0', 0.00, 12.000000, '1111'),
(1351, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU9772816', '81522', 83205, '0', 0.00, 12.000000, '1111'),
(1352, NULL, '2019-05-31 13:53:16', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU6597426', '93014', 83205, '0', 0.00, 12.000000, '1111'),
(1353, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU7295906', '49903', 83205, '0', 0.00, 12.000000, '1111'),
(1354, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU4025245', '37509', 83205, '0', 0.00, 12.000000, '1111'),
(1355, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU8071531', '48141', 83205, '0', 0.00, 12.000000, '1111'),
(1356, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU4645427', '25591', 83205, '0', 0.00, 12.000000, '1111'),
(1357, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU4994313', '72064', 83205, '0', 0.00, 12.000000, '1111'),
(1358, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU9944152', '30848', 83205, '0', 0.00, 12.000000, '1111'),
(1359, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU5898679', '92492', 83205, '0', 0.00, 12.000000, '1111'),
(1360, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU4860669', '48755', 83205, '0', 0.00, 12.000000, '1111'),
(1361, NULL, '2019-05-31 13:53:17', NULL, 57.00, NULL, 1.00, 'luis', 'PRU6502475', '25272', 83205, '0', 0.00, 12.000000, '1111'),
(1362, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU9595592', '26336', 83205, '0', 0.00, 12.000000, '1111'),
(1363, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU9086300', '93444', 83205, '0', 0.00, 12.000000, '1111'),
(1364, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU2912069', '30057', 83205, '0', 0.00, 12.000000, '1111'),
(1365, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU1796790', '66373', 83205, '0', 0.00, 12.000000, '1111'),
(1366, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU8053640', '40130', 83205, '0', 0.00, 12.000000, '1111'),
(1367, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU4753172', '29855', 83205, '0', 0.00, 12.000000, '1111'),
(1368, NULL, '2019-05-31 13:53:18', NULL, 57.00, NULL, 1.00, 'JOSE', 'PRU5405157', '77573', 83205, '0', 0.00, 12.000000, '1111'),
(1369, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU3189337', '20950', 83205, '0', 0.00, 12.000000, '1111'),
(1370, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU2146598', '97912', 83205, '0', 0.00, 12.000000, '1111'),
(1371, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU1221862', '88397', 83205, '0', 0.00, 12.000000, '1111'),
(1372, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU8085569', '59263', 83205, '0', 0.00, 12.000000, '1111'),
(1373, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU6064836', '55207', 83205, '0', 0.00, 12.000000, '1111'),
(1374, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU2705300', '12451', 83205, '0', 0.00, 12.000000, '1111'),
(1375, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU2715477', '43934', 83205, '0', 0.00, 12.000000, '1111'),
(1376, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU2553449', '28443', 83205, '0', 0.00, 12.000000, '1111'),
(1377, NULL, '2019-05-31 13:53:19', NULL, 57.00, NULL, 1.00, 'Pedro', 'PRU2780448', '53688', 83205, '0', 0.00, 12.000000, '1111'),
(1378, NULL, NULL, '2019-05-31 16:04:11', 102.00, 2.00, NULL, 'Pedro', 'PRU971659', 's', 47812, '1', 2.00, 7.630000, '1111'),
(1379, NULL, '2019-06-13 21:44:01', NULL, 56.00, NULL, 1.00, 'pedro', 'PRU7295906', '93106', 83205, '0', 0.00, 12.000000, '1111'),
(1380, NULL, '2019-06-13 21:44:02', NULL, 55.00, NULL, 1.00, 'pedro', 'PRU7295906', '13612', 83205, '0', 0.00, 12.000000, '1111'),
(1381, NULL, '2019-06-13 21:45:36', NULL, 54.00, NULL, 1.00, 'pedro', 'PRU7295906', '21278', 83205, '0', 0.00, 12.000000, '1111'),
(1382, NULL, '2019-06-13 21:45:36', NULL, 56.00, NULL, 1.00, 'manuel', 'PRU3189337', '17425', 83205, '0', 0.00, 12.000000, '1111'),
(1383, NULL, '2019-06-13 21:45:36', NULL, 56.00, NULL, 1.00, 'manuel', 'PRU2146598', '85244', 83205, '0', 0.00, 12.000000, '1111'),
(1384, NULL, '2019-06-13 21:45:36', NULL, 56.00, NULL, 1.00, 'manuel', 'PRU1221862', '54490', 83205, '0', 0.00, 12.000000, '1111'),
(1385, NULL, '2019-06-13 21:45:36', NULL, 56.00, NULL, 1.00, 'manuel', 'PRU8085569', '95238', 83205, '0', 0.00, 12.000000, '1111'),
(1386, NULL, '2019-06-13 21:45:36', NULL, 56.00, NULL, 1.00, 'manuel', 'PRU2780448', '71606', 83205, '0', 0.00, 12.000000, '1111'),
(1387, '2019-06-17 17:54:29', NULL, NULL, 112.00, 10.00, NULL, 'asd', 'PRU971659', NULL, 47812, '1', 10.00, 7.630000, '1111'),
(1388, NULL, '2019-07-10 17:33:31', NULL, 102.00, NULL, 10.00, 'pedro', 'PRU971659', '69513', 27405, '0', 0.00, 7.630000, '1111'),
(1389, NULL, NULL, '2019-07-10 17:39:26', 112.00, 10.00, NULL, 'Pedro', 'PRU971659', 'ads', 47812, '1', 10.00, 7.627119, '1111'),
(1390, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU2455130', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1391, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU5511947', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1392, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU7952051', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1393, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU2263805', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1394, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU8282396', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1395, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU3400171', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1396, '2019-07-22 15:36:45', NULL, NULL, 10.00, 10.00, NULL, 'ads', 'PRU4874221', NULL, 47812, '1', 10.00, 2.000000, '1111'),
(1397, NULL, '2019-07-25 21:43:52', NULL, 44.00, NULL, 10.00, 'asd', 'PRU7295906', '97305', 83205, '0', 0.00, 12.000000, '1111'),
(1398, NULL, '2019-07-26 16:30:55', NULL, 102.00, NULL, 10.00, 'pedro', 'PRU971659', '84843', 27405, '0', 0.00, 7.630000, '1111'),
(1399, NULL, '2019-07-26 16:30:55', NULL, 92.00, NULL, 10.00, 'pedro', 'PRU971659', '89579', 27405, '0', 0.00, 7.630000, '1111'),
(1400, NULL, '2019-07-26 16:31:09', NULL, 82.00, NULL, 10.00, 'pedro', 'PRU971659', '32119', 27405, '0', 0.00, 7.630000, '1111'),
(1401, NULL, NULL, '2019-07-26 16:31:17', 92.00, 10.00, NULL, 'Pedro', 'PRU971659', NULL, 47812, '1', 10.00, 9.000000, '1111'),
(1402, NULL, NULL, '2019-07-26 16:31:40', 140.00, 100.00, NULL, 'Pedro', 'PRU8409456', NULL, 47812, '1', 100.00, 12.000000, '1111'),
(1403, NULL, '2019-07-26 16:31:56', NULL, 130.00, NULL, 10.00, 'pedro', 'PRU8409456', '76758', 83205, '0', 0.00, 12.000000, '1111'),
(1404, '2019-07-26 17:14:26', NULL, NULL, 61.00, 5.00, NULL, 'c', 'PRU2146598', NULL, 47812, '1', 5.00, 12.000000, '1111'),
(1405, '2019-07-26 17:14:26', NULL, NULL, 61.00, 5.00, NULL, 'c', 'PRU8085569', NULL, 47812, '1', 5.00, 12.000000, '1111'),
(1406, '2019-07-26 17:14:26', NULL, NULL, 62.00, 5.00, NULL, 'c', 'PRU6064836', NULL, 47812, '1', 5.00, 12.000000, '1111'),
(1407, '2019-07-26 17:14:26', NULL, NULL, 62.00, 5.00, NULL, 'c', 'PRU2705300', NULL, 47812, '1', 5.00, 12.000000, '1111'),
(1408, NULL, '2019-07-26 20:25:46', NULL, 82.00, NULL, 10.00, 'ads', 'PRU971659', '39464', 84212, '0', 0.00, 7.630000, '1111'),
(1409, NULL, '2019-07-26 20:25:46', NULL, 34.00, NULL, 10.00, 'asd', 'PRU7295906', '98055', 84212, '0', 0.00, 12.000000, '1111'),
(1410, NULL, '2019-08-09 14:22:18', NULL, 120.00, NULL, 10.00, 'f', 'PRU8409456', '70898', 83205, '0', 0.00, 12.000000, '1111'),
(1411, NULL, '2019-08-13 21:28:02', NULL, 72.00, NULL, 10.00, 's', 'PRU971659', '30758', 27405, '0', 0.00, 7.630000, '1111'),
(1412, NULL, '2019-08-13 21:29:20', NULL, 62.00, NULL, 10.00, 'sad', 'PRU971659', '12004', 27405, '0', 0.00, 7.630000, '1111'),
(1413, NULL, '2019-08-13 21:30:32', NULL, 52.00, NULL, 10.00, 'asd', 'PRU971659', '77907', 27405, '0', 0.00, 7.630000, '1111'),
(1414, NULL, '2019-08-13 21:31:41', NULL, 50.00, NULL, 2.00, 'a', 'PRU971659', '52137', 27405, '0', 0.00, 7.630000, '1111'),
(1415, NULL, '2019-08-13 21:32:12', NULL, 45.00, NULL, 5.00, 'w', 'PRU971659', '26517', 27405, '0', 0.00, 7.630000, '1111'),
(1416, NULL, '2019-08-13 21:32:31', NULL, 40.00, NULL, 5.00, 'ws', 'PRU971659', '54713', 27405, '0', 0.00, 7.630000, '1111'),
(1417, NULL, '2019-08-13 21:33:10', NULL, 39.00, NULL, 1.00, 's', 'PRU971659', '35531', 27405, '0', 0.00, 7.630000, '1111'),
(1418, NULL, '2019-08-13 21:42:04', NULL, 38.00, NULL, 1.00, 'pedro', 'PRU971659', '34676', 27405, '0', 0.00, 7.630000, '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_kardex_producto_terminado`
--

CREATE TABLE `detalle_kardex_producto_terminado` (
  `fecha_ingreso` int(11) DEFAULT NULL,
  `fecha_salida` int(11) DEFAULT NULL,
  `trasladador_producto_terminado` int(11) NOT NULL,
  `cod_kardex_pt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_orden_compra`
--

CREATE TABLE `detalle_orden_compra` (
  `cantidad` double(10,2) NOT NULL,
  `costo_unitario` double(11,2) NOT NULL,
  `costo_total` double(11,2) NOT NULL,
  `material_recibido` tinyint(1) DEFAULT NULL,
  `pago_contado` tinyint(1) DEFAULT NULL,
  `pago_credito` tinyint(1) DEFAULT NULL,
  `fecha_pago` int(3) DEFAULT 0,
  `cod_orden_compra` char(14) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cantidad_recibida` double(10,2) NOT NULL,
  `id_detalle_oc` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cantidad_restante` double(10,2) NOT NULL,
  `fecha_deposito` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_orden_compra`
--

INSERT INTO `detalle_orden_compra` (`cantidad`, `costo_unitario`, `costo_total`, `material_recibido`, `pago_contado`, `pago_credito`, `fecha_pago`, `cod_orden_compra`, `cod_material`, `cantidad_recibida`, `id_detalle_oc`, `cantidad_restante`, `fecha_deposito`) VALUES
(6.00, 12.00, 288.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU9086300', 0.00, '241231', 6.00, NULL),
(6.00, 12.00, 288.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU1796790', 0.00, '427891', 6.00, NULL),
(5.00, 12.00, 300.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU2146598', 5.00, '565819', 0.00, '2019-07-26'),
(6.00, 12.00, 288.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU8053640', 0.00, '646093', 6.00, NULL),
(5.00, 12.00, 300.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU6064836', 5.00, '946293', 0.00, '2019-07-26'),
(5.00, 12.00, 300.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU8085569', 5.00, '968658', 0.00, '2019-07-26'),
(6.00, 12.00, 288.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU2912069', 0.00, '979829', 6.00, NULL),
(5.00, 12.00, 300.00, 0, 2, 0, 0, 'PRU2019-7', 'PRU2705300', 5.00, '996292', 0.00, '2019-07-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_orden_pedido`
--

CREATE TABLE `detalle_orden_pedido` (
  `codigo_orden_pedido` char(13) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_serie_articulo` char(11) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidades` char(27) COLLATE utf8_spanish2_ci NOT NULL,
  `costo_total` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle_orden_pedido`
--

INSERT INTO `detalle_orden_pedido` (`codigo_orden_pedido`, `codigo_serie_articulo`, `cantidades`, `costo_total`) VALUES
('PRU-2019-0001', 'PRU42252', '12,12,12,12,12,12,10', 71602.40),
('PRU-2019-0002', 'PRU42252', '12,12,12,12,6,6,6', 57631.20),
('PRU-2019-0003', 'PRU42252', '6,6,6,6,6,6,6', 36674.40),
('PRU-2019-0004', 'PRU42252', '6,6,6,6,6,6,6', 36674.40),
('PRU-2019-0004', 'PRU493392', '12,6,6,6,6,7,8', 8425.20),
('PRU-2019-0005', 'PRU445864', '7,7,7,7,8,8,8,8,8', 19257.60),
('PRU-2019-0006', 'PRU493392', '6,8,10,12,12,13,10', 11729.20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_orden_pedido_produccion`
--

CREATE TABLE `detalle_orden_pedido_produccion` (
  `codigo_orden_pedido_produccion` char(13) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_serie_articulo` char(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `cantidades` char(27) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE `distrito` (
  `cod_distrito` char(6) COLLATE utf8_spanish2_ci NOT NULL,
  `cod_ciudad` char(6) COLLATE utf8_spanish2_ci NOT NULL,
  `nomb_distrito` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `distrito`
--

INSERT INTO `distrito` (`cod_distrito`, `cod_ciudad`, `nomb_distrito`) VALUES
('1', '1', 'Alto Selva Alegre'),
('10', '1', 'Mariano Melgar'),
('11', '1', 'Miraflores'),
('12', '1', 'Mollebaya'),
('13', '1', 'Paucarpata'),
('14', '1', 'Pocsi'),
('15', '1', 'Polobaya'),
('16', '1', 'Quequeña'),
('17', '1', 'Sabandía'),
('18', '1', 'Sachaca'),
('19', '1', 'San Juan de Tarucani'),
('2', '1', 'Arequipa'),
('20', '1', 'Santa Isabel de Siguas'),
('21', '1', 'Santa Rita de Siguas'),
('22', '1', 'San Juan de Siguas'),
('23', '1', 'Socabaya'),
('24', '1', 'Tiabaya'),
('25', '1', 'Vítor'),
('26', '1', 'Yanahuara'),
('27', '1', 'Yarabamba'),
('28', '1', 'Yura'),
('29', '1', 'Uchumayo‎'),
('3', '1', 'Cayma'),
('30', '2', 'Ate'),
('4', '1', 'Cerro Colorado'),
('5', '1', 'Characato'),
('6', '1', 'Chiguata'),
('7', '1', 'Jacobo Hunter'),
('8', '1', 'Jose Luis Bustamante y Rivero'),
('9', '1', 'La joya');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nom_empresa` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `razon_social` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `representante_legal` varchar(70) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre_comercial` varchar(70) COLLATE utf8mb4_spanish_ci NOT NULL,
  `domicilio` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `telefono` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `pagina_web` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `estado_empresa` tinyint(1) NOT NULL,
  `cod_tipo_contribuyente` int(11) NOT NULL,
  `cod_regimen_laboral` int(11) NOT NULL,
  `cod_regimen_renta` int(11) NOT NULL,
  `siglas` varchar(3) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`RUC_empresa`, `nom_empresa`, `razon_social`, `representante_legal`, `nombre_comercial`, `domicilio`, `correo`, `telefono`, `pagina_web`, `imagen`, `estado_empresa`, `cod_tipo_contribuyente`, `cod_regimen_laboral`, `cod_regimen_renta`, `siglas`) VALUES
('10203040506', 'CALZADOS EPIS', 'ABC CALZADOS S.A', 'MIRINA GONZALES', 'CALZADOS EPIS', 'PIEROLA 105', 'calzados.epis@gmail.com', '254585', NULL, '10203040506.PNG', 2, 39656, 2, 1, 'EPI'),
('15849643032', 'prueba', 'Empresa Prueba EIRL', 'prueba', 'prueba', 'prueba', 'prueba@c.c', '415151', NULL, '15849643032.jpg', 1, 39656, 1, 2, 'PRU'),
('18484849656', 'ejemplo', 'ejemplo', 'Jose Pers', 'Jose Pers', 'Jose Pers', 's@s.c', '213213', NULL, '', 1, 77183, 1, 3, 'JPP'),
('20112233441', 'CALZADOS CITECCAL E.I.R.L.', 'CALZADOS CITECCAL E.I.R.L.', 'DELBYN GUITERAS RUIZ', 'CITECCAL', 'PQ. INDUSTRIAL RÍO SECO F-3', 'dguiteras@itp.gob.pe', '488128', 'www.citeccal.com.pe', '20112233441.png', 1, 48333, 1, 2, 'CIT'),
('20134956777', 'CALZADOS CITECCAL E.I.R.L.', 'CALZADOS CITECCAL E.I.R.L.', 'DELBYN ANDREE GUITERAS RUIZ', 'CITECCAL', 'PQ. INDUSTRIAL RIO SECO MZA. F LTE. 3', 'calzado@citeccal.com', '425741', 'www.citeccal.com', '20134956777.png', 1, 48333, 1, 2, 'CI'),
('20454328401', 'CALZADOS LB LOBO BLACK E.I.R.L.', 'CALZADOS LB LOBO BLACK E.I.R.L.', 'Mario Umaña Cajala', 'Lobo Black', 'CALLE VISTA ALEGRE N° 202 CERRO COLORADO', 'gabriela@loboblack.com.pe', '272148', 'www.loboblack.com.pe', '20454328401.jpg', 1, 48333, 1, 1, 'LOB'),
('20455447888', 'TRADE SANDDER GROUP SAC', 'TRADE SANDDER GROUP SAC', 'ELENA MIRTHA PANCCA BORDA', 'SANDDER', 'PARQUE INDUSTRIAL RIO SECO B-3 CERRO COLORADO', 'Admin@tradesandder.com', '443711', 'www.sandder.com', '', 1, 83127, 2, 2, 'SAN'),
('20498620591', 'KAFU', 'KAFU EXPORT E.I.R.L.', 'ZOILO YUPANQUI CHURA', 'CALZADOS KAFU', 'HEROES DE LA BREÑA MZ B LT 12 CERRO COLORADO', 'kafuexport@gmail.com', '537771', NULL, '', 1, 48333, 1, 1, 'KAF'),
('20539686195', 'FABRICA DE CALZADOS NOVA E.I.R.L.', 'FABRICA DE CALZADOS NOVA E.I.R.L.', 'REYNA YSABEL TUNQUIPA HUAMANI', 'CALZADOS NOVA', 'PQ. INDUSTRIAL RIO SECO MZA. B LTE 11', 'zapatilla_nova@hotmail.com', '111111', 'www.nova.com', '20539686195.png', 1, 48333, 1, 1, 'NOV'),
('EMPRESA', 'ABC Calzados', 'ABC microempresa', '66666666666', '66666666666', '66666666666', NULL, '666666', NULL, NULL, 1, 48333, 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escala_salarial`
--

CREATE TABLE `escala_salarial` (
  `cod_regimen_laboral` int(11) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_trabajador` varchar(13) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sueldo_mensual` int(7) NOT NULL,
  `tipo_seguro` char(2) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '1',
  `tasa_beneficio` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `escala_salarial`
--

INSERT INTO `escala_salarial` (`cod_regimen_laboral`, `RUC_empresa`, `cod_trabajador`, `sueldo_mensual`, `tipo_seguro`, `estado`, `tasa_beneficio`) VALUES
(3, '20112233441', '07227131', 930, '2', '1', 59.57),
(1, '20112233441', '07639874', 930, '1', '1', 5.78),
(2, '20112233441', '18095992', 930, '2', '1', 27.65),
(3, '20112233441', '72667879', 1000, '2', '1', 58.55),
(1, '20112233441', 'CIT-72327274', 1800, '2', '1', 13.17),
(1, '15849643032', 'PRU-11111111', 930, '2', '1', 13.17),
(1, '15849643032', 'PRU-11321421', 930, '2', '1', 13.17),
(1, '15849643032', 'PRU-12321541', 1500, '2', '0', 13.17),
(1, '15849643032', 'PRU-75765676', 1000, '2', '0', 13.17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha_historial`
--

CREATE TABLE `fecha_historial` (
  `cod_trabajador` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `descripcion` varchar(40) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `fecha_historial`
--

INSERT INTO `fecha_historial` (`cod_trabajador`, `fecha`, `descripcion`) VALUES
('CIT-78787456', '2019-02-26 15:28:07', 'Primer Ingreso'),
('CIT-84361616', '2019-02-26 15:31:38', 'Primer Ingreso'),
('CIT-84361616', '2019-02-26 15:34:41', 'Salida'),
('CIT-84361616', '2019-02-26 15:34:51', 'Reingreso'),
('CIT-15554466', '2014-04-01 00:00:00', 'Primer Ingreso'),
('CIT-72327274', '2010-12-01 00:00:00', 'Primer Ingreso'),
('CIT-36363612', '2019-03-02 00:00:00', 'Primer Ingreso'),
('CIT-36363612', '2019-03-19 16:11:51', 'Salida'),
('CIT-36363612', '2019-03-19 16:11:59', 'Reingreso'),
('CIT-36363612', '2019-03-20 08:46:54', 'Salida'),
('CIT-36363612', '2019-03-20 08:51:07', 'Reingreso'),
('CIT-36363612', '2019-03-20 14:36:30', 'Salida'),
('CIT-36363612', '2019-03-20 14:50:42', 'Reingreso'),
('07639874', '2019-03-26 11:59:54', 'Salida'),
('07639874', '2019-03-26 12:16:52', 'Reingreso'),
('KAF-85611661', '2010-11-01 00:00:00', 'Primer Ingreso'),
('PRU-75765676', '2010-12-01 00:00:00', 'Primer Ingreso'),
('JPP-12516677', '2019-04-04 00:00:00', 'Primer Ingreso'),
('PRU-11111111', '2018-12-01 00:00:00', 'Primer Ingreso'),
('PRU-11111111', '2019-07-17 16:57:24', 'Salida'),
('PRU-11111111', '2019-07-18 10:32:30', 'Reingreso'),
('PRU-11111111', '2019-07-18 10:32:38', 'Salida'),
('PRU-11111111', '2019-07-18 10:56:05', 'Reingreso'),
('PRU-12321541', '2019-07-01 00:00:00', 'Primer Ingreso'),
('PRU-11321421', '2019-07-01 00:00:00', 'Primer Ingreso'),
('PRU-11111111', '2019-07-18 11:39:21', 'Salida'),
('PRU-11111111', '2019-07-18 11:43:44', 'Reingreso'),
('PRU-11111111', '2019-07-18 12:09:16', 'Salida'),
('PRU-75765676', '2019-07-18 12:11:11', 'Salida'),
('PRU-75765676', '2019-07-18 12:11:14', 'Reingreso'),
('PRU-11111111', '2019-07-18 14:29:58', 'Reingreso'),
('PRU-11111111', '2019-07-18 14:30:52', 'Salida'),
('PRU-11111111', '2019-07-18 14:32:41', 'Reingreso'),
('PRU-11111111', '2019-07-18 14:34:21', 'Salida'),
('PRU-11111111', '2019-07-18 14:37:44', 'Reingreso'),
('PRU-11111111', '2019-07-18 14:41:20', 'Salida'),
('PRU-11111111', '2019-07-18 14:42:20', 'Reingreso'),
('PRU-11111111', '2019-07-18 14:43:51', 'Salida'),
('PRU-11111111', '2019-07-18 14:44:46', 'Reingreso'),
('PRU-11111111', '2019-07-19 12:30:10', 'Salida'),
('PRU-11111111', '2019-07-19 12:35:54', 'Reingreso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forro`
--

CREATE TABLE `forro` (
  `cod_forro` int(11) NOT NULL,
  `descrip_forro` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_forro` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `forro`
--

INSERT INTO `forro` (`cod_forro`, `descrip_forro`, `estado_forro`) VALUES
(1, 'Cuero', 1),
(2, 'Cuero con Recubrimiento', 1),
(3, 'Textiles Naturales y/o Sintéticos, tejidos o no', 1),
(4, 'Otros Materiales', 1),
(5, 'Combinado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_depreciacion_mantenimiento`
--

CREATE TABLE `gasto_depreciacion_mantenimiento` (
  `descrip_activo` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `marca` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_tipo_activo` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `anio_uso` int(11) NOT NULL,
  `anio_compra` int(11) NOT NULL,
  `valor_unitario_sin_IGV` double(8,2) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `gasto_mantenimiento` double(10,6) NOT NULL,
  `frecuencia_mantenimiento_anual` int(11) NOT NULL,
  `cod_tipo_adquisicion` int(11) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_activo` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_depreciacion` tinyint(1) NOT NULL,
  `depreciacion_mensual` double(10,6) NOT NULL,
  `gasto_mensual_depreciacion` double(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `gasto_depreciacion_mantenimiento`
--

INSERT INTO `gasto_depreciacion_mantenimiento` (`descrip_activo`, `marca`, `cod_tipo_activo`, `cod_area`, `anio_uso`, `anio_compra`, `valor_unitario_sin_IGV`, `cantidad`, `gasto_mantenimiento`, `frecuencia_mantenimiento_anual`, `cod_tipo_adquisicion`, `RUC_empresa`, `cod_activo`, `estado_depreciacion`, `depreciacion_mensual`, `gasto_mensual_depreciacion`) VALUES
('a', 'a', 4442, 83205, 1, 2015, 1000.00, 1, 299.000000, 2, 908, '15849643032', '30163', 1, 8.333333, 49.833333),
('a', 'a', 4442, 47812, 4, 2010, 10000.00, 1, 2000.000000, 2, 908, '15849643032', '64446', 1, 0.000000, 333.333333),
('s', 's', 4442, 27188, 1, 2015, 1000.00, 2, 200.000000, 2, 908, '15849643032', '66091', 1, 16.666667, 66.675840),
('mesa', '-', 4445, 36637, 0, 2014, 200.00, 2, 0.000000, 0, 909, '15849643032', '76939', 1, 0.000000, 0.000000),
('maquina', '-', 4442, 36637, 0, 2009, 2000.00, 2, 0.000000, 0, 909, '15849643032', '95994', 1, 33.333333, 0.000000),
('Armadora de punto', '-', 4442, 83205, 0, 2015, 50000.00, 2, 500.000000, 2, 909, '15849643032', 'PRU-734290', 1, 833.333333, 166.666667);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_desarrollo_producto`
--

CREATE TABLE `gasto_desarrollo_producto` (
  `cod_area` int(11) NOT NULL,
  `descrip_gasto` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cantidad_mensual` int(11) NOT NULL,
  `gasto_total` double(11,6) NOT NULL,
  `importe` double(11,6) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo_gasto` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_tipo_desarrollo` int(11) NOT NULL,
  `cod_gasto_desarrollo_producto` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `gasto_desarrollo_producto`
--

INSERT INTO `gasto_desarrollo_producto` (`cod_area`, `descrip_gasto`, `cantidad_mensual`, `gasto_total`, `importe`, `RUC_empresa`, `tipo_gasto`, `cod_tipo_desarrollo`, `cod_gasto_desarrollo_producto`, `estado`) VALUES
(83307, 'Cuero', 20, 700.000000, 35.000000, '15849643032', 'Material', 1, '23119', 1),
(83307, 'Seriado', 24, 50.000000, 1200.000000, '15849643032', 'Servicios', 2, '32127', 1),
(83307, 'Seriado', 1, 2000.000000, 2000.000000, '15849643032', 'Servicios', 3, '37148', 1),
(83307, 'cuero', 20, 20.000000, 1.000000, '15849643032', 'Material', 1, 'PRU-266260', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_distribucion`
--

CREATE TABLE `gasto_distribucion` (
  `id_gastoDistr` char(11) NOT NULL,
  `area` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `gasto_mensual` double NOT NULL,
  `estado` varchar(1) NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gasto_distribucion`
--

INSERT INTO `gasto_distribucion` (`id_gastoDistr`, `area`, `descripcion`, `gasto_mensual`, `estado`, `RUC_empresa`) VALUES
('123057', 87883, 'PETROLEO', 500, '1', '15849643032'),
('PRU-125230', 27405, 'gasolina', 1000, '1', '15849643032');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_financiero`
--

CREATE TABLE `gasto_financiero` (
  `cod_gasto` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `intereses` double(9,2) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_prestamo` date DEFAULT NULL,
  `meses_pago` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `descripcion_financiera` varchar(250) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `gasto_financiero`
--

INSERT INTO `gasto_financiero` (`cod_gasto`, `intereses`, `RUC_empresa`, `fecha_prestamo`, `meses_pago`, `estado`, `descripcion_financiera`) VALUES
('PRU-42245', 100.00, '15849643032', '2019-03-01', 3, 1, 'bbva'),
('PRU-65968', 100.00, '15849643032', NULL, 10, 3, 'cueros sac'),
('PRU-70036', 100.00, '15849643032', NULL, 1, 2, 'venta a 30 dias'),
('PRU-92218', 222.00, '15849643032', '2018-12-26', 10, 1, 's');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_otros`
--

CREATE TABLE `gasto_otros` (
  `id_otros` char(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `gasto` double NOT NULL,
  `cantidad_anual` double(7,2) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gasto_otros`
--

INSERT INTO `gasto_otros` (`id_otros`, `descripcion`, `gasto`, `cantidad_anual`, `estado`, `RUC_empresa`) VALUES
('PRU-3990', 'CANASTA NAVIDEÑA', 200, 5.00, '1', '15849643032'),
('PRU-5459', 'GASTOS VARIOS', 1000, 2.00, '1', '15849643032'),
('PRU-692441', 'GASTOS DE CENAS', 500, 5.00, '1', '15849643032'),
('PRU-7755', 's', 2, 22.00, '1', '15849643032'),
('PRU-8276', 'comida con empresarios', 3000, 2.00, '1', '15849643032');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_representacion`
--

CREATE TABLE `gasto_representacion` (
  `cod_gasrepre` varchar(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `cantidad_anual` double NOT NULL,
  `estado` varchar(1) NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `gasto` double(11,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gasto_representacion`
--

INSERT INTO `gasto_representacion` (`cod_gasrepre`, `descripcion`, `cantidad_anual`, `estado`, `RUC_empresa`, `gasto`) VALUES
('PRU-641765', 'VIAJES', 10, '1', '15849643032', 1000.000000),
('PRU-7113', 'Comida', 5, '1', '15849643032', 100.000000),
('PRU-8824', 'ALMUERZOS', 5, '1', '15849643032', 500.000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_servicios_basicos`
--

CREATE TABLE `gasto_servicios_basicos` (
  `cod_area` int(11) NOT NULL,
  `descrip_servicio_basico` varchar(70) COLLATE utf8mb4_spanish_ci NOT NULL,
  `gasto_mensual` double(10,6) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_gasto_servicio_basico` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_gasto_basico` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `gasto_servicios_basicos`
--

INSERT INTO `gasto_servicios_basicos` (`cod_area`, `descrip_servicio_basico`, `gasto_mensual`, `RUC_empresa`, `cod_gasto_servicio_basico`, `estado_gasto_basico`) VALUES
(75359, 'agua', 423.728814, '15849643032', '752341', 1),
(75359, 'LUZ', 500.000000, '15849643032', 'PRU-999410', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_sueldos`
--

CREATE TABLE `gasto_sueldos` (
  `id_gastos_sueldos` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_area` int(11) DEFAULT NULL,
  `puesto` varchar(500) DEFAULT NULL,
  `sueldo_mensual` double(11,6) DEFAULT NULL,
  `beneficios` double(10,6) DEFAULT NULL,
  `otros` double(10,6) DEFAULT NULL,
  `gasto_mensual` double(10,6) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gasto_sueldos`
--

INSERT INTO `gasto_sueldos` (`id_gastos_sueldos`, `cod_area`, `puesto`, `sueldo_mensual`, `beneficios`, `otros`, `gasto_mensual`, `estado`, `RUC_empresa`) VALUES
('PRU-1066', 83307, 'jefsito', 1000.000000, 131.700000, 300.000000, 1431.700000, '1', '15849643032'),
('PRU-1551', 53586, 'as', 111.000000, 14.618700, 1.000000, 126.618700, '1', '15849643032'),
('PRU-2695', 87883, 'Jefe de Produccion', 11.000000, 1.448700, 1.000000, 13.448700, '1', '15849643032'),
('PRU-3359', 36637, 'administradorororo', 100.000000, 13.170000, 3000.000000, 3113.170000, '1', '15849643032'),
('PRU-397547', 53586, 'ventas', 800.000000, 105.360000, 1000.000000, 1905.360000, '1', '15849643032'),
('PRU-413104', 83307, 'wwwwww', 930.000000, 122.481000, 1000.000000, 2052.481000, '1', '15849643032'),
('PRU-480709', 36637, 'ADMINISTRACION', 200.000000, 26.340000, 2.000000, 228.340000, '1', '15849643032'),
('PRU-7031', 36637, 'asd', 1.000000, 0.131700, 1.000000, 2.131700, '1', '15849643032'),
('PRU-8188', 63488, 'limpieza', 100.000000, 13.170000, 10.000000, 123.170000, '1', '15849643032'),
('PRU-895720', 63488, 'cuidador', 930.000000, 122.481000, 0.000000, 1052.481000, '1', '15849643032'),
('PRU-9060', 53586, 'marketeterroo', 1000.000000, 131.700000, 200.000000, 1331.700000, '1', '15849643032'),
('PRU-9898', 47812, 'almacenero', 30.000000, 3.951000, 30.000000, 63.951000, '1', '15849643032'),
('PRU-9972', 83307, 'modelista', 20.000000, 2.634000, 20.000000, 42.634000, '1', '15849643032');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto_suministro`
--

CREATE TABLE `gasto_suministro` (
  `codigo_suministro` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `consumo` int(11) NOT NULL,
  `meses_duracion` double NOT NULL,
  `cod_area` int(11) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_suministro` tinyint(1) NOT NULL,
  `gasto_mensual_suministro` double(11,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `gasto_suministro`
--

INSERT INTO `gasto_suministro` (`codigo_suministro`, `cod_material`, `consumo`, `meses_duracion`, `cod_area`, `RUC_empresa`, `estado_suministro`, `gasto_mensual_suministro`) VALUES
('PRU-117351', 'PRU110066', 4, 5, 30844, '15849643032', 1, 12.000000),
('PRU-127447', 'PRU110066', 1, 4, 63488, '15849643032', 1, 3.750000),
('PRU-163411', 'PRU110066', 7, 4, 27405, '15849643032', 1, 26.250000),
('PRU-176625', 'PRU110066', 1, 5, 83307, '15849643032', 1, 3.000000),
('PRU-911579', 'PRU110066', 1, 3, 47812, '15849643032', 1, 5.000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horas_trabajo`
--

CREATE TABLE `horas_trabajo` (
  `cod` char(7) COLLATE utf8_spanish2_ci NOT NULL,
  `RUC_empresa` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `hora` time NOT NULL,
  `dia` char(2) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `horas_trabajo`
--

INSERT INTO `horas_trabajo` (`cod`, `RUC_empresa`, `hora`, `dia`) VALUES
('12031', '15849643032', '08:00:00', 'Sa'),
('17204', '20112233441', '08:00:00', 'Ju'),
('18365', '15849643032', '08:00:00', 'Ju'),
('23054', '20112233441', '08:00:00', 'Vi'),
('27514', '20112233441', '08:00:00', 'Mi'),
('29166', '15849643032', '08:00:00', 'Ma'),
('45039', '15849643032', '08:00:00', 'Lu'),
('46855', '15849643032', '08:00:00', 'Vi'),
('52165', '20112233441', '08:00:00', 'Lu'),
('53621', '20112233441', '08:00:00', 'Ma'),
('84710', '15849643032', '08:00:00', 'Mi'),
('88798', '20112233441', '08:00:00', 'Sa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `importaciones`
--

CREATE TABLE `importaciones` (
  `id_detalle_oc` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo_gasto_moneda` char(1) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_gasto` char(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1',
  `descripcion_gasto` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  `importe_gasto` double(12,2) NOT NULL,
  `codigo_importaciones` char(14) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex_material`
--

CREATE TABLE `kardex_material` (
  `lugar_almacenaje` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cod_almacen` int(11) NOT NULL,
  `cod_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `stock_total` double(11,2) NOT NULL DEFAULT 0.00,
  `cod_kardex_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `kardex_material`
--

INSERT INTO `kardex_material` (`lugar_almacenaje`, `cod_almacen`, `cod_material`, `RUC_empresa`, `stock_total`, `cod_kardex_material`) VALUES
('p-11', 72104, '3211700', '20112233441', 40.00, '3211700'),
('p-11', 72104, '4465557', '20112233441', 40.00, '4465557'),
('b-11', 71161, '4718216', '15849643032', 10.00, '4718216'),
('p-11', 72104, '5997853', '20112233441', 40.00, '5997853'),
('P-11', 41241, '611022', '20112233441', 10.00, '611022'),
('b-11', 71161, '6585644', '15849643032', 40.00, '6585644'),
('p-11', 72104, '7249312', '20112233441', 40.00, '7249312'),
('p-11', 72104, '7919626', '20112233441', 40.00, '7919626'),
('b-11', 71161, '9193584', '15849643032', 20.00, '9193584'),
('b-11', 71161, '9217658', '15849643032', 30.00, '9217658'),
('P-11', 72104, 'CIT3908422', '20112233441', 5.00, 'CIT3908422'),
('C-11', 41241, 'CIT428538', '20112233441', 500.00, 'CIT428538'),
('P-11', 72104, 'CIT9072887', '20112233441', 5.00, 'CIT9072887'),
('E-20', 71161, 'PRU1221862', '15849643032', 56.00, 'PRU1221862'),
('c-44', 71161, 'PRU1227927', '15849643032', 42.00, 'PRU1227927'),
('t-11', 71161, 'PRU1317125', '15849643032', 46.00, 'PRU1317125'),
('c-44', 71161, 'PRU1610155', '15849643032', 42.00, 'PRU1610155'),
('c-24', 71161, 'PRU1627293', '15849643032', 42.00, 'PRU1627293'),
('t-11', 71161, 'PRU1691607', '15849643032', 38.00, 'PRU1691607'),
('c-24', 71161, 'PRU1765526', '15849643032', 42.00, 'PRU1765526'),
('E-15', 71161, 'PRU1796790', '15849643032', 57.00, 'PRU1796790'),
('c-44', 71161, 'PRU1927090', '15849643032', 42.00, 'PRU1927090'),
('t-11', 71161, 'PRU1966258', '15849643032', 43.00, 'PRU1966258'),
('c-44', 71161, 'PRU2014292', '15849643032', 42.00, 'PRU2014292'),
('E-20', 71161, 'PRU2146598', '15849643032', 61.00, 'PRU2146598'),
('ac', 71161, 'PRU2263805', '15849643032', 10.00, 'PRU2263805'),
('ac', 71161, 'PRU2455130', '15849643032', 10.00, 'PRU2455130'),
('c-24', 71161, 'PRU2464518', '15849643032', 42.00, 'PRU2464518'),
('E-20', 71161, 'PRU2553449', '15849643032', 57.00, 'PRU2553449'),
('E-20', 71161, 'PRU2705300', '15849643032', 62.00, 'PRU2705300'),
('E-20', 71161, 'PRU2715477', '15849643032', 57.00, 'PRU2715477'),
('E-20', 71161, 'PRU2780448', '15849643032', 56.00, 'PRU2780448'),
('E-15', 71161, 'PRU2912069', '15849643032', 57.00, 'PRU2912069'),
('t-11', 71161, 'PRU2971377', '15849643032', 45.00, 'PRU2971377'),
('c-24', 71161, 'PRU3102621', '15849643032', 42.00, 'PRU3102621'),
('E-20', 71161, 'PRU3189337', '15849643032', 56.00, 'PRU3189337'),
('ac', 71161, 'PRU3400171', '15849643032', 10.00, 'PRU3400171'),
('c-24', 71161, 'PRU3522135', '15849643032', 42.00, 'PRU3522135'),
('V-11', 71161, 'PRU3943870', '15849643032', 57.00, 'PRU3943870'),
('c-44', 71161, 'PRU3958481', '15849643032', 42.00, 'PRU3958481'),
('P-11', 71161, 'PRU4025245', '15849643032', 57.00, 'PRU4025245'),
('c-24', 71161, 'PRU4123941', '15849643032', 42.00, 'PRU4123941'),
('t-11', 71161, 'PRU4393533', '15849643032', 37.00, 'PRU4393533'),
('P-11', 71161, 'PRU4645427', '15849643032', 57.00, 'PRU4645427'),
('P-102', 71161, 'PRU4710897', '15849643032', 57.00, 'PRU4710897'),
('E-15', 71161, 'PRU4753172', '15849643032', 57.00, 'PRU4753172'),
('t-11', 71161, 'PRU4826616', '15849643032', 39.00, 'PRU4826616'),
('P-11', 71161, 'PRU4860669', '15849643032', 57.00, 'PRU4860669'),
('ac', 71161, 'PRU4874221', '15849643032', 10.00, 'PRU4874221'),
('P-11', 71161, 'PRU4994313', '15849643032', 57.00, 'PRU4994313'),
('V-11', 71161, 'PRU5022807', '15849643032', 57.00, 'PRU5022807'),
('c-24', 71161, 'PRU5202290', '15849643032', 42.00, 'PRU5202290'),
('E-15', 71161, 'PRU5405157', '15849643032', 57.00, 'PRU5405157'),
('ac', 71161, 'PRU5511947', '15849643032', 10.00, 'PRU5511947'),
('P-11', 71161, 'PRU5898679', '15849643032', 57.00, 'PRU5898679'),
('P-102', 71161, 'PRU5988061', '15849643032', 57.00, 'PRU5988061'),
('E-20', 71161, 'PRU6064836', '15849643032', 62.00, 'PRU6064836'),
('P-11', 71161, 'PRU6502475', '15849643032', 57.00, 'PRU6502475'),
('P-102', 71161, 'PRU6567800', '15849643032', 57.00, 'PRU6567800'),
('P-102', 71161, 'PRU6573249', '15849643032', 57.00, 'PRU6573249'),
('V-11', 71161, 'PRU6583044', '15849643032', 57.00, 'PRU6583044'),
('V-11', 71161, 'PRU6597426', '15849643032', 57.00, 'PRU6597426'),
('c-24', 71161, 'PRU6705771', '15849643032', 42.00, 'PRU6705771'),
('P-102', 71161, 'PRU6755509', '15849643032', 57.00, 'PRU6755509'),
('c-44', 71161, 'PRU6855268', '15849643032', 42.00, 'PRU6855268'),
('t-11', 71161, 'PRU6934667', '15849643032', 42.00, 'PRU6934667'),
('P-11', 71161, 'PRU7295906', '15849643032', 34.00, 'PRU7295906'),
('t-11', 71161, 'PRU7545757', '15849643032', 44.00, 'PRU7545757'),
('c-24', 71161, 'PRU7618406', '15849643032', 42.00, 'PRU7618406'),
('t-11', 71161, 'PRU7794082', '15849643032', 41.00, 'PRU7794082'),
('ac', 71161, 'PRU7952051', '15849643032', 10.00, 'PRU7952051'),
('E-15', 71161, 'PRU8053640', '15849643032', 57.00, 'PRU8053640'),
('P-11', 71161, 'PRU8071531', '15849643032', 57.00, 'PRU8071531'),
('E-20', 71161, 'PRU8085569', '15849643032', 61.00, 'PRU8085569'),
('V-11', 71161, 'PRU8090177', '15849643032', 57.00, 'PRU8090177'),
('c-24', 71161, 'PRU8142419', '15849643032', 42.00, 'PRU8142419'),
('ac', 71161, 'PRU8282396', '15849643032', 10.00, 'PRU8282396'),
('c-24', 71161, 'PRU8303033', '15849643032', 42.00, 'PRU8303033'),
('c-24', 71161, 'PRU8323795', '15849643032', 42.00, 'PRU8323795'),
('t-11', 71161, 'PRU8409456', '15849643032', 120.00, 'PRU8409456'),
('c-24', 71161, 'PRU8451945', '15849643032', 42.00, 'PRU8451945'),
('c-24', 71161, 'PRU8527333', '15849643032', 42.00, 'PRU8527333'),
('P-102', 71161, 'PRU8866631', '15849643032', 57.00, 'PRU8866631'),
('E-15', 71161, 'PRU9086300', '15849643032', 57.00, 'PRU9086300'),
('c-24', 71161, 'PRU9289152', '15849643032', 42.00, 'PRU9289152'),
('E-15', 71161, 'PRU9595592', '15849643032', 57.00, 'PRU9595592'),
('c-24', 71161, 'PRU9637844', '15849643032', 42.00, 'PRU9637844'),
('C-11', 14011, 'PRU971659', '15849643032', 38.00, 'PRU971659'),
('V-11', 71161, 'PRU9772816', '15849643032', 57.00, 'PRU9772816'),
('P-11', 71161, 'PRU9944152', '15849643032', 57.00, 'PRU9944152'),
('V-11', 71161, 'PRU9973923', '15849643032', 57.00, 'PRU9973923');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex_producto_terminado`
--

CREATE TABLE `kardex_producto_terminado` (
  `costo` decimal(5,2) DEFAULT NULL,
  `costo_almacenaje` decimal(5,2) DEFAULT NULL,
  `lugar_almacenaje` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `stock_minimo` int(11) DEFAULT NULL,
  `cod_almacen` int(11) NOT NULL,
  `cod_producto_terminado` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_kardex_pt` int(11) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea`
--

CREATE TABLE `linea` (
  `cod_linea` int(11) NOT NULL,
  `nombre_linea` varchar(35) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_linea` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `linea`
--

INSERT INTO `linea` (`cod_linea`, `nombre_linea`, `RUC_empresa`, `estado_linea`) VALUES
(22019, 'Botin', '15849643032', 1),
(45840, 'Casual', '15849643032', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `descrip_material` varchar(70) COLLATE utf8mb4_spanish_ci NOT NULL,
  `costo_sin_igv_material` double(9,6) NOT NULL,
  `estado_material` tinyint(1) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_subcategoria` int(11) NOT NULL,
  `unidad_medida` int(11) NOT NULL,
  `stock_maximo` int(11) NOT NULL,
  `stock_minimo` int(11) NOT NULL,
  `factor_equivalencia` double(10,2) NOT NULL,
  `unidad_compra` int(11) NOT NULL,
  `cod_material` char(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `t_compra` char(1) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_proveedor` varchar(14) COLLATE utf8mb4_spanish_ci NOT NULL,
  `t_moneda` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`descrip_material`, `costo_sin_igv_material`, `estado_material`, `RUC_empresa`, `cod_subcategoria`, `unidad_medida`, `stock_maximo`, `stock_minimo`, `factor_equivalencia`, `unidad_compra`, `cod_material`, `t_compra`, `RUC_proveedor`, `t_moneda`) VALUES
('Poliester Piramide N° 30 cafe 120 gr #452', 5.930000, 1, '20112233441', 956, 11, 50, 10, 120.00, 14, '100455', '0', '', 0),
('Africa apache 1.8 mm 1ra Cuerosaqp', 8.200000, 0, '20112233441', 424, 13, 500, 100, 929.03, 66203, '105753', '0', '', 0),
('tej 01-33', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '1118568', '1', '', 0),
('ch 102 azul/negro-43', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '1161426', '1', '', 0),
('TR 01 azul-31', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '1343406', '1', '', 0),
('Puntera termoplastico Artelax 2mm A453', 0.310000, 1, '20112233441', 667, 12, 1500, 400, 1.00, 12, '137088', '0', '', 0),
('Africa apache 1.8 mm 1ra Curtiembresol', 7.300000, 1, '20112233441', 424, 13, 200, 100, 929.03, 66203, '138310', '0', '', 0),
('aj 21-46', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '1540365', '1', '', 0),
('ej 01-20', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '1649736', '1', '', 0),
('ejemplo piso', 11.000000, 1, '20134956777', 191, 3, 200, 50, 40.00, 4, '165941', '0', '', 0),
('af 02 azul-41', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '1886358', '1', '', 0),
('ta 02 masasc-34', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '1966112', '1', '', 0),
('ta 02 masasc-35', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '1966621', '1', '', 0),
('tej 01-32', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '2003931', '1', '', 0),
('CA 547 negro/amarillo-30', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '2016779', '1', 'PRU21313132131', 0),
('piso dolares 1-22', 3.000000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, '2031043', '1', 'CIT20154455884', 1),
('Halogenante', 15.800000, 1, '20112233441', 955, 9, 20, 10, 1.00, 19087, '203895', '0', '', 0),
('Badana plomo 1.2 mm 1ra Cueroaqp', 3.200000, 1, '20112233441', 424, 66203, 200, 50, 929.03, 13, '208110', '0', '', 0),
('ta 02 masasc-40', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '2141189', '1', '', 0),
('aj 21-40', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '2304978', '1', '', 0),
('Contrafuerte Celastic Contax 1.5 mm C345', 0.310000, 1, '20112233441', 667, 12, 1500, 300, 0.31, 12, '242683', '0', '', 0),
('Agujas maq. Aparadora', 5.080000, 1, '20112233441', 461, 12, 10, 3, 10.00, 51890, '255381', '0', '', 0),
('ejemplo cuero', 20.000000, 0, '20112233441', 424, 14, 50, 20, 1.00, 14, '260395', '0', '', 0),
('ta 02 masasc-39', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '2710295', '1', '', 0),
('Nobuck cafe 1.8mm 1ra Curtiembresol', 9.300000, 1, '20112233441', 424, 66203, 600, 200, 929.03, 13, '273251', '0', '', 0),
('Africa apache 1.8 mm 1ra Curtiembresol', 7.300000, 0, '20112233441', 424, 66203, 200, 100, 929.03, 13, '274019', '0', '', 0),
('aj 21-24', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '2751899', '1', 'CIT20154548666', 0),
('af 02 azul-25', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '2935752', '1', '', 0),
('asdas', 12.000000, 1, '20134956777', 173, 19, 1, 1, 1.00, 19, '294538', '0', '', 0),
('aj 21-25', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '2971282', '1', 'CIT20154548666', 0),
('TR 01 azul-30', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '2984143', '1', '', 0),
('piso dolares 1-21', 3.000000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, '3005326', '1', 'CIT20154455884', 1),
('cuero ej', 50.000000, 1, '20112233441', 424, 13, 200, 20, 1.00, 13, '301941', '0', '', 0),
('CA 547 negro/amarillo-29', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '3074266', '1', 'PRU21313132131', 0),
('ch 102 azul/negro-38', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '3153975', '1', '', 0),
('af 02 azul-23', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '3211700', '1', '', 0),
('aj 21-41', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '3214759', '1', '', 0),
('ejemplo mat', 11.000000, 1, '20134956777', 424, 4, 500, 20, 100.00, 5, '323471', '0', '', 0),
('Tinte de cuero negro', 4.240000, 1, '20112233441', 663, 9, 10, 2, 0.25, 19087, '332844', '0', '', 0),
('lol 102 azul/rojo-40', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '3384199', '1', '', 0),
('tej 05-20', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '3391775', '1', '', 0),
('cuerito dolares', 4.000000, 1, '20112233441', 424, 13, 1000, 0, 1.00, 13, '346623', '0', 'CIT20154845465', 1),
('Ojalillos circulares negro 6 mm', 10.170000, 1, '20112233441', 874, 12, 15, 2, 1000.00, 17, '357082', '0', '', 0),
('lol 102 azul/rojo-44', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '3715114', '1', '', 0),
('Lapicero plateado marcador de cuero', 3.500000, 1, '20112233441', 461, 12, 5, 2, 1.00, 12, '374326', '0', '', 0),
('Badana plomo 1.2 mm 1ra Cuerosaqp', 3.500000, 0, '20112233441', 424, 13, 160, 50, 929.03, 66203, '378176', '0', '', 0),
('TR vestir 42 negro-marron TR-120', 7.060000, 1, '20112233441', 191, 92970, 72, 24, 1.00, 92970, '386537', '0', '', 0),
('ch 102 azul/negro-42', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '3920699', '1', '', 0),
('pt-02-42', 20.000000, 1, '20112233441', 191, 92970, 50, 10, 12.00, 19, '3937733', '0', '', 0),
('Cuero Recuperado Framsac', 9.320000, 1, '20112233441', 909, 66203, 20, 5, 30000.00, 15, '402608', '0', '', 0),
('lol 102 azul/rojo-42', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '4057524', '1', '', 0),
('asd', 8.730000, 1, '20134956777', 235, 2, 200, 11, 100.00, 3, '405898', '0', '', 0),
('Chaveta de corte', 5.300000, 1, '20112233441', 461, 12, 5, 2, 1.00, 12, '410493', '0', '', 0),
('ta 02 masasc-30', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '4134619', '1', '', 0),
('ch 102 azul/negro-39', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '4209859', '1', '', 0),
('taj 03', 10.000000, 1, '20112233441', 191, 92970, 200, 20, 2.00, 92970, '423702', '0', '', 0),
('af 02 azul-24', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '4465557', '1', '', 0),
('aj 21-20', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '4495385', '1', 'CIT20154548666', 0),
('tej 05-21', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '4651815', '1', '', 0),
('CA 547 negro/amarillo-23', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '4718216', '1', 'PRU21313132131', 0),
('tej 05-22', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '4775368', '1', '', 0),
('ta 02 masasc-37', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '4786097', '1', '', 0),
('Terokal', 141.190000, 1, '20112233441', 955, 9, 10, 3, 17.00, 16, '480992', '0', '', 0),
('piso sin talla', 10.000000, 1, '20112233441', 191, 92970, 40, 10, 12.00, 19, '497422', '0', 'CIT10225587877', 0),
('ss', 120.000000, 1, '20134956777', 424, 1, 6000, 200, 100.00, 1, '499144', '0', '', 0),
('Cemento Universal Pegatonsac', 177.970000, 1, '20112233441', 955, 9, 10, 5, 17.00, 16, '508967', '0', '', 0),
('Cras azul 1.8mm 1ra Austral', 7.200000, 1, '20112233441', 424, 66203, 1000, 10, 929.03, 13, '529290', '0', '', 0),
('ch 102 azul/negro-45', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '5342405', '1', '', 0),
('ejemplo indi', 10.000000, 1, '20134956777', 461, 3, 50, 20, 11.00, 3, '559768', '0', '', 0),
('ta 02 masasc-38', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '5796186', '1', '', 0),
('CA 547 negro/amarillo-24', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '5830703', '1', 'PRU21313132131', 0),
('ejemplo cuero', 12.000000, 1, '20112233441', 424, 13, 144, 12, 1.00, 13, '585697', '0', 'CIT20154845465', 0),
('taj 02', 20.000000, 1, '20112233441', 191, 19, 200, 10, 2.00, 92970, '592518', '0', '', 0),
('aj 21-42', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '5926437', '1', '', 0),
('af 02 azul-22', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '5997853', '1', '', 0),
('af 02 azul-46', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '6056482', '1', '', 0),
('Africa apache 1.8 mm 1ra Cuerosaqp', 8.500000, 1, '20112233441', 424, 66203, 500, 100, 929.03, 13, '611022', '0', '', 0),
('aj 21-44', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '6115734', '1', '', 0),
('lol 102 azul/rojo-43', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '6292573', '1', '', 0),
('tej 01-38', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '6380013', '1', '', 0),
('af 02 azul-42', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '6468655', '1', '', 0),
('tej 01-36', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '6487236', '1', '', 0),
('piso dolares 1-20', 3.000000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, '6549295', '1', 'CIT20154455884', 1),
('CA 547 negro/amarillo-21', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '6585644', '1', 'PRU21313132131', 0),
('af 02 azul-40', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '6649412', '1', '', 0),
('tej 01-35', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '6701960', '1', '', 0),
('ej 01-25', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '6737412', '1', '', 0),
('ta 02 masasc-36', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '6777075', '1', '', 0),
('aj 21-45', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '6791341', '1', '', 0),
('tej 05-24', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '6874513', '1', '', 0),
('Caucho dama 36 balnco', 6.320000, 1, '20112233441', 191, 92970, 36, 12, 1.00, 92970, '692632', '0', '', 0),
('aj 21-43', 10.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '7062952', '1', '', 0),
('lol 102 azul/rojo-41', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '7197707', '1', '', 0),
('af 02 azul-20', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '7249312', '1', '', 0),
('af 02 azul-43', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '7302461', '1', '', 0),
('ej 01-23', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '7368838', '1', '', 0),
('Latex Varesac 1mm', 13.560000, 1, '20112233441', 173, 66203, 100, 15, 14000.00, 1, '738762', '0', '', 0),
('ch 102 azul/negro-44', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '7579795', '1', '', 0),
('Tallas', 4.240000, 1, '20112233441', 874, 12, 10, 2, 1000.00, 17, '775570', '0', '', 0),
('Africa apache 1.8 mm 1ra Austral', 8.200000, 0, '20112233441', 424, 13, 500, 100, 929.03, 66203, '778659', '0', '', 0),
('ej 01-22', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '7805559', '1', '', 0),
('TR 01 azul-33', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '7826988', '1', '', 0),
('tej 01-31', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '7915621', '1', '', 0),
('af 02 azul-21', 15.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '7919626', '1', '', 0),
('af 02 azul-44', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '7973772', '1', '', 0),
('TR 01 azul-34', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '8116629', '1', '', 0),
('ta 02 masasc-31', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '8153021', '1', '', 0),
('tej 01-37', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '8274932', '1', '', 0),
('ch 102 azul/negro-40', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '8316544', '1', '', 0),
('Caja presentación CITECCAL', 1.270000, 1, '20112233441', 874, 13, 5000, 200, 1.00, 51890, '832391', '0', '', 0),
('tej 01-34', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '8453959', '1', '', 0),
('tej 05-23', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '8461806', '1', '', 0),
('aj 21-23', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '8477753', '1', 'CIT20154548666', 0),
('lol 102 azul/rojo-45', 20.000000, 1, '20112233441', 191, 92970, 150, 100, 1.00, 92970, '8481943', '1', '', 0),
('af 02 azul-45', 10.000000, 1, '20112233441', 191, 92970, 100, 10, 1.00, 92970, '8562002', '1', '', 0),
('CA 547 negro/amarillo-28', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '8681158', '1', 'PRU21313132131', 0),
('tej 05-25', 10.000000, 1, '20112233441', 191, 92970, 200, 15, 1.00, 92970, '8697608', '1', '', 0),
('ta 02 masasc-32', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '8791636', '1', '', 0),
('CA 547 negro/amarillo-26', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '8886221', '1', 'PRU21313132131', 0),
('ch 102 azul/negro-41', 21.000000, 1, '20112233441', 191, 92970, 100, 12, 1.00, 92970, '8920570', '1', '', 0),
('TR 01 azul-32', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '9179128', '1', '', 0),
('CA 547 negro/amarillo-20', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '9193584', '1', 'PRU21313132131', 0),
('ta 02 masasc-33', 20.000000, 1, '20112233441', 191, 92970, 13, 15, 12.00, 19, '9195612', '1', '', 0),
('CA 547 negro/amarillo-22', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '9217658', '1', 'PRU21313132131', 0),
('Espuma 12 mm Framsac', 14.410000, 1, '20112233441', 663, 66203, 10, 5, 30000.00, 15, '924234', '0', '', 0),
('tej 01-30', 10.000000, 1, '20112233441', 191, 92970, 500, 1, 1.00, 92970, '9307594', '1', '', 0),
('aj 21-21', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '9508552', '1', 'CIT20154548666', 0),
('ej 01-24', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '9657957', '1', '', 0),
('CA 547 negro/amarillo-25', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '9695593', '1', 'PRU21313132131', 0),
('ej 01-21', 40.000000, 1, '20112233441', 191, 92970, 400, 40, 1.00, 92970, '9723348', '1', '', 0),
('TR 01 azul-35', 20.000000, 1, '20112233441', 191, 2, 50, 20, 40.00, 3, '9746474', '1', '', 0),
('aj 21-22', 5.000000, 1, '20112233441', 191, 92970, 522, 10, 1.00, 92970, '9785405', '1', 'CIT20154548666', 0),
('CA 547 negro/amarillo-27', 12.000000, 1, '15849643032', 191, 92970, 1222222, 1, 1.00, 92970, '9985007', '1', 'PRU21313132131', 0),
('Piso nuevo cod azul-44', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT2634922', '1', 'CIT10225587877', 0),
('Piso nuevo cod azul-42', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT3750752', '1', 'CIT10225587877', 0),
('Piso nuevo cod azul-40', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT3908422', '1', 'CIT10225587877', 0),
('cuero nuevo cod', 15.500000, 1, '20112233441', 424, 13, 5000, 0, 1.00, 13, 'CIT428538', '0', 'CIT42343434422', 0),
('Piso nuevo cod azul-45', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT5920109', '1', 'CIT10225587877', 0),
('Piso nuevo cod azul-43', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT6909187', '1', 'CIT10225587877', 0),
('Piso nuevo cod azul-41', 2.750000, 1, '20112233441', 191, 92970, 1000, 0, 1.00, 92970, 'CIT9072887', '1', 'CIT10225587877', 0),
('Hojas A5', 25.000000, 1, '20112233441', 975, 17, 2, 1, 1.00, 17, 'CIT988741', '0', 'CIT20154455884', 0),
('cuero ejemplo', 4.510000, 1, '18484849656', 424, 13, 5000, 1, 1.00, 13, 'JPP921085', '0', 'JPP12567537373', 0),
('TR 500 ROJO_TOFFI-41', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU1083230', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-46', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU1100294', '1', 'PRU21313132131', 0),
('horma h-01', 15.000000, 1, '15849643032', 969, 19, 2, 0, 1.00, 2, 'PRU110066', '0', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-32', 12.000000, 0, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU1221862', '1', 'PRU21313132131', 0),
('TR 600 AZUL_AMARILLO-33', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU1227927', '1', 'PRU21313132131', 0),
('tr 290 T-39', 11.000000, 1, '15849643032', 191, 92970, 11, 10, 1.00, 92970, 'PRU1288064', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-39', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU1317125', '1', 'PRU21313132131', 0),
('TR 600 AZUL_AMARILLO-32', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU1610155', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-35', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU1627293', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-31', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU1691607', '1', 'PRU21313132131', 0),
('tr 296 T-42', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU1712425', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-38', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU1765526', '1', 'PRU21313132131', 0),
('CA 150 ROJO-43', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU1796790', '1', 'PRU21313132131', 0),
('piso en dolares-31', 5.000000, 1, '15849643032', 191, 92970, 12, 1, 1.00, 92970, 'PRU1853882', '1', 'PRU21313132131', 1),
('TR 600 AZUL_AMARILLO-34', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU1927090', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-36', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU1966258', '1', 'PRU21313132131', 0),
('TR 600 AZUL_AMARILLO-35', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU2014292', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-31', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2146598', '1', 'PRU21313132131', 0),
('ej 500-41', 2.000000, 0, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU2263805', '1', 'PRU21313132131', 0),
('cas_05-38', 10.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU2300688', '1', 'PRU21313132131', 0),
('tr 296 T-40', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU2307162', '1', 'PRU21313132131', 0),
('ej 500-38', 2.000000, 0, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU2455130', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-36', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU2464518', '1', 'PRU21313132131', 0),
('tr 290 T-41', 11.000000, 1, '15849643032', 191, 92970, 11, 10, 1.00, 92970, 'PRU2525122', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-37', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2553449', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-43', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU2675678', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-35', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2705300', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-36', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2715477', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-38', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2780448', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-40', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU2871720', '1', 'PRU21313132131', 0),
('CA 150 ROJO-42', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU2912069', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-38', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU2971377', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-40', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU3102621', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-30', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU3189337', '1', 'PRU21313132131', 0),
('ej 500-43', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU3400171', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-39', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU3522135', '1', 'PRU21313132131', 0),
('da_001-34', 12.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU3637148', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-33', 20.000000, 1, '15849643032', 191, 92970, 500, 0, 1.00, 92970, 'PRU3643273', '1', 'PRU21313132131', 0),
('piso en dolares-32', 5.000000, 1, '15849643032', 191, 92970, 12, 1, 1.00, 92970, 'PRU3875961', '1', 'PRU21313132131', 1),
('CA 800 Negro CERCO_AZUL-40', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU3943870', '1', 'PRU21313132131', 0),
('TR 600 AZUL_AMARILLO-31', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU3958481', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-34', 20.000000, 1, '15849643032', 191, 92970, 500, 0, 1.00, 92970, 'PRU4019054', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-39', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU4025245', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-45', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU4123941', '1', 'PRU21313132131', 0),
('cas_05-39', 10.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU4151387', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-30', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU4393533', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-41', 12.000000, 0, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU4645427', '1', 'PRU21313132131', 0),
('piramide 120 gr rojo todo_hilos', 6.779661, 1, '15849643032', 956, 11, 12, 0, 120.00, 14, 'PRU464641', '0', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-33', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU4710897', '1', 'PRU21313132131', 0),
('CA 150 ROJO-45', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU4753172', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-32', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU4826616', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-37', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU4860299', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-45', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU4860669', '1', 'PRU21313132131', 0),
('ej 500-44', 2.000000, 0, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU4874221', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-31', 20.000000, 1, '15849643032', 191, 92970, 500, 0, 1.00, 92970, 'PRU4984404', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-42', 12.000000, 0, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU4994313', '1', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-44', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU5022807', '1', 'PRU21313132131', 0),
('pegamento 20 l tekno', 12.000000, 1, '15849643032', 955, 9, 5, 0, 15.00, 19087, 'PRU512239', '0', 'PRU21313132131', 0),
('da_001-36', 12.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU5152609', '1', 'PRU21313132131', 0),
('cas_05-41', 10.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU5154439', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-31', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU5202290', '1', 'PRU21313132131', 0),
('CA 150 ROJO-46', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU5405157', '1', 'PRU21313132131', 0),
('ej 500-39', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU5511947', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-42', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU5534266', '1', 'PRU21313132131', 0),
('piso en dolares-30', 5.000000, 1, '15849643032', 191, 92970, 12, 1, 1.00, 92970, 'PRU5572896', '1', 'PRU21313132131', 1),
('da_001-35', 12.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU5729086', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-44', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU5898679', '1', 'PRU21313132131', 0),
('da_001-37', 12.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU5947027', '1', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-35', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU5988061', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-34', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU6064836', '1', 'PRU21313132131', 0),
('tr 296 T-39', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU6158152', '1', 'PRU21313132131', 0),
('ass sss-27', 11.000000, 1, '15849643032', 191, 92970, 10, 0, 1.00, 92970, 'PRU6251418', '1', 'PRU21313132131', 0),
('tr 290 T-40', 11.000000, 1, '15849643032', 191, 92970, 11, 10, 1.00, 92970, 'PRU6349146', '1', 'PRU21313132131', 0),
('as', 12.000000, 0, '15849643032', 424, 66203, 1, 1, 1.00, 18, 'PRU647614', '0', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-46', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU6502475', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-35', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU6551361', '1', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-34', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU6567800', '1', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-32', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU6573249', '1', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-43', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU6583044', '1', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-46', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU6597426', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-41', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU6705771', '1', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-31', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU6755509', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-38', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU6801210', '1', 'PRU21313132131', 0),
('TR 600 AZUL_AMARILLO-30', 14.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU6855268', '1', 'PRU21313132131', 0),
('tr 290 T-42', 11.000000, 1, '15849643032', 191, 92970, 11, 10, 1.00, 92970, 'PRU6882322', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-35', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU6934667', '1', 'PRU21313132131', 0),
('cuero dolaress', 2.000000, 1, '15849643032', 424, 13, 11, 1, 10.00, 13, 'PRU720158', '0', 'PRU21313132131', 1),
('TR 500 ROJO_TOFFI-44', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU7292352', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-38', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU7295906', '1', 'PRU21313132131', 0),
('cas_05-40', 10.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU7351332', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-45', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU7495502', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-37', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU7545757', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-44', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU7618406', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-34', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU7794082', '1', 'PRU21313132131', 0),
('Hojas A4', 15.000000, 1, '15849643032', 975, 17, 3, 1, 1.00, 17, 'PRU786752', '0', 'PRU21313132131', 0),
('aguja aparadora', 15.000000, 1, '15849643032', 235, 12, 12, 1, 12.00, 19, 'PRU794648', '0', 'PRU21313132131', 0),
('ej 500-40', 2.000000, 0, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU7952051', '1', 'PRU21313132131', 0),
('CA 150 ROJO-44', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU8053640', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-40', 12.000000, 0, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU8071531', '1', 'PRU21313132131', 0),
('CA 540 AZUL_BLANCO-33', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU8085569', '1', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-41', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU8090177', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-43', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU8142419', '1', 'PRU21313132131', 0),
('ej 500-42', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU8282396', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-37', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU8303033', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-42', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU8323795', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-36', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU8374446', '1', 'PRU21313132131', 0),
('tr 296 T-41', 2.000000, 1, '15849643032', 191, 92970, 11, 1, 1.00, 92970, 'PRU8380469', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-33', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU8409456', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-34', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU8451945', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-30', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU8527333', '1', 'PRU21313132131', 0),
('TR 500 ROJO_TOFFI-40', 12.000000, 1, '15849643032', 191, 92970, 1000, 500, 1.00, 92970, 'PRU8529273', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-39', 12.000000, 1, '15849643032', 191, 92970, 500, 1, 1.00, 92970, 'PRU8606610', '1', 'PRU21313132131', 0),
('TR 100 AZUL_MARINO-30', 12.000000, 1, '15849643032', 191, 92970, 8000, 1, 1.00, 92970, 'PRU8866631', '1', 'PRU21313132131', 0),
('CA 150 ROJO-41', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU9086300', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-32', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU9289152', '1', 'PRU21313132131', 0),
('da_001-38', 12.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU9390801', '1', 'PRU21313132131', 0),
('cas_05-42', 10.000000, 1, '15849643032', 969, 92970, 5, 2, 1.00, 92970, 'PRU9484385', '1', 'PRU21313132131', 0),
('CA 150 ROJO-40', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU9595592', '1', 'PRU21313132131', 0),
('TR 150 NEGRO_AZUL-33', 15.000000, 1, '15849643032', 191, 92970, 10000, 1, 1.00, 92970, 'PRU9637844', '1', 'PRU21313132131', 0),
('cerato marron tannerys', 9.000000, 1, '15849643032', 424, 13, 2000, 0, 1.00, 13, 'PRU971659', '0', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-45', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU9772816', '1', 'PRU21313132131', 0),
('CA 547 negro/amarillo-32', 20.000000, 1, '15849643032', 191, 92970, 500, 0, 1.00, 92970, 'PRU9798474', '1', 'PRU21313132131', 0),
('CA 100 BLANCO_NEGRO-43', 12.000000, 0, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU9944152', '1', 'PRU21313132131', 0),
('CA 800 Negro CERCO_AZUL-42', 12.000000, 1, '15849643032', 191, 92970, 800, 1, 1.00, 92970, 'PRU9973923', '1', 'PRU21313132131', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelo`
--

CREATE TABLE `modelo` (
  `nombre` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_modelo` tinyint(1) NOT NULL,
  `cod_modelo` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_linea` int(11) NOT NULL,
  `cod_capellada` int(11) NOT NULL,
  `cod_piso` int(11) NOT NULL,
  `cod_forro` int(11) NOT NULL,
  `cod_plantilla` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_coleccion` char(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_horma` char(11) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `modelo`
--

INSERT INTO `modelo` (`nombre`, `descripcion`, `RUC_empresa`, `estado_modelo`, `cod_modelo`, `cod_linea`, `cod_capellada`, `cod_piso`, `cod_forro`, `cod_plantilla`, `imagen`, `cod_coleccion`, `cod_horma`) VALUES
('DESAGUADERO ADULTO', 'ADULTO', '15849643032', 1, 'PRU DES 001', 22019, 1, 1, 2, 4, 'PRU DES 001.PNG', 'PRU-19684', 'PRU3637148'),
('DESAGUADERO ADULTO', 'asd', '15849643032', 3, 'PRU DES 001-DeJo-1', 22019, 1, 1, 1, 1, 'PRU DES 001-DeJo-1.PNG', 'PRU-19684', 'PRU3637148'),
('DESAGUADERO ADULTO', 'Bueno', '15849643032', 3, 'PRU DES 001-DeJo-2', 22019, 1, 2, 3, 3, 'PRU DES 001-DeJo-2.jpg', 'PRU-19684', 'PRU3637148'),
('Sandalia blanca', 'Sandalia blanca', '15849643032', 1, 'PRU san-01', 45840, 1, 5, 1, 3, 'PRU san-01.jpg', 'PRU-32144', 'PRU3637148'),
('VARON CASUAL', 'VARON CASUAL', '15849643032', 1, 'PRU VA-CAS-01', 45840, 1, 1, 1, 1, 'PRU VA-CAS-01.jpg', 'PRU-32144', 'PRU2300688'),
('VARON CASUAL', 'Aqert', '15849643032', 3, 'PRU VA-CAS-01-DeAd-1', 45840, 1, 3, 3, 5, 'PRU VA-CAS-01-DeAd-1.jpg', 'PRU-32144', 'PRU2300688'),
('VARON CASUAL 20', 'VARON CASUAL 20', '15849643032', 1, 'PRU VAR-CAS-20', 45840, 1, 1, 1, 1, 'PRU VAR-CAS-20.jpg', 'PRU-19684', 'PRU2300688');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `cod` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `url_imagen` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_evento` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish2_ci NOT NULL,
  `categoria_difusion` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`cod`, `url_imagen`, `nombre_evento`, `descripcion`, `categoria_difusion`) VALUES
('ev-01', 'ev-01.jpg', 'evento de prueba', 'prueba de eventos', 1),
('ev-03', 'ev-03.jpg', 'evento de prueba 3', 'eventos e pruebas3', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_compra`
--

CREATE TABLE `orden_compra` (
  `costo_total_oc` decimal(11,2) NOT NULL,
  `comentario_oc` varchar(2000) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_orden_compra` timestamp NOT NULL DEFAULT current_timestamp(),
  `estado_orden_compra` tinyint(1) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_orden_compra` char(14) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo_moneda_orden` int(1) NOT NULL DEFAULT 0,
  `t_cambio` double(6,2) DEFAULT NULL,
  `RUC_proveedor` varchar(14) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_entrega` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `orden_compra`
--

INSERT INTO `orden_compra` (`costo_total_oc`, `comentario_oc`, `fecha_orden_compra`, `estado_orden_compra`, `RUC_empresa`, `cod_orden_compra`, `tipo_moneda_orden`, `t_cambio`, `RUC_proveedor`, `fecha_entrega`) VALUES
('1043.12', NULL, '2019-04-04 17:07:13', 1, '20112233441', '179946', 0, NULL, 'CIT10225587877', '2019-04-11'),
('219.48', NULL, '2019-04-11 19:46:58', 0, '20112233441', '182334', 0, NULL, 'CIT21548441545', '2019-04-19'),
('1093.60', NULL, '2019-03-07 20:59:54', 1, '20112233441', '188072', 0, NULL, 'CIT10225587877', '2019-04-11'),
('236.00', NULL, '2019-04-04 17:05:43', 1, '20112233441', '200768', 0, NULL, 'CIT10225587877', '2019-04-11'),
('606.81', NULL, '2019-04-11 21:06:27', 1, '20112233441', '228014', 0, NULL, 'CIT21548441545', '2019-04-21'),
('1640.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '237756', 0, NULL, 'CIT10225587877', '2019-04-11'),
('566.40', NULL, '2019-04-04 16:46:30', 0, '15849643032', '304797', 0, NULL, 'PRU21313132131', '2019-04-11'),
('8200.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '312643', 0, NULL, 'CIT10225587877', '2019-04-11'),
('13560.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '314044', 0, NULL, 'CIT10225587877', '2019-04-11'),
('566.40', NULL, '2019-04-04 16:42:48', 0, '15849643032', '351204', 0, NULL, 'PRU21313132131', '2019-04-11'),
('450.00', NULL, '2019-03-04 21:37:05', 0, '20112233441', '388534', 0, NULL, 'CIT10225587877', '2019-04-11'),
('750.00', NULL, '2019-03-05 17:53:28', 1, '20112233441', '435463', 0, NULL, 'CIT10225587877', '2019-04-11'),
('4000.00', NULL, '2019-03-05 17:01:38', 1, '20112233441', '437641', 0, NULL, 'CIT10225587877', '2019-04-11'),
('991.97', NULL, '2019-04-09 20:05:10', 1, '20112233441', '445344', 0, NULL, 'CIT10225587877', '2019-04-11'),
('339.84', NULL, '2019-04-22 16:30:39', 1, '20112233441', '456650', 0, NULL, 'CIT20154455884', '2019-04-30'),
('47.20', NULL, '2019-04-11 19:42:57', 1, '20112233441', '470233', 1, 3.30, 'CIT21548441545', '2019-04-19'),
('14300.00', NULL, '2019-03-07 20:43:27', 1, '20112233441', '478185', 0, NULL, 'CIT10225587877', '2019-04-11'),
('7750.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '529331', 0, NULL, 'CIT10225587877', '2019-04-11'),
('9335.54', NULL, '2019-01-20 06:00:00', 1, '20112233441', '556339', 0, NULL, 'CIT10225587877', '2019-04-11'),
('135.60', NULL, '2019-03-04 20:46:04', 1, '20112233441', '558334', 0, NULL, 'CIT10225587877', '2019-04-11'),
('2420.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '590570', 0, NULL, 'CIT10225587877', '2019-04-11'),
('1185.62', NULL, '2019-01-20 06:00:00', 0, '20112233441', '592270', 0, NULL, 'CIT10225587877', '2019-04-11'),
('9750.00', NULL, '2019-03-05 19:59:16', 1, '20112233441', '613364', 0, NULL, 'CIT10225587877', '2019-04-11'),
('450.00', NULL, '2019-03-06 21:21:04', 1, '20112233441', '675432', 0, NULL, 'CIT10225587877', '2019-04-11'),
('360000.00', NULL, '2019-04-01 15:30:08', 1, '20112233441', '702742', 0, NULL, 'CIT10225587877', '2019-04-11'),
('1055.00', NULL, '2019-03-04 06:00:00', 1, '20112233441', '711960', 0, NULL, 'CIT10225587877', '2019-04-11'),
('2000.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '724578', 0, NULL, 'CIT10225587877', '2019-04-11'),
('3000.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '729258', 0, NULL, 'CIT10225587877', '2019-04-11'),
('6000.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '729905', 0, NULL, 'CIT10225587877', '2019-04-11'),
('401.55', NULL, '2019-04-09 20:19:47', 1, '20112233441', '741162', 0, NULL, 'CIT10225587877', '2019-04-11'),
('74.58', NULL, '2019-04-04 17:04:59', 1, '20112233441', '750997', 0, NULL, 'CIT10225587877', '2019-04-11'),
('31300.00', NULL, '2019-03-07 20:13:28', 1, '20112233441', '754010', 0, NULL, 'CIT10225587877', '2019-04-11'),
('1003.00', 'Referencia al documento CIT-0000111', '2019-04-10 21:27:44', 1, '20112233441', '759515', 0, NULL, 'CIT20154845465', '2019-04-11'),
('400.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '768343', 0, NULL, 'CIT10225587877', '2019-04-11'),
('271.20', NULL, '2019-04-04 14:18:23', 1, '20112233441', '771100', 0, NULL, 'CIT10225587877', '2019-04-11'),
('5600.00', NULL, '2019-01-20 06:00:00', 1, '20112233441', '774952', 0, NULL, 'CIT10225587877', '2019-04-11'),
('24.00', NULL, '2019-01-20 06:00:00', 1, '20134956777', '792278', 0, NULL, 'CIT20111212123', '2019-04-11'),
('135.60', NULL, '2019-03-06 21:39:03', 1, '20112233441', '793144', 0, NULL, 'CIT10225587877', '2019-04-11'),
('730.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '811928', 0, NULL, 'CIT10225587877', '2019-04-11'),
('16000.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '813230', 0, NULL, 'CIT10225587877', '2019-04-11'),
('15356.00', NULL, '2019-03-01 06:00:00', 1, '20112233441', '814960', 0, NULL, 'CIT10225587877', '2019-04-11'),
('16970.00', NULL, '2019-01-20 06:00:00', 0, '20112233441', '836521', 0, NULL, 'CIT10225587877', '2019-04-11'),
('2106.00', NULL, '2019-03-05 16:40:57', 1, '20112233441', '837353', 0, NULL, 'CIT10225587877', '2019-04-11'),
('360.00', NULL, '2019-04-04 13:48:40', 0, '15849643032', '853616', 0, NULL, 'PRU21313132131', '2019-04-11'),
('708.00', NULL, '2019-04-12 13:46:34', 1, '20112233441', '857290', 0, NULL, 'CIT20154864556', '2019-04-20'),
('3640.00', NULL, '2019-03-04 06:00:00', 1, '20112233441', '860323', 0, NULL, 'CIT10225587877', '2019-04-11'),
('863.55', NULL, '2019-04-09 20:23:02', 1, '20112233441', '860538', 0, NULL, 'CIT10225587877', '2019-04-11'),
('519.20', NULL, '2019-04-10 21:01:37', 1, '20112233441', '917268', 1, 3.30, 'CIT21548441545', '2019-04-11'),
('3776.00', NULL, '2019-04-11 21:02:19', 1, '20112233441', '936383', 0, NULL, 'CIT21548441545', '2019-04-19'),
('7720.00', NULL, '2019-03-06 18:41:58', 1, '20112233441', '938509', 0, NULL, 'CIT10225587877', '2019-04-11'),
('2832.00', NULL, '2019-04-09 20:22:21', 1, '20112233441', '958282', 0, NULL, 'CIT10225587877', '2019-04-11'),
('708.00', NULL, '2019-04-11 20:49:23', 0, '20112233441', '959046', 0, NULL, 'CIT21548441545', '2019-04-19'),
('120.00', NULL, '2019-01-02 06:00:00', 1, '20134956777', '964466', 0, NULL, 'CIT20111212123', '2019-04-11'),
('450.00', NULL, '2019-03-04 21:37:32', 0, '20112233441', '965387', 0, NULL, 'CIT10225587877', '2019-04-11'),
('17.23', NULL, '2019-04-22 17:16:29', 1, '20112233441', 'CIT2019-0', 0, NULL, 'CIT20154455884', '2019-04-30'),
('484.27', NULL, '2019-04-26 13:16:28', 1, '20112233441', 'CIT2019-1', 0, NULL, 'CIT20154845465', '2019-05-09'),
('182.90', NULL, '2019-04-26 13:17:41', 0, '20112233441', 'CIT2019-2', 0, NULL, 'CIT20154845465', '2019-05-10'),
('194.70', NULL, '2019-04-26 14:42:12', 1, '20112233441', 'CIT2019-3', 0, NULL, 'CIT10225587877', '2019-05-20'),
('501.50', NULL, '2019-05-06 19:36:47', 1, '20112233441', 'CIT2019-4', 0, NULL, 'CIT49491313194', '2019-05-30'),
('9145.00', NULL, '2019-05-06 21:58:13', 1, '20112233441', 'CIT2019-5', 0, NULL, 'CIT20154845465', '2019-05-30'),
('8850.00', NULL, '2019-05-06 22:19:23', 1, '20112233441', 'CIT2019-6', 0, NULL, 'CIT20154845465', '2019-05-30'),
('5.32', NULL, '2019-04-22 17:10:40', 1, '18484849656', 'JPP2019-0', 0, NULL, 'JPP12567537373', '2019-04-30'),
('10.64', NULL, '2019-04-22 17:15:59', 1, '18484849656', 'JPP2019-1', 0, NULL, 'JPP12567537373', '2019-04-30'),
('2250.00', NULL, '2019-05-14 14:52:55', 0, '15849643032', 'PRU2019-0', 0, NULL, 'PRU21313132131', '2019-05-30'),
('14160.00', NULL, '2019-05-30 20:51:35', 0, '15849643032', 'PRU2019-1', 0, NULL, 'PRU21313132131', '2019-06-01'),
('38232.00', NULL, '2019-05-31 13:30:11', 0, '15849643032', 'PRU2019-2', 0, NULL, 'PRU21313132131', '2019-06-30'),
('53808.00', NULL, '2019-05-31 13:40:50', 0, '15849643032', 'PRU2019-3', 0, NULL, 'PRU21313132131', '2019-06-20'),
('90.00', NULL, '2019-07-11 14:38:30', 0, '15849643032', 'PRU2019-4', 0, NULL, 'PRU21313132131', '2019-07-11'),
('453.12', NULL, '2019-07-11 16:27:50', 1, '15849643032', 'PRU2019-5', 0, NULL, 'PRU21313132131', '2019-07-12'),
('165.20', NULL, '2019-07-22 15:36:22', 0, '15849643032', 'PRU2019-6', 0, NULL, 'PRU21313132131', '2019-07-22'),
('1146.96', NULL, '2019-07-22 15:55:28', 1, '15849643032', 'PRU2019-7', 0, NULL, 'PRU21313132131', '2019-08-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pedido`
--

CREATE TABLE `orden_pedido` (
  `codigo_pedido` char(13) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_cliente` char(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_entrega` date NOT NULL,
  `deuda` double(10,2) NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `total_pedido` double(10,2) NOT NULL,
  `estado_orden_pedido` char(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orden_pedido`
--

INSERT INTO `orden_pedido` (`codigo_pedido`, `codigo_cliente`, `fecha`, `fecha_entrega`, `deuda`, `RUC_empresa`, `total_pedido`, `estado_orden_pedido`) VALUES
('PRU-2019-0001', 'PRU-52326', '2019-09-19 21:06:08', '2019-09-30', 31602.40, '15849643032', 71602.40, '1'),
('PRU-2019-0002', 'PRU-52326', '2019-09-21 23:25:40', '2019-10-04', 37631.20, '15849643032', 57631.20, '1'),
('PRU-2019-0003', 'PRU-35340', '2019-09-23 00:19:03', '2019-10-07', 21674.40, '15849643032', 36674.40, '1'),
('PRU-2019-0004', 'PRU-35340', '2019-09-23 02:17:27', '2019-10-15', 20099.60, '15849643032', 45099.60, '1'),
('PRU-2019-0005', 'PRU-52326', '2019-09-23 02:38:30', '2019-10-15', 9257.60, '15849643032', 19257.60, '1'),
('PRU-2019-0006', 'PRU-35340', '2019-09-25 22:32:23', '2019-10-15', 6729.20, '15849643032', 11729.20, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pedido_produccion`
--

CREATE TABLE `orden_pedido_produccion` (
  `codigo_orden_pedido_produccion` char(13) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_orden_pedido` char(13) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `codigo_cliente` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `estado_orden_pedido_produccion` char(1) COLLATE utf8mb4_spanish_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_produccion`
--

CREATE TABLE `orden_produccion` (
  `cod_op` int(11) NOT NULL,
  `Numero` int(4) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cliente` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `destino` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_pedido` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `marca` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cod_linea` int(11) DEFAULT NULL,
  `cod_serie` int(11) DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `ingreso_prod` date DEFAULT NULL,
  `estado_op` tinyint(1) DEFAULT NULL,
  `cod_modelo` varchar(12) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `total_pares` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `name` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_roles`
--

CREATE TABLE `permisos_roles` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permisos_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `piso`
--

CREATE TABLE `piso` (
  `cod_piso` int(11) NOT NULL,
  `descrip_piso` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_piso` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `piso`
--

INSERT INTO `piso` (`cod_piso`, `descrip_piso`, `estado_piso`) VALUES
(1, 'Cuero', 1),
(2, 'Cuero con Recubrimiento', 1),
(3, 'Textiles Naturales y/o Sintéticos, tejidos o no', 1),
(4, 'Otros Materiales', 1),
(5, 'Combinado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantilla`
--

CREATE TABLE `plantilla` (
  `cod_plantilla` int(11) NOT NULL,
  `descrip_plantilla` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_plantilla` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `plantilla`
--

INSERT INTO `plantilla` (`cod_plantilla`, `descrip_plantilla`, `estado_plantilla`) VALUES
(1, 'Cuero', 1),
(2, 'Cuero con Recubrimiento', 1),
(3, 'Textiles Naturales y/o Sintéticos, tejidos o no', 1),
(4, 'Otros Materiales', 1),
(5, 'Combinado', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_terminado`
--

CREATE TABLE `producto_terminado` (
  `cod_producto_terminado` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descrip_producto_terminado` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `talla` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_producto_terminado` tinyint(4) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `RUC_proveedor` varchar(14) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nom_proveedor` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `direc_proveedor` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `direc_tienda` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nomb_contacto` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `telefono_contacto` char(9) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `cel_proveedor` char(9) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `correo_proveedor` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `estado_proveedor` tinyint(1) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `proveedor_subcategoria` int(11) NOT NULL,
  `correo_contacto` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nomb_contacto2` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `telefono_contacto2` char(9) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `correo_contacto2` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`RUC_proveedor`, `nom_proveedor`, `direc_proveedor`, `direc_tienda`, `nomb_contacto`, `telefono_contacto`, `cel_proveedor`, `correo_proveedor`, `estado_proveedor`, `RUC_empresa`, `proveedor_subcategoria`, `correo_contacto`, `nomb_contacto2`, `telefono_contacto2`, `correo_contacto2`) VALUES
('CIT10225587877', 'Juan Martin Salas', 'Jr. Pachacutec 901 Dpto. 501', 'Jr. Pachacutec 901 Dpto. 501', NULL, '413545', '958477454', 'tiendascalzado@gmail.com', 1, '20112233441', 381, '', '', '', ''),
('CIT10546543216', 'JUAN PEREZ SOLIS', 'Jr. Almte Guisse 1051', 'Av. Mariategui 167', NULL, '235456', '955443135', 'suelas.juan@gmail.com', 1, '20112233441', 191, '', '', '', ''),
('CIT12132165142', 'categoria 2', 'asda', 'safsa', NULL, NULL, '123213132', 'a@s.c', 1, '20112233441', 666, NULL, NULL, NULL, NULL),
('CIT12312121231', 'Austral', 'Calle las gardenias 512', 'Calle pizarro segundo piso m3', NULL, '123123', '1212', '11@a.com', 1, '20134956777', 173, '', '', '', ''),
('CIT20111212123', 'Cueros & Badana 41', 'Calle San Felipe 521', 'Calle San Felipe 414', NULL, '431121', '923123566', 'CYB1231@prv.com', 1, '20134956777', 424, '', '', '', ''),
('CIT20115356453', 'PEGATONES E.I.R.L.', 'Av. 28 de Julio 328', 'Av. Horacio Urteaga 752 Dpto. 101', NULL, '658744', '998655477', 'pegatones@adhesivos.com', 1, '20112233441', 955, '', '', '', ''),
('CIT20154455884', 'ARTICALZ S.A.C.', 'Jr. Ramón Ribeyro 234 Dpto. 503', 'Av. Horacio Urteaga 574 Dpto. 101', NULL, '423546', '965844633', 'punterascontrafuertes@artical.com', 1, '20112233441', 667, '', '', '', ''),
('CIT20154548666', 'SUELAS REAL E.I.R.L.', 'Jr. Gral. Córdova 1021 Dpto. 103', 'Jr. Gral. Córdova 1127', NULL, '854697', '965334584', 'suelasreal@hotmail.com', 1, '20112233441', 191, '', '', '', ''),
('CIT20154654864', 'SINTETICMAT E.I.R.L.', 'Av. Brasil 769 Dpto. 214', 'Av. Gral. Garzón 836 Dpto. 01', NULL, '485877', '965487878', 'sinteticosaqp@gmail.com', 1, '20112233441', 403, '', '', '', ''),
('CIT20154845465', 'CUEROS AQP S.A.C.', 'Jr. Mcal. Miller 970 Dpto. 202', 'Jr. Mcal. Miller 970 Dpto. 402', NULL, '457842', '954675581', 'cueros@aqp.sac', 1, '20112233441', 424, '', '', '', ''),
('CIT20154864556', 'TODO CALZADO S.A.C', 'Jr. Nazca 359', 'Jr. Nazca 359', NULL, '454777', '966685544', 'materialesdecalzado@gmail.com', 1, '20112233441', 874, '', '', '', ''),
('CIT20455138231', 'PIELES DEL SUR E.I.R.L.', 'MZA. J LOTE. 3-B1 PARQ. INDUSTRIAL RIO SECO AREQUIPA - AREQUIPA - CERRO COLORADO', NULL, NULL, NULL, '123456456', NULL, 1, '20112233441', 424, NULL, NULL, NULL, NULL),
('CIT21548441545', 'CURTIEMBRE SOL E.I.R.L.', 'Jr. Mcal. Miller 936', 'Jr. Gral. Cóprdova 927 Dpto. 406', 'asd', '442727244', '956412154', 'curtiembre@sol.com.pe', 1, '20112233441', 424, 'as@sc.c', 'asdsa', '411111111', 'sad@c.s'),
('CIT42343434422', 'asdsadsadd', 'asdasdsa', NULL, NULL, NULL, '545677675', 's@s.s', 0, '20112233441', 424, NULL, NULL, NULL, NULL),
('CIT42437387827', 'hermanos insu', 'aasdaf', NULL, 'jose', '174171741', '171471147', 'sdzado@gmail.com', 0, '20112233441', 381, '', '', '', ''),
('CIT47475757457', 'sadsadsadsa', 'fafsa', NULL, NULL, NULL, '757457575', NULL, 0, '20112233441', 191, '', '', '', ''),
('CIT49491313194', 'categorias', 'asdsa', 'dsada', NULL, NULL, '766756757', 'dsadsa@sc.d', 1, '20112233441', 666, NULL, NULL, NULL, NULL),
('JPP12567537373', 'asd', 'asd', 'asd', NULL, NULL, '155535235', NULL, 1, '18484849656', 424, NULL, NULL, NULL, NULL),
('PRU21313132131', 'asd', 'ads', 'ads', NULL, NULL, '123213123', 'a@s.c', 1, '15849643032', 191, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto`
--

CREATE TABLE `puesto` (
  `descrip_puesto` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `sueldo_mensual` double(8,2) DEFAULT NULL,
  `cod_tipo_trabajador` int(11) NOT NULL,
  `cod_puesto` int(11) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_puesto` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `puesto`
--

INSERT INTO `puesto` (`descrip_puesto`, `sueldo_mensual`, `cod_tipo_trabajador`, `cod_puesto`, `RUC_empresa`, `estado_puesto`) VALUES
('Aparador', 1123.00, 3, 1, '20134956777', 1),
('Aparador', 1500.00, 3, 41, '20112233441', 1),
('Supervisor', 3500.00, 3, 45, '20112233441', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regimen_laboral`
--

CREATE TABLE `regimen_laboral` (
  `descrip_regimen_laboral` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_regimen_laboral` tinyint(1) NOT NULL,
  `cod_regimen_laboral` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `regimen_laboral`
--

INSERT INTO `regimen_laboral` (`descrip_regimen_laboral`, `estado_regimen_laboral`, `cod_regimen_laboral`) VALUES
('Microempresa', 1, 1),
('Pequeña empresa', 1, 2),
('General', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regimen_renta`
--

CREATE TABLE `regimen_renta` (
  `descrip_regimen_renta` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_regimen_renta` tinyint(1) NOT NULL,
  `cod_regimen_renta` int(11) NOT NULL,
  `porcentaje` double(5,2) DEFAULT NULL,
  `afecta_a` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `regimen_renta`
--

INSERT INTO `regimen_renta` (`descrip_regimen_renta`, `estado_regimen_renta`, `cod_regimen_renta`, `porcentaje`, `afecta_a`) VALUES
('Especial', 1, 1, 1.50, 'INGRESOS'),
('REMYPE', 1, 2, 10.00, 'UTILIDADES'),
('General', 1, 3, 29.50, 'UTILIDADES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retencion_utilidades`
--

CREATE TABLE `retencion_utilidades` (
  `cod_retencion_utilidades` int(11) NOT NULL,
  `descrip_retencion_utilidades` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `porcentaje_retencion` double(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `retencion_utilidades`
--

INSERT INTO `retencion_utilidades` (`cod_retencion_utilidades`, `descrip_retencion_utilidades`, `porcentaje_retencion`) VALUES
(1, 'SI', 10.00),
(2, 'NO', 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `name` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado_rol` tinyint(1) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`name`, `description`, `created_at`, `updated_at`, `estado_rol`, `id`) VALUES
('Administrador', 'Administrador', NULL, NULL, 1, 1),
('Dueño de la Empresa', 'Dueño de la Empresa', '2018-11-09 20:00:00', '2018-11-09 20:00:00', 1, 2),
('Jefe de Logistica', 'Jefe de Logistica', '2018-11-09 20:00:00', '2018-11-09 20:00:00', 1, 3),
('Jefe de desarrollo de producto', 'desarrollo de producto', NULL, NULL, 1, 40909),
('Jefe de Costos', 'Estructura de costos', NULL, NULL, 1, 46409),
('Jefe de RRHH', 'Jefe de RRHH', NULL, NULL, 1, 69506);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie`
--

CREATE TABLE `serie` (
  `cod_serie` int(11) NOT NULL,
  `nombre_serie` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tallaInicial` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tallaFinal` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_serie` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `serie`
--

INSERT INTO `serie` (`cod_serie`, `nombre_serie`, `tallaInicial`, `tallaFinal`, `RUC_empresa`, `estado_serie`) VALUES
(10943, 'Desaguadero adulto', '38', '46', '15849643032', 1),
(28956, 'Desaguadero Joven', '30', '36', '15849643032', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie_modelo`
--

CREATE TABLE `serie_modelo` (
  `codigo` char(11) COLLATE utf8_spanish2_ci NOT NULL,
  `codigo_modelo` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `codigo_serie` int(11) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish2_ci NOT NULL,
  `RUC_empresa` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `serie_modelo`
--

INSERT INTO `serie_modelo` (`codigo`, `codigo_modelo`, `codigo_serie`, `estado`, `RUC_empresa`) VALUES
('PRU42252', 'PRU DES 001-DeJo-1', 28956, '3', '15849643032'),
('PRU445864', 'PRU VA-CAS-01-DeAd-1', 10943, '3', '15849643032'),
('PRU45008', 'PRU VAR-CAS-20', 28956, '1', '15849643032'),
('PRU493392', 'PRU DES 001-DeJo-2', 28956, '3', '15849643032'),
('PRU58425', 'PRU san-01', 28956, '1', '15849643032'),
('PRU60638', 'PRU VA-CAS-01', 10943, '1', '15849643032'),
('PRU84819', 'PRU DES 001', 28956, '1', '15849643032'),
('PRU95556', 'PRU DES 001', 10943, '1', '15849643032');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE `subcategoria` (
  `nom_subcategoria` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_subcategoria` tinyint(1) NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `cod_subcategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`nom_subcategoria`, `estado_subcategoria`, `cod_categoria`, `cod_subcategoria`) VALUES
('Plantillas', 1, 306, 173),
('Pisos', 1, 306, 191),
('Repuestos', 1, 634, 235),
('Tacones', 1, 306, 330),
('Varios Materiales', 1, 306, 381),
('Sintéticos', 1, 306, 403),
('Cueros', 1, 969, 424),
('Textiles y Telas', 1, 306, 453),
('Materiales Indirectos', 1, 634, 461),
('Badanas', 1, 969, 480),
('Troqueles', 1, 634, 496),
('Otros Insumos', 1, 306, 663),
('Forros', 1, 306, 665),
('varias categorias', 1, 306, 666),
('Punteras y Contrafuertes', 1, 306, 667),
('Latas de Corte', 1, 634, 735),
('Equipos de Protección Personal', 1, 634, 778),
('Accesorios', 1, 306, 874),
('Palmillas', 1, 306, 909),
('Cambrillones', 1, 306, 917),
('ejemplo', 0, 306, 921),
('Adhesivos', 1, 306, 955),
('Hilos', 1, 306, 956),
('Hormas', 1, 634, 969),
('Materiales Administrativos', 1, 634, 975);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talla_pares`
--

CREATE TABLE `talla_pares` (
  `cod_op` int(11) NOT NULL,
  `cod_serie` int(11) NOT NULL,
  `talla` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `pares` int(8) NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `talla_pares`
--

INSERT INTO `talla_pares` (`cod_op`, `cod_serie`, `talla`, `pares`, `RUC_empresa`) VALUES
(48647, 98373, '1', 0, '20134956777'),
(48647, 98373, '2', 0, '20134956777'),
(48647, 98373, '3', 0, '20134956777'),
(48647, 98373, '4', 0, '20134956777'),
(48647, 98373, '5', 3, '20134956777'),
(48647, 98373, '6', 0, '20134956777'),
(70959, 42646, '34', 3, '20134956777'),
(70959, 42646, '35', 2, '20134956777'),
(70959, 42646, '36', 1, '20134956777'),
(70959, 42646, '37', 2, '20134956777'),
(70959, 42646, '38', 4, '20134956777'),
(48387, 99253, '38', 2, '20134956777'),
(48387, 99253, '39', 3, '20134956777'),
(48387, 99253, '40', 1, '20134956777'),
(48387, 99253, '41', 2, '20134956777'),
(48387, 99253, '42', 2, '20134956777'),
(48387, 99253, '43', 2, '20134956777'),
(84197, 42646, '34', 3, '20134956777'),
(84197, 42646, '35', 2, '20134956777'),
(84197, 42646, '36', 3, '20134956777'),
(84197, 42646, '37', 3, '20134956777'),
(84197, 42646, '38', 1, '20134956777'),
(62590, 15901, '21', 2, '20134956777'),
(62590, 15901, '22', 2, '20134956777'),
(62590, 15901, '23', 3, '20134956777'),
(62590, 15901, '24', 4, '20134956777'),
(62590, 15901, '25', 5, '20134956777'),
(62590, 15901, '26', 3, '20134956777'),
(77347, 32579, '38', 3, '20134956777'),
(77347, 32579, '39', 2, '20134956777'),
(77347, 32579, '40', 4, '20134956777'),
(77347, 32579, '41', 5, '20134956777'),
(77347, 32579, '42', 1, '20134956777'),
(28124, 32579, '38', 3, '20134956777'),
(28124, 32579, '39', 2, '20134956777'),
(28124, 32579, '40', 4, '20134956777'),
(28124, 32579, '41', 2, '20134956777'),
(28124, 32579, '42', 5, '20134956777'),
(79026, 99253, '38', 5, '20134956777'),
(79026, 99253, '39', 10, '20134956777'),
(79026, 99253, '40', 5, '20134956777'),
(79026, 99253, '41', 13, '20134956777'),
(79026, 99253, '42', 13, '20134956777'),
(79026, 99253, '43', 5, '20134956777'),
(31281, 32579, '38', 3, '20134956777'),
(31281, 32579, '39', 4, '20134956777'),
(31281, 32579, '40', 5, '20134956777'),
(31281, 32579, '41', 8, '20134956777'),
(31281, 32579, '42', 5, '20134956777'),
(49676, 99253, '38', 5, '20134956777'),
(49676, 99253, '39', 4, '20134956777'),
(49676, 99253, '40', 7, '20134956777'),
(49676, 99253, '41', 8, '20134956777'),
(49676, 99253, '42', 5, '20134956777'),
(49676, 99253, '43', 3, '20134956777'),
(94769, 42646, '34', 10, '20134956777'),
(94769, 42646, '35', 12, '20134956777'),
(94769, 42646, '36', 32, '20134956777'),
(94769, 42646, '37', 12, '20134956777'),
(94769, 42646, '38', 12, '20134956777'),
(44521, 32579, '38', 4, '20134956777'),
(44521, 32579, '39', 5, '20134956777'),
(44521, 32579, '40', 8, '20134956777'),
(44521, 32579, '41', 1, '20134956777'),
(44521, 32579, '42', 9, '20134956777'),
(90231, 42646, '34', 8, '20134956777'),
(90231, 42646, '35', 5, '20134956777'),
(90231, 42646, '36', 4, '20134956777'),
(90231, 42646, '37', 9, '20134956777'),
(90231, 42646, '38', 6, '20134956777'),
(68647, 61717, '35', 6, '20134956777'),
(68647, 61717, '36', 8, '20134956777'),
(68647, 61717, '37', 4, '20134956777'),
(68647, 61717, '38', 7, '20134956777'),
(68647, 61717, '39', 6, '20134956777'),
(62695, 42646, '34', 8, '20134956777'),
(62695, 42646, '35', 9, '20134956777'),
(62695, 42646, '36', 12, '20134956777'),
(62695, 42646, '37', 8, '20134956777'),
(62695, 42646, '38', 7, '20134956777'),
(71196, 99253, '38', 6, '20134956777'),
(71196, 99253, '39', 7, '20134956777'),
(71196, 99253, '40', 8, '20134956777'),
(71196, 99253, '41', 10, '20134956777'),
(71196, 99253, '42', 5, '20134956777'),
(71196, 99253, '43', 4, '20134956777'),
(35514, 99253, '38', 8, '20134956777'),
(35514, 99253, '39', 7, '20134956777'),
(35514, 99253, '40', 6, '20134956777'),
(35514, 99253, '41', 4, '20134956777'),
(35514, 99253, '42', 8, '20134956777'),
(35514, 99253, '43', 3, '20134956777'),
(41834, 99253, '38', 8, '20134956777'),
(41834, 99253, '39', 9, '20134956777'),
(41834, 99253, '40', 6, '20134956777'),
(41834, 99253, '41', 7, '20134956777'),
(41834, 99253, '42', 4, '20134956777'),
(41834, 99253, '43', 8, '20134956777'),
(11686, 32579, '38', 8, '20134956777'),
(11686, 32579, '39', 7, '20134956777'),
(11686, 32579, '40', 9, '20134956777'),
(11686, 32579, '41', 2, '20134956777'),
(11686, 32579, '42', 4, '20134956777'),
(70786, 32579, '38', 8, '20134956777'),
(70786, 32579, '39', 9, '20134956777'),
(70786, 32579, '40', 4, '20134956777'),
(70786, 32579, '41', 6, '20134956777'),
(70786, 32579, '42', 2, '20134956777'),
(26354, 99253, '38', 8, '20134956777'),
(26354, 99253, '39', 7, '20134956777'),
(26354, 99253, '40', 6, '20134956777'),
(26354, 99253, '41', 4, '20134956777'),
(26354, 99253, '42', 8, '20134956777'),
(26354, 99253, '43', 9, '20134956777'),
(43726, 69449, '27', 8, '20134956777'),
(43726, 69449, '28', 4, '20134956777'),
(43726, 69449, '29', 5, '20134956777'),
(43726, 69449, '30', 8, '20134956777'),
(43726, 69449, '31', 6, '20134956777'),
(43726, 69449, '32', 4, '20134956777'),
(91556, 42646, '34', 6, '20134956777'),
(91556, 42646, '35', 4, '20134956777'),
(91556, 42646, '36', 5, '20134956777'),
(91556, 42646, '37', 8, '20134956777'),
(91556, 42646, '38', 4, '20134956777'),
(28425, 42646, '34', 4, '20134956777'),
(28425, 42646, '35', 3, '20134956777'),
(28425, 42646, '36', 5, '20134956777'),
(28425, 42646, '37', 1, '20134956777'),
(28425, 42646, '38', 4, '20134956777'),
(70296, 69449, '27', 4, '20134956777'),
(70296, 69449, '28', 7, '20134956777'),
(70296, 69449, '29', 8, '20134956777'),
(70296, 69449, '30', 5, '20134956777'),
(70296, 69449, '31', 9, '20134956777'),
(70296, 69449, '32', 4, '20134956777'),
(80373, 42646, '34', 4, '20134956777'),
(80373, 42646, '35', 2, '20134956777'),
(80373, 42646, '36', 3, '20134956777'),
(80373, 42646, '37', 8, '20134956777'),
(80373, 42646, '38', 5, '20134956777'),
(93193, 42646, '34', 4, '20134956777'),
(93193, 42646, '35', 8, '20134956777'),
(93193, 42646, '36', 6, '20134956777'),
(93193, 42646, '37', 3, '20134956777'),
(93193, 42646, '38', 4, '20134956777'),
(91534, 42646, '34', 4, '20134956777'),
(91534, 42646, '35', 8, '20134956777'),
(91534, 42646, '36', 6, '20134956777'),
(91534, 42646, '37', 4, '20134956777'),
(91534, 42646, '38', 8, '20134956777'),
(11367, 99253, '38', 8, '20134956777'),
(11367, 99253, '39', 9, '20134956777'),
(11367, 99253, '40', 4, '20134956777'),
(11367, 99253, '41', 7, '20134956777'),
(11367, 99253, '42', 8, '20134956777'),
(11367, 99253, '43', 9, '20134956777'),
(73248, 32579, '38', 9, '20134956777'),
(73248, 32579, '39', 8, '20134956777'),
(73248, 32579, '40', 4, '20134956777'),
(73248, 32579, '41', 7, '20134956777'),
(73248, 32579, '42', 8, '20134956777'),
(23207, 32579, '38', 7, '20134956777'),
(23207, 32579, '39', 5, '20134956777'),
(23207, 32579, '40', 8, '20134956777'),
(23207, 32579, '41', 4, '20134956777'),
(23207, 32579, '42', 4, '20134956777'),
(21331, 42646, '34', 7, '20134956777'),
(21331, 42646, '35', 5, '20134956777'),
(21331, 42646, '36', 4, '20134956777'),
(21331, 42646, '37', 8, '20134956777'),
(21331, 42646, '38', 8, '20134956777'),
(42733, 15901, '21', 0, '20134956777'),
(42733, 15901, '22', 0, '20134956777'),
(42733, 15901, '23', 0, '20134956777'),
(42733, 15901, '24', 0, '20134956777'),
(42733, 15901, '25', 0, '20134956777'),
(42733, 15901, '26', 789, '20134956777'),
(25098, 15901, '21', 1, '20134956777'),
(25098, 15901, '22', 2, '20134956777'),
(25098, 15901, '23', 3, '20134956777'),
(25098, 15901, '24', 4, '20134956777'),
(25098, 15901, '25', 4, '20134956777'),
(25098, 15901, '26', 5, '20134956777'),
(70253, 61717, '35', 2, '20134956777'),
(70253, 61717, '36', 3, '20134956777'),
(70253, 61717, '37', 4, '20134956777'),
(70253, 61717, '38', 5, '20134956777'),
(70253, 61717, '39', 2, '20134956777'),
(12545, 69399, '38', 35, '20112233441'),
(12545, 69399, '39', 25, '20112233441'),
(12545, 69399, '40', 73, '20112233441'),
(12545, 69399, '41', 56, '20112233441'),
(12545, 69399, '42', 46, '20112233441'),
(12545, 69399, '43', 30, '20112233441'),
(78541, 69399, '38', 34, '20112233441'),
(78541, 69399, '39', 48, '20112233441'),
(78541, 69399, '40', 72, '20112233441'),
(78541, 69399, '41', 82, '20112233441'),
(78541, 69399, '42', 65, '20112233441'),
(78541, 69399, '43', 23, '20112233441'),
(32249, 36115, '34', 35, '20112233441'),
(32249, 36115, '35', 42, '20112233441'),
(32249, 36115, '36', 85, '20112233441'),
(32249, 36115, '37', 74, '20112233441'),
(32249, 36115, '38', 0, '20112233441'),
(46222, 96760, '35', 45, '20112233441'),
(46222, 96760, '36', 78, '20112233441'),
(46222, 96760, '37', 64, '20112233441'),
(46222, 96760, '38', 23, '20112233441'),
(46222, 96760, '39', 58, '20112233441'),
(24209, 69399, '38', 98, '20112233441'),
(24209, 69399, '39', 85, '20112233441'),
(24209, 69399, '40', 74, '20112233441'),
(24209, 69399, '41', 201, '20112233441'),
(24209, 69399, '42', 42, '20112233441'),
(24209, 69399, '43', 35, '20112233441'),
(55162, 69399, '38', 85, '20112233441'),
(55162, 69399, '39', 74, '20112233441'),
(55162, 69399, '40', 65, '20112233441'),
(55162, 69399, '41', 78, '20112233441'),
(55162, 69399, '42', 95, '20112233441'),
(55162, 69399, '43', 99, '20112233441'),
(86292, 36115, '34', 48, '20112233441'),
(86292, 36115, '35', 75, '20112233441'),
(86292, 36115, '36', 89, '20112233441'),
(86292, 36115, '37', 65, '20112233441'),
(86292, 36115, '38', 48, '20112233441'),
(42639, 69399, '38', 48, '20112233441'),
(42639, 69399, '39', 48, '20112233441'),
(42639, 69399, '40', 75, '20112233441'),
(42639, 69399, '41', 86, '20112233441'),
(42639, 69399, '42', 95, '20112233441'),
(42639, 69399, '43', 64, '20112233441'),
(14044, 96760, '35', 54, '20112233441'),
(14044, 96760, '36', 78, '20112233441'),
(14044, 96760, '37', 98, '20112233441'),
(14044, 96760, '38', 64, '20112233441'),
(14044, 96760, '39', 48, '20112233441'),
(32132, 69399, '38', 87, '20112233441'),
(32132, 69399, '39', 78, '20112233441'),
(32132, 69399, '40', 98, '20112233441'),
(32132, 69399, '41', 76, '20112233441'),
(32132, 69399, '42', 48, '20112233441'),
(32132, 69399, '43', 98, '20112233441'),
(34416, 36115, '34', 89, '20112233441'),
(34416, 36115, '35', 78, '20112233441'),
(34416, 36115, '36', 68, '20112233441'),
(34416, 36115, '37', 98, '20112233441'),
(34416, 36115, '38', 78, '20112233441'),
(15450, 69399, '38', 98, '20112233441'),
(15450, 69399, '39', 75, '20112233441'),
(15450, 69399, '40', 85, '20112233441'),
(15450, 69399, '41', 96, '20112233441'),
(15450, 69399, '42', 98, '20112233441'),
(15450, 69399, '43', 75, '20112233441'),
(96996, 96760, '35', 89, '20112233441'),
(96996, 96760, '36', 87, '20112233441'),
(96996, 96760, '37', 47, '20112233441'),
(96996, 96760, '38', 85, '20112233441'),
(96996, 96760, '39', 89, '20112233441'),
(96996, 96760, '35', 89, '20112233441'),
(96996, 96760, '36', 87, '20112233441'),
(96996, 96760, '37', 47, '20112233441'),
(96996, 96760, '38', 85, '20112233441'),
(96996, 96760, '39', 89, '20112233441'),
(32059, 69399, '38', 86, '20112233441'),
(32059, 69399, '39', 98, '20112233441'),
(32059, 69399, '40', 74, '20112233441'),
(32059, 69399, '41', 85, '20112233441'),
(32059, 69399, '42', 96, '20112233441'),
(32059, 69399, '43', 98, '20112233441'),
(98541, 69399, '38', 98, '20112233441'),
(98541, 69399, '39', 87, '20112233441'),
(98541, 69399, '40', 58, '20112233441'),
(98541, 69399, '41', 68, '20112233441'),
(98541, 69399, '42', 98, '20112233441'),
(98541, 69399, '43', 78, '20112233441'),
(60695, 36115, '34', 87, '20112233441'),
(60695, 36115, '35', 88, '20112233441'),
(60695, 36115, '36', 98, '20112233441'),
(60695, 36115, '37', 65, '20112233441'),
(60695, 36115, '38', 75, '20112233441'),
(68046, 69399, '38', 98, '20112233441'),
(68046, 69399, '39', 78, '20112233441'),
(68046, 69399, '40', 96, '20112233441'),
(68046, 69399, '41', 85, '20112233441'),
(68046, 69399, '42', 74, '20112233441'),
(68046, 69399, '43', 95, '20112233441'),
(99750, 96760, '35', 89, '20112233441'),
(99750, 96760, '36', 78, '20112233441'),
(99750, 96760, '37', 85, '20112233441'),
(99750, 96760, '38', 75, '20112233441'),
(99750, 96760, '39', 98, '20112233441'),
(23878, 36115, '34', 87, '20112233441'),
(23878, 36115, '35', 88, '20112233441'),
(23878, 36115, '36', 98, '20112233441'),
(23878, 36115, '37', 75, '20112233441'),
(23878, 36115, '38', 48, '20112233441'),
(28864, 69399, '38', 89, '20112233441'),
(28864, 69399, '39', 78, '20112233441'),
(28864, 69399, '40', 75, '20112233441'),
(28864, 69399, '41', 86, '20112233441'),
(28864, 69399, '42', 95, '20112233441'),
(28864, 69399, '43', 65, '20112233441'),
(42976, 69399, '38', 85, '20112233441'),
(42976, 69399, '39', 47, '20112233441'),
(42976, 69399, '40', 58, '20112233441'),
(42976, 69399, '41', 68, '20112233441'),
(42976, 69399, '42', 74, '20112233441'),
(42976, 69399, '43', 98, '20112233441'),
(75446, 36115, '34', 89, '20112233441'),
(75446, 36115, '35', 87, '20112233441'),
(75446, 36115, '36', 58, '20112233441'),
(75446, 36115, '37', 78, '20112233441'),
(75446, 36115, '38', 95, '20112233441'),
(77204, 69399, '38', 85, '20112233441'),
(77204, 69399, '39', 78, '20112233441'),
(77204, 69399, '40', 98, '20112233441'),
(77204, 69399, '41', 98, '20112233441'),
(77204, 69399, '42', 96, '20112233441'),
(77204, 69399, '43', 99, '20112233441'),
(80136, 96760, '35', 87, '20112233441'),
(80136, 96760, '36', 89, '20112233441'),
(80136, 96760, '37', 87, '20112233441'),
(80136, 96760, '38', 65, '20112233441'),
(80136, 96760, '39', 88, '20112233441'),
(15746, 69399, '38', 88, '20112233441'),
(15746, 69399, '39', 78, '20112233441'),
(15746, 69399, '40', 96, '20112233441'),
(15746, 69399, '41', 85, '20112233441'),
(15746, 69399, '42', 75, '20112233441'),
(15746, 69399, '43', 99, '20112233441'),
(15848, 69399, '38', 89, '20112233441'),
(15848, 69399, '39', 87, '20112233441'),
(15848, 69399, '40', 86, '20112233441'),
(15848, 69399, '41', 96, '20112233441'),
(15848, 69399, '42', 48, '20112233441'),
(15848, 69399, '43', 85, '20112233441'),
(15848, 69399, '38', 89, '20112233441'),
(15848, 69399, '39', 87, '20112233441'),
(15848, 69399, '40', 86, '20112233441'),
(15848, 69399, '41', 96, '20112233441'),
(15848, 69399, '42', 48, '20112233441'),
(15848, 69399, '43', 85, '20112233441'),
(55271, 69399, '38', 2, '20112233441'),
(55271, 69399, '39', 2, '20112233441'),
(55271, 69399, '40', 3, '20112233441'),
(55271, 69399, '41', 2, '20112233441'),
(55271, 69399, '42', 2, '20112233441'),
(55271, 69399, '43', 1, '20112233441'),
(11231, 69399, '38', 2, '20112233441'),
(11231, 69399, '39', 2, '20112233441'),
(11231, 69399, '40', 3, '20112233441'),
(11231, 69399, '41', 1, '20112233441'),
(11231, 69399, '42', 2, '20112233441'),
(11231, 69399, '43', 2, '20112233441'),
(69500, 96760, '35', 3, '20112233441'),
(69500, 96760, '36', 2, '20112233441'),
(69500, 96760, '37', 4, '20112233441'),
(69500, 96760, '38', 2, '20112233441'),
(69500, 96760, '39', 1, '20112233441'),
(15703, 69399, '38', 1, '20112233441'),
(15703, 69399, '39', 2, '20112233441'),
(15703, 69399, '40', 4, '20112233441'),
(15703, 69399, '41', 2, '20112233441'),
(15703, 69399, '42', 3, '20112233441'),
(15703, 69399, '43', 0, '20112233441'),
(62001, 36115, '34', 2, '20112233441'),
(62001, 36115, '35', 2, '20112233441'),
(62001, 36115, '36', 4, '20112233441'),
(62001, 36115, '37', 2, '20112233441'),
(62001, 36115, '38', 2, '20112233441'),
(56600, 36115, '34', 3, '20112233441'),
(56600, 36115, '35', 5, '20112233441'),
(56600, 36115, '36', 4, '20112233441'),
(56600, 36115, '37', 2, '20112233441'),
(56600, 36115, '38', 1, '20112233441');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempo_recepcion`
--

CREATE TABLE `tiempo_recepcion` (
  `id_detalle_oc` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `codigo_tienda` char(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_tienda` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion_tienda` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` char(9) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` char(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_activo`
--

CREATE TABLE `tipo_activo` (
  `concepto` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `anios_vida_util` int(11) NOT NULL,
  `cod_tipo_activo` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_activo`
--

INSERT INTO `tipo_activo` (`concepto`, `anios_vida_util`, `cod_tipo_activo`, `estado`) VALUES
('Edificios y Construcciones', 33, 4441, 1),
('Maquinaria y Equipo Industrial', 10, 4442, 1),
('Herramientas', 10, 4443, 1),
('Muebles y Enseres', 10, 4444, 1),
('Equipos de Computo y Tecnologias de Informacion', 4, 4445, 1),
('Vehiculos de Transporte', 5, 4446, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_adquisicion`
--

CREATE TABLE `tipo_adquisicion` (
  `descrip_tipo_adquisicion` varchar(70) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_tipo_adquisicion` int(11) NOT NULL,
  `estado_adquisicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_adquisicion`
--

INSERT INTO `tipo_adquisicion` (`descrip_tipo_adquisicion`, `cod_tipo_adquisicion`, `estado_adquisicion`) VALUES
('Repotenciado', 908, 1),
('Nuevo', 909, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contribuyente`
--

CREATE TABLE `tipo_contribuyente` (
  `descrip_tipo_contribuyente` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_tipo_contribuyente` tinyint(1) NOT NULL,
  `cod_tipo_contribuyente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_contribuyente`
--

INSERT INTO `tipo_contribuyente` (`descrip_tipo_contribuyente`, `estado_tipo_contribuyente`, `cod_tipo_contribuyente`) VALUES
('SAC', 1, 39656),
('EMPRESA INDIVIDUAL DE RESPONSABILIDAD LIMITADA', 1, 48333),
('EIRL', 1, 62712),
('n', 0, 66067),
('SOCIEDAD ANÓNIMA ABIERTA', 1, 77183),
('SOCIEDAD ANÓNIMA CERRADA', 1, 83127),
('PERSONA NATURAL', 1, 94442);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_desarrollo`
--

CREATE TABLE `tipo_desarrollo` (
  `cod_tipo_desarrollo` int(11) NOT NULL,
  `descrip_tipo_desarrollo` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_desarrollo`
--

INSERT INTO `tipo_desarrollo` (`cod_tipo_desarrollo`, `descrip_tipo_desarrollo`, `estado`) VALUES
(1, 'Prototipo', 1),
(2, 'Hormas', 1),
(3, 'Troquel', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_gasto`
--

CREATE TABLE `tipo_gasto` (
  `descripcion` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_tipo_gasto` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_gasto`
--

INSERT INTO `tipo_gasto` (`descripcion`, `cod_tipo_gasto`, `estado`) VALUES
('Materiales Indirectos', 10101, 1),
('Repuestos y Herramientas de Maquina', 10102, 1),
('Equipos de Seguridad', 10103, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `cod_tipo_trabajador` int(11) NOT NULL,
  `cod_tipo_pago` int(11) NOT NULL,
  `unidad` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_seguro`
--

CREATE TABLE `tipo_seguro` (
  `cod` char(2) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` char(10) COLLATE utf8_spanish2_ci NOT NULL,
  `porcentaje` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo_seguro`
--

INSERT INTO `tipo_seguro` (`cod`, `nombre`, `porcentaje`) VALUES
('1', 'SIS', 0),
('2', 'ESSALUD', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_trabajador`
--

CREATE TABLE `tipo_trabajador` (
  `descrip_tipo_trabajador` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_tipo_trabajador` tinyint(1) NOT NULL,
  `cod_tipo_trabajador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_trabajador`
--

INSERT INTO `tipo_trabajador` (`descrip_tipo_trabajador`, `estado_tipo_trabajador`, `cod_tipo_trabajador`) VALUES
('Planilla', 1, 1),
('Jornal', 1, 2),
('Destajo', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `DNI_trabajador` varchar(13) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombres` varchar(40) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellido_paterno` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellido_materno` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `telefono` char(9) COLLATE utf8mb4_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `cod_distrito` char(6) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `sexo` varchar(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `puesto` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `experiencia_laboral` varchar(500) COLLATE utf8mb4_spanish_ci DEFAULT 'Ninguna',
  `estado_trabajador` tinyint(1) NOT NULL,
  `cod_tipo_trabajador` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `g_instruccion` char(2) COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario` char(8) COLLATE utf8mb4_spanish_ci NOT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`DNI_trabajador`, `nombres`, `apellido_paterno`, `apellido_materno`, `telefono`, `direccion`, `cod_distrito`, `sexo`, `fecha_nacimiento`, `puesto`, `foto`, `experiencia_laboral`, `estado_trabajador`, `cod_tipo_trabajador`, `cod_area`, `g_instruccion`, `fecha_registro`, `usuario`, `RUC_empresa`, `correo`) VALUES
('07180571', 'DAGOBERTO', 'BARRERA', 'ARISTA', '', 'Av. Horacio Urteaga 574 Dpto. 101', '1', 'H', '1986-05-22', 'Cortador 01', NULL, 'Ninguna', 0, 3, 27405, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('07195806', 'ROSARIO', 'HERRERA', 'HUARANGA', '999666522', 'Jr. Mcal. Miller 933 Dpto. 302', '1', 'M', '1974-08-01', 'Encargado de Almacén de Cueros', NULL, 'Ninguna', 1, 2, 47812, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('07227131', 'JOSE', 'NORABUENA', 'CHAMBI', '999666522', 'Jr. Ramón Dagnino 241 Dpto. 210', '1', 'H', '1982-05-03', 'Jefe de Logística', NULL, 'Ninguna', 1, 1, 47812, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('07258710', 'MIGUEL', 'LOPEZ', 'CHORRES', '414171741', 'Jr. Ramón Ribeyro 234 Dpto. 504', '1', 'H', '1989-06-25', 'Cortador 02', NULL, 'Ninguna', 1, 2, 27405, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('07639874', 'LUZ', 'PALOMINO', 'VERASTEGUI', '444444444', 'Av. Cuba 167', '1', 'M', '1982-05-02', 'Encargado de Almacén Insumos', NULL, 'Ninguna', 1, 1, 47812, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('16191591', 'LUZ', 'ALE', 'ROJAS', '', 'Puente Zamacola Mza- C Lote 4 Calle Sanushi  103 Cerro Colorado', '1', 'M', '1981-01-05', 'Operario 003', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('18095992', 'NICK', 'YANEZ', 'QUILCATE', '', 'Av. Arenales 970 Dpto. 512', '1', 'H', '1994-08-03', 'Encargado de Almacén Pisos', NULL, 'Ninguna', 1, 1, 47812, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('29386526', 'RICARDO', 'PINTO', 'GONZALES', '', 'Portales de Cayma  I-11', '1', 'H', '1961-02-05', 'Vendedor', NULL, 'Ninguna', 1, 1, 53586, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('29393353', 'MANUEL', 'CONDORI', 'APAZA', '', 'PARQUE INDUSTRIAL RIO SECO MANZADA  B LOTE 11 A', '1', 'H', '0057-07-20', 'JEFE DE PRODUCCION', NULL, 'Ninguna', 1, 2, 87883, '', '0000-00-00 00:00:00', '0', '20539686195', ''),
('29553527', 'MARIO', 'UMANA', 'CAJALA', '', 'Calle Vista Alegre 202 Cerro Colorado', '1', 'H', '1969-09-20', 'Gerente', NULL, 'Ninguna', 1, 1, 36637, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('29699371', 'LOURDES', 'FERNANDEZ', 'FERNANDEZ', '', 'Urb monte bello L-15 Cerro Colorado', '1', 'H', '1972-09-24', 'Gerente Administrativo', NULL, 'Ninguna', 1, 1, 36637, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('40319040', 'RAUL', 'HUAHUASONCCO', 'PAXI', '', 'Calle Vista Alegre 200 La Libertad  Cerro Colorado', '1', 'H', '1977-08-15', 'Encargado de Produccion', NULL, 'Ninguna', 1, 1, 87883, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('40353852', 'ROSALIO', 'ACERO', 'MAMANI', '', 'Urb. Los Camineros Lt 30 Mza-K Yura', '1', 'H', '1975-09-04', 'Chofer', NULL, 'Ninguna', 1, 2, 47812, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('40622868', 'WILBERT', 'CHUCTAYA', 'SULLA', '', 'Asociación Pro Vivienda Jorge Chavez  Mz I Lote 9', '1', 'H', '1980-04-22', 'desarrollo y patronaje', NULL, 'Ninguna', 1, 2, 83307, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('42916025', 'ALFREDO', 'MAMANI', 'ARELA', '', 'Cerro Colorado', '1', 'H', '1985-02-05', 'desarrollo del calzado', NULL, 'Ninguna', 1, 1, 83307, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('43154807', 'ROXANA', 'QUISPE', 'CCORIHUAMAN', '', 'Mujeres con Esperanza  s/n Cayma', '1', 'M', '1985-09-15', 'pintado de cuero', NULL, 'Ninguna', 1, 2, 30844, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('43282616', 'REYNA', 'MAMANI', 'CALACHUA', '', 'Jose Carlos Maria Tegui M-B3 Lote -8 Casimiro Cuadros  II Cayma', '1', 'M', '1985-10-22', 'Operario  001', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('43357268', 'RUTH', 'CALDERON', 'NAYHUA', '', 'Asentamiento Humano  Hijos de la Ciudad de Dios Mz H  Lote 16 Yura', '1', 'M', '1985-10-16', 'Operario 004', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('44235061', 'GABRIELA', 'FERNANDEZ', 'FERNANDEZ', '', 'Pasaje Jose Carlos Maria Tegui 113 La Tomilla Cayma', '1', 'M', '1987-02-25', 'Contador', NULL, 'Ninguna', 1, 2, 36637, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('45513159', 'CANDIDA', 'MINAYA', 'HUAMANI', '', 'Mza-M Lote 6 Pueblo Joven Juan Velasco Alvarado  Cayma', '1', 'M', '1992-12-20', 'Vendedor', NULL, 'Ninguna', 1, 3, 53586, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('45670630', 'MILWAR', 'MAYHUA', 'ASENCIO', '', 'Villa Cerillos Cerro Colorado', '1', 'H', '1989-02-04', 'Cardado de Suelas', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('45823790', 'JAIME', 'MENDOZA', 'ALCAMARI', '', 'Asociación Virgen de la Candelaria Cayma', '1', 'H', '1989-07-25', 'Operario de Suela Natural', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('46243080', 'GUSTAVO', 'BUSTINZA', 'MANRIQUE', '', 'rafael belaunde zona C Mz k Lt 18', '1', 'H', '1990-03-17', 'Jefe de logistica', NULL, 'Ninguna', 1, 2, 47812, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('46872280', 'LUCILA', 'VILCA', 'HANCCO', '', 'Jose Luis Bustamante Rivero Sector 2 Mz- A Lote 5 Cerro Colorado', '1', 'M', '1991-03-12', 'Operario de Costura Manual', NULL, 'Ninguna', 1, 2, 79770, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('46951441', 'DELIA', 'CACERES', 'CACERES', '', 'Parque Industria APIPA  Cerro Colorado\r\nSector  III Mza- D', '1', 'M', '1990-10-10', 'Aparador  10', NULL, 'Ninguna', 1, 3, 27188, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('47094403', 'EMIR', 'YUCRA', 'MAMANI', '', 'Av 28 de Julio 882 Cayma', '1', 'H', '1985-06-25', 'Cementado de Suelas', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('47231678', 'OSCAR', 'PAYE', 'ANCACHI', '', 'Villa Cerrilos Mz- I  Lote - 16 Cerro Colorado', '1', 'H', '1991-03-04', 'Cortador 005', NULL, 'Ninguna', 1, 3, 27405, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('47409673', 'ROGER', 'PUMA', 'QUISPE', '', 'Asociacion Jose Juis Bustamante y Rivero Sector III Mz- O Lote 10 Cerro Colorado', '1', 'H', '1991-11-02', 'Cardado de Horma|', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('47668285', 'XUXAN', 'QUISPE', 'CHURATA', '', 'Juan Manuel Polar 302 Acequia Alta Cayma', '1', 'M', '1991-11-12', 'Secretaria', NULL, 'Ninguna', 1, 2, 36637, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('47866440', 'ELVIS', 'CORDOVA', 'MANGO', '', 'Asociación 28 de Julio Zona A Mz- C Lote 8 Cerro Colorado', '1', 'H', '1992-02-16', 'cortador 002', NULL, 'Ninguna', 1, 3, 27405, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('60399888', 'ROSEL', 'HUAMANI', 'SARAYASI', '', 'Andres Avelino Mz-C Lote 3 Cayma', '1', 'H', '1997-09-05', 'Pegamento en la Suela|', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('66631231', 'MARCO', 'TABARES', 'RODRIGUEZ', '', 'Calle Melgar 244', '1', 'H', '2000-12-12', 'Aparador', NULL, 'Ninguna', 1, 1, 11297, '', '0000-00-00 00:00:00', '0', '20134956777', ''),
('70900671', 'ROMARIO', 'LAURA', 'RUTIA', '', 'UPIS SALVADOR 123 CERRO COLORADO', '1', 'H', '1987-08-18', 'CORTADOR 003', NULL, 'Ninguna', 1, 3, 27405, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('71074444', 'ERNESTO', 'ALFARO', 'QUISPE', '', 'APIPA Sector 11  Mz-G Lote 5 Cerro Colorado', '1', 'H', '1991-06-02', 'Operario Pre- Acabado', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('71093414', 'DANY', 'QUICO', 'NOA', '', 'Jose Luis Bustamante y Rivero Sectro 4 Cerro Colorado |', '1', 'H', '1998-02-02', 'Operario de Pegado y Safado', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('72034727', 'YANET', 'YUCRA', 'ALVAREZ', '', 'Upis 19 de Enero 1-4 Cayma', '1', 'M', '1995-10-24', 'Troquelado y Pagado Plantillas', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('72153759', 'JUDITH', 'ANCONEIRA', 'SANCHEZ', '', 'Asociacion  Andres Avelino Caceres Mz- M  Lote 21 Cerro Colorado', '1', 'M', '1993-02-27', 'empastado de punteras', NULL, 'Ninguna', 1, 2, 79770, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('72667879', 'JOSE DANIELS', 'PERE', 'LOPE', '999666522', 'jose luis f-521', '27', 'H', '1996-02-01', 'Encagado de programar el erp', NULL, 'Experiencia desarrollando sistemas web\r\nExperiencia desarrollando sistemas moivles\r\nExperiencia', 1, 1, 27405, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('73532801', 'AURORA', 'ASCENCIO', 'ESCOBAR', '', 'Cerro Colorado', '1', 'M', '1999-12-05', 'Costura de Plantillas', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('73758614', 'YESSICA', 'LEQUE', 'CRUZ', '', 'Asociación Francisco Garcia Calderon Mz-O Lote 3 Cerro Colorado', '1', 'M', '2000-05-04', 'Operario 002', NULL, 'Ninguna', 1, 2, 11297, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('73778202', 'REYNA', 'ESCOBAR', 'MAYHUA', '', 'Mz-N Lote 6 Pallpata Cayma', '1', 'M', '1998-11-28', 'Operario de Empastado 002', NULL, 'Ninguna', 1, 2, 79770, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('74227017', 'VANESA', 'FLORES', 'CAZANI', '', 'Urpi  el Salvador Mz-A Lote A Cerro Colorado', '1', 'M', '1997-12-10', 'Grabados en cuero', NULL, 'Ninguna', 1, 2, 30844, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('74613165', 'LOURDES', 'MENDOZA', 'QUISPE', '', 'Asociación Villa Paraíso La Isla Mz-A Lote 6 Cerro Colorado', '1', 'M', '1997-09-18', 'Aparador 003', NULL, 'Ninguna', 1, 3, 27188, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('75459522', 'PAMELA', 'PACOMPIA', 'LIMACHE', '', 'AV-54 Cerro Colorado', '1', 'M', '2000-06-07', 'Operario Desvastado 001', NULL, 'Ninguna', 1, 3, 30844, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('76246682', 'PAULINA', 'HUAMANE', 'MEZA', '', 'Asociación Mario Vargas LLosa Cerro Colorado', '1', 'M', '1996-05-04', 'Aparador 005', NULL, 'Ninguna', 1, 3, 27188, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('76408955', 'ROXANA', 'DIAZ', 'ELGUERA', '', 'Victor Andres  Belaunde  Comite 24 Mza-I-2 Lote 2 Zona C  Cerro Colorado', '1', 'M', '1997-02-07', 'conteo y entrega de piezas', NULL, 'Ninguna', 1, 2, 30844, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('76479738', 'ALBINO', 'HUAMANI', 'ASENCIO', '', 'Villa Cerrillos Cerro Colorado', '1', 'H', '2002-02-10', 'Limpieza de Suelas', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('76922874', 'HUMBERTO', 'SACSI', 'CASQUINA', '', 'Lote 9 Cerro Colorado', '1', 'H', '1996-03-01', 'Camara de Vaporizado', NULL, 'Ninguna', 1, 2, 83205, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('80219024', 'FERNANDO', 'FERNANDEZ', 'FERNANDEZ', '', 'Pasaje Jose Carlos Maria Tegui 113', '1', 'H', '1979-07-20', 'Cortador 001', NULL, 'Ninguna', 1, 1, 27405, '', '0000-00-00 00:00:00', '0', '20454328401', ''),
('CIT-11166667', 'JOSE', 'LOPEZ', 'LOPEZ', '887779994', 'Chiguata c-12', '6', 'H', '1998-10-02', 'Encargadoo', NULL, 'Experiencia laborla', 1, 2, 36637, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('CIT-15554466', 'TRABAJADOR ACTUALIZADO', 'LOPEZ', 'LOPEZ', '145544546', 'altoselvaalegre', '1', 'H', '1888-11-01', 'Encargado de admi', NULL, 'Bachiller en ing de', 1, 2, 36637, 'UN', '2019-02-27 11:53:19', '55555555', '20112233441', ''),
('CIT-36363612', 'LEONARDO DANI', 'DAVILA', 'LOPEZ', '132456464', 'AREQUIPA MZ12 LT21', '2', 'H', '1888-02-01', 'ENCARGADO DE SEGURIDAD', NULL, 'ES UN TRABAJADOR BUENO', 1, 2, 63488, 'UN', '2019-03-01 14:10:50', '55555555', '20112233441', 'dani@gmail.com'),
('CIT-44555666', 'NUEVO', 'NEVOTRA', 'TRABAJADOR', '445588777', 'adasd F-1', '1', 'H', '1996-12-02', 'Encargad', NULL, 'asdsa', 0, 2, 36637, '', '0000-00-00 00:00:00', '0', '20112233441', ''),
('CIT-72327274', 'SSSS', 'AAAAAAA', 'AAAAAA', '244241727', 'asdsad', '2', 'H', '1888-11-01', 'dsa', NULL, 'asd', 1, 1, 36637, 'PC', '2019-02-27 13:01:35', '55555555', '20112233441', 'assa@sa.c'),
('CIT-78787456', 'EJ', 'EJ', 'EJ', '101441146', 'alto selva alegre', '1', 'H', '1844-12-05', 'encargado', NULL, 'das', 1, 3, 36637, '', '2019-02-26 15:28:07', '55555555', '20112233441', ''),
('CIT-84361616', 'EJEMPLOS', 'EJEMPLOS', 'EJEMPLOS', '134546464', 'aaaaaaa', '1', 'H', '1995-12-01', 'es', NULL, 'as', 1, 2, 83307, '', '2019-02-26 15:31:38', '55555555', '20112233441', ''),
('JPP-12516677', 'ASD', 'ASD', 'ASD', '111213214', 'SAD', '1', 'H', '1996-12-01', 'LOGISTICA', NULL, 'E', 1, 1, 47812, 'NA', '2019-04-22 11:57:51', '33334444', '18484849656', 's@s.c'),
('KAF-85611661', 'JOSE', 'MANRIQUE', 'RIVERA', '147417241', 'JIRON 12', '2', 'H', '1888-11-01', 'ENCARGADO DE ALMACEN', NULL, 'NINGUNA', 1, 1, 47812, 'UN', '2019-04-01 10:49:01', '70418335', '20498620591', 'J@j.c'),
('PRU-11111111', 'QWQWQ', 'QWQW', 'QWQW', '123213213', 'ASD', '7', 'H', '2001-02-01', 'DSA', NULL, 'ADS', 1, 1, 27405, 'PI', '2019-07-17 16:55:28', '00001111', '15849643032', 'asd@s.c'),
('PRU-11321421', 'DDD', 'DD', 'DD', '124211521', 'ADS', '4', 'H', '1996-07-11', 'ADS', NULL, 'DSA', 1, 1, 83307, 'NA', '2019-07-18 11:13:17', '00001111', '15849643032', 'asd@s.c'),
('PRU-12321541', 'AA', 'AA', 'AA', '122142144', 'ADS', '2', 'H', '1995-03-01', 'SA', NULL, 'ADS', 1, 3, 75359, 'PI', '2019-07-18 10:58:48', '00001111', '15849643032', 'afds@s.c'),
('PRU-75765676', 'ADS', 'ASD', 'ADS', '545445454', 'FDA', '1', 'H', '1111-12-01', 'LOIGISTICA', NULL, 'NINGUNA', 1, 3, 47812, 'SC', '2019-04-04 08:47:20', '00001111', '15849643032', 'a@s.c');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_compra`
--

CREATE TABLE `unidad_compra` (
  `unidad` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `descrip_unidad_compra` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `magnitud_unidad_medida` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_unidad_medida` tinyint(1) NOT NULL,
  `cod_unidad_medida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `unidad_compra`
--

INSERT INTO `unidad_compra` (`unidad`, `descrip_unidad_compra`, `magnitud_unidad_medida`, `estado_unidad_medida`, `cod_unidad_medida`) VALUES
('m', 'metro', 'Longitud', 1, 1),
('kg', 'kilogramo', 'Masa', 1, 2),
('s', 'segundo', 'Tiempo', 1, 3),
('m2', 'metro cuadrado', 'Superficie', 1, 4),
('m3', 'metro cúbico', 'Volumen', 1, 5),
('min', 'minuto', 'Tiempo', 1, 6),
('h', 'hora', 'Tiempo', 1, 7),
('d', 'día', 'Tiempo', 1, 8),
('l', 'litro', 'Volumen', 1, 9),
('t', 'tonelada', 'Masa', 1, 10),
('g', 'gramo', 'Masa', 1, 11),
('und', 'unidad', 'Unidad', 1, 12),
('pie2', 'pie cuadrado', 'Superficie', 1, 13),
('cono', 'cono', 'Unidad', 1, 14),
('plancha', 'plancha', 'Unidad', 1, 15),
('lata', 'lata', 'Unidad', 1, 16),
('millar', 'millar', 'Unidad', 1, 17),
('ciento', 'ciento', 'Unidad', 1, 18),
('doc', 'docena', 'Unidad', 1, 19),
('gal', 'galón', 'Volumen', 1, 20),
('botella', 'botella', 'Volumen', 1, 19087),
('paquete', 'paquete', 'Unidad', 1, 51890),
('gruesa', 'gruesa', 'Unidad', 1, 61744),
('cm2', 'centímetro cuadrado', 'Superficie', 1, 66203),
('ejemplo', 'ejemplo', 'ej', 0, 87378),
('par', 'par', 'Unidad', 1, 92970),
('ejemplo', 'ejemplo', 'ej', 0, 95467);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `unidad` varchar(10) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descrip_unidad_medida` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `magnitud_unidad_medida` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `estado_unidad_medida` tinyint(1) NOT NULL,
  `cod_unidad_medida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`unidad`, `descrip_unidad_medida`, `magnitud_unidad_medida`, `estado_unidad_medida`, `cod_unidad_medida`) VALUES
('m', 'metro', 'Longitud', 1, 1),
('kg', 'kilogramo', 'Masa', 1, 2),
('s', 'segundo', 'Tiempo', 1, 3),
('m2', 'metro cuadrado', 'Superficie', 1, 4),
('m3', 'metro cúbico', 'Volumen', 1, 5),
('min', 'minuto', 'Tiempo', 1, 6),
('h', 'hora', 'Tiempo', 1, 7),
('d', 'día', 'Tiempo', 1, 8),
('l', 'litro', 'Volumen', 1, 9),
('t', 'tonelada', 'Masa', 1, 10),
('g', 'gramo', 'Masa', 1, 11),
('und', 'unidad', 'Unidad', 1, 12),
('pie2', 'pie cuadrado', 'Superficie', 1, 13),
('cono', 'cono', 'Unidad', 1, 14),
('plancha', 'plancha', 'Unidad', 1, 15),
('lata', 'lata', 'Unidad', 1, 16),
('millar', 'millar', 'Unidad', 1, 17),
('ciento', 'ciento', 'Unidad', 1, 18),
('doc', 'docena', 'Unidad', 1, 19),
('gal', 'galón', 'Volumen', 1, 20),
('botella', 'botella', 'Volumen', 1, 19087),
('paquete', 'paquete', 'Unidad', 1, 51890),
('gruesa', 'gruesa', 'Unidad', 1, 61744),
('cm2', 'centímetro cuadrado', 'Superficie', 1, 66203),
('ejemplo', 'ejemplo', 'ej', 0, 87378),
('par', 'par', 'Unidad', 1, 92970),
('ejemplo', 'ejemplo', 'ej', 0, 95467);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `name` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_spanish_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` char(1) COLLATE utf8mb4_spanish_ci NOT NULL,
  `session_id` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `RUC_empresa` varchar(11) COLLATE utf8mb4_spanish_ci NOT NULL,
  `role` int(11) NOT NULL,
  `extra` char(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `id` char(10) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `estado`, `session_id`, `RUC_empresa`, `role`, `extra`, `id`) VALUES
('usuarioh', '00003333', NULL, '$2y$10$ldyPQ8d0/nOGaPJNWnCOxuzA.eGgtk2I2TYU31BTIesCHpp26zwou', 'bxp86rp8sFLmDZdbz3IgCtSGV7xRyjXVJyt8rRirmFQFB9kPVKgVHm1lRv1Z', '2019-07-09 16:30:10', '2019-07-09 16:42:00', '3', 'xGINL5F3IUUNdLUTUqDfV7CRsrjbgx8CKue9eL2F', '15849643032', 69506, NULL, '00003333'),
('usuariodes', '00004444', NULL, '$2y$10$IJ5kH40iMKfBTPxp7emGOevvDqmy5CE2D19ggy86GlMge2jjQd6iu', '7U17moZyMZxXYLIm0R1GxBY5GAaPpO1M6jS8rqGOQKcLnPwkfsgAehD89tsN', '2019-07-09 16:31:01', '2019-07-09 16:42:59', '3', 'eomUNLgjI0lxEnSk15JCK0iIqDL4Jwc0e707VT6S', '15849643032', 40909, NULL, '00004444'),
('usuarioc', '00005555', NULL, '$2y$10$NgIfozMQ3b9bGyGCJdxQdeV3eDSxeaRjeEGhd3SrdhyM3ZGBlmnvO', 'FBGzFXPC5BSpFzH4u9OE5IALqFM7gbzzyhYVWqwzI3pgi5sx4wqJcDh7p2FV', '2019-07-09 16:31:21', '2019-07-09 16:43:23', '3', 'uRDD1yicc3pWFxeoAj4UYxEBjywrZFDSgZA3VNSB', '15849643032', 46409, NULL, '00005555'),
('ALEXANDER GONZALES', '101010', NULL, '$2y$10$XZkN9mGiXZ3J2MXWqcG6ReJKUuNQSTk1qnnBQiuVfN0nxmbWASjs2', 'JBY208QpjFVjO3flQ1MCQmeztrDWZ5ZeGaRm1UNs1Q2Dxmvoe7xD3AMEoRzd', '2019-01-20 23:45:08', '2019-01-21 06:39:02', '3', 'bNVQKTiz6pGmMpa7oBQFwCY8DtxYqlVLz5eJsspy', '10203040506', 2, NULL, '101010'),
('KAREN ARENAS', '102030', NULL, '$2y$10$8YUnzJt6VXtVQF/Q6TUOEuAGiWfiuWddCwQGUhGxgSdPTKwk56nne', NULL, '2019-01-20 23:44:30', '2019-01-20 23:52:44', '1', NULL, '10203040506', 3, NULL, '102030'),
('usuariod', '00001111', NULL, '$2y$10$BryjafyH3u7WknroDF7OtuJNBhL09W9QUhqyXTmEcfOGYRSlMJszq', 'dRuRuZYr3PBxnq5nGtzZynoxLcBcrvdnyPtNsB4fhPIL42uJD7PK0Z0lkkp0', '2019-04-01 15:26:41', '2019-09-16 14:24:50', '3', 'tImtJyTZyO7lsRU43fyG4CFv8nZSZPPPywjofWmK', '15849643032', 2, NULL, '1111'),
('prueba', '11112222', NULL, '$2y$10$fTZf4HEVRadJShFcDu1CtO011FRig/8/Iq/6w7w7iIBmbr6NXvfIu', 'GGCAtw1ZbBXcMQ1F5kHwhXhdNHcUEwMrhWtYCH0o1iMMek0S8l4gODBzgcVu', '2019-04-22 16:40:47', '2019-04-22 16:42:55', '3', 'rCNEvYfkszfXtgtJwqSlFZcYyYXryG0oIfUIeqvw', '10203040506', 2, NULL, '11112222'),
('Delbyn Andree Guiteras Ruiz', '11223344', NULL, '$2y$10$7k7jQdJbKNVfhv5sMWqdeOj7q/PjHhiHTivXE7760xafhbZPUAk7e', 'KBRhAyRkw7aaWFz5ryrwrp059jK6Mbe1FTN3Lw87sc5SVoIbPmL3EInQ0uQ1', '2018-12-17 04:49:19', '2019-02-13 04:16:55', '3', 'SShPU3byv81ogrdJ3RCdTThQYEovhV43UuWdwvhG', '20134956777', 2, NULL, '11223344'),
('asd', '00002222', NULL, '$2y$10$0KcXkHY5MH8kpGsjGglrReRDXuEmO9dS6LGAiZedkfZOIKqaGoA3S', 'HzZbYk7pqYfZbOq3eqbogHauknB0vNTBy8WCvJyuInBdbs4J4mnSGmLSiTh9', '2019-06-13 20:13:02', '2019-07-09 16:41:02', '3', 'a1ZOOx09UmGeZe3YwK9vMmkxWSUVJCJvg7s5IZMn', '15849643032', 3, NULL, '2222'),
('Manuel Condori Apaza', '29393353', NULL, '$2y$10$VoU/P.kEJsWSe5Px9GBZA.caQprzRLUfpBhNqchBahRGUf.GDb07G', 'mePT9xm6coBxAxxpj9TbxIAEyOskfU8t7khLCOxmGjiLkOi0F16l7E5XyPO2', '2019-01-22 20:39:13', '2019-02-04 17:03:46', '1', 'zU367SJ0AzieBG8DcyFkGAI0RXTIRRCpremjvmF0', '20539686195', 2, NULL, '29393353'),
('Lourdes Fernandez', '29699371', NULL, '$2y$10$WAJVSY9mPICcBl28eAH8W.hZaBc075QUcrpAKsRaIl7qyb5jjCl.G', '390pnzJvwZQT7jYtbQEIiqQzUrFiDzNnUwYAnGSUkzkZf51T1mEZSDy9wBIj', '2019-01-22 20:44:38', '2019-02-04 15:52:31', '1', 'gdVr3S0jra58BUUPwcQ1vGQUFOLhmNYMA3zRAZJ4', '20454328401', 2, NULL, '29699371'),
('EJEMPLO', '33334444', NULL, '$2y$10$Ix8ZAplarvnA.EPSFkM48OQHz37VjaYsLoMQQIxalQMHsEtQZPanO', 'Xn1ocHYYzVwy2kinMXwXFDSQ6v1qbwCWtwP8UtUEJIC2zQNmhWYu38NmET31', '2019-04-22 16:56:49', '2019-04-22 16:56:53', '3', 'pzfFVRe9JqIRsPPZ54OGLN1SScfV3cp3JTmTfV60', '18484849656', 2, NULL, '33334444'),
('ELENA PANCCA', '40056277', NULL, '$2y$10$xRIkBybe3gkDVFJ6tltMUeDqdYWLsrG8CezzggjZSYKpPdddnUM06', '36YoYb4PoCloAPU0UoZJ5XtSTGtt8BjlRWbXLiUnsd4EvJtVV35q21r9gxfL', '2019-01-23 19:55:18', '2019-09-05 14:37:35', '3', 'qM3s6pYJ1Yjg9yqQLF2mO2o5Ff4K8aeOyQ5betIs', '20455447888', 2, NULL, '40056277'),
('Mariela Chavez Basurto', '44332211', NULL, '$2y$10$feeCk7im72W5xAxo3HzLvegFl61OAwWu5ClrBBV0xCGaq982dL74a', NULL, '2018-12-17 04:49:48', '2018-12-17 04:49:48', '3', NULL, '20134956777', 3, NULL, '44332211'),
('Delbyn Guiteras Ruiz', '55555555', NULL, '$2y$10$6tr42c4t2G2hoa5VgctzHOow2V9z.R/1o14UPRWOIWLNIYc0nk.mS', 'gnQajumHgvpWtAwfJvsSKp2JbJEYR462hExZVTetHe1VF60HgmGYlMP3wrEC', '2019-01-20 21:36:03', '2019-05-30 14:36:39', '1', 'aAWDq7L5m0gjY8USYDwhIoMEdXjfJmMgeNAK5agT', '20112233441', 2, NULL, '55555555'),
('JUNIOR', '70418335', NULL, '$2y$10$cDnZwVpCLIFYU33rTCeaxu/85BXjf6diBhyDUKUw7MCV/b1SI5j66', 'RU8Ug7GTDXQeWhCaHol4dwCtYMawKw0KSxmEEe7FkKWFz5xcPMpGWbTlRBbh', '2019-01-24 21:04:08', '2019-04-01 15:47:43', '3', 'n2WCAhsS9wFlVUD7hQLqCAligDOUm8kl1LX46643', '20498620591', 2, NULL, '70418335'),
('Administrador', '70869564', NULL, '$2y$10$QSJYP1h1SkQ7SC6dq6bs9ud7m2GXbHKuk/LzE3PQ6L603CDntvGtK', 'zCzD1ERD1Oq7Fq9fzHZX8m8VZNgv8hwinEL1VrmdwJuK6NKdqRKmeMbXCPOP', '2018-11-09 20:00:00', '2019-08-02 21:28:59', '1', 'NHhpajIKdPbOZkV4NKaiFHUNR3BAz0dR1zjcIeEh', 'EMPRESA', 1, NULL, '70869564'),
('superusuario', '7777777', NULL, '$2y$10$YI4hbi4gj4Ks8cMOO6lxRuX.B5DYy9C6HKR7qxMZuXmbOBWk0qDtW', 'i3u2djKRk3BzWdhwGR704czlhN6y4INy2vTQi5TQn1FHrVkwNrjYHiDANdWq', '2018-12-17 05:04:16', '2019-01-22 01:35:27', '3', 'H9Zve0QUCpdHf39ojhV8VLrb2J7ZV1Hcrv9k8Dxh', '20134956777', 2, NULL, '7777777'),
('Jorge', '78896554', NULL, '$2y$10$QKxGXU19ObCylApt460ZdeNHg9ZgQ2hSIXiH2JfPEVLO9WkRXOOVW', NULL, '2019-01-21 11:49:48', '2019-01-21 11:50:43', '3', 'jerHQuUh0uSno3SbvjCFpDq0DokNsymDiNmREpFl', '20112233441', 3, NULL, '78896554');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`cod_almacen`),
  ADD KEY `almacen_empresa` (`RUC_empresa`),
  ADD KEY `almacen_trabajador` (`DNI_trabajador`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`cod_area`);

--
-- Indices de la tabla `capellada`
--
ALTER TABLE `capellada`
  ADD PRIMARY KEY (`cod_capellada`);

--
-- Indices de la tabla `catalogo_precios`
--
ALTER TABLE `catalogo_precios`
  ADD PRIMARY KEY (`cod_articulo`),
  ADD KEY `precio_usuario` (`usuario`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Indices de la tabla `categoria_noticias`
--
ALTER TABLE `categoria_noticias`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`cod_ciudad`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `cliente_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `coleccion`
--
ALTER TABLE `coleccion`
  ADD PRIMARY KEY (`codigo_coleccion`),
  ADD KEY `coleccion_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`codigo_comentario`),
  ADD KEY `comentario_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `configuracion_valores`
--
ALTER TABLE `configuracion_valores`
  ADD PRIMARY KEY (`codigo_valor`);

--
-- Indices de la tabla `costo_modelo`
--
ALTER TABLE `costo_modelo`
  ADD PRIMARY KEY (`cod_costo_modelo`),
  ADD KEY `costo_modelo_empresa` (`RUC_empresa`),
  ADD KEY `costo_modelo_modelo` (`modelo_serie`);

--
-- Indices de la tabla `datos_generales`
--
ALTER TABLE `datos_generales`
  ADD PRIMARY KEY (`RUC_empresa`),
  ADD KEY `datos_generales_empresa` (`RUC_empresa`),
  ADD KEY `datos_generales_retencion_utilidades` (`cod_retencion_utilidades`);

--
-- Indices de la tabla `detalle_costo_modelo_manoobra`
--
ALTER TABLE `detalle_costo_modelo_manoobra`
  ADD PRIMARY KEY (`cod_detalle_costo_mano`),
  ADD KEY `detalle_costo_modelo_manoObra_area` (`cod_area`),
  ADD KEY `detalle_costo_modelo_manoObra_costo_modelo` (`cod_costo_modelo`),
  ADD KEY `detalle_costo_modelo_manoObra_tipo_trabajador` (`cod_tipo_trabajador`);

--
-- Indices de la tabla `detalle_costo_modelo_materiales`
--
ALTER TABLE `detalle_costo_modelo_materiales`
  ADD PRIMARY KEY (`cod_detalle_costo_material`),
  ADD KEY `detalle_costo_modelo_materiales_area` (`cod_area`),
  ADD KEY `detalle_costo_modelo_materiales_costo_modelo` (`cod_costo_modelo`),
  ADD KEY `detalle_costo_modelo_materiales_material` (`cod_material`);

--
-- Indices de la tabla `detalle_kardex_material`
--
ALTER TABLE `detalle_kardex_material`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `detalle_kardex_material_kardex_material` (`cod_kardex_material`),
  ADD KEY `detalle_area` (`cod_area`),
  ADD KEY `detalle_usuario` (`dni_usuario`);

--
-- Indices de la tabla `detalle_kardex_producto_terminado`
--
ALTER TABLE `detalle_kardex_producto_terminado`
  ADD KEY `detalle_kardex_producto_terminado_kardex_producto_terminado` (`cod_kardex_pt`);

--
-- Indices de la tabla `detalle_orden_compra`
--
ALTER TABLE `detalle_orden_compra`
  ADD PRIMARY KEY (`id_detalle_oc`),
  ADD KEY `detalle_orden_compra_material` (`cod_material`),
  ADD KEY `detalle_orden_compra_orden_compra` (`cod_orden_compra`);

--
-- Indices de la tabla `detalle_orden_pedido`
--
ALTER TABLE `detalle_orden_pedido`
  ADD KEY `detalle_codigo_op` (`codigo_orden_pedido`),
  ADD KEY `detalle_codigo_serie_mod` (`codigo_serie_articulo`);

--
-- Indices de la tabla `detalle_orden_pedido_produccion`
--
ALTER TABLE `detalle_orden_pedido_produccion`
  ADD KEY `codigo_orden_pedido_produccion` (`codigo_orden_pedido_produccion`),
  ADD KEY `codigo_serie_articulo` (`codigo_serie_articulo`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`cod_distrito`),
  ADD KEY `cod_ciudad` (`cod_ciudad`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`RUC_empresa`),
  ADD KEY `empresa_regimen_laboral` (`cod_regimen_laboral`),
  ADD KEY `empresa_regimen_renta` (`cod_regimen_renta`),
  ADD KEY `empresa_tipo_contribuyente` (`cod_tipo_contribuyente`);

--
-- Indices de la tabla `escala_salarial`
--
ALTER TABLE `escala_salarial`
  ADD PRIMARY KEY (`cod_trabajador`),
  ADD KEY `escala_salarial_empresa` (`RUC_empresa`),
  ADD KEY `escala_salarial_seguro` (`tipo_seguro`),
  ADD KEY `escala_salarial_renta` (`cod_regimen_laboral`);

--
-- Indices de la tabla `fecha_historial`
--
ALTER TABLE `fecha_historial`
  ADD KEY `fecha_trabajador` (`cod_trabajador`);

--
-- Indices de la tabla `forro`
--
ALTER TABLE `forro`
  ADD PRIMARY KEY (`cod_forro`);

--
-- Indices de la tabla `gasto_depreciacion_mantenimiento`
--
ALTER TABLE `gasto_depreciacion_mantenimiento`
  ADD PRIMARY KEY (`cod_activo`),
  ADD KEY `depreciacion_mantenimiento_area` (`cod_area`),
  ADD KEY `depreciacion_mantenimiento_tipo_activo` (`cod_tipo_activo`),
  ADD KEY `depreciacion_mantenimiento_tipo_adquisicion` (`cod_tipo_adquisicion`),
  ADD KEY `gasto_depreciacion_mantenimiento_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_desarrollo_producto`
--
ALTER TABLE `gasto_desarrollo_producto`
  ADD PRIMARY KEY (`cod_gasto_desarrollo_producto`),
  ADD KEY `gasto_desarrollo_producto_area` (`cod_area`),
  ADD KEY `gasto_desarrollo_producto_empresa` (`RUC_empresa`),
  ADD KEY `gasto_desarrollo_producto_tipo_desarrollo` (`cod_tipo_desarrollo`);

--
-- Indices de la tabla `gasto_distribucion`
--
ALTER TABLE `gasto_distribucion`
  ADD PRIMARY KEY (`id_gastoDistr`),
  ADD KEY `transporte_area` (`area`),
  ADD KEY `transporte_ruc` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_financiero`
--
ALTER TABLE `gasto_financiero`
  ADD PRIMARY KEY (`cod_gasto`),
  ADD KEY `gasto_finaniero_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_otros`
--
ALTER TABLE `gasto_otros`
  ADD PRIMARY KEY (`id_otros`),
  ADD KEY `otros_ruc_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_representacion`
--
ALTER TABLE `gasto_representacion`
  ADD PRIMARY KEY (`cod_gasrepre`),
  ADD KEY `representacion_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_servicios_basicos`
--
ALTER TABLE `gasto_servicios_basicos`
  ADD PRIMARY KEY (`cod_gasto_servicio_basico`),
  ADD KEY `servicios_basicos_area` (`cod_area`),
  ADD KEY `gasto_servicios_basicos_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `gasto_sueldos`
--
ALTER TABLE `gasto_sueldos`
  ADD PRIMARY KEY (`id_gastos_sueldos`),
  ADD KEY `gasto_sueldo_empresa` (`RUC_empresa`),
  ADD KEY `gasto_sueldo_area` (`cod_area`);

--
-- Indices de la tabla `gasto_suministro`
--
ALTER TABLE `gasto_suministro`
  ADD PRIMARY KEY (`codigo_suministro`),
  ADD KEY `gasto_suministro_area` (`cod_area`),
  ADD KEY `gasto_suministro_empresa` (`RUC_empresa`),
  ADD KEY `gasto_suministro_material` (`cod_material`);

--
-- Indices de la tabla `horas_trabajo`
--
ALTER TABLE `horas_trabajo`
  ADD PRIMARY KEY (`cod`),
  ADD KEY `hora_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `importaciones`
--
ALTER TABLE `importaciones`
  ADD PRIMARY KEY (`codigo_importaciones`),
  ADD KEY `importacion_detalle_orden` (`id_detalle_oc`);

--
-- Indices de la tabla `kardex_material`
--
ALTER TABLE `kardex_material`
  ADD PRIMARY KEY (`cod_kardex_material`),
  ADD KEY `kardex_material_almacen` (`cod_almacen`),
  ADD KEY `kardex_material_empresa` (`RUC_empresa`),
  ADD KEY `kardex_material_material` (`cod_material`);

--
-- Indices de la tabla `kardex_producto_terminado`
--
ALTER TABLE `kardex_producto_terminado`
  ADD PRIMARY KEY (`cod_kardex_pt`),
  ADD KEY `kardex_producto_terminado_almacen` (`cod_almacen`),
  ADD KEY `kardex_producto_terminado_producto_terminado` (`cod_producto_terminado`),
  ADD KEY `kardex_producto_terminado_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `linea`
--
ALTER TABLE `linea`
  ADD PRIMARY KEY (`cod_linea`),
  ADD KEY `linea_empresa` (`RUC_empresa`) USING BTREE;

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`cod_material`),
  ADD KEY `material_empresa` (`RUC_empresa`),
  ADD KEY `material_subcategoria` (`cod_subcategoria`),
  ADD KEY `unidad_medida` (`unidad_medida`),
  ADD KEY `material_unidad_medida` (`unidad_compra`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD PRIMARY KEY (`cod_modelo`),
  ADD KEY `modelo_empresa` (`RUC_empresa`) USING BTREE,
  ADD KEY `modelo_linea` (`cod_linea`),
  ADD KEY `modelo_capellada` (`cod_capellada`),
  ADD KEY `modelo_forro` (`cod_forro`),
  ADD KEY `modelo_plantilla` (`cod_plantilla`),
  ADD KEY `modelo_piso` (`cod_piso`),
  ADD KEY `modelo_coleccion` (`cod_coleccion`),
  ADD KEY `modelo_horma` (`cod_horma`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`cod`),
  ADD KEY `noticia_categoria` (`categoria_difusion`);

--
-- Indices de la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  ADD PRIMARY KEY (`cod_orden_compra`),
  ADD KEY `orden_compra_empresa` (`RUC_empresa`),
  ADD KEY `orden_compra_proveedor` (`RUC_proveedor`);

--
-- Indices de la tabla `orden_pedido`
--
ALTER TABLE `orden_pedido`
  ADD PRIMARY KEY (`codigo_pedido`),
  ADD KEY `pedido_cliente` (`codigo_cliente`),
  ADD KEY `pedido_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `orden_pedido_produccion`
--
ALTER TABLE `orden_pedido_produccion`
  ADD PRIMARY KEY (`codigo_orden_pedido_produccion`),
  ADD KEY `codigo_orden_pedido` (`codigo_orden_pedido`),
  ADD KEY `codigo_cliente` (`codigo_cliente`),
  ADD KEY `RUC_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `orden_produccion`
--
ALTER TABLE `orden_produccion`
  ADD PRIMARY KEY (`cod_op`),
  ADD KEY `cod_linea` (`cod_serie`),
  ADD KEY `cod_serie` (`cod_serie`),
  ADD KEY `op_linea_2` (`cod_linea`) USING BTREE,
  ADD KEY `cod_serie_2` (`cod_serie`),
  ADD KEY `oprod_empresa` (`RUC_empresa`) USING BTREE,
  ADD KEY `ordprod_empresa` (`RUC_empresa`) USING BTREE,
  ADD KEY `orden_produccion_modelo` (`cod_modelo`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos_roles`
--
ALTER TABLE `permisos_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permisos_roles_permisos` (`permisos_id`),
  ADD KEY `permisos_roles_roles` (`roles_id`);

--
-- Indices de la tabla `piso`
--
ALTER TABLE `piso`
  ADD PRIMARY KEY (`cod_piso`);

--
-- Indices de la tabla `plantilla`
--
ALTER TABLE `plantilla`
  ADD PRIMARY KEY (`cod_plantilla`);

--
-- Indices de la tabla `producto_terminado`
--
ALTER TABLE `producto_terminado`
  ADD PRIMARY KEY (`cod_producto_terminado`),
  ADD KEY `producto_terminado_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`RUC_proveedor`),
  ADD KEY `proveedor_empresa` (`RUC_empresa`),
  ADD KEY `proveedor_subcategoria` (`proveedor_subcategoria`);

--
-- Indices de la tabla `puesto`
--
ALTER TABLE `puesto`
  ADD PRIMARY KEY (`cod_puesto`),
  ADD KEY `puesto_tipo_trabajador` (`cod_tipo_trabajador`),
  ADD KEY `puesto_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `regimen_laboral`
--
ALTER TABLE `regimen_laboral`
  ADD PRIMARY KEY (`cod_regimen_laboral`);

--
-- Indices de la tabla `regimen_renta`
--
ALTER TABLE `regimen_renta`
  ADD PRIMARY KEY (`cod_regimen_renta`);

--
-- Indices de la tabla `retencion_utilidades`
--
ALTER TABLE `retencion_utilidades`
  ADD PRIMARY KEY (`cod_retencion_utilidades`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `serie`
--
ALTER TABLE `serie`
  ADD PRIMARY KEY (`cod_serie`),
  ADD KEY `serie_empresa` (`RUC_empresa`) USING BTREE;

--
-- Indices de la tabla `serie_modelo`
--
ALTER TABLE `serie_modelo`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `serie_modelo_modelo` (`codigo_modelo`),
  ADD KEY `serie_modelo_serie` (`codigo_serie`),
  ADD KEY `serie_modelo_empresa` (`RUC_empresa`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`cod_subcategoria`),
  ADD KEY `subcategoria_categoria` (`cod_categoria`);

--
-- Indices de la tabla `talla_pares`
--
ALTER TABLE `talla_pares`
  ADD KEY `cod_op` (`cod_op`),
  ADD KEY `cod_op_2` (`cod_op`,`cod_serie`),
  ADD KEY `tallapares_serie` (`cod_serie`) USING BTREE,
  ADD KEY `tallapares_empresa` (`RUC_empresa`) USING BTREE;

--
-- Indices de la tabla `tiempo_recepcion`
--
ALTER TABLE `tiempo_recepcion`
  ADD KEY `tiempo_detalle` (`id_detalle_oc`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`codigo_tienda`);

--
-- Indices de la tabla `tipo_activo`
--
ALTER TABLE `tipo_activo`
  ADD PRIMARY KEY (`cod_tipo_activo`);

--
-- Indices de la tabla `tipo_adquisicion`
--
ALTER TABLE `tipo_adquisicion`
  ADD PRIMARY KEY (`cod_tipo_adquisicion`);

--
-- Indices de la tabla `tipo_contribuyente`
--
ALTER TABLE `tipo_contribuyente`
  ADD PRIMARY KEY (`cod_tipo_contribuyente`);

--
-- Indices de la tabla `tipo_desarrollo`
--
ALTER TABLE `tipo_desarrollo`
  ADD PRIMARY KEY (`cod_tipo_desarrollo`);

--
-- Indices de la tabla `tipo_gasto`
--
ALTER TABLE `tipo_gasto`
  ADD PRIMARY KEY (`cod_tipo_gasto`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`cod_tipo_pago`),
  ADD KEY `tipo_pago_tipo_trabajador` (`cod_tipo_trabajador`);

--
-- Indices de la tabla `tipo_seguro`
--
ALTER TABLE `tipo_seguro`
  ADD PRIMARY KEY (`cod`);

--
-- Indices de la tabla `tipo_trabajador`
--
ALTER TABLE `tipo_trabajador`
  ADD PRIMARY KEY (`cod_tipo_trabajador`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`DNI_trabajador`),
  ADD KEY `trabajador_area` (`cod_area`),
  ADD KEY `trabajador_empresa` (`RUC_empresa`),
  ADD KEY `trabajador_tipo_trabajador` (`cod_tipo_trabajador`),
  ADD KEY `trabajador_distrito` (`cod_distrito`);

--
-- Indices de la tabla `unidad_compra`
--
ALTER TABLE `unidad_compra`
  ADD PRIMARY KEY (`cod_unidad_medida`);

--
-- Indices de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`cod_unidad_medida`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_empresa` (`RUC_empresa`),
  ADD KEY `users_roles` (`role`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `cod_almacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94138;

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `cod_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87884;

--
-- AUTO_INCREMENT de la tabla `capellada`
--
ALTER TABLE `capellada`
  MODIFY `cod_capellada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=970;

--
-- AUTO_INCREMENT de la tabla `configuracion_valores`
--
ALTER TABLE `configuracion_valores`
  MODIFY `codigo_valor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_kardex_material`
--
ALTER TABLE `detalle_kardex_material`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1419;

--
-- AUTO_INCREMENT de la tabla `forro`
--
ALTER TABLE `forro`
  MODIFY `cod_forro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `kardex_producto_terminado`
--
ALTER TABLE `kardex_producto_terminado`
  MODIFY `cod_kardex_pt` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permisos_roles`
--
ALTER TABLE `permisos_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `piso`
--
ALTER TABLE `piso`
  MODIFY `cod_piso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `plantilla`
--
ALTER TABLE `plantilla`
  MODIFY `cod_plantilla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `puesto`
--
ALTER TABLE `puesto`
  MODIFY `cod_puesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `regimen_laboral`
--
ALTER TABLE `regimen_laboral`
  MODIFY `cod_regimen_laboral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `regimen_renta`
--
ALTER TABLE `regimen_renta`
  MODIFY `cod_regimen_renta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `retencion_utilidades`
--
ALTER TABLE `retencion_utilidades`
  MODIFY `cod_retencion_utilidades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69507;

--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `cod_subcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=976;

--
-- AUTO_INCREMENT de la tabla `tipo_activo`
--
ALTER TABLE `tipo_activo`
  MODIFY `cod_tipo_activo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4447;

--
-- AUTO_INCREMENT de la tabla `tipo_adquisicion`
--
ALTER TABLE `tipo_adquisicion`
  MODIFY `cod_tipo_adquisicion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=910;

--
-- AUTO_INCREMENT de la tabla `tipo_contribuyente`
--
ALTER TABLE `tipo_contribuyente`
  MODIFY `cod_tipo_contribuyente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94443;

--
-- AUTO_INCREMENT de la tabla `tipo_desarrollo`
--
ALTER TABLE `tipo_desarrollo`
  MODIFY `cod_tipo_desarrollo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_gasto`
--
ALTER TABLE `tipo_gasto`
  MODIFY `cod_tipo_gasto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10104;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `cod_tipo_pago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_trabajador`
--
ALTER TABLE `tipo_trabajador`
  MODIFY `cod_tipo_trabajador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `cod_unidad_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95468;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD CONSTRAINT `almacen_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `almacen_trabajador` FOREIGN KEY (`DNI_trabajador`) REFERENCES `trabajador` (`DNI_trabajador`);

--
-- Filtros para la tabla `catalogo_precios`
--
ALTER TABLE `catalogo_precios`
  ADD CONSTRAINT `precio_articulo` FOREIGN KEY (`cod_articulo`) REFERENCES `serie_modelo` (`codigo`),
  ADD CONSTRAINT `precio_usuario` FOREIGN KEY (`usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `coleccion`
--
ALTER TABLE `coleccion`
  ADD CONSTRAINT `coleccion_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentario_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `costo_modelo`
--
ALTER TABLE `costo_modelo`
  ADD CONSTRAINT `costo_modelo_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `costo_serie_modelo` FOREIGN KEY (`modelo_serie`) REFERENCES `serie_modelo` (`codigo`);

--
-- Filtros para la tabla `datos_generales`
--
ALTER TABLE `datos_generales`
  ADD CONSTRAINT `datos_generales_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `datos_generales_retencion_utilidades` FOREIGN KEY (`cod_retencion_utilidades`) REFERENCES `retencion_utilidades` (`cod_retencion_utilidades`);

--
-- Filtros para la tabla `detalle_costo_modelo_manoobra`
--
ALTER TABLE `detalle_costo_modelo_manoobra`
  ADD CONSTRAINT `detalle_costo_modelo_manoObra_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `detalle_costo_modelo_manoObra_tipo_trabajador` FOREIGN KEY (`cod_tipo_trabajador`) REFERENCES `tipo_trabajador` (`cod_tipo_trabajador`),
  ADD CONSTRAINT `detalle_costo_modelo_mano_costo` FOREIGN KEY (`cod_costo_modelo`) REFERENCES `costo_modelo` (`cod_costo_modelo`);

--
-- Filtros para la tabla `detalle_costo_modelo_materiales`
--
ALTER TABLE `detalle_costo_modelo_materiales`
  ADD CONSTRAINT `detalle_costo_material` FOREIGN KEY (`cod_material`) REFERENCES `material` (`cod_material`),
  ADD CONSTRAINT `detalle_material_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `detalle_material_costo` FOREIGN KEY (`cod_costo_modelo`) REFERENCES `costo_modelo` (`cod_costo_modelo`);

--
-- Filtros para la tabla `detalle_kardex_material`
--
ALTER TABLE `detalle_kardex_material`
  ADD CONSTRAINT `detalle_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `detalle_kardex_material` FOREIGN KEY (`cod_kardex_material`) REFERENCES `kardex_material` (`cod_kardex_material`),
  ADD CONSTRAINT `detalle_usuario` FOREIGN KEY (`dni_usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `detalle_kardex_producto_terminado`
--
ALTER TABLE `detalle_kardex_producto_terminado`
  ADD CONSTRAINT `detalle_kardex_producto_terminado_kardex_producto_terminado` FOREIGN KEY (`cod_kardex_pt`) REFERENCES `kardex_producto_terminado` (`cod_kardex_pt`);

--
-- Filtros para la tabla `detalle_orden_compra`
--
ALTER TABLE `detalle_orden_compra`
  ADD CONSTRAINT `detal` FOREIGN KEY (`cod_material`) REFERENCES `material` (`cod_material`),
  ADD CONSTRAINT `detalle_orden_compra_cod` FOREIGN KEY (`cod_orden_compra`) REFERENCES `orden_compra` (`cod_orden_compra`);

--
-- Filtros para la tabla `detalle_orden_pedido`
--
ALTER TABLE `detalle_orden_pedido`
  ADD CONSTRAINT `detalle_codigo_op` FOREIGN KEY (`codigo_orden_pedido`) REFERENCES `orden_pedido` (`codigo_pedido`),
  ADD CONSTRAINT `detalle_codigo_serie_mod` FOREIGN KEY (`codigo_serie_articulo`) REFERENCES `serie_modelo` (`codigo`);

--
-- Filtros para la tabla `detalle_orden_pedido_produccion`
--
ALTER TABLE `detalle_orden_pedido_produccion`
  ADD CONSTRAINT `detalle_orden_pedido_produccion_ibfk_1` FOREIGN KEY (`codigo_orden_pedido_produccion`) REFERENCES `orden_pedido_produccion` (`codigo_orden_pedido_produccion`),
  ADD CONSTRAINT `detalle_orden_pedido_produccion_ibfk_2` FOREIGN KEY (`codigo_serie_articulo`) REFERENCES `serie_modelo` (`codigo`);

--
-- Filtros para la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD CONSTRAINT `distrito_ibfk_1` FOREIGN KEY (`cod_ciudad`) REFERENCES `ciudad` (`cod_ciudad`);

--
-- Filtros para la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `empresa_regimen_laboral` FOREIGN KEY (`cod_regimen_laboral`) REFERENCES `regimen_laboral` (`cod_regimen_laboral`),
  ADD CONSTRAINT `empresa_regimen_renta` FOREIGN KEY (`cod_regimen_renta`) REFERENCES `regimen_renta` (`cod_regimen_renta`),
  ADD CONSTRAINT `empresa_tipo_contribuyente` FOREIGN KEY (`cod_tipo_contribuyente`) REFERENCES `tipo_contribuyente` (`cod_tipo_contribuyente`);

--
-- Filtros para la tabla `escala_salarial`
--
ALTER TABLE `escala_salarial`
  ADD CONSTRAINT `escala_salarial_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `escala_salarial_renta` FOREIGN KEY (`cod_regimen_laboral`) REFERENCES `regimen_laboral` (`cod_regimen_laboral`),
  ADD CONSTRAINT `escala_salarial_seguro` FOREIGN KEY (`tipo_seguro`) REFERENCES `tipo_seguro` (`cod`),
  ADD CONSTRAINT `escala_salarial_trabajador` FOREIGN KEY (`cod_trabajador`) REFERENCES `trabajador` (`DNI_trabajador`);

--
-- Filtros para la tabla `fecha_historial`
--
ALTER TABLE `fecha_historial`
  ADD CONSTRAINT `fecha_trabajador` FOREIGN KEY (`cod_trabajador`) REFERENCES `trabajador` (`DNI_trabajador`);

--
-- Filtros para la tabla `gasto_depreciacion_mantenimiento`
--
ALTER TABLE `gasto_depreciacion_mantenimiento`
  ADD CONSTRAINT `depreciacion_mantenimiento_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `depreciacion_mantenimiento_tipo_activo` FOREIGN KEY (`cod_tipo_activo`) REFERENCES `tipo_activo` (`cod_tipo_activo`),
  ADD CONSTRAINT `depreciacion_mantenimiento_tipo_adquisicion` FOREIGN KEY (`cod_tipo_adquisicion`) REFERENCES `tipo_adquisicion` (`cod_tipo_adquisicion`),
  ADD CONSTRAINT `gasto_depreciacion_mantenimiento_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_desarrollo_producto`
--
ALTER TABLE `gasto_desarrollo_producto`
  ADD CONSTRAINT `gasto_desarrollo_producto_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `gasto_desarrollo_producto_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `gasto_desarrollo_producto_tipo_desarrollo` FOREIGN KEY (`cod_tipo_desarrollo`) REFERENCES `tipo_desarrollo` (`cod_tipo_desarrollo`);

--
-- Filtros para la tabla `gasto_distribucion`
--
ALTER TABLE `gasto_distribucion`
  ADD CONSTRAINT `transporte_area` FOREIGN KEY (`area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `transporte_ruc` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_financiero`
--
ALTER TABLE `gasto_financiero`
  ADD CONSTRAINT `gasto_finaniero_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_otros`
--
ALTER TABLE `gasto_otros`
  ADD CONSTRAINT `otros_ruc_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_representacion`
--
ALTER TABLE `gasto_representacion`
  ADD CONSTRAINT `representacion_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_servicios_basicos`
--
ALTER TABLE `gasto_servicios_basicos`
  ADD CONSTRAINT `gasto_servicios_basicos_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `servicios_basicos_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`);

--
-- Filtros para la tabla `gasto_sueldos`
--
ALTER TABLE `gasto_sueldos`
  ADD CONSTRAINT `gasto_sueldo_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `gasto_sueldo_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `gasto_suministro`
--
ALTER TABLE `gasto_suministro`
  ADD CONSTRAINT `gasto_suministro_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `gasto_suministro_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `gasto_suministro_material` FOREIGN KEY (`cod_material`) REFERENCES `material` (`cod_material`);

--
-- Filtros para la tabla `horas_trabajo`
--
ALTER TABLE `horas_trabajo`
  ADD CONSTRAINT `hora_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `importaciones`
--
ALTER TABLE `importaciones`
  ADD CONSTRAINT `importacion_detalle_orden` FOREIGN KEY (`id_detalle_oc`) REFERENCES `detalle_orden_compra` (`id_detalle_oc`);

--
-- Filtros para la tabla `kardex_material`
--
ALTER TABLE `kardex_material`
  ADD CONSTRAINT `kardex_material_almacen` FOREIGN KEY (`cod_almacen`) REFERENCES `almacen` (`cod_almacen`),
  ADD CONSTRAINT `kardex_material_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `kardex_material_material` FOREIGN KEY (`cod_material`) REFERENCES `material` (`cod_material`);

--
-- Filtros para la tabla `kardex_producto_terminado`
--
ALTER TABLE `kardex_producto_terminado`
  ADD CONSTRAINT `kardex_producto_terminado_almacen` FOREIGN KEY (`cod_almacen`) REFERENCES `almacen` (`cod_almacen`),
  ADD CONSTRAINT `kardex_producto_terminado_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `kardex_producto_terminado_producto_terminado` FOREIGN KEY (`cod_producto_terminado`) REFERENCES `producto_terminado` (`cod_producto_terminado`);

--
-- Filtros para la tabla `linea`
--
ALTER TABLE `linea`
  ADD CONSTRAINT `linea_ibfk_1` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`unidad_medida`) REFERENCES `unidad_medida` (`cod_unidad_medida`),
  ADD CONSTRAINT `material_subcategoria` FOREIGN KEY (`cod_subcategoria`) REFERENCES `subcategoria` (`cod_subcategoria`),
  ADD CONSTRAINT `material_unidad_medida` FOREIGN KEY (`unidad_compra`) REFERENCES `unidad_medida` (`cod_unidad_medida`);

--
-- Filtros para la tabla `modelo`
--
ALTER TABLE `modelo`
  ADD CONSTRAINT `modelo_capellada` FOREIGN KEY (`cod_capellada`) REFERENCES `capellada` (`cod_capellada`),
  ADD CONSTRAINT `modelo_coleccion` FOREIGN KEY (`cod_coleccion`) REFERENCES `coleccion` (`codigo_coleccion`),
  ADD CONSTRAINT `modelo_forro` FOREIGN KEY (`cod_forro`) REFERENCES `forro` (`cod_forro`),
  ADD CONSTRAINT `modelo_horma` FOREIGN KEY (`cod_horma`) REFERENCES `material` (`cod_material`),
  ADD CONSTRAINT `modelo_ibfk_2` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `modelo_linea` FOREIGN KEY (`cod_linea`) REFERENCES `linea` (`cod_linea`),
  ADD CONSTRAINT `modelo_piso` FOREIGN KEY (`cod_piso`) REFERENCES `piso` (`cod_piso`),
  ADD CONSTRAINT `modelo_plantilla` FOREIGN KEY (`cod_plantilla`) REFERENCES `plantilla` (`cod_plantilla`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticia_categoria` FOREIGN KEY (`categoria_difusion`) REFERENCES `categoria_noticias` (`cod_categoria`);

--
-- Filtros para la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  ADD CONSTRAINT `orden_compra_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `orden_compra_proveedor` FOREIGN KEY (`RUC_proveedor`) REFERENCES `proveedor` (`RUC_proveedor`);

--
-- Filtros para la tabla `orden_pedido`
--
ALTER TABLE `orden_pedido`
  ADD CONSTRAINT `pedido_cliente` FOREIGN KEY (`codigo_cliente`) REFERENCES `cliente` (`codigo`),
  ADD CONSTRAINT `pedido_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `orden_pedido_produccion`
--
ALTER TABLE `orden_pedido_produccion`
  ADD CONSTRAINT `orden_pedido_produccion_ibfk_1` FOREIGN KEY (`codigo_orden_pedido`) REFERENCES `orden_pedido` (`codigo_pedido`),
  ADD CONSTRAINT `orden_pedido_produccion_ibfk_2` FOREIGN KEY (`codigo_cliente`) REFERENCES `cliente` (`codigo`),
  ADD CONSTRAINT `orden_pedido_produccion_ibfk_3` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `orden_produccion`
--
ALTER TABLE `orden_produccion`
  ADD CONSTRAINT `orden_produccion_ibfk_1` FOREIGN KEY (`cod_op`) REFERENCES `talla_pares` (`cod_op`),
  ADD CONSTRAINT `orden_produccion_ibfk_3` FOREIGN KEY (`cod_serie`) REFERENCES `serie` (`cod_serie`),
  ADD CONSTRAINT `orden_produccion_ibfk_4` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `orden_produccion_modelo` FOREIGN KEY (`cod_modelo`) REFERENCES `modelo` (`cod_modelo`);

--
-- Filtros para la tabla `permisos_roles`
--
ALTER TABLE `permisos_roles`
  ADD CONSTRAINT `permisos_roles_permisos` FOREIGN KEY (`permisos_id`) REFERENCES `permisos` (`id`),
  ADD CONSTRAINT `permisos_roles_roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `producto_terminado`
--
ALTER TABLE `producto_terminado`
  ADD CONSTRAINT `producto_terminado_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `proveedor_subcategoria` FOREIGN KEY (`proveedor_subcategoria`) REFERENCES `subcategoria` (`cod_subcategoria`);

--
-- Filtros para la tabla `puesto`
--
ALTER TABLE `puesto`
  ADD CONSTRAINT `puesto_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `puesto_tipo_trabajador` FOREIGN KEY (`cod_tipo_trabajador`) REFERENCES `tipo_trabajador` (`cod_tipo_trabajador`);

--
-- Filtros para la tabla `serie`
--
ALTER TABLE `serie`
  ADD CONSTRAINT `serie_ibfk_1` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `serie_modelo`
--
ALTER TABLE `serie_modelo`
  ADD CONSTRAINT `serie_modelo_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `serie_modelo_modelo` FOREIGN KEY (`codigo_modelo`) REFERENCES `modelo` (`cod_modelo`),
  ADD CONSTRAINT `serie_modelo_serie` FOREIGN KEY (`codigo_serie`) REFERENCES `serie` (`cod_serie`);

--
-- Filtros para la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `subcategoria_categoria` FOREIGN KEY (`cod_categoria`) REFERENCES `categoria` (`cod_categoria`);

--
-- Filtros para la tabla `talla_pares`
--
ALTER TABLE `talla_pares`
  ADD CONSTRAINT `talla_pares_ibfk_1` FOREIGN KEY (`cod_serie`) REFERENCES `serie` (`cod_serie`),
  ADD CONSTRAINT `talla_pares_ibfk_2` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`);

--
-- Filtros para la tabla `tiempo_recepcion`
--
ALTER TABLE `tiempo_recepcion`
  ADD CONSTRAINT `tiempo_detalle` FOREIGN KEY (`id_detalle_oc`) REFERENCES `detalle_orden_compra` (`id_detalle_oc`);

--
-- Filtros para la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD CONSTRAINT `tipo_pago_tipo_trabajador` FOREIGN KEY (`cod_tipo_trabajador`) REFERENCES `tipo_trabajador` (`cod_tipo_trabajador`);

--
-- Filtros para la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD CONSTRAINT `trabajador_area` FOREIGN KEY (`cod_area`) REFERENCES `area` (`cod_area`),
  ADD CONSTRAINT `trabajador_distrito` FOREIGN KEY (`cod_distrito`) REFERENCES `distrito` (`cod_distrito`),
  ADD CONSTRAINT `trabajador_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `trabajador_tipo_trabajador` FOREIGN KEY (`cod_tipo_trabajador`) REFERENCES `tipo_trabajador` (`cod_tipo_trabajador`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_empresa` FOREIGN KEY (`RUC_empresa`) REFERENCES `empresa` (`RUC_empresa`),
  ADD CONSTRAINT `users_roles` FOREIGN KEY (`role`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
